CREATE FUNCTION      get_parents_by_child_id (child_id number)
   RETURN VARCHAR2
IS
   l_text    VARCHAR2 (32767 ) := NULL;
   nleft1    NUMBER;
   nright1   NUMBER;
 
BEGIN
 
   SELECT nleft, nright
   INTO nleft1, nright1
   FROM humres_kadr.hr_structure_tree
   WHERE id = child_id;

   FOR cur_rec
   IN (SELECT st.id AS cc
       FROM    humres_kadr.hr_structure_tree st
       WHERE     st.active = 1
             
             AND st.nlevel > 2
             AND st.nleft <= nleft1
             AND st.nright >= nright1
       ORDER BY st.nleft)
   LOOP
      l_text   := l_text || '*' || cur_rec.cc;
   END LOOP;

   l_text   := SUBSTR (l_text, 2, LENGTH (l_text)) || '!';

   RETURN TRIM (l_text);
END;
/

