ALTER FUNCTION             get_name_ids_bytreeid (tree_id number)
   RETURN VARCHAR2
IS
   l_text    VARCHAR2 (1500000000) := NULL;
   nleft1    NUMBER;
   nright1   NUMBER;
   str_id NUMBER;
BEGIN
   select id into str_id from HUMRES_KADR.HR_STRUCTURE_TREE nm where tree_id.id=tree_id;
   SELECT nleft, nright
   INTO nleft1, nright1
   FROM humres_kadr.hr_structure_tree;

   FOR cur_rec
   IN (SELECT sn.id AS cc
       FROM    humres_kadr.hr_structure_tree st

       WHERE     st.active = 1

             AND st.nlevel > 2
             AND st.nleft <= nleft1
             AND st.nright >= nright1
       ORDER BY st.nleft)
   LOOP
      l_text   := l_text || '*' || cur_rec.cc;
   END LOOP;

   l_text   := SUBSTR (l_text, 2, LENGTH (l_text)) || '!';

   RETURN TRIM (l_text);
END;
/

