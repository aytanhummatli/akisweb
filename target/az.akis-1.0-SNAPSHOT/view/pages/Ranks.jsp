<%--
  Created by IntelliJ IDEA.
  User: Moonlight
  Date: 28.01.2019
  Time: 12:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> </title>
    <jsp:include page="../layout/rakCss.jsp" />

    <style>
        .addButton {
            float: left;
            font-size: 8px;
        }
    </style>
</head>


<body   class="nav-md" ng-app="myApp" >

<div class="container body" ng-controller="RankController as rankCtrl">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="../Views/index.jsp" class="site_title"> <span style="float: center">    AKİS</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="../static/images/smilee.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>  wellcome</span>
                        <h2>user</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <!-- Dropdown for selecting language -->

                <br />

                <!-- sidebar menu -->
                <jsp:include page="../layout/sidebarMenu.jsp" />
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <%--<jsp:include page="../layout/MenuFooterButtons.jsp" />--%>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu" ng-controller="RankController as rankktrl">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="images/img.jpg" alt="">User
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;"> Profile</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li><a href="javascript:;">Help</a></li>
                                <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">

                    </div>

                </div>

                    <div class="clearfix"></div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>ranks</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>

                            <div class="x_content">

                                <div class="table-responsive">
                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                        <tr class="headings">
                                            <th>
                                                <input type="checkbox" id="check-all" class="flat">
                                                                                    </th>
                                            <th class="column-title">ID </th>
                                            <th class="column-title">Rütbə </th>
                                            <th class="column-title">Tipi </th>
                                            <th class="column-title">Növbəti rütbə vaxtı </th>
                                            <th width="25%"></th>

                                        <button style="float: right" id="addButton" type="button"  ng-click="rankCtrl.add(0)" class="btn btn-success  "> Əlavə et</button>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <%--<tr class="even pointer"  ng-repeat="r in rankCtrl.ranks">--%>
                                        <tr ng-show="rankCtrl.ranks.length <= 0"><td colspan="5" style="text-align:center;">Loading new data!!</td></tr>
                                        <tr  class="even pointer"   dir-paginate="r in rankCtrl.ranks |itemsPerPage:rankCtrl.itemsPerPage" total-items="rankCtrl.total_count"  pagination-id="paginid" >
                                            <td class="a-center ">
                                                <input type="checkbox" class="flat" name="table_records">
                                            </td>
                                            <td class=" "><span ng-bind="r.id"></span></td>
                                            <td class=" "><span ng-bind="r.name"></span></td>
                                            <td class=" "><span ng-bind="r.rankType.Type"></span></td>
                                            <td class=" "><span ng-bind="r.nextRankPeriod"> </span> ildən sonra</td>
                                            <td class="pull-right">

                                                <button type="button"   ng-click="rankCtrl.edit(r.id)" class="btn btn-success custom-width">Redaktə</button>
                                                <%--<button type="button" ng-click="rankCtrl.remove(r.id)" class="btn btn-danger custom-width">Sil</button>--%>
                                                <button type="button" ng-click="rankCtrl.askDelete(r.id)" class="btn btn-danger custom-width">Sil</button>

                                            </td>

                                            </tr>

                                        </tbody>


                                    </table>

                                    <dir-pagination-controls
                                            boundary-links="true"
                                            max-size="8"
                                            direction-links="true"
                                            on-page-change="rankCtrl.getData(newPageNumber)"
                                    <%--template-url="/static/Angular/paginationTemplate"--%>
                                            pagination-id="paginid">

                                    </dir-pagination-controls>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                 Admin Template <a href="https://colorlib.com">Colorlib</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->




<script type="text/ng-template" id="myModalContent.html">
<div class="modal-header">
    <h4>Rütbələr</h4>
</div>

<div uib-alert ng-show="alert.show" ng-model="alert" ng-class="'alert-' +alert.type" close="closeAlert()">{{alert.msg}}</div>

<div class="modal-body">

    <div class="formcontainer">

        <form ng-submit="submit()" name="myForm" class="form-horizontal">
            <input type="hidden" ng-model="r.id" />
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-4 control-lable" for="name"> Rütbənin adı</label>
                    <div class="col-md-7">


                        <input type="text" ng-model="rank.name"   name="name" id="name" class="field form-control input-sm" placeholder="Rütbəni daxil edin"/>


                        <div class="has-error" ng-show="myForm.$dirty">
                            <span ng-show="myForm.name.$error.required">Rütbənin adını daxil edin</span>
                          <%--  <span ng-show="myForm.name.$error.minlength">Başlığı 7 sinvoldan ibarət olmaldi</span>  --%>
                            <span ng-show="myForm.name.$invalid">Daxil edilən məlumatlar düzgün deyil </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-4 control-lable" for="nextRankPeriod">Növbəti rütbə vaxtı</label>
                    <div class="col-md-7">
                    <input type="number" ng-model="rank.nextRankPeriod" name="nextRankPeriod" id="nextRankPeriod" class="field form-control input-sm" placeholder="Müddəti daxil edin" required/>
                        <div class="has-error" ng-show="myForm.$dirty">
                            <span ng-show="r.nextRankPeriod.$error.required"> Bu sahənin doldurulması zəruridir</span>
                            <span ng-show="r.nextRankPeriod.$invalid">Daxil edilən məlumatlar düzgün deyil </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-4 control-lable" for="rankType">Rütbənin tipi</label>
                    <div class="col-md-7">
                        <select name="rankType"  id="rankType" ng-model="rankType"
                                ng-options="rankType.Type for rankType in rankTypes track by rankType.id" class="field form-control input-sm" required  >

                        </select>
                        <div class="has-error" ng-show="myForm.$dirty">
                            <%--  <span ng-show="myForm.$error.required">kategoryani daxil edin</span>
                              <span ng-show="myForm.technicalFailure.id.$invalid">kategorya düzgün daxil edilməyib </span> --%>
                          </div>
                      </div>
                  </div>
              </div>

              <div class="row">
                  <div class="form-group col-md-12">
                      <label class="col-md-4 control-lable" for="rankType"></label>
                      <label class="col-md-4 control-lable" for="name"></label>
                      <label class="col-md-4 control-lable" for="nextRankPeriod"></label>
                  </div>
              </div>


              <div class="row" style="position: absolute; bottom:5px;right: 20px">
                  <div>
                      <input type="submit"  value="{{!rank.id ? 'Əlavə et' : 'Yadda saxla'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                      <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                  </div>
              </div>
          </form>

      </div>


</div>
  <!-- jQuery -->

</script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.1/angular.js"></script>

    <script src="../../static/resource/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../static/resource/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../static/resource/js/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../static/resource/js/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../../static/resource/js/icheck.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../static/resource/js/custom.min.js"></script>

    <%--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js">  </script>--%>
    <%--<script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-1.3.3.js"></script>--%>

    <script src="<c:url value='/static/resource/js/ui-bootstrap-tpls-2.5.0.min.js' />"></script>


    <%--<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.js"></script>--%>
    <%--<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.0.js"></script>--%>

    <script src="<c:url value='/static/angular/dirPagination.js' />"></script>
    <script src="<c:url value='/static/angular/app.js' />"></script>
    <script src="<c:url value='/static/angular/Ranks/Services/rank_service.js' />"></script>
    <script src="<c:url value='/static/angular/Ranks/Controllers/rank_controller.js' />"></script>
    <script src="<c:url value='/static/angular/Ranks/Controllers/RanksModalController.js' />"></script>

    <script scr="<c:url value='/static/angular/angular-translate.js'/>"></script>

    <script scr="<c:url value='/static/angular/angular-translate.min.js'/>"></script>

    <script scr="<c:url value='/static/angular/angular-translate-loader-static-files.js'/>"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.2/angular.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.2/angular-sanitize.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-translate/2.14.0/angular-translate.js"></script>


</body>



</html>