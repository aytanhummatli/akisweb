<%--
  Created by IntelliJ IDEA.
  User: Moonlight
  Date: 05.04.2019
  Time: 12:55
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/ng-template" id="addRelative.html">

    <div class="modal-header">

        <h4>Yeni qohum məlumatlarının əlavə edilməsi</h4>

    </div>

    <div class="modal-body">

        <div class="formcontainer">

            <form ng-submit="submit()" name="myForm" class="form-horizontal">

                <div class="row">

                    <div class="form-group col-md-12">

                        <label class="col-md-3 control-lable" for="cmbsService">Qohumluq dərəcəsi</label>

                        <div class="col-md-9">

                            <select name="relativeDegree" id="relativeDegree" ng-model="relativeDegree"

                                    ng-options="relativeDegree.description for relativeDegree in relativeDegreeList track by relativeDegree.id"
                                    class="field form-control input-sm" required>

                                <option value="">Seçim et</option>

                            </select>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <label class="col-md-3 control-label" for="name">Soyadı</label>

                    <div class="form-group col-md-3">

                        <input type="text" ng-model="relative.person.name" name="name"

                               id="name" class="field form-control input-sm" placeholder="Soyadı"/>
                    </div>

                    <div class="form-group col-md-6">

                        <label class="col-md-5 control-label" for="surName">Adı</label>

                        <div class="col-md-7">

                            <input type="text" ng-model="relative.person.surName" name="surName"

                                   id="surName" class="field form-control input-sm" placeholder="Adı"/>
                        </div>

                    </div>

                </div>

                <div class="row">

                    <label class="col-md-3 control-label" for="patronymic">Ata adı</label>

                    <div class="form-group col-md-3">


                        <input type="text" ng-model="relative.person.patronymic" name="patronymic"

                               id="patronymic" class="field form-control input-sm" placeholder="Ata adı"/>

                    </div>

                    <div class="form-group col-md-6">

                        <label class="col-md-5 control-label" for="maidenName"> Qızlıq soyadı</label>

                        <div class="col-md-7">

                            <input type="text" ng-model="relative.person.maidenName" name="maidenName"

                                   id="maidenName" class="field form-control input-sm" placeholder=" Qızlıq soyadı"/>

                        </div>

                    </div>

                </div>


                <div class="row">

                    <label class="col-md-3 control-label" for="adress_fullname">Doğulduğu yer</label>

                    <div class="col-md-7">

                        <input type="text" ng-model="relative.person.bornAddress.adress_fullname" name="adress_fullname"

                               id="adress_fullname" class="field form-control input-sm" placeholder="Doğulduğu yer"/>
                    </div>

                    <div class="col-md-2">

                        <a style="height: 30px" class="  btn btn-default btn-xs"
                           data-nodrag

                           ng-click="selectTree(this)"><span

                                class="glyphicon glyphicon-option-horizontal"></span></a>

                        <a style="height: 30px" class="  btn btn-default btn-xs"
                           data-nodrag

                           ng-click="clearBirthLocation()" ng-model="birthLocation"><span

                                class="glyphicon glyphicon-remove"></span></a>
                    </div>

                </div>


                <div class="row">


                    <label class="col-md-3 control-label" for="birthDate">Tarix</label>

                    <div class="col-md-3">

                        <input type="text" ng-model="relative.person.birthDate" name="birthDate"

                               id="birthDate" class="field form-control input-sm" placeholder="Tarix"/>


                    </div>

                    <div class="col-md-6"></div>

                </div>
                <br>
                <div class="row">

                    <label class="col-md-3 control-label" for="workPlace">İş yeri</label>

                    <div class="col-md-9">

                        <select name="workPlace" id="workPlace"

                                class="field form-control input-sm" required>

                            <option value="">Seçim et</option>

                        </select>

                    </div>

                </div>
                <br>
                <div class="row">

                    <label class="col-md-3 control-label" for="profession">Vəzifəsi</label>

                    <div class="col-md-9">

                            <textarea name="profession" ng-model="workPlace" id="profession" style="min-width: 100%">

                            </textarea>


                    </div>
                </div>
                <br>

                <div class="row">


                    <label class="col-md-3 control-label" for="adress_fullname">Ünvanı</label>

                    <div class="col-md-6 col-sm-6 col-xs-6">

                        <input ng-model="relative.livingAddress.adress_fullname" type="text" class="form-control"

                               placeholder="Ünvanı">
                    </div>

                    <div class="col-md-3 col -sm-3 col-xs-3">

                        <a style="height: 30px" class="  btn btn-default btn-xs"
                           data-nodrag
                           ng-click="selectTree(this)"><span
                                class="glyphicon glyphicon-option-horizontal"></span></a>

                        <%--</div>--%>
                        <%--<div class="col-md-1 col -sm-1 col-xs-1">--%>

                        <a style="height: 30px" class="  btn btn-default btn-xs" data-nodrag
                           ng-click="selectTree(this)"><span
                                class="glyphicon glyphicon-remove"></span></a>

                        <a style="height: 30px" class="  btn btn-default btn-xs" data-nodrag
                           ng-click="selectTree(this)"><span
                                class="glyphicon glyphicon-chevron-left"></span></a>
                    </div>
                </div>
                <br>
                <br>

                <div class="row" style="position: absolute; bottom:5px;right: 20px">

                    <div>

                        <input type="submit" value="Əlavə et" class="btn btn-primary btn-sm"
                               ng-disabled="myForm.$invalid">

                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>

                    </div>

                </div>

            </form>

        </div>

    </div>

</script>


