<%--
  Created by IntelliJ IDEA.
  User: Moonlight
  Date: 21.01.2019
  Time: 23:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Audentifikasiya</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../../static/resource/images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../../static/resource/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../../static/resource/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../static/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../static/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../static/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../../static/resource/css/util.css">

    <link rel="stylesheet" type="text/css" href="../../static/resource/css/main.css">
    <!--===============================================================================================-->

</head>
<body>

<div class="limiter" >
    <div class="container-login100" >
        <div class="wrap-login100">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="../static/images/img-01.png" alt="IMG">
            </div>

            <form name='login' action="/login" method='POST' class="login100-form validate-form" method='POST'>

					<span class="login100-form-title">
						İstifadəçinin audentifikasiyası
					</span>

                              <div class="wrap-input100 validate-input" data-validate = "Mail ünvanını daxil etmək zəruridir: numune@abc.xyz">
                    <input class="input100" type="text" name="username" placeholder="Mail ünvanı">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
                </div>

                <div class="wrap-input100 validate-input" data-validate = "Şifrəni daxil etmək zəruridir">
                    <input class="input100" type="password" name="password" placeholder="Şifrə">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn"  type="submit" >
                        Sistemə daxil ol
                    </button>
                </div>

                <div class="text-center p-t-136">
                    <a class="txt2" href="Views/UserManagement.jsp">
                        Yeni istifadəçilərin qeydiyyatı
                        <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                    </a>
                </div>

            </form>
        </div>
    </div>
</div>




<!--===============================================================================================-->
<script src="${pageContext.request.contextPath}/static/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="${pageContext.request.contextPath}/static/vendor/bootstrap/js/popper.js"></script>
<script src="${pageContext.request.contextPath}/static/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="${pageContext.request.contextPath}/static/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="${pageContext.request.contextPath}/static/vendor/tilt/tilt.jquery.min.js"></script>
<script >
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<!--===============================================================================================-->
<script src="../../static/resource/js/main.js"></script>

</body>
</html>
