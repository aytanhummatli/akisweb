<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 2/20/2019
  Time: 6:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <jsp:include page="/static/resource/jsp/resourcesCss.jsp"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="/user/" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
                </div>

                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <jsp:include page="/view/layout/menuProfile.jsp"/>
                <!-- /menu profile quick info -->

                <!-- sidebar menu -->
                <jsp:include page="/view/layout/sidebarMenu.jsp"/>
                <!-- sidebar menu-->

                <!-- /menu footer buttons -->
                <jsp:include page="/view/layout/menuFooter.jsp"/>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <jsp:include page="/view/layout/header.jsp"/>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main" ng-app="nomApp">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Tables
                            <small>Some examples to get you started</small>
                        </h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix">

                </div>

                <div class="row">
                    <!-- table panel-->
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <div class="table-responsive " >
                                    <%--Nomenclature  Table--%>
                                    <jsp:include page="/view/part/userInfoTable.jsp"/>
                                    <%--Nomenclature  Table--%>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <jsp:include page="/view/layout/footer.jsp"/>
        <!-- /footer content -->
    </div>
</div>



<script src="${pageContext.request.contextPath}/static/angular/userInfo/userInfoApp.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/static/angular/userInfo/userInfoCtrl.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/static/angular/userInfo/userInfoService.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/static/angular/userInfo/userInfoAddModalCtrl.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/static/angular/userInfo/userInfoEditModalCtrl.js"
        type="text/javascript"></script>

<%--<script src="https://raw.githubusercontent.com/michaelbromley/angularUtils/master/src/directives/pagination/dirPagination.js" type="text/javascript"></script>--%>
<script src="${pageContext.request.contextPath}/static/angular/dirPagination.js" type="text/javascript"></script>
<jsp:include page="/static/resource/jsp/resourcesJs.jsp"/>






</body>
</html>

