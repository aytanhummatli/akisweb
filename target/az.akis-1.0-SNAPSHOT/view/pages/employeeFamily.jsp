<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ailə məlumatları </title>

    <jsp:include page="../layout/rakCss.jsp"/>

    <!-- bootstrap-wysiwyg -->
    <link href="../../static/resource/css/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../../static/resource/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../../static/resource/css/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="../../static/resource/css/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
   <%--<link href="../../static/resource/css/daterangepicker.css" rel="stylesheet">--%>

    <!-- Custom Theme Style -->
    <link href="../../static/resource/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">



    <link href="../../static/resource/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">


</head>

<body class="nav-md" ng-app="appEmployeeFamily">
<div class="container body" ng-controller="employeeFamilyController as empCtrl">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <br>
                <!-- sidebar menu -->
                <jsp:include page="../layout/sidebarMenuForEmployeeDetails.jsp"/>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>

                <!-- /menu footer buttons -->

            </div>

        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>

                    <div class="nav toggle">

                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>

                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="images/img.jpg" alt="">John Doe
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;"> Profile</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li><a href="javascript:;">Help</a></li>
                                <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">

                <div class="page-title">

                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Ailə məlumatları

                                </h2>

                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form id="demo-form6" data-parsley-validate class="form-horizontal form-label-left">
                                    <%--  Soyad, şəxsi iş, şəxsi işi tamdır   --%>
                                    <div class="row">

                                        <div class="panel panel-default">

                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    <div class="x_content">

                                                        <table id="contdact" class="table table-hover"
                                                               style="font-size: small">
                                                            <thead>
                                                            <tr class="headings">
                                                                <th class="column-title"><label>Qohumluq
                                                                    dərəcəsi </label></th>
                                                                <th class="column-title"><label>Soyadı </label></th>
                                                                <th class="column-title"><label>Adı </label></th>
                                                                <th class="column-title"><label>Ata adı </label></th>
                                                                <th class="column-title"><label>Doğulduğu yer </label>
                                                                </th>
                                                                <th class="column-title"><label>Doğum ili </label></th>
                                                                <th class="column-title"><label>Məşğuliyyəti </label>
                                                                </th>
                                                                <th class="column-title"><label>Ünvan</label></th>

                                                                <th class="column-title">

                                                                    <a class="  btn btn-default btn-xs"

                                                                       ng-click="addRelative()"><span

                                                                            class="glyphicon glyphicon-plus">  </span></a>
                                                                 </th>

                                                            </tr>

                                                            </thead>

                                                            <tbody>

                                                            <tr ng-repeat="inf in employee.relatives">

                                                                <td class=" "><span
                                                                        ng-bind="inf.relativeDegree.description"></span>
                                                                </td>

                                                                <td class=" "><span
                                                                        ng-bind="inf.person.surName"></span></td>
                                                                <td class=" "><span
                                                                        ng-bind="inf.person.name"></span></td>

                                                                <td class=" "><span
                                                                        ng-bind="inf.person.patronymic"></span></td>
                                                                <td class=" "><span
                                                                        ng-bind="inf.person.bornAddress.adress_fullname"></span>
                                                                </td>
                                                                <td class=" "><span
                                                                        ng-bind="inf.person.birthDate | date:'dd.mm.yyyy'"></span>
                                                                </td>

                                                                <td class=" "><span
                                                                        ng-bind="inf.workPlace"></span></td>
                                                                <td class=" "><span
                                                                        ng-bind="inf.livingAddress.adress_fullname"></span>
                                                                </td>

                                                                <td class="pull-right">
                                                                    <a class="  btn btn-default btn-xs"
                                                                       ng-click="updateRelative(this)"><span
                                                                            class="glyphicon glyphicon-edit">  </span></a>

                                                                    <a class="  btn btn-default btn-xs"
                                                                       ng-click="removeRelative(inf.id)"><span
                                                                            class="glyphicon glyphicon-remove">  </span></a>

                                                                </td>
                                                            </tr>

                                                            </tbody>

                                                        </table>

                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                 </div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- /page content -->

    <!-- footer content -->

    <footer>
        <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
        </div>
        <div class="clearfix"></div>
    </footer>

    <!-- /footer content -->

</div>

</div>

<jsp:include page="modals/add/addRelative.jsp"></jsp:include>

<jsp:include page="modals/update/updateRelative.jsp"></jsp:include>

<jsp:include page="modals/add/addLocation.jsp"></jsp:include>

<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.1/angular.js"></script>

<script src="<c:url value='/static/resource/js/ui-bootstrap-tpls-2.5.0.min.js' />"></script>

<script src="<c:url value='/static/angular/Employees/EmployeFamily/appEmployeeFamily.js' />"></script>

<script src="/static/angular/Employees/EmployeFamily/employeeFamilyController.js"></script>

<script src="<c:url value='/static/angular/Employees/EmployeFamily/employeeFamilyService.js ' />"></script>

<script src="<c:url value='/static/angular/Employees/EmployeFamily/modals/modalEmployeeFamily.js'  />"></script>

<script src="<c:url value='/static/angular/Employees/EmployeFamily/modals/modalLocation.js'  />"></script>

<script src="../../static/resource/js/jquery.min.js"></script>

<%--<script type="text/javascript" src="../../static/resource/js/jquery-1.8.3.min.js" charset="UTF-8"></script>--%>
<!-- Bootstrap -->
<script src="../../static/resource/js/bootstrap.min.js"></script>

<!-- FastClick -->
<script src="../../static/resource/js/fastclick.js"></script>

<!-- NProgress -->
<script src="../../static/resource/js/nprogress.js"></script>

<!-- iCheck -->
<script src="../../static/resource/js/icheck.min.js"></script>

<script src="../../static/resource/js/bootstrap-progressbar.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../../static/resource/js/moment.min.js"></script>
<script src="../../static/resource/js/daterangepicker.js"></script>

<!-- bootstrap-wysiwyg -->
<script src="../../static/resource/js/bootstrap-wysiwyg.min.js"></script>

<script src="../../static/resource/js/jquery.hotkeys.js"></script>
<script src="../../static/resource/js/prettify.js"></script>

<!-- jQuery Tags Input -->
<script src="../../static/resource/js/jquery.tagsinput.js"></script>

<!-- Switchery -->
<script src="../../static/resource/js/switchery.min.js"></script>

<!-- Select2 -->
<script src="../../static/resource/js/select2.full.min.js"></script>

<!-- Parsley -->
<script src="../../static/resource/js/parsley.min.js"></script>

<!-- Autosize -->
<script src="../../static/resource/js/autosize.min.js"></script>

<!-- jQuery autocomplete -->
<script src="../../static/resource/js/jquery.autocomplete.min.js"></script>

<!-- starrr -->
<script src="../../static/resource/js/starrr.js"></script>

<!-- Custom Theme Scripts -->
<script src="../../static/resource/js/custom.min.js"></script>

<script type="text/javascript" src="../../static/resource/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="../../static/resource/js/bootstrap-datetimepicker.rs-latin.js" charset="UTF-8"></script>

<script type="text/javascript">

    $('.form_date').datetimepicker({
     //   language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

</script>

</body>

</html>