<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 1/22/2019
  Time: 2:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body ng-app="hpgApp" ng-controller="HpgController as hpgctrl">
<table class="table table-striped jambo_table bulk_action">
    <thead>
    <tr class="headings">
        <th class="column-title">Id</th>
        <th class="column-title">Vəzifə Qrupları</th>
        <th class="column-title">Redaktə</th>
        <th class="column-title">Sil</th>
    </tr>
    </thead>

    <tbody>
    <tr ng-show="hpgctrl.hpgList.length <= 0">
        <td colspan="5" style="text-align:center;">Loading new data!!</td>
    </tr>
    <td>
        <button type="button"ng-click="hpgctrl.add(null)" class="btn btn-success custom-width"
                id="add-button" value="Əlavə et">Əlavə Et</button>
    </td>
    <tr class="odd pointer" pagination-id="hpgPaginid"
        dir-paginate="hpg in hpgctrl.hpgList|itemsPerPage:hpgctrl.itemsPerPage"
        total-items="hpgctrl.total_count">

        <td class=" "><span ng-bind="hpg.id"></span></td>
        <td class=" "><span ng-bind="hpg.name"></span></td>
        <td>
            <button ng-click="hpgctrl.edit(hpg.id)" id="edit-button" value="Dəyiş"
                    class="btn btn-success custom-width">Dəyiş
            </button>
        </td>
        <td>
            <button ng-click="hpgctrl.remove(hpg.id)" id="delete-button" value="Sil"
                    class="btn btn-danger custom-width">Sil
            </button>
        </td>

    </tr>
    </tbody>
</table>
<dir-pagination-controls
        pagination-id="hpgPaginid"
        max-size="8"
        direction-links="true"
        boundary-links="true"
        on-page-change="hpgctrl.getHpgList(newPageNumber)">
</dir-pagination-controls>

<script type="text/ng-template" id="positionGorupAddUpdateModal.html">
    <div class="modal-header">
        <h4>HR Vəzifə qrupları</h4>
    </div>

    <div uib-alert ng-show="alert.show" ng-model="alert" ng-class="'alert-' +alert.type" close="closeAlert()">
        {{alert.msg}}
    </div>

    <div class="modal-body">


        <div class="formcontainer">
            <form ng-submit="submit()" name="myForm" class="form-horizontal">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable" for="name">Vəzifə</label>
                        <div class="col-md-7">
                                        <textarea type="text" ng-model="hpg.name" rows="1" id="name"
                                                  class="field form-control input-sm" placeholder=""
                                                  required ng-minlength="7"></textarea>
                            <div class="has-error" ng-show="myForm.$dirty">
                                <span ng-show="myForm.name.$error.required">Adı daxil edin</span>
                                <span ng-show="myForm.name.$error.minlength">Ad 7 sinvoldan ibarət olmaldi</span>
                                <span ng-show="myForm.name.$invalid">Düzgün daxil edilməyib </span>
                            </div>
                        </div>
                    </div>
                </div>

        <br><br>
        <div class="row" style="position: absolute; bottom:5px;right: 20px">
            <div>
                <input type="submit" value="{{!hpg.id ? 'Əlavə et' : 'Yadda saxla'}}"
                       class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
            </div>
        </div>
            </form>
        </div>
    </div>
</script>
<script type="text/ng-template" id="positionGroupRemoveModal.html">
    <div class="modal-header">
        <h4>HR Vəzifə qrupları</h4>
    </div>

    <div uib-alert ng-show="alert.show" ng-model="alert" ng-class="'alert-' +alert.type" close="closeAlert()">
        {{alert.msg}}
    </div>

    <div class="modal-body">


        <div class="formcontainer">

            <form ng-submit="ok()" name="deleteForm" class="form-horizontal">
                <h3>Silməyə Əminsinizmi?</h3>
                <input type="hidden" ng-model="hpg.id"/>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable"><h4>{{hpg.name}}</h4></label>
                    </div>
                </div>

                <div class="row" style="position: absolute; bottom:5px;right: 20px">
                    <div>
                        <input type="submit" value="Bəli" class="btn btn-primary btn-sm"
                               ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</script>


</body>
</html>
