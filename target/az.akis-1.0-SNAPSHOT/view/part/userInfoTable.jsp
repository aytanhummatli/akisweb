<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 3/1/2019
  Time: 2:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html ng-app="UserInfoApp">
<head>
    <title>Title</title>


    <!--
      IE8 support, see AngularJS Internet Explorer Compatibility https://docs.angularjs.org/guide/ie
      For Firefox 3.6, you will also need to include jQuery and ECMAScript 5 shim
    -->
    <!--[if lt IE 9]>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.2.0/es5-shim.js"></script>
    <script>
        document.createElement('ui-select');
        document.createElement('ui-select-match');
        document.createElement('ui-select-choices');
    </script>
    <![endif]-->

    <%--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.js"></script>--%>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-sanitize.js"></script>

    <!-- ui-select files -->
    <script src="/static/resource/js/select.js"></script>
    <link rel="stylesheet" href="/static/resource/css/select.css"/>

    <!-- themes -->
    <%--<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.bootstrap2.css"> -->
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.bootstrap3.css">-->

    <style>
        body {
            padding: 15px;
        }

        .select2 > .select2-choice.ui-select-match {
            /* Because of the inclusion of Bootstrap */
            height: 29px;
        }

        .selectize-control > .selectize-dropdown {
            top: 36px;
        }
        /* Some additional styling to demonstrate that append-to-body helps achieve the proper z-index layering. */
        .select-box {
            background: #fff;
            position: relative;
            z-index: 1;
        }
        .alert-info.positioned {
            margin-top: 1em;
            position: relative;
            z-index: 10000; /* The select2 dropdown has a z-index of 9999 */
        }
    </style>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/resource/css/style.css" type="text/css"/>


</head>
<body  ng-controller="UserInfoController">

<table class="table table-striped jambo_table bulk_action">
    <thead>
    <tr class="headings">
        <th class="column-title">Id</th>
        <th class="column-title">İstifadəçi adı</th>
        <th class="column-title">Soyadı,Adı,Atasının adı</th>
        <th class="column-title">Nomenklatura</th>
        <th class="column-title">Aktivi</th>
        <th class="column-title">Redaktə</th>
        <th class="column-title">Sil</th>
    </tr>
    </thead>

    <tbody>
    <tr ng-show="userList.length <= 0">
        <td colspan="5" style="text-align:center;">Loading new data!!</td>
    </tr>
    <tr class="odd pointer" pagination-id="userInfoPaginid"
        dir-paginate="userInfo in userList | itemsPerPage:itemsPerPage"
        total-items="total_count">
        <input type="button" ng-click="add(null)" class="btn btn-success custom-width"
               id="add-button" value="Əlavə et">
        <td class=" "><span ng-bind="userInfo.user.id"></span></td>
        <td class=" " ><span ng-bind="userInfo.user.userName"></span></td>
        <td class=" "><span ng-bind="userInfo.person.surname"></span>&nbsp<span ng-bind="userInfo.person.name"></span>&nbsp<span
                ng-bind="userInfo.person.patronymic"></span></td>
        <td class=" " ><span ng-bind="userInfo.nomenclature.name"></span></td>
        <td class=" "><span ng-bind="userInfo.user.active"></span></td>

        <td>
            <button ng-click="edit(userInfo.user.id)" id="edit-button" value="Dəyiş"
                    class="btn btn-success custom-width">Dəyiş
            </button>
        </td>
        <td>
            <button ng-click="remove(userInfo.user.id)" id="delete-button" value="Sil"
                    class="btn btn-danger custom-width">Sil
            </button>
        </td>

    </tr>
    </tbody>
</table>

<dir-pagination-controls
        pagination-id="userInfoPaginid"
        max-size="8"
        direction-links="true"
        boundary-links="true"
        on-page-change="getUserInfoList(newPageNumber)">
</dir-pagination-controls>

<script type="text/ng-template" id="userInfoAddModal.html">

    <div class="modal-header">
        <h4>Yeni İstifadəçi</h4>
    </div>

    <div uib-alert ng-show="alert.show" ng-model="alert" ng-class="'alert-' +alert.type" close="closeAlert()">
        {{alert.msg}}
    </div>

    <div class="modal-body">


        <div class="formcontainer">

            <form ng-submit="submit()" name="myForm" class="form-horizontal">
                <div class="row">
                    <div class="form-group col-md-12">

                        <label class="col-md-4 control-lable">Şəxsin adı,soyadı,atasının adı</label>
                        <div class="col-md-7">
                            <input list="userData" type="text" ng-model="userInfo.person.fullName"
                                   ng-change="searchFullName()" placeholder="soyadı,adı,atasının adı"
                                   class="field form-control input-sm"/><br> {{userInfo.person.id}}

                            <datalist id="userData">
                                <option ng-model="person.fullName" ng-repeat="u in userFullnameList"
                                        value="{{u.person.fullName}}"></option>
                            </datalist>
                        </div>

                        <label class="col-md-4 control-lable">İstifadəçi adı</label>
                        <div class="col-md-7">
                                        <textarea type="text" rows="1" ng-model="userInfo.user.userName"
                                                  class="field form-control input-sm" placeholder=""

                                        ng-keyup="checkUsername()"></textarea></br>
                            <div ng-class="addClass(unamestatus)" >{{ unamestatus }}</div>
                        </div>

                        <label class="col-md-4 control-lable">Nomenklatura</label><br>
                        <div class="col-md-7">
                            <input list="nomData" type="text" ng-model="userInfo.nomenclature.name"
                                   ng-change="searchNomName()"
                                   placeholder="nomenklatura" class="field form-control input-sm"/><br><h4>
                            {{nomenclature.id}}</h4>
                            <datalist id="nomData">

                                <option ng-model="nomenclature.name" ng-repeat="n in nomNameList"
                                        value="{{n.nomenclature.name}}"></option>
                            </datalist>
                        </div>
                        <br>

                        <label class="col-md-4 control-lable" for="password">Password</label>
                        <div class="col-md-7">
                            <input id="password" type="password" class="form-control"  ng-model="password" placeholder="Password" name="password"required="required" ng-minlength="1"/><br>
                            <div ng-if="myForm.password.$touched || signupSubmitted">
                                <p ng-show="myForm.password.$error.required" class="help-block">şifrə daxil edilməyib</p>
                                <p ng-show="myForm.password.$error.minlength" class="help-block">minimum 1 simvol</p>
                            </div>
                        </div><br>

                        <label class="col-md-4 control-lable" for="confirm_password">Confirm Password</label>
                        <div class="col-md-7">
                           <input id="confirm_password" type="password" class="form-control"  name="userInfo.password.password" ng-model="confirm_password" placeholder="təkrar şifrəni daxil et" match-password="password" required><br>
                            <div ng-if="myForm.confirm_password.$touched || signupSubmitted">
                                <p ng-show="myForm.confirm_password.$error.required" class="help-block"> təkrar şifrə daxil edilməyib</p>
                                <p ng-show="myForm.confirm_password.$error.matchPassword && !myForm.confirm_password.$error.required" class="help-block">şifrə düzgün təyin edilməyib</p>
                            </div>
                        </div><br>
                        <label class="col-md-4 control-lable">Düymələr üzrə qaydalar</label>
                        <div class="col-md-7">
                        <ui-select multiple ng-model="userButtonRule.userButtonRules" theme="bootstrap" ng-disabled="disabled" close-on-select="false"  title="Choose a buttonRule" class="field form-control input-sm">
                            <ui-select-match placeholder="qaydaları seç">{{$item.ruleNames}}</ui-select-match>
                            <ui-select-choices repeat="buttonRule in buttonRules | filter:$select.search">
                                {{buttonRule.ruleNames}}
                            </ui-select-choices>
                        </ui-select><p>Selected</p><br>
                            <p ng-repeat="userButtonRule in userButtonRule.userButtonRules">{{userButtonRule.ruleNames}}</p>
                        </div><br>

                        <label class="col-md-4 control-lable">İstifadəçi qaydaları</label>
                        <div class="col-md-7">
                            <ui-select multiple ng-model="userRule.userRules" theme="bootstrap" ng-disabled="disabled" close-on-select="false"  title="Choose a Rule" class="field form-control input-sm">
                                <ui-select-match placeholder="qaydaları seç">{{$item.ruleName}}</ui-select-match>
                                <ui-select-choices repeat="rule in rules | filter:$select.search">
                                    {{rule.ruleName}}
                                </ui-select-choices>
                            </ui-select><p>Selected</p>
                            <p ng-repeat="userRule in userRule.userRules">{{userRule.ruleName}}</p>
                        </div>
                    </div>
                </div>

                <br><br>
                <div class="row" style="position: absolute; bottom:5px;right: 20px">
                    <div>
                        <input type="submit" value="{{'Əlavə et'}}"
                               class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/ng-template" id="userInfoEditModal.html">

    <div class="modal-header">
        <h4>İstifadəçi</h4>
    </div>

    <div uib-alert ng-show="alert.show" ng-model="alert" ng-class="'alert-' +alert.type" close="closeAlert()">
        {{alert.msg}}
    </div>

    <div class="modal-body">


        <div class="formcontainer">

            <form ng-submit="submit()" name="myForm" class="form-horizontal">
                <div class="row">
                    <div class="form-group col-md-12">

                        <label class="col-md-4 control-lable">İstifadəçi adı</label>
                        <div class="col-md-7">
                                        <textarea type="text" rows="1" ng-model="userInfo.user.userName"
                                                  class="field form-control input-sm" placeholder=""
                                                  required ng-minlength="7"></textarea><br>
                        </div>

                        <label class="col-md-4 control-lable">Nomenklatura</label>
                        <div class="col-md-7">
                            <input list="data" type="text" ng-model="userInfo.nomenclature.name"
                                   ng-change="searchNomName()"
                                   placeholder="nomenklatura" class="field form-control input-sm"/><br><h4>
                            {{nomenclature.id}}</h4>
                            <datalist id="data">

                                <option ng-model="nomenclature.name" ng-repeat="n in nomNameList"
                                        value="{{n.nomenclature.name}}"></option>

                            </datalist>
                        </div>
                        <label class="col-md-4 control-lable">Düymələr üzrə qaydalar</label>
                        <div class="col-md-7">
                            <ui-select multiple ng-model="userButtonRule.userButtonRules" theme="bootstrap" ng-disabled="disabled" close-on-select="false"  title="Choose a buttonRule" class="field form-control input-sm">
                                <ui-select-match placeholder="qaydaları seç">{{$item.ruleNames}}</ui-select-match>
                                <ui-select-choices repeat="buttonRule in buttonRules | filter:$select.search">
                                    {{buttonRule.ruleNames}}
                                </ui-select-choices>
                            </ui-select><br>
                            <%--<p ng-repeat="userButtonRule in userButtonRule.userButtonRules">{{userButtonRule.ruleNames}}</p>--%>
                        </div><br>

                        <label class="col-md-4 control-lable">İstifadəçi qaydaları</label>
                        <div class="col-md-7">
                            <ui-select multiple ng-model="userRule.userRules" theme="bootstrap" ng-disabled="disabled" close-on-select="false"  title="Choose a Rule" class="field form-control input-sm">
                                <ui-select-match placeholder="qaydaları seç">{{$item.ruleName}}</ui-select-match>
                                <ui-select-choices repeat="rule in rules | filter:$select.search">
                                    {{rule.ruleName}}
                                </ui-select-choices>
                            </ui-select>{{userInfo.user.id}}
                            <%--<p ng-repeat="userRule in userRule.userRules">{{userRule.ruleName}}</p>--%>
                        </div>

                    </div>
                </div>

                <br><br>
                <div class="row" style="position: absolute; bottom:5px;right: 20px">
                    <div>
                        <input type="submit" value="{{'Yadda Saxla'}}"
                               class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/ng-template" id="userRemoveModal.html">
    <div class="modal-header">
        <h4>İstifadəçi</h4>
    </div>

    <div uib-alert ng-show="alert.show" ng-model="alert" ng-class="'alert-' +alert.type" close="closeAlert()">
        {{alert.msg}}
    </div>

    <div class="modal-body">


        <div class="formcontainer">

            <form ng-submit="ok()" name="deleteForm" class="form-horizontal">
                <h3>Silməyə Əminsinizmi?</h3>
                <%--<input type="hidden" ng-models="nom"/>--%>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable"></label>
                    </div>
                </div>

                <div class="row" style="position: absolute; bottom:5px;right: 20px">
                    <div>
                        <input type="submit" value="Bəli" class="btn btn-primary btn-sm"
                               ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</script>

</body>
</html>
