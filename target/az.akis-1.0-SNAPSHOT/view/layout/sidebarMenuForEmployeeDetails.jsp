<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div>
    <%--<div class="profile_pic">--%>
    <img style="    height: 150px;
    padding-right: 10%;
    padding-left: 20px;" src="/static/images/employeeDefault.png"
         alt="..." class="img-responsive">
    <%--// </div>--%>
    <div class="profile_info" >
        <h2  > Aliyev Amir Aslan</h2>
        <i>Leytenant</i>
    </div>
</div>
<!-- /menu profile quick info -->

<br/>
<br/>
<br/>
<br/>
<!-- sidebar menu -->

<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

    <div class="menu_section">

        <h3></h3>

        <ul class="nav side-menu">

            <li><a href="/employeeDetails/"><i class="fas fa-user" aria-hidden="true"></i> Şəxsiyyət məlumatları  </a>

            </li>

            <li><a href="/employeeGeneral/"><i class="fas fa-info-circle" ></i> Ümumi məlumatlar  </a>

            </li>

            <li><a href="/employeeEducation/"><i class="fas fa-user-graduate" size="5x"></i> Təhsil və attestasiya </a>

            </li>

            <li><a href="/employeeFamily/"><i class="fas fa-home"></i> Ailə məlumatları </a>

            </li>

            <li><a href="/employeeDİO/"><i class="fas fa-info"></i> Dio-da fəaliyyəti. </a>

            </li>

            <li><a href="/employeeEzam/"><i class="fas fa-info"></i> Ezamiyyət və əvvəlki fəal. </a>

            </li>

            <li><a href="/employeeMukCeza/"><i class="fas fa-gifts"></i> Mükafat və Cəzaları </a>

            </li>

            </li>
            <li><a href="/employeeMezYar/"><i class="fas fa-user-injured"></i> Məzuniyyət, Yaralanma </a>

            </li>

            </li>

            <li><a href="/employeeOther/"><i class="fas fa-info"></i> Digər </a>

            </li>

        </ul>

    </div>

</div>

<!-- /sidebar menu -->
