<%--
  Created by IntelliJ IDEA.
  User: Moonlight
  Date: 05.02.2019
  Time: 16:06
  To change this template use File | Settings | File Templates.
--%>
<!-- Bootstrap -->


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<!-- Font Awesome -->
<link href="${pageContext.request.contextPath}/static/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="${pageContext.request.contextPath}/static/resource/css/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="${pageContext.request.contextPath}/static/resource/css/green.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="${pageContext.request.contextPath}/static/resource/css/custom.min.css" rel="stylesheet">