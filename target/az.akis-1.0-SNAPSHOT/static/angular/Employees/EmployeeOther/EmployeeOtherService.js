'use strict';

angular.module('appEmployeeOther').factory('EmployeeOtherService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:8060/';


     var factory = {

        getEmployeeOther:getEmployeeOther,

        deleteQualificationCourse:deleteQualificationCourse,
         deleteWarActivity:deleteWarActivity,
         getCombo:getCombo,

        getObject:getObject,

        setObject:setObject
                    };
    return factory;

    var myObject={};


    function  getObject() {

        return myObject;

    }

    function setObject(value) {

        myObject =value ;

    }

    function  getEmployeeOther(empId ) {

    var deferred = $q.defer();

    $http.get(REST_SERVICE_URI+'Employee/getEmployeeOther/'+ empId )
        .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){

                console.error('Error while fetching data');

                deferred.reject(errResponse);
            }
        );
    return deferred.promise;
}

    function  getCombo(comboNo ) {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI+'combo/getInfoForCombo/'+ comboNo )
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){

                    console.error('Error while fetching data');

                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

function deleteQualificationCourse(elementId) {

        var deferred = $q.defer();

        $http.put(REST_SERVICE_URI + 'qualificationCourse/deleteQualificationCourse/'+elementId)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.error(reason);
                }
            );

        return deferred.promise;
    }
    function deleteWarActivity(elementId) {

        var deferred = $q.defer();

        $http.put(REST_SERVICE_URI + 'warActivity/deleteWarActivity/'+elementId)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.error(reason);
                }
            );

        return deferred.promise;
    }

}]);