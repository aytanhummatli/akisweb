'use strict';

angular.module('appEmployeeEducation').factory('employeEducationService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:8060/';


    var factory = {

        getEmployeeEducation:getEmployeeEducation,
        deleteLanguageLevel:deleteLanguageLevel,
        deleteEducationHistory:deleteEducationHistory,
        deleteAttesstationHistory:deleteAttesstationHistory,
        getCombo:getCombo,
    //    Data:Data,

        getObject:getObject,
        setObject:setObject
                    };
    return factory;

    var myObject={};


    function  getObject() {

        return myObject;

    }

    function setObject(value) {

        myObject =value ;

    }

    function  getCombo(comboNo ) {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI+'combo/getInfoForCombo/'+ comboNo )
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){

                    console.error('Error while fetching data');

                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function  getEmployeeEducation(empId) {

    var deferred = $q.defer();

    $http.get(REST_SERVICE_URI+'Employee/getEmployeeEducation/'+ empId)
        .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){

                console.error('Error while fetching general data');

                deferred.reject(errResponse);
            }
        );
    return deferred.promise;
}

    function deleteLanguageLevel(elementId) {

        var deferred = $q.defer();

        $http.put(REST_SERVICE_URI + 'languageLevel/deleteLanguageLevel/'+elementId)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.error(reason);
                }
            );

        return deferred.promise;
    }
    function deleteEducationHistory(elementId) {

        var deferred = $q.defer();

        $http.put(REST_SERVICE_URI + 'languageLevel/deleteLanguageLevel/'+elementId)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.error(reason);
                }
            );

        return deferred.promise;
    }
    function deleteAttesstationHistory(elementId) {

        var deferred = $q.defer();

        $http.put(REST_SERVICE_URI + 'attesstationHistory/deleteAttesstationHistory/'+elementId)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.error(reason);
                }
            );

        return deferred.promise;
    }
}]);