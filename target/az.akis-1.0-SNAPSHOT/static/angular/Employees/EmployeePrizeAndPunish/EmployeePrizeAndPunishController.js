
'use strict';

angular.module('appEmployeePrizeAndPunish').controller('EmployeePrizeAndPunishController', ['$window', '$scope', '$http', '$sce', '$uibModal', 'EmployeePrizeAndPunishService', function ($window, $scope, $http, $sce, $uibModal,EmployeePrizeAndPunishService) {



    // prize
  //  $scope.empId =1340000501512;

    //punish
    $scope.empId=900000203663;

    $scope.employee ={
        prize:[{getDate: '', id: null, orderNo: '',  orderOrgan: {  fullName: null,  name: '', },
    organFullNameId: null,    prizeType: { combo: {name: null, no: null, id: 0, description: ''},
        id: null,
        name: '',
    },
    punishEndDate: null,
    punishEndOrder: null,
    punishEndReason: {id: null, name: null, type: {id: 0,
            name: null}},
    reason:{  id: null,  name: '', type:{description: null, id: 0,name: null}}
            }],

        punishlist:[{getDate: '', id: null, orderNo: '',  orderOrgan: {  fullName: null,  name: '', },
            organFullNameId: null,    prizeType: { combo: {name: null, no: null, id: 0, description: ''},
                id: null,
                name: '',
            },
            punishEndDate: null,
            punishEndOrder: null,
            punishEndReason: {id: null, name: null, type: {id: 0,
                    name: null}},
            reason:{  id: null,  name: '', type:{description: null, id: 0,name: null}}
        }],

        medalList:[{getDate: '', id: null, orderNo: '',  orderOrgan: {  fullName: null,  name: '', },
            organFullNameId: null,    prizeType: { combo: {name: null, no: null, id: 0, description: ''},
                id: null,
                name: '',
            },
            punishEndDate: null,
            punishEndOrder: null,
            punishEndReason: {id: null, name: null, type: {id: 0,
                    name: null}},
            reason:{  id: null,  name: '', type:{description: null, id: 0,name: null}}
        }],
        dioPrizeList:[{getDate: '', id: null, orderNo: '',  orderOrgan: {  fullName: null,  name: '', },
            organFullNameId: null,    prizeType: { combo: {name: null, no: null, id: 0, description: ''},
                id: null,
                name: '',
            },
            punishEndDate: null,
            punishEndOrder: null,
            punishEndReason: {id: null, name: null, type: {id: 0,
                    name: null}},
            reason:{  id: null,  name: '', type:{description: null, id: 0,name: null}}
        }]
    };

    $scope.employees = [$scope.employee];

    $scope.getEmployeePrizeAndPunish = getEmployeePrizeAndPunish;

    getEmployeePrizeAndPunish( $scope.empId );

    function getEmployeePrizeAndPunish(empId) {

        EmployeePrizeAndPunishService.getEmployeePrizeAndPunish(empId)
            .then(
                function (d) {
                    $scope.employee  = d;

                    console.log('getEmployeePrizeAndPunish results d=',d);

                    console.log('getEmployeePrizeAndPunish results $scope.employee',$scope.employee );
                },

                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }



    function deletePrizeOrPunish(elementID) {

        EmployeePrizeAndPunishService.deletePrizeOrPunish(elementID)
            .then(
                function (d) {

                },
                function (errResponse) {

                }
            );
    }

    $scope.removePrizeAndPunish = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';

        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: ModalInstanceCtrlPrizeAndPunish
            }
        );
        modalInstance.result.then(function () {
            deletePrizeOrPunish(id);
            getEmployeePrizeAndPunish($scope.empId);
        });
    };

    var empId = $scope.empId;

    var ModalInstanceCtrlPrizeAndPunish = function ($scope, $uibModalInstance) {

        $scope.ok = function () {

            //  getEmployeeEducation(empId);
            getEmployeePrizeAndPunish(empId);
            $uibModalInstance.close();
        };

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');
        };
    };


}]);

