'use strict';

angular.module('appEmployeeGeneral').controller('employeeGeneralController', ['$window', '$scope', '$http', '$sce', '$uibModal', 'employeeGeneralService', function ($window, $scope, $http, $sce, $uibModal, employeeGeneralService) {

 //   $scope.empId=500000002465;
    $scope.setSwitch=setSwitch;

    $scope.militaryServieUpdateOrInsert=militaryServieUpdateOrInsert;
    $scope.empId= 20000001376;

    $scope.employee = {

        id: null,

        person: {id: '', name: '', surName:'', patronymic: '' },

        image: {id: '', imgURL: '', ftpFoder: ''},

        employeeRank:[{ empId:null,orderNo:null,createDate:null,rankOutOfTurn:null, id:'',

        rank: {name: '',suffix:'',rankType:{id:null,Type:''}},

        cmbsService:{id:'', name:'', description:''}

        }],

        successDegreeHistory:[{orderNo:null,createDate:'',successDegree:{name:''}}],

        employeeMilitaryService:{id:'',armyPort:'',beginDate:'',endDate:'',serialNo:'',isRegistered:'',militaryDuty:{description:''}}

    };

    $scope.militaryDutyList=[];


    $scope.employees = [$scope.employee];

    getEmployeeGeneral($scope.empId);

    getHerbiMukellefiyyetList();

    function getEmployeeGeneral(empId) {

        employeeGeneralService.getEmployeeGeneral(empId)
            .then(
                function (d) {
                    $scope.employee = d;

                    $scope.employeeMilitaryServiceOrign=d.employeeMilitaryService;

                    $scope.militaryDuty=d.employeeMilitaryService.militaryDuty;

                    setSwitch($scope.militaryDuty);

                    $scope.employeeMilitaryService=d.employeeMilitaryService;



                    if (d.employeeMilitaryService.isRegistered === 1){

                        $scope.registration=true;
                    }

                    console.log('getEmployeeDetails results $scope.employee',$scope.employee);
                },
                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }





     function setSwitch(element){

         $scope.employeeMilitaryService={armyPort:'',beginDate:'',endDate:'',serialNo:'',isRegistered:'' };

         $scope.registration=false;


         $scope.date=false;

         $scope.serialNo=false;

         $scope.reg=false;

         $scope.armyPort=false;


        if( $scope.militaryDuty.id===3026  || $scope.militaryDuty.id===33571 ){

            $scope.date=false;

            $scope.serialNo=false;

            $scope.reg=false;

            $scope.armyPort=false;

        }

        else  if ( !$scope.militaryDuty ||$scope.militaryDuty.id===3027  ) {

            $scope.date=true;

            $scope.serialNo=true;

            $scope.reg=true;

            $scope.armyPort=true;
        }
        else  if ($scope.militaryDuty.id===3130 ) {

            $scope.date=true;

            $scope.serialNo=false;

            $scope.reg=true;

            $scope.armyPort=true;
        }
        else  if ($scope.militaryDuty.id===3131 ) {

            $scope.date=true;

            $scope.serialNo=true;

            $scope.reg=true;

            $scope.armyPort=true;
        }

    }

    $scope.updateEmployeeRank=function(scope){

        var myObj={};

        myObj.object=scope.inf;

        myObj.update=true;

        employeeGeneralService.setObject(myObj);


        var modalInstance = $uibModal.open({
            templateUrl: 'updateEmployeeRank.html',
            controller: 'modalEmployeeRank',
            backdrop: 'static'
        });

        modalInstance.result.then(function () {

            getEmployeeGeneral($scope.empId);
        });


  }

    function deleteEmployeeRank(empId) {

        employeeGeneralService.deleteEmployeeRank(empId)
            .then(
                function (d) {
                    getEmployeeGeneral($scope.empId);
                },
                function (errResponse) {

                }
            );
    }

    $scope.addEmployeeRank=function () {

        var myObj={};

        myObj.empId=$scope.empId;

        myObj.update=false;

        employeeGeneralService.setObject(myObj);

        var modalInstance = $uibModal.open({
            templateUrl: 'addEmployeeRank.html',
            controller: 'modalEmployeeRank',
            backdrop: 'static'
        });

        modalInstance.result.then(function () {

            getEmployeeGeneral($scope.empId);
        });
    }



    ////*****begin*************///****delete Employee rank*************///////////////////////******************///

    var empId=$scope.empId;
    $scope.removeRank = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';
        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
            template: modalHtml,
            controller: ModalInstanceCtrlRank
        }
        );

        modalInstance.result.then(function () {
            console.log('id for deleteeeee',id);
            console.log('empId for delete',$scope.empId);

            deleteEmployeeRank(id);

           //   getEmployeeGeneral($scope.empId);

        });

    };
    var ModalInstanceCtrlRank = function ($scope, $uibModalInstance) {
        $scope.ok = function () {

               console.log('empId',empId);

            getEmployeeGeneral(empId);

            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    };
    ////******end************///****delete Employee rank*************///////////////////////******************///



    //////     ***********  Service degreee************         ////////////////


    $scope.addServiceDegree=function () {

        var myObj={};

        myObj.empId=$scope.empId;

        myObj.update=false;

        employeeGeneralService.setObject(myObj);

        var modalInstance = $uibModal.open({
            templateUrl: 'addServiceDegree.html',
            controller: 'modalServiceDegree',
            backdrop: 'static'
        });

        modalInstance.result.then(function () {
            getEmployeeGeneral($scope.empId);
        });
    }


    $scope.updateServiceDegree=function(scope){

        console.log('',scope);

        var myObj={};

        myObj.empId=$scope.empId;

        myObj.update=true;

        myObj.object=scope.inf;

        employeeGeneralService.setObject(myObj);

        var modalInstance=$uibModal.open({

             templateUrl:'updateServiceDegree.html',
             controller:'modalServiceDegree',
             backdrop:'static',
                        }
         );
         modalInstance.result.then(function () {
             getEmployeeGeneral($scope.empId);

         })

    }

       function deleteSuccessDegreeHistory(id){

        employeeGeneralService.deleteSuccessDegreeHistory(id)
            .then(
            function (d) {

            console.log('');
            },

        function(errResponse){

                console.error(errResponse);
        }

    );

}

    ////*******begin***********///****delete Employee service degree*************///////////////////////******************///

    $scope.removeServiceDegree = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';
        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
            template: modalHtml,
            controller: ModalInstanceCtrlServDeg
        });

        modalInstance.result.then(function () {

           // getEmployeeGeneral($scope.empId);
            deleteSuccessDegreeHistory(id);

        });

    };
    var ModalInstanceCtrlServDeg = function ($scope, $uibModalInstance) {
        $scope.ok = function () {

            getEmployeeGeneral($scope.empId);

            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    };

    ////*******end***********///****delete Employee service degree*************///////////////////////******************///



    //  *************************************** begin military service*******************************//


  function getHerbiMukellefiyyetList() {

    employeeGeneralService.getHerbiMukellefiyyetList()
        .then(
            function (value) {

                $scope.militaryDutyList=value;
                console.log(' $scope.militaryDutyList ', $scope.militaryDutyList);
            },
            function (errResp) {

                console.error(errResp);
            }
        );
}

    function updateMilitaryService(element){

        employeeGeneralService.updateMilitaryService(element)

            .then(
                function (value) {},

                function (errRes) {
                    console.error(errRes);
                }
            );}


function insertMilitaryService(element){

      employeeGeneralService.insertMilitaryService(element)

          .then(
                  function (value) {},

              function (errRes) {
                  console.error(errRes);
              }
                        );}

    function militaryServieUpdateOrInsert(){

        $scope.employeeMilitaryService.militaryDuty=$scope.militaryDuty;

        var oldValue=$scope.employeeMilitaryServiceOrign;

        var newValue=$scope.employeeMilitaryService;

        if ($scope.reg===true) {
            newValue.isRegistered=1;
        }
        else {  newValue.isRegistered=0;  }
        newValue.empId=$scope.empId;

        if  (!oldValue.militaryDuty.id ) {
            console.log('military insert',newValue);
            insertMilitaryService(newValue);
        }
           else
        {
            console.log('military update',newValue);

           updateMilitaryService(newValue);
        }

    }

    $(function () {
        var bindDatePicker = function() {
            $(".date").datetimepicker({
                format:'DD.MM.YYYY',
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
            }).find('input:first').on("blur",function () {
                // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
                // update the format if it's yyyy-mm-dd
                var date = parseDate($(this).val());

                if (! isValidDate(date)) {
                    //create date based on momentjs (we have that)
                    date = moment().format('DD.MM.YYYY');
                }

                $(this).val(date);
            });
        }

        var isValidDate = function(value, format) {
            format = format || false;
            // lets parse the date to the best of our knowledge
            if (format) {
                value = parseDate(value);
            }

            var timestamp = Date.parse(value);

            return isNaN(timestamp) == false;
        }

        var parseDate = function(value) {
            var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
            if (m)
                value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

            return value;
        }

        bindDatePicker();
    });

}]);

