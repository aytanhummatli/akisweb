'use strict';

angular.module('appEmployeeFamily').factory('employeeFamilyService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8060/';


    var factory = {

        getEmployeeFamily: getEmployeeFamily,
        getRelativeDegreeList: getRelativeDegreeList,
        getWorkList: getWorkList,
        getCountryList: getCountryList,
        getRegionList: getRegionList,
        getSubRegionList:getSubRegionList,
        getVillageList:getVillageList,
        getStreetList:getStreetList,
        deleteRelative:deleteRelative,
        getObject: getObject,
        setObject: setObject
    };
    return factory;

    var myObject = {};


    function getStreetList() {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'street/getStreetList')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {

                    console.error('Error while fetching data');

                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }

    function getVillageList() {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'village/getVillageList')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {

                    console.error('Error while fetching data');

                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getSubRegionList() {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'subRegion/getSubRegionList')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {

                    console.error('Error while fetching data');

                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getRegionList( ) {
        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'region/getRegionList')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {

                    console.error('Error while fetching data');

                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getCountryList() {
        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'country/getCountryList')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {

                    console.error('Error while fetching data');

                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getObject() {

        return myObject;

    }

    function setObject(value) {

        myObject = value;

    }

    function getEmployeeFamily(empId) {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'Employee/getEmployeeFamilybyId/' + empId)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {

                    console.error('Error while fetching data');

                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getRelativeDegreeList() {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'relative/getRelativeDegreeList')
            .then(
                function (value) {

                    deferred.resolve(value.data);

                },

                function (reason) {
                    console.error(reason);
                });

        return deferred.promise;

    }

    function getWorkList() {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'relative/workList')
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.error(reason);
                }
            );

        return deferred.promise;

    }

    function deleteRelative(elementId) {

        var deferred = $q.defer();

        $http.put(REST_SERVICE_URI + 'relative/deleteRelative/'+elementId)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.error(reason);
                }
            );

        return deferred.promise;
    }
}]);