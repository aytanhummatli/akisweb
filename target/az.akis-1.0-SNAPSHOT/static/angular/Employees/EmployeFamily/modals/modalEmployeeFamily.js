'use strict';

  angular.module('appEmployeeFamily').controller('modalEmployeeFamily', ['$scope', '$uibModalInstance', '$uibModal','employeeFamilyService', function ($scope, $uibModalInstance, $uibModal,employeeFamilyService) {

     $scope.relative={

        id: null, workPlace: '', bornYear: '', livingAddressNote: null,

        livingAddress: {id: null, adress_fullname: ''},

        person: {

            id: null, name: '', surName: '', patronymic: '', birthDate: null,maidenName:'',

            bornAddress: {id: null, adress_fullname: ''}
        },

        relativeDegree: {id:'',description: ''},

    };

     $scope.relativeDegree= {id:'',description: ''};

    var update=employeeFamilyService.getObject().update;


      if (update == true){

          var myObjForUpdate = employeeFamilyService.getObject().object;

          console.log('myObjForUpdate ', myObjForUpdate);

          $scope.relativeDegree=myObjForUpdate.relativeDegree;

          $scope.relative=myObjForUpdate;

          $scope.workPlace=myObjForUpdate.workPlace;
          $scope.person=myObjForUpdate.person;

      }  else   {}

        employeeFamilyService.getRelativeDegreeList()
            .then(
                function (value) {

                    $scope.relativeDegreeList=value;
                },
                function (reason) { console.error(reason); }
            );


        employeeFamilyService.getWorkList()
            .then(
                function (value) {
                    $scope.workChooserList=value;
                },
                function (reason) { console.error(reason); }
            );

$scope.addChoosenWork=function(){

    console.log('$scope.workChooser',$scope.workChooser );

    $scope.relative.workPlace=$scope.workChooser.description;

}


 $scope.chooseBirthPlace=function (scope) {

    var modalInstance=$uibModal.open({

        templateUrl:'addLocation.html',

        controller:'modalLocation',

        backdrop:'static'

        });
  }


  $scope.clearBirthLocation=function(){

$scope.person.bornAddress.adress_fullname='';

console.log('elementttt ',$scope.relative.person.bornAddress.adress_fullname );

  }
    $scope.submit = function () {

        $uibModalInstance.close();
     };

    $scope.cancel = function () {

    $uibModalInstance.dismiss('cancel');

    };

}]);



