'use strict';

angular.module('appEmployeeDetails').controller('modalContactInformation', ['$scope', '$uibModalInstance', 'employeeDetailsService',  function ($scope, $uibModalInstance, employeeDetailsService) {

    $scope.contactInformation={name:'',value:'',id:''};

    $scope.contactInformations=[$scope.contactInformation];



    $scope.contactInformations = employeeDetailsService.getContactInfo()
        .then(
            function (type) {
                $scope.contactInformations = type;
            },
            function (errResponse) {
                console.error('Error while fetching ContactInfo');
            }
        );

    function add(){

       var myObj ={};

        var  personId=employeeDetailsService.getObject().personId;

       employeeDetailsService.addContactInfo($scope.contactInformation,personId);


    }

    $scope.submit = function () {

        add();

      $uibModalInstance.close();

    };

    $scope.cancel = function () {

        $uibModalInstance.dismiss('cancel');
    };


}]);



