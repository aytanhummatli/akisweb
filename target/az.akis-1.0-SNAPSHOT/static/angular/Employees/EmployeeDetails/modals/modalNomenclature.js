'use strict';

angular.module('appEmployeeDetails').controller('modalNomenclatureCtrl', ['$scope', '$uibModalInstance', 'employeeDetailsService',  function ($scope, $uibModalInstance, employeeDetailsService) {

    $scope.getSearchResults=getSearchResults;

    $scope.selectNomenklature=selectNomenklature;

    $scope.nomenkList=[{name:'',str:'',id:''}];

    getNomenkForSelect();

    function getNomenkForSelect() {

        employeeDetailsService.getNomenkForSelect()
            .then(

                function (d) {

                    $scope.nomenkList = d;

                    console.log('$scope.nomenkList',$scope.nomenkList);
                },

                function (errResponse) {

                    console.error('Error while fetching nomenk',errResponse);
                }
            );
    }


    function getSearchResults( ) {

        var search=$scope.search;

        if(!search)

        { search="666" }

        console.log('search',search);

        employeeDetailsService.getSearchResults(search)
            .then(
                function (d) {
                    $scope.nomenkList = d;

                    console.log('$scope.nomenkList',$scope.nomenkList);
                },

                function (errResponse) {

                    console.error('Error while fetching nomenk',errResponse);
                }
            );
    };

function selectNomenklature(scope){

    var myObj={};

   myObj.nomenk=scope.info;

    employeeDetailsService.setObject(myObj);

    $uibModalInstance.close();
}

    $scope.submit = function () {

     //   $uibModalInstance.close();

    };

    $scope.cancel = function () {

        $uibModalInstance.dismiss('cancel');

    };


}]);