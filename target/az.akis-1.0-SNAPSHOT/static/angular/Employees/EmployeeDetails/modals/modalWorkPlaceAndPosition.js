'use strict';

angular.module('appEmployeeDetails').controller('modalWorkPlaceAndPositionCtrl', ['$scope', '$uibModalInstance', 'employeeDetailsService',  function ($scope, $uibModalInstance, employeeDetailsService) {


    $scope.submit = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


}]);