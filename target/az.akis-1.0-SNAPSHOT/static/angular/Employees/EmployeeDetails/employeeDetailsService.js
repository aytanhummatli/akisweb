'use strict';

angular.module('appEmployeeDetails').factory('employeeDetailsService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8060/';


    var factory = {

        getEmployeeDetails: getEmployeeDetails,
        addContactInfo: addContactInfo,
        Data: Data,
        getContactInfo: getContactInfo,
        getCombo: getCombo,
        getNomenkForSelect:getNomenkForSelect,
        getSearchResults:getSearchResults,
        getDataFromClass:getDataFromClass,
        getObject: getObject,
        setObject: setObject
    };
    return factory;

    var myObject = {};

    function getObject() {

        return myObject;

    }

    function setObject(value) {

        myObject = value;

        console.log('myobjjj from service', myObject);

    }


    function  getDataFromClass() {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI+'Employee/getEmployeeId')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){

                    console.error('Error while fetching data');

                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }



    function Data(parentId) {
        console.log('parentId' + parentId);

        var deferred = $q.defer();

        $http.get('http://localhost:8060/StructureMap/' + 'getParents/' + parentId)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                    console.log('response.data' + response.data);
                },
                function (errResponse) {
                    console.log('http://localhost:8060/StructureMap/' + 'getParents/' + parentId);
                    console.error('Error while fetching trees');
                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;

    }

    function getCombo(comboNo) {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'combo/getInfoForCombo/' + comboNo)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {

                    console.error('Error while fetching data');

                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getEmployeeDetails(empId) {

        console.log('i am in details services', empId);

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'Employee/getEmployeebyId/' + empId)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {

                    console.error('Error while fetching data');

                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;

    }

    function addContactInfo(element, personId) {

        console.log('elem', element);

        console.log('personId', personId);

        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI + 'contact/addContactInfo/' + personId, element)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while creating element');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getContactInfo() {

        console.log('i am hereeeee  serviceee');

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'contact/getAllContacts')
            .then(
                function (response) {

                    deferred.resolve(response.data);
                    console.log('sdfsdn sdfsdfsd', response.data);
                },
                function (errREsponse) {

                    console.error('Error while fetching contct list aaa');
                    deferred.reject(errREsponse);
                }
            );

        return deferred.promise;

    }

    function getNomenkForSelect() {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'nomenklature/getNomenkForSelect' )
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {

                    console.error('Error while fetching data',errResponse);

                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }


    function getSearchResults(elementName) {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'nomenklature/getSearchResults/'+elementName )
            .then(
                function (response) {

                    deferred.resolve(response.data);

                    console.log('response data',response);
                },
                function (errResponse) {

                    console.error('Error while fetching data',errResponse);

                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }


}]);