'use strict';

    angular.module('appStructure').factory('StructureMapservice', ['$http','$q', function($http,$q){

        var   myObject={};

        var REST_SERVICE_URI = 'http://localhost:8060/StructureMap/';

        var factory = {

            Data:Data,

            searchResults:searchResults,
            removeElement:removeElement,
            addElement:addElement,
            getObject:getObject,
            setObject:setObject,
            getOrgantypeById:getOrgantypeById,
            getOrgantypes:getOrgantypes,
            getElementById:getElementById,
            getEmployees:getEmployees
        };
        return factory;


        function  searchResults(searchValue) {

            var deferred = $q.defer();

            $http.get(REST_SERVICE_URI+'searchResult/'+searchValue)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function(errResponse){
                        console.log("REST_SERVICE_URI   "+REST_SERVICE_URI);
                        console.error('Error while fetching trees');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }


        function  Data(parentId) {
            console.log('parentId'+parentId);

            var deferred = $q.defer();

            $http.get(REST_SERVICE_URI+'getParents/'+parentId)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                        console.log('response.data'+response.data);
                    },
                    function(errResponse){
                        console.log("REST_SERVICE_URI   "+REST_SERVICE_URI+'getParents/'+parentId);
                        console.error('Error while fetching trees');
                        deferred.reject(errResponse);
                    }
                );

            return deferred.promise;

        }

        function  getElementById(id) {

            var deferred = $q.defer();

            $http.get(REST_SERVICE_URI+'getElementById/'+id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function(errResponse){
                        console.log("REST_SERVICE_URI   "+REST_SERVICE_URI+'getElementById/'+id);
                        console.error('Error while fetching Organtypes');
                        deferred.reject(errResponse);
                    }
                );

            return deferred.promise;

        }

        function  getOrgantypeById(id) {
        console.log('organ id',id);
            var deferred = $q.defer();

            $http.get(REST_SERVICE_URI+'getOrgantypeById/'+id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function(errResponse){
                        console.log("REST_SERVICE_URI   "+REST_SERVICE_URI+'getOrgantypeById/'+id);
                        console.error('Error while fetching Organtypes');
                        deferred.reject(errResponse);
                    }
                );

            return deferred.promise;

        }




        function  getOrgantypes() {

            var deferred = $q.defer();

            $http.get(REST_SERVICE_URI+'getOrgantypes')
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function(errResponse){
                        console.log("REST_SERVICE_URI   "+REST_SERVICE_URI+'getOrgantypes');
                        console.error('Error while fetching Organtypes');
                        deferred.reject(errResponse);
                    }
                );

            return deferred.promise;

        }


        function  removeElement(element_id) {

            var deferred = $q.defer();

            $http.get(REST_SERVICE_URI+'removeElement/'+element_id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);

                    },
                    function(errResponse){
                        console.log("REST_SERVICE_URI   "+REST_SERVICE_URI+'removeElement/'+element_id);
                        console.error('Error while deleting elements');
                        deferred.reject(errResponse);
                    }
                );

            return deferred.promise;

        }


        function addElement(element) {

            var deferred = $q.defer();
            $http.post(REST_SERVICE_URI+'addElement/', element)
                .then(
                    function (response) {
                        deferred.resolve(response.data);

                    },

                    function(errResponse){
                        console.error('Error while creating element');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }


        function updateElement(element) {
            var deferred = $q.defer();
            $http.post(REST_SERVICE_URI+'updateElement/', element)
                .then(
                    function (response) {
                        deferred.resolve(response.data);

                    },
                    function(errResponse){
                        console.error('Error while updating element');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }


function  getObject() {

    return myObject;

}

function setObject(value) {

    myObject =value ;

    }


    //////////////////////////////////-----Stafff Section//////////////////
        function  getEmployees(scope) {
            console.log(scope );
            var deferred = $q.defer();

            $http.post('http://localhost:8060/Staff/getEmployees/',scope)
                .then(
                    function (response) {
                        deferred.resolve(response.data);

                    },
                    function(errResponse){

                        console.error('Error while fetching emplpyees');
                        deferred.reject(errResponse);
                    }
                );

            return deferred.promise;

        }

    }]);


