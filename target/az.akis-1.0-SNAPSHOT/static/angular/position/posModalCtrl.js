var hrPositionApp = angular.module('positionApp');
hrPositionApp.controller('PositionModalController', ['$scope', '$uibModalInstance', 'PositionService', 'editId', function ($scope, $uibModalInstance, PositionService, editId) {

    $scope.alert = {type: '', msg: '', show: false};
    $scope.positionGroups = [];
    $scope.positionGroup = {};
    $scope.closeAlert = function () {
        $scope.alert = null;
    };

    self.positionName={id:null,name:'',active:null,position:{}};
    self.position={id:null,active:'',createDate:'',endDate:'',positionGroup:{}};
    self.positionGroup={id:null,name:'',createDate:'',endDate:'',active:'',groupCode:'',groupParent:null};

    $scope.createPosition = createPosition;
    $scope.updatePosition = updatePosition;


    if (!(editId === null || editId === 0 || editId === '')) {
        console.log(editId + 'edit id consol');
        PositionService.getPositionNamesById(editId)
            .then(
                function (data) {
                    console.log('functiona daxil oldu' + editId)
                    $scope.positionName = data;
                    console.log('-----',$scope.positionName.name)
                    console.log('====='+$scope.positionName.name)
                },
                function (errResponse) {
                    console.log('error while get hp type' + errResponse)
                }
            );
    } else {
        self.positionName={id:null,name:'',active:null,position:{}};
    }

    $scope.positionGroups = PositionService.getCategory()
        .then(
            function (type) {
                $scope.positionGroups = type;
                console.log('mesaj', type)
            },
            function (errResponse) {
                console.log('error while get category' + errResponse)
            }
        );

    function createPosition(hp) {
        console.log('posGroupId: ',$scope.positionName.position.id)
        PositionService.createPosition(hp)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'Elave edildi', show: true}
                    console.log('pos names name: ' + $scope.positionName.name)
                    console.log('pos group id: ' + $scope.positionName.position.positionGroup.id)
                },
                function (errResponse) {
                    $scope.alert = {type: 'error', msg: 'Error bash verdi', show: true}
                    console.log('error while create position' + errResponse)
                }
            );

    }

    function updatePosition(hp) {
        PositionService.updatePosition(hp)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'Elave edildi', show: true}
                    console.log('ugurlu in modalctrl')
                },
                function (errReponse) {
                    $scope.alert = {type: 'error', msg: 'error bash verdi', show: true}
                    console.log('error while update position' + errReponse)
                }
            );
    }


    $scope.submit = function () {
        console.log('submit duymesi ishledi');
        console.log('editId  ',$scope.positionName.position.id);
        console.log(editId);
      if (editId === 0 || editId === null) {
        console.log('hp.hrPositionNames.posId  ',$scope.positionName.position.id);
           // if (editId === 0 || editId === null) {
            console.log('new hp added' + $scope.positionName);
            createPosition($scope.positionName);
            console.log('posGroupId: ',$scope.positionName.position.positionGroup.id)
            $uibModalInstance.close();
        } else {
            updatePosition($scope.positionName);
            console.log('ugurla deyishdirildi: ' + $scope.positionName.position.id);
            console.log("hp.id"+$scope.positionName.position.id+"        qrup id    "+$scope.positionName.position.positionGroup.id+"            vefize adi   " +$scope.positionName.name)
            $uibModalInstance.close();
            console.log('Hpg updated with id ');
        }

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };



}]);