var hrPositionApp = angular.module('positionApp');
hrPositionApp.controller('PosDelModalCtrl', ['$scope', '$uibModalInstance', 'PositionService', 'delId',function ($scope,$uibModalInstance,PositionService,delId) {

    $scope.alert = {type: '', msg: '', show: false};
    $scope.categorys = [];
    $scope.category = {};
    $scope.closeAlert = function () {
        $scope.alert = null;
    };

    self.positionName={id:null,name:'',active:null,position:{}};
    self.position={id:null,active:'',createDate:'',endDate:'',positionGroup:{}};
    self.positionGroup={id:null,name:'',createDate:'',endDate:'',active:'',groupCode:'',groupParent:null};

    $scope.deletePosition = deletePosition;

    function deletePosition(id) {
        PositionService.deletePosition(id)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'silindi', show: true}
                },
                function (errResponse) {
                    $scope.alert = {type: 'error', msg: 'error bash verdi', show: true}
                    console.log('error while delete position' + errResponse)
                }
            );
    }

    $scope.ok=function(){
        console.log('ok functionuna daxil oldu');
        deletePosition(delId);
        console.log('del Id :'+delId )
        console.log('melumat silindi '+$scope.positionName.position.id);
        $uibModalInstance.close();
    }


    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}]);
