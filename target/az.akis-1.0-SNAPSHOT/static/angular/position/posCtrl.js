angular.module('positionApp').controller('PositionController', ['$scope','$uibModal', 'PositionService', function ($scope, $uibModal, PositionService) {

    var self = this;

    self.pageNumber=1;
    self.total_count=0;
    self.itemsPerPage = 25;

    self.positionName={id:null,name:'',active:null,position:{}};
    self.position={id:null,active:'',createDate:'',endDate:'',positionGroup:{}};
    self.positionGroup={id:null,name:'',createDate:'',endDate:'',active:'',groupCode:'',groupParent:null};
    self.positionList=[self.positionName];

    self.getPositionList = getPositionList;
    self.add = add;
    self.edit = edit;
    self.remove = remove;

    getPositionList(self.pageNumber);


    function getPositionList(pageNumber) {
        PositionService.getPositionList(pageNumber)
            .then(
                function (data) {
                    self.positionList=data;
                    self.total_count=8000;
                },
                function (errorResponse) {
                    console.error('error while get list' + errorResponse);
                }
            );
    }

    function add() {
        console.log('add  ');
        var modalInstance = $uibModal.open({
            templateUrl: 'positionAddModal.html',
            controller: 'PositionModalController',
            resolve: {
                editId: function () {
                    return null;
                }
            },
            backdrop: 'static'
        });

        modalInstance.closed.then(function () {
            getPositionList(self.pageNumber);
        });
        modalInstance.result.catch(function (res) {
            if (!(res === 'cancel' || res === 'escape press key')) {
                getPositionList(self.pageNumber);
                throw res;
            }
        });
    }

    function edit(id) {
        console.log('edit');
        var modalInstance = $uibModal.open({
            templateUrl: 'positionUpdateModal.html',
            controller: 'PositionModalController',
            resolve: {
                editId: function () {
                    return id;
                }
            },
            backdrop: 'static'
        });
        modalInstance.closed.then(function () {
            getPositionList(self.pageNumber);
        });
        modalInstance.result.catch(function (reason) {
            if (!(reason === 'cancel' || reason === 'escape key press')) {
                getPositionList(self.pageNumber);
                throw reason;
            }
        });
    }

    function remove(id) {
        var modalInstance = $uibModal.open({
            templateUrl: 'positionRemoveModal.html',
            controller: 'PosDelModalCtrl',
            resolve: {
                delId: function () {
                    return id;
                }
            },
            backdrop: 'static'
        });
        modalInstance.closed.then(function () {
            getPositionList(self.pageNumber);
        });
        modalInstance.result.catch(function (reason) {
            if (!(reason === 'cancel' || reason === 'escape key presss')) {
                getPositionList(self.pageNumber);
                throw reason;
            }
        });

    }

}]);