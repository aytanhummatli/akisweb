angular.module('UserInfoApp').controller('UserInfoAddModalController', ['$scope', '$uibModalInstance', '$filter', '$compile', '$timeout', '$interval', '$http', 'UserInfoService', 'editId', function ($scope, $uibModalInstance, $filter, $compile, $timeout, $interval, $http, UserInfoService, editId) {

    var REST_SERVICE_URL = 'http://localhost:8092/';

    $scope.alert = {type: '', msg: '', show: false};
    $scope.closeAlert = function () {
        $scope.alert = null;
    };
    $scope.userInfo = {
        user: {},
        employee: {},
        person: {},
        card: {},
        nomenclature: {},
        imgHistory: {},
        password: {},
        userRule:{},
        userButtonRule:{},
        rule:{},
        buttonRule:{}
    };

    $scope.user = {
        id: null,
        userName: '',
        employeeId: null,
        createDate: '',
        endDate: '',
        active: '',
        ident: null,
        nomenklaturaId: null,
        doupdate: ''
    };
    $scope.employee = {
        id: null,
        person: {},
        createDate: '',
        endDate: '',
        personalMatterNo: '',
        policeCardNo: '',
        nationalityId: null,
        civil: '',
        ok: '',
        cardMustBeChanged: '',
        cardReasonId: null,
        rank: {},
        image: {},
        positionName: {},
        fullStrName: '',
        tree: {}
    };
    $scope.person = {
        id: null, name: '', surname: '', patronymic: '', birthDate: '', marriedStatus: null,
        bornAddressId: null, bornAddressNote: '', relationId: null, maidenName: '', maleFemaleId: null, fullName: ''
    };

    $scope.nomenclature = {
        id: null,
        name: '',
        createDate: '',
        endDate: '',
        active: '',
        str: '',
        strTreeId: null,
        depNomId: null,
        nomDependency: {}
    };
    $scope.password = {id: null, userId: null, password: '', createDate: '', endDate: '', active: '', newPassword: ''};

    $scope.userFullnameList = [$scope.person];
    $scope.nomNameList = [$scope.nomenclature];


    $scope.searchFullName = function () {


        if (!$scope.userInfo.person.fullName) {
            $scope.valFullname = 0;
        } else {
            $scope.valFullname = $scope.userInfo.person.fullName;
            console.log(' $scope.fullname', $scope.userInfo.person.fullName);
        }


        UserInfoService.searchUserFullName($scope.valFullname)
            .then(
                function (value) {
                    $scope.userFullnameList = value;
                    console.log("user  list ", value);
                }, function (reason) {
                    console.log("error while search user fullname list  ", reason);
                }
            );


    };


    $scope.searchNomName = function () {


        if (!$scope.userInfo.nomenclature.name) {
            $scope.valNomName = 0;
        } else {
            $scope.valNomName = $scope.userInfo.nomenclature.name;
            console.log(' $scope.nomname', $scope.userInfo.nomenclature.name);
        }

        UserInfoService.searchNomName($scope.valNomName)
            .then(
                function (value) {
                    $scope.nomNameList = value;
                    console.log('nom list', value);
                }, function (reason) {
                    console.log('error while search nom name list', reason);
                }
            )
    }

    // $scope.checkUsername = function () {
    //     if (!$scope.userInfo.user.userName) {
    //         $scope.checkText = 0;
    //     } else {
    //         $scope.checkText = $scope.userInfo.user.userName;
    //         console.log('==1=1=1=1=1  ', $scope.checkText)
    //     }
    //     // UserInfoService.checkUsername($scope.checkText)
    //     //     .then(
    //     //         function successCallback(response) {
    //     //             $scope.unamestatus = response.data;
    //     //         },function (reason) {
    //     //             console.log('error while user check',reason);
    //     //         }
    //     //     );
    //     $http({
    //         method: 'post',
    //         url: REST_SERVICE_URL + 'user/checkUsername/' + $scope.checkText,
    //         data: {username: $scope.username}
    //     }).then(function successCallback(response) {
    //         $scope.unamestatus = response.data;
    //     });
    // };
    //
    // $scope.addClass = function (unamestatus) {
    //     if (unamestatus == 'available') {
    //         return 'response exists';
    //     } else if (unamestatus == 'not available') {
    //         return 'response not-exists';
    //     } else {
    //         return 'hide';
    //     }
    // }

    function createUserInfo(userInfo) {
        UserInfoService.createUserInfo(userInfo)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'əlavə edildi', show: true};
                }, function (reason) {
                    $scope.alert = {type: 'error', msg: 'xəta baş verdi', show: true};
                    console.log('error while create user', reason);
                }
            );
    }


    $scope.buttonRules = UserInfoService.getButtonRuleCombo()
        .then(
            function (value) {
                $scope.buttonRules = value;
                console.log('button rule combo ', $scope.buttonRules);

            }, function (reason) {
                console.log('error while get button rule combo', reason);
            }
        );
    $scope.rules = UserInfoService.getRuleCombo()
        .then(
            function (value) {
                $scope.rules = value;
                console.log('rule combo', $scope.rules);
            }, function (reason) {
                console.log('error while get rule combo', reason)
            }
        );

    function createUserRule(userRule) {
        UserInfoService.createUserRule(userRule)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'elave edildi', show: true}
                }, function (reason) {
                    $scope.alert = {type: 'error', msg: 'xeta bash verdi', show: true}
                    console.log('error while create user rule',reason)
                }
            );

    }

    function createButtonUserRule(userButtonRule){
        UserInfoService.createButtonUserRule(userButtonRule)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'elave edildi', show: true}
                },function (reason) {
                    $scope.alert = {type: 'error', msg: 'xeta bash verdi', show: true}
                    console.log('error while create button buser rule',reason)
                }
            );
    }

    $scope.submit = function () {
        createUserInfo($scope.userInfo);
         createUserRule($scope.userRule.userRules);
        console.log('$scope.userRules',$scope.userRule.userRules);
         createButtonUserRule($scope.userButtonRule.userButtonRules);
        console.log('$scope.userButtonRules',$scope.userButtonRule.userButtonRules);
        $uibModalInstance.close();
    }


    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
///////////////////////////////////////////////////////////////////////////////////////////////
    $scope.buttonRule = {id: null, createByUserId: null, ruleNames: '', createDate: '', endDate: '', active: ''};
    $scope.buttonRules = [$scope.buttonRule];
    $scope.userButtonRule={};
    $scope.userButtonRule.userButtonRules = [];

    $scope.rule = {id: null, createByUserId: null, ruleName: '', createDate: '', endDate: '', active: ''}
    $scope.rules = [$scope.rule];

    $scope.userRule={};
    $scope.userRule.userRules = [];


    app.filter('propsFilter', function () {
        return function (items, props) {
            var out = [];

            if (angular.isArray(items)) {
                var keys = Object.keys(props);

                items.forEach(function (item) {
                    var itemMatches = false;

                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    });


    $scope.disabled = undefined;
    $scope.searchEnabled = undefined;

    $scope.setInputFocus = function () {
        $scope.$broadcast('UiSelectDemo1');
    };

    $scope.enable = function () {
        $scope.disabled = false;
    };

    $scope.disable = function () {
        $scope.disabled = true;
    };

    $scope.enableSearch = function () {
        $scope.searchEnabled = true;
    };

    $scope.disableSearch = function () {
        $scope.searchEnabled = false;
    };

    $scope.clear = function() {
        $scope.person.selected = undefined;
        $scope.address.selected = undefined;
        $scope.country.selected = undefined;
        };

    $scope.someGroupFn = function (item) {

        if (item.name[0] >= 'A' && item.name[0] <= 'M')
            return 'From A - M';

        if (item.name[0] >= 'N' && item.name[0] <= 'Z')
            return 'From N - Z';

    };

    $scope.firstLetterGroupFn = function (item) {
        return item.name[0];
    };

    $scope.reverseOrderFilterFn = function (groups) {
        return groups.reverse();
    };


    $scope.counter = 0;
    $scope.onSelectCallback = function (item, model) {
        $scope.counter++;
        $scope.eventResult = {item: item, model: model};
    };

    $scope.removed = function (item, model) {
        $scope.lastRemoved = {
            item: item,
            model: model
        };
    };

    $scope.tagTransform = function (newTag) {
        var item = {
            name: newTag,
            email: newTag.toLowerCase() + '@email.com',
            age: 'unknown',
            country: 'unknown'
        };

        return item;
    };



    $scope.appendToBodyDemo = {
        remainingToggleTime: 0,
        present: true,
        startToggleTimer: function () {
            var scope = $scope.appendToBodyDemo;
            var promise = $interval(function () {
                if (scope.remainingTime < 1000) {
                    $interval.cancel(promise);
                    scope.present = !scope.present;
                    scope.remainingTime = 0;
                } else {
                    scope.remainingTime -= 1000;
                }
            }, 1000);
            scope.remainingTime = 3000;
        }
    };

    $scope.address = {};
    $scope.refreshAddresses = function(address) {
            var params = {address: address, sensor: false};
            return $http.get(
                'http://maps.googleapis.com/maps/api/geocode/json',
                {params: params}
            ).then(function(response) {
                $scope.addresses = response.data.results;
            });
        };


}]);
app.directive("matchPassword", function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=matchPassword"
        },
        link: function (scope, element, attributes, ngModel) {

            ngModel.$validators.matchPassword = function (modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
});
