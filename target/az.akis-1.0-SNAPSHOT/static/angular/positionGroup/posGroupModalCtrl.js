var hrPositionApp = angular.module('hpgApp');
hrPositionApp.controller("HpgModalController", ['$scope', '$uibModalInstance', 'HpgService', 'editId', function ($scope, $uibModalInstance, HpgService, editId) {
    $scope.hpgList = [];
    $scope.alert = {type: '', msg: '', show: false};
    $scope.closeAlert = function () {
        $scope.alert = null;
    };
    $scope.hpg = {id: null, name: '', createDate: '', endDate: '', active: '', groupCode: '', groupParent: null};


    if (!(editId === null || editId === '' || editId === 0)) {
        HpgService.getHpgById(editId).then(
            function (type) {
                $scope.hpg = type;
            },
            function (errResponse) {
                console.error('Error while fetching hpgType' + errResponse);
            }
        );

    } else {
        $scope.hpg = {id: null, name: '', createDate: '', endDate: '', active: '', groupCode: '', groupParent: null};

    }

    function createHpg(hpg) {
        HpgService.createHpg(hpg)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'Məqalə əlavə edildi', show: true};
                },
                function (errResponse) {
                    $scope.alert = {type: 'danger', msg: 'Xəta baş verdi', show: true},
                        hpg.categoryId = $scope.category.id;
                    console.error('Error while create hpg');
                }
            );
    }

    function updateHpg(hpg) {
        HpgService.updateHpg(hpg)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'Məqalə dəyişdirildi', show: true};
                },
                function (errResponse) {
                    $scope.alert = {type: 'danger', msg: 'Xəta baş verdi', show: true},
                        console.error('Error while update article');
                }
            );
    }

    function deleteHpg(id) {
        HpgService.deleteHpg(id)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'Məqalə silindi', show: true};
                },
                function (errResponse) {
                    $scope.alert = {type: 'danger', msg: 'Xəta baş verdi', show: true},
                        console.error('Error while delete hpg' + errResponse);
                }
            );
    }


    $scope.submit = function () {
        if ($scope.hpg.id === null || $scope.hpg.id === 0) {
            createHpg($scope.hpg);

            $scope.hpg = {};
//                $uibModalInstance.close();
        } else {
            updateHpg($scope.hpg);
            $uibModalInstance.close();
        }
    };

    $scope.ok = function () {
        deleteHpg($scope.hpg.id);
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);


