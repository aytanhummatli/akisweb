'use strict';

angular.module('hpgApp').factory('HpgService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URL = 'http://localhost:8060/';

    var factory = {
        getHpgList: getHpgList,
        createHpg: createHpg,
        updateHpg: updateHpg,
        deleteHpg: deleteHpg,
        getHpgById: getHpgById,
        setEditId: setEditId,
        getEditId: getEditId,
    };

    return factory;

    function getHpgList(pageNumber) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URL + 'getPositionGroupList/' + 15 + '/' + pageNumber)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while fetching hpg' + errResponse);
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function createHpg(hpg) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URL + 'addPositionGroup/', hpg)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while create hpg' + errResponse);
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function updateHpg(hpg) {
        var deferred = $q.defer();
        $http.put(REST_SERVICE_URL + 'updatePositionGroup/', hpg)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while update hpg' + errResponse);
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function deleteHpg(id) {
        var deferred = $q.defer();
        $http.put(REST_SERVICE_URL + 'deletePositionGroup/' + id)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while delete hpg' + errResponse);
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getHpgById(id) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URL + 'getPositionGroupById/' + id)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while get  by id hpg' + errResponse);
                    deferred.reject(errResponse.data);
                }
            );
        return deferred.promise;
    }

    function setEditId(id) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URL + 'setEditId/' + id)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while set id');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getEditId() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URL + 'getEditId/')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while get id hpg');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

}]);