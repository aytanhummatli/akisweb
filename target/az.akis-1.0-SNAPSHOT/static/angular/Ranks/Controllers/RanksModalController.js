var app = angular.module('myApp');

app.controller("RanksModalController", ['$scope', '$uibModalInstance', 'RankService', 'editId', function ($scope, $uibModalInstance, RankService, editId) {

        $scope.ranks = [];
        $scope.alert = {type: '', msg: '', show: false};
        $scope.rankTypes=[];
        $scope.rankType={};
        $scope.closeAlert = function () {

            $scope.alert = null;
        };

        $scope.rank={id:null,name:'',rankType:{},nextRankPeriod:''};

        if (!(editId === null || editId === '' || editId === 0)) {
            console.log(editId);

            $scope.rank = RankService.getRankById(editId).then(

                function (type) {

                    console.log("type   "+type.rankType.Type);

                    $scope.rank = type;

                    $scope.rankType= type.rankType.Type;

                    console.log("type.nameeeee   "+type.name);
                },

                function (errResponse) {

                    console.error('Error while fetching rankType');

                }

            );

        } else {

        $scope.rank={id:null,name:'',rankType:'',nextRankPeriod:''};

    }

    $scope.rankTypes = RankService.getRankTypes()
        .then(
            function (type) {
                $scope.rankTypes = type;
            },
            function (errResponse) {
                console.error('Error while fetching Type');
            }
        );

    function createRank(rank){
        rank.rankType=$scope.rankType;
        console.error('222222  '+rank.name);
        RankService.createRank(rank)
            .then(
                function () {
                    console.error('3333333  '+rank.name);
                    $scope.alert = {type: 'success', msg: 'Rütbə əlavə edildi', show: true};
                },
                function (errResponse) {
                    $scope.alert = {type: 'danger', msg: 'Xəta baş verdi', show: true},
                        console.error('Error while update Rank');
                }
            );
    }

    function updateRank(rank) {
        rank.rankType=$scope.rankType;
     //   Rank=$scope.Rank;
        console.error('$scope.rank.name'+$scope.rank.name);
        console.error('rank.name  '+rank.name);
        RankService.updateRank(rank)
            .then(
                    function () {
                        $scope.alert = {type: 'success', msg: 'Rütbə dəyişdirildi', show: true};
                    },
                    function (errResponse) {
                        $scope.alert = {type: 'danger', msg: 'Xəta baş verdi', show: true},

                        console.error('Error while update Rank');
                }
            );
    }

    $scope.submit = function () {

        console.log('log from submit function id='+$scope.rank.id);

        if ($scope.rank.id === null || $scope.rank.id === 0  || editId===0) {

            console.log('Saving New Rank' + $scope.rank);

            createRank($scope.rank);
            console.log("$scope.rank  "+$scope.rank.name);
              RankService.fetchAllRanks();
            $scope.rank = {};

          //  $uibModalInstance.close();
        } else {

            updateRank($scope.rank);
            console.log('Rank updated  '+$scope.rank.name);
            RankService.fetchAllRanks();
            $uibModalInstance.close();

            console.log('Rank updated with id ');
        };


    };

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };



}]);


