angular.module('nomApp').controller('NomModalController', ['$scope', '$uibModalInstance', 'NomService', 'editId',function ($scope, $uibModalInstance, NomService, editId) {
    $scope.nom = {id: null, name: '',str:''};
    $scope.nomCombo = [$scope.nom];
    $scope.alert = {type: '', msg: '', show: false};
    $scope.closeAlert = function () {
        $scope.alert = null;
    };
    //$scope.nameById = {id:null,name: '',str:''}
    $scope.updateNom = updateNom;
    $scope.deleteNom = deleteNom;
    $scope.createNom=createNom;

    if (!(editId === 0 || editId === null || editId === '')) {
        NomService.getNomNameById(editId).then(
            function (value) {
                $scope.nom = value;
                console.log('nameby id ', $scope.nom.name+'     '+$scope.nom.id+'    '+$scope.nom.str);
            },
            function (reason) {
                console.log('error while get name by id', reason);
            }
        );
    } else {
        $scope.nom = {id: null, name: '',str:''};
    }


    $scope.nomCombo = NomService.getComboList().then(
        function (value) {
            $scope.nomCombo = value;
            console.log('combolist', $scope.nomCombo);
        }, function (reason) {
            console.log('error while getcombo', reason)
        }
    );

    function deleteNom(id) {
        NomService.deleteNom(id)
            .then(
                function () {
                    $scope.alert = {type: 'succes', msg: 'silindi', show: true}
                },
                function (reason) {
                    $scope.alert = {type: 'error', msg: 'silinmede xeta', show: true}
                    console.log('xeta bash verdi', reason)
                }
            );
    };

    function createNom(nom) {
        console.log('modal ctrl create nom',$scope.nom);
        NomService.createNom(nom)
            .then(
                function () {
                    console.log('modal ctrl then funct create nom ',$scope.nom);
                    $scope.alert = {type: 'success', msg: 'elave edildi', show: true}
                },
                function (error) {
                    $scope.alert = {type: 'error', msg: 'xeta bash verdi', show: true}
                    console.log('error while create  nom', error);
                }
            );
    }

    function updateNom(nom) {
        NomService.updateNom(nom)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'deyishdirildi', show: true}
                },
                function (err) {
                    $scope.alert = {type: 'error', msg: 'xeta bash verdi', show: true}
                    console.log('error while update nom', err);
                }
            );
    }

    $scope.submit=function(){
        console.log('submit button');
        if($scope.nom.id===0 || $scope.nom.id===null){
            $scope.nom.id=$scope.depNomId;
            createNom($scope.nom);
            $uibModalInstance.close();
        }else {
            updateNom($scope.nom);
            console.log('updated nom  id :' ,$scope.nom.id);
            $uibModalInstance.close();
        }
    };


    $scope.ok = function () {
        deleteNom($scope.nom.id);
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


}]);