angular.module('nomApp').controller('NomController', ['$scope', '$uibModal', '$location', 'NomService', function ($scope, $uibModal, $location, NomService) {

    $scope.pageNumber = 1;
    $scope.itemsPerPage = 25;
    $scope.total_count = 0;

    $scope.nomNameById = {id: null, name: '',nomDependency:{}};
    $scope.nomDependency={id:null,nomID:null,depNomId:null}
    $scope.nomListById = [self.nomNameById];


    $scope.nom = {id: null, name: '', str: '', depNomId: null};
    $scope.nomList = [self.nom];

    $scope.getList = getList;
    $scope.addChild = addChild;
    $scope.addNom = addNom;
    $scope.editNom = editNom;
    $scope.removeNom = removeNom;
    $scope.getChildList=getChildList;


    getList($scope.pageNumber);

    function getList(pageNumber) {
        NomService.getList(pageNumber)
            .then(
                function (data) {
                    $scope.nomList = data;
                    console.log('list: ', $scope.nomList);
                    $scope.total_count = 125;
                    console.log('----------------------')
                },
                function (err) {
                    console.log('error while get nomen list' + err);
                }
            );
    }


    $scope.selected = function getNomListById(id) {
        NomService.getNomListById(id)
            .then(
                function (data) {
                    console.log('ctrl function')
                    $scope.nomListById = data;
                    $scope.nomNameById.name = $scope.nomListById;

                    console.log('nom list by id', data)
                },
                function (reason) {
                    console.log('error while get list to textarea' + reason);
                }
            );
    }

    function  getChildList() {
        $scope.selected();
    }

    function addNom() {
        var modalInstance = $uibModal.open({
            templateUrl: 'nomAddUpdateModal.html',
            controller: 'NomModalController',
            backdrop: 'static',
            resolve: {
                editId: function () {
                    return null;
                }
            }
        });
        modalInstance.closed.then(function () {
            getList($scope.pageNumber);
        });
        modalInstance.result.catch(function (reason) {
            if (!(reason === 'cancel' || reason === 'escape press key'))
                getList($scope.pageNumber);
            throw  reason;
        });
    }

    function editNom(id) {
        var modalInstance = $uibModal.open({
            templateUrl: 'nomAddUpdateModal.html',
            controller: 'NomModalController',
            resolve: {
                editId: function () {
                    return id;
                }
            },
            backdrop: 'static'
        });
        modalInstance.closed.then(function () {
            getList($scope.pageNumber);
        });
        modalInstance.result.catch(function (reason) {
            if (!(reason === 'cancel' || reason === 'escape press key'))
                getList($scope.pageNumber);
            throw  reason;
        });
    }

    function removeNom(id) {
        var modalInstance = $uibModal.open({
            templateUrl: 'nomRemoveModal.html',
            controller: 'NomModalController',
            resolve: {
                editId: function () {
                    return id;
                }
            },
            backdrop: 'static'
        });
        modalInstance.closed.then(function () {
            getList($scope.pageNumber);
        });
        modalInstance.result.catch(function (reason) {
            if (!(reason === 'cancel' || reason === 'escape press key'))
                getList($scope.pageNumber);
            throw reason;
        })
    };

    function addChild(id) {
        var modalInstance = $uibModal.open({
            templateUrl: 'nomAddChildModal.html',
            controller: 'NomAddModalController',
            backdrop: 'static',
            resolve: {
                addId: function () {
                    return id;
                }
            }
        });
        modalInstance.closed.then(function () {
            getList($scope.pageNumber);
        });
        modalInstance.result.catch(function (reason) {
            if (!(reason === 'cancel' || reason === 'escape press key'))
                getList($scope.pageNumber);
            throw reason;

        });
    }

    $scope.removeChild = function (id) {
        console.log('ID: ',id);
        var message = "Seçilmiş elementi silmək istəyirsniz?";

        // var modalHtml = '<div class="modal-body">' + message + '</div>';
        // modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
            templateUrl: 'nomRemoveModal.html',
            controller: RemoveChildModalController
        });

        modalInstance.result.then(function () {

            NomService.deleteChildNom(id)
                .then(
                    function () {
                    },
                    function (errResponse) {
                        console.error('Error while delete element' + errResponse);
                    }
                );
        });

        modalInstance.closed.then(function () {
            getChildList();
            getList($scope.pageNumber);
        })
    };
    var RemoveChildModalController = function ($scope, $uibModalInstance) {
        $scope.ok = function () {
            $uibModalInstance.close();
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    };
}]);