package az.akis.util;

import java.security.MessageDigest;

public class EncDecMd5 {
    private static MessageDigest md5;

    public static String cryptMd5(String pass) {
        try {
            md5 = MessageDigest.getInstance("MD5");
            byte[] passBytes = pass.getBytes();
            md5.reset();
            byte[] digest = md5.digest(passBytes);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < digest.length; i++) {
                sb.append(Integer.toHexString(0xff & digest[i]));
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
