package az.akis.dao.EmployeeInjured;

import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Locale;

@Repository
public class EmployeeInjuredDaoImpl implements EmployeeInjuredDao {

    @Autowired
    DataSource dataSource;

    @Override
    public void deleteInjured (Long elementId){

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall mySimpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)

                    .withSchemaName("COMMON_KADR").withCatalogName("EMPLOYEE_INJURED").withProcedureName("deleteInjure");

            mySimpleJdbcCall.setAccessCallParameterMetaData(false);

            mySimpleJdbcCall.addDeclaredParameter(new SqlParameter("elementId", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("elementId",elementId);

            mySimpleJdbcCall.execute(parameterSource);

        } catch (Exception e) {

            e.printStackTrace();
        }

    }
}