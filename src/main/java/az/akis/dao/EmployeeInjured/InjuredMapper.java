package az.akis.dao.EmployeeInjured;

import az.akis.models.EmployeeInjured;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InjuredMapper implements RowMapper<EmployeeInjured> {

    @Override
    public EmployeeInjured mapRow(ResultSet resultSet, int i) throws SQLException {

       EmployeeInjured injuredList=new EmployeeInjured();

       injuredList.setId(resultSet.getLong("id"));
       injuredList.setCreateDate(resultSet.getDate("CREATE_DATE"));

       injuredList.getPrizePhunishReason().setName(resultSet.getString("name"));

        System.out.println("injuredmapper");

        return injuredList;
    }
}
