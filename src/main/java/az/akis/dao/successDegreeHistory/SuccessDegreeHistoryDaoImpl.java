package az.akis.dao.successDegreeHistory;

import az.akis.models.SuccessDegree;
import az.akis.models.SuccessDegreeHistory;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service
public class SuccessDegreeHistoryDaoImpl implements SuccessdegreeHistoryDao {
    @Autowired
    private DataSource dataSource;

    @Override
    public List<SuccessDegree> getSuccessdegreeList() {

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<SuccessDegree> successDegreeList = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("COMMON_KADR").withCatalogName("COM_SUCCESS_DEGREE_HISTORY").withProcedureName("getSuccesDegrees");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("successDegreeList", OracleTypes.CURSOR, new BeanPropertyRowMapper(SuccessDegree.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            successDegreeList = (List<SuccessDegree>) map.get("successDegreeList");

        } catch (Exception e) {

            e.printStackTrace();

        }

        return successDegreeList;
    }

    @Override
    public void createSuccessdegreeHist(SuccessDegreeHistory successDegreeHistory) {

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        System.out.println("success degree hist from insert method"+successDegreeHistory);
        try {
            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("COMMON_KADR").withCatalogName("COM_SUCCESS_DEGREE_HISTORY").withProcedureName("insertSuccesDegree");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("succesDegreeId", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("orderNo", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("empId", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("createDate", OracleTypes.DATE));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("succesDegreeId", successDegreeHistory.getSuccessDegree().getId());
            parameterSource.addValue("orderNo", successDegreeHistory.getOrderNo());
            parameterSource.addValue("empId", successDegreeHistory.getEmpId());
            parameterSource.addValue("createDate", successDegreeHistory.getCreateDate());

            simpleJdbcCall.execute(parameterSource);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void deleteSuccessdegreeHist(Long Id) {

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {
            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("COMMON_KADR").withCatalogName("COM_SUCCESS_DEGREE_HISTORY").withProcedureName("deleteSuccesDegree");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("elementId", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("elementId", Id);


            simpleJdbcCall.execute(parameterSource);


        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void updateSuccessdegreeHist(SuccessDegreeHistory successDegreeHistory) {

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        System.out.println("succes degreeee  "+successDegreeHistory);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("COMMON_KADR").withCatalogName("COM_SUCCESS_DEGREE_HISTORY").withProcedureName("updateSuccesDegree");

            simpleJdbcCall.setAccessCallParameterMetaData(false);
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("elementId", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("succesDegreeId", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("orderNo", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("createDate", OracleTypes.DATE));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("elementId", successDegreeHistory.getId());
            parameterSource.addValue("succesDegreeId", successDegreeHistory.getSuccessDegree().getId());
            parameterSource.addValue("orderNo", successDegreeHistory.getOrderNo());
            parameterSource.addValue("createDate", successDegreeHistory.getCreateDate());
            simpleJdbcCall.execute(parameterSource);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}