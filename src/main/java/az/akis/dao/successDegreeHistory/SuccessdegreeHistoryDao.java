package az.akis.dao.successDegreeHistory;

import az.akis.models.SuccessDegree;
import az.akis.models.SuccessDegreeHistory;

import java.util.List;

public interface SuccessdegreeHistoryDao {

    public List<SuccessDegree> getSuccessdegreeList();

    void createSuccessdegreeHist(SuccessDegreeHistory successDegreeHistory);

    public  void deleteSuccessdegreeHist(Long Id);

    void updateSuccessdegreeHist(SuccessDegreeHistory successDegreeHistory);
}
