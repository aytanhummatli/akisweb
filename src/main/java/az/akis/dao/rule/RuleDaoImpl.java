package az.akis.dao.rule;

import az.akis.models.*;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class RuleDaoImpl implements RuleDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<ButtonRule> getButtonRuleList() {
        List<ButtonRule> getList = null;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("humres_kadr")
                    .withCatalogName("pckg_user_info")
                    .withProcedureName("get_button_rules_combo");
            jdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new ButtonRuleMapper()));
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            Map<String, Object> out = jdbcCall.execute(parameterSource);
            getList = (List<ButtonRule>) out.get("cur");

//            for (UserInfo list:getList
//                 ) {
//                System.out.println("button rule list "+list);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getList;
    }

    @Override
    public List<Rule> getRuleCombo() {
        List<Rule> ruleCombo = null;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("humres_kadr")
                    .withCatalogName("pckg_user_info")
                    .withProcedureName("get_rules_combo");
            jdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new RuleMapper()));
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            Map<String, Object> out = jdbcCall.execute(parameterSource);
            ruleCombo = (List<Rule>) out.get("cur");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ruleCombo;
    }

    @Override
    public List<UserRule> getUserRuleListById(BigDecimal userId) {
        List<UserRule> getList = null;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("humres_kadr")
                    .withCatalogName("pckg_user_info")
                    .withProcedureName("get_user_rules_by_id");
            jdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR,new RuleMapper()));
            jdbcCall.addDeclaredParameter(new SqlParameter("us_id", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("us_id", userId);
            Map<String, Object> out = jdbcCall.execute(parameterSource);
            getList = (List<UserRule>) out.get("cur");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getList;
    }

    @Override
    public List<UserButtonRule> getUserButtonRuleListById(BigDecimal userId) {
        List<UserButtonRule> getList = null;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("humres_kadr")
                    .withCatalogName("pckg_user_info")
                    .withProcedureName("get_user_button_rules_by_id");
            jdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR,new ButtonRuleMapper()));
            jdbcCall.addDeclaredParameter(new SqlParameter("us_id", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("us_id", userId);
            Map<String, Object> out = jdbcCall.execute(parameterSource);
            getList = (List<UserButtonRule>) out.get("cur");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getList;
    }

    @Override
    public void createUserRule(ArrayList<Rule> ruleList){
        try {
            for (Rule rule : ruleList) {
                SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                        .withSchemaName("humres_kadr")
                        .withCatalogName("pckg_user_info")
                        .withProcedureName("create_user_rule");
                jdbcCall.addDeclaredParameter(new SqlParameter("u_rule_id", OracleTypes.NVARCHAR));
                MapSqlParameterSource parameterSource = new MapSqlParameterSource();

                parameterSource.addValue("u_rule_id", rule.getId());
                System.out.println("button rule id list" + rule.getId());

                jdbcCall.execute(parameterSource);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void createUserButtonRule(ArrayList<ButtonRule> buttonRuleList){
try {
    for (ButtonRule buttonRule : buttonRuleList) {
    SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
            .withSchemaName("humres_kadr")
            .withCatalogName("pckg_user_info")
            .withProcedureName("create_user_button_rule");
    jdbcCall.addDeclaredParameter(new SqlParameter("u_button_rule_id", OracleTypes.NVARCHAR));
    MapSqlParameterSource parameterSource = new MapSqlParameterSource();

        parameterSource.addValue("u_button_rule_id", buttonRule.getId());
        System.out.println("button rule id list" + buttonRule.getId());
        jdbcCall.execute(parameterSource);
    }

}catch (Exception e){
    e.printStackTrace();
}
    }

    @Override
    public void updateUserRule(ArrayList<Rule> userRuleList,UserInfo info) {

        try{
            for (Rule userRule : userRuleList) {
                SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                        .withSchemaName("humres_kadr")
                        .withCatalogName("pckg_user_info")
                        .withProcedureName("update_user_rule");
                jdbcCall.addDeclaredParameter(new SqlParameter("u_rule_id", OracleTypes.NUMBER));
                jdbcCall.addDeclaredParameter(new SqlParameter("u_user_id", OracleTypes.NUMBER));
                MapSqlParameterSource parameterSource = new MapSqlParameterSource();

                parameterSource.addValue("u_rule_id", userRule.getId());
                parameterSource.addValue("u_user_id",info.getUser().getId());
                System.out.println("update rule id list" + userRule.getId());
                System.out.println("u_user_id" + info.getUser().getId() );
                jdbcCall.execute(parameterSource);

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void updateUserButtonRule(ArrayList<ButtonRule> userButtonRuleList,UserInfo info) {
        try{
            for (ButtonRule userButtonRule : userButtonRuleList) {
                SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                        .withSchemaName("humres_kadr")
                        .withCatalogName("pckg_user_info")
                        .withProcedureName("update_user_button_rule");
                jdbcCall.addDeclaredParameter(new SqlParameter("u_button_rule_id", OracleTypes.NUMBER));
                jdbcCall.addDeclaredParameter(new SqlParameter("u_button_user_id", OracleTypes.NUMBER));
                MapSqlParameterSource parameterSource = new MapSqlParameterSource();

                parameterSource.addValue("u_button_rule_id", userButtonRule.getId());
                parameterSource.addValue("u_button_user_id",info.getUser().getId());
                System.out.println("update button rule id list" + userButtonRule.getId());
                System.out.println("u_button_user_id" +  info.getUser().getId());
                jdbcCall.execute(parameterSource);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
