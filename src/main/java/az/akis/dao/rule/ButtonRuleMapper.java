package az.akis.dao.rule;

import az.akis.models.ButtonRule;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ButtonRuleMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        ButtonRule buttonRule=new ButtonRule();
        buttonRule.setId(rs.getBigDecimal("br_id"));
        buttonRule.setRuleNames(rs.getString("ruleNames"));
        return buttonRule;
    }
}
