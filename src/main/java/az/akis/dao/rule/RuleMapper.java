package az.akis.dao.rule;

import az.akis.models.Rule;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RuleMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        Rule ruleMapper=new Rule();
        ruleMapper.setId(rs.getBigDecimal(1));
        ruleMapper.setRuleName(rs.getString(2));
        return ruleMapper;
    }
}
