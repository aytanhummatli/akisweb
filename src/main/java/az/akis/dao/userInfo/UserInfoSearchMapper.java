package az.akis.dao.userInfo;
import az.akis.models.UserInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserInfoSearchMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserInfo userInfoMapper=new UserInfo();
        userInfoMapper.getEmployee().setId(rs.getLong("empId"));
        userInfoMapper.getPerson().setFullName(rs.getString("fullName"));
        return userInfoMapper;
    }
}
