package az.akis.dao.userInfo;

import az.akis.models.UserInfo;
import az.akis.util.EncDecMd5;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Repository
public class UserInfoDaoImpl implements UserInfoDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<UserInfo> getUserInfoList(int itemsPerPage, int pageNumber) {
        List<UserInfo> getList = null;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_USER_INFO")
                    .withProcedureName("get_user_info_list");
            jdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new UserInfoMapper()));
            jdbcCall.addDeclaredParameter(new SqlParameter("u_start", OracleTypes.NUMBER));
            jdbcCall.addDeclaredParameter(new SqlParameter("u_end", OracleTypes.NUMBER));
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("u_start", ((pageNumber - 1) * itemsPerPage + 1));
            parameterSource.addValue("u_end", ((pageNumber - 1) * itemsPerPage + itemsPerPage));
            Map<String, Object> out = jdbcCall.execute(parameterSource);
            getList = (List<UserInfo>) out.get("cur");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return getList;
    }

    @Override
    public List<UserInfo> getUserFullNameCombo(String fullName) {
        List<UserInfo> getCombo = null;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_USER_INFO")
                    .withProcedureName("search_user_fullname_combo");
            jdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new UserInfoSearchMapper()));
            jdbcCall.addDeclaredParameter(new SqlParameter("search_fullname", OracleTypes.NVARCHAR));
            //jdbcCall.addDeclaredParameter(new SqlParameter("search_nom_name",OracleTypes.NVARCHAR));
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("search_fullname", fullName);
            //parameterSource.addValue("search_nom_name",nomName);

            Map<String, Object> out = jdbcCall.execute(parameterSource);
            getCombo = (List<UserInfo>) out.get("cur");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getCombo;
    }

    @Override
    public List<UserInfo> getNomNameCombo(String nomName) {
        List<UserInfo> getCombo = null;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_USER_INFO")
                    .withProcedureName("search_nom_name_combo");
            jdbcCall.addDeclaredParameter(new SqlOutParameter("cursor", OracleTypes.CURSOR, new NomNameSearchMapper()));
            jdbcCall.addDeclaredParameter(new SqlParameter("search_nom_name", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("search_nom_name", nomName);
            Map<String, Object> out = jdbcCall.execute(parameterSource);
            getCombo = (List<UserInfo>) out.get("cursor");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getCombo;
    }

    @Override
    public void createUserInfo(UserInfo userInfo) {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_USER_INFO")
                    .withProcedureName("create_user");
            jdbcCall.addDeclaredParameter(new SqlParameter("u_username", OracleTypes.NVARCHAR));
            jdbcCall.addDeclaredParameter(new SqlParameter("per_fullname", OracleTypes.NVARCHAR));
            jdbcCall.addDeclaredParameter(new SqlParameter("nom_name", OracleTypes.NVARCHAR));
            jdbcCall.addDeclaredParameter(new SqlParameter("p_password", OracleTypes.NVARCHAR));
            jdbcCall.addDeclaredParameter(new SqlParameter("p_new_password", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("u_username", userInfo.getUser().getUserName());
            parameterSource.addValue("per_fullname", userInfo.getPerson().getFullName());
            parameterSource.addValue("nom_name", userInfo.getNomenclature().getName());
            parameterSource.addValue("p_password", userInfo.getPassword().getPassword());

            String crypt = EncDecMd5.cryptMd5(userInfo.getPassword().getPassword());
            parameterSource.addValue("p_new_password", crypt);

//            System.out.println("user info object  " + userInfo.toString());
            System.out.println("u_username  " + userInfo.getUser().getUserName().toString());
            System.out.println("per_fullname  " + userInfo.getPerson().getFullName().toString());
            System.out.println("nom_name  " + userInfo.getNomenclature().getName().toString());
            System.out.println("p_password  " + userInfo.getPassword().getPassword().toString());
            System.out.println("p_new_password  " + crypt);
            jdbcCall.execute(parameterSource);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error while create user");
        }
    }

    @Override
    public UserInfo getUserNameById(BigDecimal id) {
        UserInfo userInfo = null;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("humres_kadr")
                    .withCatalogName("pckg_user_info")
                    .withProcedureName("get_user_info_by_id");
            jdbcCall.addDeclaredParameter(new SqlInOutParameter("u_id", OracleTypes.NUMBER));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("u_user_name", OracleTypes.NVARCHAR));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("n_nom_name", OracleTypes.NVARCHAR));
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("u_id", id);
            Map<String, Object> out = jdbcCall.execute(parameterSource);
            userInfo = new UserInfo();
            userInfo.getUser().setId((BigDecimal) out.get("u_id"));
            userInfo.getUser().setUserName((String) out.get("u_user_name"));
            userInfo.getNomenclature().setName((String) out.get("n_nom_name"));

            System.out.println("param id " + id);
            System.out.println("username and user id  " + userInfo.toString());


        } catch (Exception e) {

        }
        return userInfo;
    }

    @Override
    public UserInfo updateUser(UserInfo userInfo) {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("humres_kadr")
                    .withCatalogName("pckg_user_info")
                    .withProcedureName("update_user");
            jdbcCall.addDeclaredParameter(new SqlParameter("u_user_name", OracleTypes.NVARCHAR));
            jdbcCall.addDeclaredParameter(new SqlParameter("u_id", OracleTypes.NUMBER));
            jdbcCall.addDeclaredParameter(new SqlParameter("u_nom_name", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("u_user_name", userInfo.getUser().getUserName());
            parameterSource.addValue("u_id", userInfo.getUser().getId());
            parameterSource.addValue("u_nom_name", userInfo.getNomenclature().getName());
//Map<String,Object> out=jdbcCall.execute(parameterSource);
//userInfo.getUser().setId((BigDecimal) out.get("u_id"));


        } catch (Exception e) {
            e.printStackTrace();
        }
        return userInfo;
    }

    @Override
    public void deleteUser(long id) {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("humres_kadr")
                    .withCatalogName("pckg_user_info")
                    .withProcedureName("delete_user");
            jdbcCall.addDeclaredParameter(new SqlParameter("u_id", OracleTypes.NUMBER));
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("u_id", id);

            jdbcCall.execute(parameterSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public String checkUsername(String username) {
int checkCount;
        String checkText="";
        try{
            SimpleJdbcCall jdbcCall=new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("humres_kadr")
                    .withCatalogName("pckg_user_info")
                    .withProcedureName("check_username");
            jdbcCall.addDeclaredParameter(new SqlOutParameter("check_count",OracleTypes.NUMBER));
            jdbcCall.addDeclaredParameter(new SqlParameter("u_username",OracleTypes.NVARCHAR));
            MapSqlParameterSource parameterSource=new MapSqlParameterSource();
            parameterSource.addValue("u_username",username);
            Map<String,Object> out=jdbcCall.execute(parameterSource);


             checkCount= Integer.parseInt(out.get("check_count").toString());

            System.out.println(checkText);
            if(checkCount==0){
                checkText="available";
                System.out.println(checkText);
            }else if(checkCount > 0){
                checkText="not available";
                System.out.println(checkText);
            }
            System.out.println("check username    "+username+"    check count  "+checkCount+"   check text    " +checkText);
        }catch (Exception e){
            e.printStackTrace();
        }
        return checkText;
    }

    @Override
    public UserInfo getNomNameByUserId(BigDecimal userId) {
        UserInfo userInfo=null;
        try{
            SimpleJdbcCall jdbcCall=new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("humres_kadr")
                    .withCatalogName("pckg_user_info")
                    .withProcedureName("get_nom_name_by_user_id");
            jdbcCall.addDeclaredParameter(new SqlOutParameter("nom_name",OracleTypes.NVARCHAR));
            jdbcCall.addDeclaredParameter(new SqlParameter("u_user_id",OracleTypes.NUMBER));
            MapSqlParameterSource parameterSource=new MapSqlParameterSource();
            parameterSource.addValue("u_user_id",userId);

            Map<String,Object> out=jdbcCall.execute(parameterSource);

            userInfo=new UserInfo();
            userInfo.getNomenclature().setName((String) out.get("nom_name"));
            //userInfo.getUser().setId((BigDecimal) out.get("u_user_id"));
        }catch (Exception e){
            e.printStackTrace();
        }
        return userInfo;
    }


}
