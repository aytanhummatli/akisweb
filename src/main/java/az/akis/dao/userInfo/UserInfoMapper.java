package az.akis.dao.userInfo;

import az.akis.models.UserInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserInfoMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserInfo userInfoMapper = new UserInfo();
        userInfoMapper.getUser().setId(rs.getBigDecimal("c1"));
        userInfoMapper.getUser().setUserName(rs.getString("c2"));
        userInfoMapper.getUser().setActive(rs.getString("c3"));
        userInfoMapper.getUser().setIdent(rs.getLong("c4"));
        userInfoMapper.getUser().setDoupdate(rs.getString("c5"));
        userInfoMapper.getUser().setNomenklaturaId(rs.getLong("c6"));
        userInfoMapper.getEmployee().setId(rs.getLong("c7"));
        userInfoMapper.getPerson().setName(rs.getString("c8"));
        userInfoMapper.getPerson().setSurName(rs.getString("c9"));
        userInfoMapper.getPerson().setPatronymic(rs.getString("c10"));
        userInfoMapper.getCard().setId(rs.getLong("c11"));
        userInfoMapper.getCard().setCardData(rs.getString("c12"));
        userInfoMapper.getImgHistory().setImgURL(rs.getString("c13"));
        userInfoMapper.getImgHistory().setFtpFolder(rs.getString("c14"));
        userInfoMapper.getNomenclature().setName(rs.getString("c15"));
        //userInfoMapper.getPerson().setFullName(rs.getString("fullname"));
        return userInfoMapper;
    }
}
