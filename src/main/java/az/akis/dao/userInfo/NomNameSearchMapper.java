package az.akis.dao.userInfo;

import az.akis.models.UserInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NomNameSearchMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserInfo userInfo=new UserInfo();
        userInfo.getNomenclature().setName(rs.getString("nom_name"));
        userInfo.getNomenclature().setId(rs.getBigDecimal("nom_id"));
        return userInfo;
    }
}
