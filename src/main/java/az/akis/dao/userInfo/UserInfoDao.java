package az.akis.dao.userInfo;
import az.akis.models.UserInfo;

import java.math.BigDecimal;
import java.util.List;

public interface UserInfoDao {
    List<UserInfo> getUserInfoList(int itemsPerPage, int pageNumber);

    List<UserInfo> getUserFullNameCombo(String fullName);

    List<UserInfo> getNomNameCombo(String nomName);

    void createUserInfo(UserInfo userInfo);

    UserInfo getUserNameById(BigDecimal id);

    UserInfo updateUser(UserInfo userInfo);

    void deleteUser(long id);



    String checkUsername(String username);

    UserInfo getNomNameByUserId(BigDecimal userId);
}
