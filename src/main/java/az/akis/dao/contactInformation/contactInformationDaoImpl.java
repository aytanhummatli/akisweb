package az.akis.dao.contactInformation;

import az.akis.models.ContactInformation;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Repository
public class contactInformationDaoImpl implements ContactInformationDao {

    @Autowired
    private DataSource dataSource;


    @Override
    public void addContactInfo(Long personId,ContactInformation contactInformation) {

        Locale.setDefault(Locale.ENGLISH);
      try{
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")

          .withCatalogName("COM_CONTACT").withProcedureName("addContact");

        simpleJdbcCall.setAccessCallParameterMetaData(false);

        simpleJdbcCall.addDeclaredParameter(new SqlParameter("note", OracleTypes.NVARCHAR));

        simpleJdbcCall.addDeclaredParameter(new SqlParameter("name", OracleTypes.NVARCHAR));

        simpleJdbcCall.addDeclaredParameter(new SqlParameter("personId", OracleTypes.NUMBER));

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();

        parameterSource.addValue("note", contactInformation.getValue());

        parameterSource.addValue("name", contactInformation.getName());

        parameterSource.addValue("personId", personId);

        Map map = simpleJdbcCall.execute(parameterSource);


       }   catch (Exception e) {

        e.printStackTrace();
    }
    }

    @Override
    public List<ContactInformation> getAllContacts( ) {

            Locale.setDefault(Locale.ENGLISH);

            List<ContactInformation> contactList = new ArrayList<ContactInformation>();

            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

            try {
                SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")
                        .withCatalogName("COM_CONTACT").withProcedureName("getAllContacts");
                simpleJdbcCall.setAccessCallParameterMetaData(false);

                simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("contactList", OracleTypes.CURSOR, new BeanPropertyRowMapper(ContactInformation.class)));

                MapSqlParameterSource parameterSource = new MapSqlParameterSource();

                Map map = simpleJdbcCall.execute(parameterSource);

                contactList = (List) map.get("contactList");

            } catch (Exception e) {

                e.printStackTrace();
            }

        return contactList;
    }
}
