package az.akis.dao.contactInformation;

import az.akis.models.ContactInformation;

import java.util.List;

public interface ContactInformationDao {

    public void addContactInfo(Long personId,ContactInformation contactInformation);

    public List<ContactInformation> getAllContacts();

}
