package az.akis.dao.EmployeeVacation;

import az.akis.models.EmployeeVacation;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeVacationMappper implements RowMapper<EmployeeVacation> {


    @Override
    public EmployeeVacation mapRow(ResultSet rs, int i) throws SQLException {

        EmployeeVacation employeeVacation = new EmployeeVacation();

        employeeVacation.setId(rs.getLong("id"));
        employeeVacation.setCreateDate(rs.getDate("create_date"));
        employeeVacation.setEndDate(rs.getDate("end_date"));
        employeeVacation.getVacationType().setDescription(rs.getString("description"));
        employeeVacation.setVacationNote(rs.getString("vocation_note"));
        employeeVacation.setVacationCardNo(rs.getString("vacation_card_no"));
employeeVacation.setDayCount(rs.getLong("day_count"));
        return employeeVacation;
    }

}
