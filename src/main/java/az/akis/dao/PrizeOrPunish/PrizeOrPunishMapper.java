package az.akis.dao.PrizeOrPunish;

import az.akis.models.PrizeOrPunish;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PrizeOrPunishMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {

        PrizeOrPunish prizeOrPunish = new PrizeOrPunish();

        prizeOrPunish.setId(resultSet.getLong("id"));

        prizeOrPunish.getPrizeType().setName(resultSet.getString("Prize_Punish_name"));

        prizeOrPunish.getOrderOrgan().setName(resultSet.getString("order_organ_name"));

        prizeOrPunish.getReason().setName(resultSet.getString("sebeb"));

        prizeOrPunish.setOrderNo(resultSet.getString("order_no"));

        prizeOrPunish.setGetDate(resultSet.getDate("get_date"));

        prizeOrPunish.getPrizeType().getCombo().setDescription(resultSet.getString("description"));

        prizeOrPunish.getOrderOrgan().setFullName(resultSet.getString("order_organ"));

        prizeOrPunish.getPunishEndReason().setName(resultSet.getString("punish_end_reason"));

        prizeOrPunish.setPunishEndDate(resultSet.getDate("punish_end_date"));

        prizeOrPunish.setPunishEndOrder(resultSet.getString("PUNISH_END_ORDER"));

        return prizeOrPunish;
    }
}
