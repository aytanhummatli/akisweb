package az.akis.dao.Rank;

import az.akis.models.Rank;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RankMapper implements RowMapper<Rank> {

    @Override
    public Rank mapRow(ResultSet rs, int i) throws SQLException {

        Rank rankList = new Rank();

        rankList.setId(rs.getLong("ID"));

    //  System.out.println("rs.getString(\"TYPE\")"+ rs.getString("TYPE"));

        rankList.getRankType().setType(rs.getString("TYPE"));
        rankList.setNextRankPeriod(rs.getLong("NEXT_RANK_PERIOD"));
        rankList.setOrderP(rs.getLong("ORDER_P"));
        rankList.setName(rs.getString("NAME"));
        rankList.setEndDate(rs.getDate("END_DATE"));
        rankList.setCreateDate(rs.getDate("CREATE_DATE"));

        return rankList;
    }


}

