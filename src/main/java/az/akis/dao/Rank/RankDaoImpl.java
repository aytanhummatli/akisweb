package az.akis.dao.Rank;

import az.akis.models.Rank;
import az.akis.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class RankDaoImpl  implements RankDao{

    @Autowired
    private DataSource dataSource;

    private static final AtomicLong counter = new AtomicLong();

    List<Rank>rankList=null;



    public List<Rank> findAllRanks() {

        List<Rank> rankList = null;

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {
            String sql = "select r.ID ,r.NAME,r.RANK_TYPE_ID,r.CREATE_DATE,r.END_DATE, r.SUFFIX,r.ORDER_P,rt.TYPE ,r.NEXT_RANK_PERIOD  from HUMRES_KADR.HR_RANK r INNER JOIN\n" +
                    "HUMRES_KADR.HR_RANK_TYPE  rt on r.RANK_TYPE_ID = rt.ID  where r.ACTIVE=1 and r.NAME is not null  \n" +
                    "ORDER BY  r.ID DESC ";

            rankList = jdbcTemplate.query(sql, new RankMapper());

        }catch (Exception e){

            e.printStackTrace();
        }
        return rankList;
    }
    public List<Rank> findAllRanksByPage(int pageno, int itemsperpage) {

    //    System.out.println("i am here");

        List<Rank> rankList = null;

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            String sql = "SELECT * FROM (select r.ID ,r.NAME,r.RANK_TYPE_ID,r.CREATE_DATE,r.END_DATE, r.SUFFIX,r.ORDER_P,rt.TYPE ,r.NEXT_RANK_PERIOD, ROW_NUMBER () OVER (ORDER BY r.ID DESC) rn\n" +
                    "\n" +
                    "               from HUMRES_KADR.HR_RANK r INNER JOIN\n" +
                    "\n" +
                    "                 HUMRES_KADR.HR_RANK_TYPE  rt on r.RANK_TYPE_ID = rt.ID  where r.ACTIVE=1 ORDER BY  r.ID DESC )\n" +
                    "\n" +
                    "              pro_ls WHERE pro_ls.rn>="+(pageno-1)*itemsperpage+ "  and pro_ls.rn<="+((pageno-1)*itemsperpage)+itemsperpage;


            rankList = jdbcTemplate.query(sql, new RankMapper());

           System.out.println("ranklarin siyahisi :   " + rankList.toString());


        }catch (Exception e){

            e.printStackTrace();
        }
        return rankList;
    }

      public Rank findById(int id) {

        System.out.println("from rankdaoimpl id="+id);
          Locale.setDefault(Locale.ENGLISH);
          List<Rank> rankList = new ArrayList<Rank>();
          try {
          JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
          String sql = "select r.ID ,r.NAME,r.RANK_TYPE_ID,r.CREATE_DATE,r.END_DATE, r.SUFFIX,r.ORDER_P,rt.TYPE ,r.NEXT_RANK_PERIOD \n" +
                  "\n" +
                  "from HUMRES_KADR.HR_RANK r INNER JOIN\n" +
                  "\n" +
                  "  HUMRES_KADR.HR_RANK_TYPE  rt on r.RANK_TYPE_ID = rt.ID\n" +
                  "  \n" +
                  "WHERE  r.ACTIVE=1 and r.ID= " + id;
          rankList = jdbcTemplate.query(sql, new RankMapper());
          System.out.println("from rankdaoimpl ranklist="+rankList.toString());

          for(Rank rank : rankList){

              if(rank.getId() == id){
                  return rank;

              }
          }

          }catch (Exception e){

              e.printStackTrace();
          }
          return null;
      }

    public Rank findByName(String name) {

        Locale.setDefault(Locale.ENGLISH);
        List<Rank> rankList = new ArrayList<Rank>();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "select * from HUMRES_KADR.HR_RANK  where ACTIVE=1 and NAME='"+ name+ "'";

       rankList = jdbcTemplate.query(sql,new RankMapper());

        for (Rank user : rankList) {
            if (user.getName().equalsIgnoreCase(name)) {
                return user;
            }
        }
        return null;
    }


    public void saveRank (Rank rank){
        System.out.println("I am in RankDaoImpl from saveRank metod  ");

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
    String active="1";
      try {

          String sql = "INSERT into HUMRES_KADR.HR_RANK(ID,NAME,NEXT_RANK_PERIOD,RANK_TYPE_ID,ACTIVE) VALUES ((select max(ID) from HUMRES_KADR.HR_RANK)+1,?,?,?,?)";

          jdbcTemplate.update(sql, rank.getName(), rank.getNextRankPeriod(), rank.getRankType().getId(),active);

          System.out.println("I am in RankDaoImpl from saveRank metod 2222 ");

     } catch (Exception e)

{

    e.printStackTrace();
}
    }
    public void updateRank (Rank rank){

        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);

        System.out.println("I am in RankDaoImpl from updateRank metod");
        System.out.println("I am in RankDaoImpl from updateRank metod  Rank=" +rank.toString());

        try {
            String sql = "UPDATE HUMRES_KADR.HR_RANK set NAME=?, RANK_TYPE_ID=?, NEXT_RANK_PERIOD=?  where ACTIVE=1 and ID=" + rank.getId();
              System.out.println("Rank.getRankType().getId()   "+rank.getRankType().getId());
            jdbcTemplate.update(sql, rank.getName(), rank.getRankType().getId(),rank.getNextRankPeriod());
        }
        catch(Exception e) {

            e.printStackTrace();
        }
    }

    public void deleteRankById ( int id){

        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);

        try {

            String sql = "UPDATE HUMRES_KADR.HR_RANK set ACTIVE=0  where ID=?";

            jdbcTemplate.update(sql, id);

            System.out.println("i am in rankdaoimpl from delete metod");
        }
        catch (Exception e)
        {

            e.printStackTrace();
        }

    }

    public boolean isRankExist (Rank rank){

        return findByName(rank.getName()) != null;
    }

    public void deleteAllRanks() {

        rankList.clear();
    }

    public Rank testLogin(String username,String hexPassword){
        Rank rank=null;
        Locale.setDefault(Locale.ENGLISH);
        List<Rank> rankList = new ArrayList<Rank>();

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "Select * from  users where status=1";
        rankList = jdbcTemplate.query(sql, new BeanPropertyRowMapper(User.class));

        if (rankList!=null)
        {
            for (Rank rankr : rankList) {
                rank=new Rank();
                return rank=rankr;
            }
        }
        return rank;
    }





}
