package az.akis.dao.Employee;

import az.akis.models.AcademicDegreePosition;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AcademicDegreeMapper implements RowMapper<AcademicDegreePosition> {

    @Override
    public AcademicDegreePosition mapRow(ResultSet rs, int i) throws SQLException {

        AcademicDegreePosition academicDegrees = new AcademicDegreePosition();

        academicDegrees.setGetDate(rs.getDate("degree_get_date"));

        academicDegrees.getAcademicDegree().setDescription(rs.getString("academic_degree"));

        academicDegrees.getAcademicDegree().setId(rs.getLong("academic_degree_id"));

        return academicDegrees;
    }

}

