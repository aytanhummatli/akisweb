package az.akis.dao.Employee;

import az.akis.models.EmployeeRank;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeRankMapper implements RowMapper<EmployeeRank> {

    @Override
    public EmployeeRank mapRow(ResultSet rs, int i) throws SQLException {

        EmployeeRank employeeRankList = new EmployeeRank();
        employeeRankList.setOrderNo(rs.getString("rankOrderNo"));
        employeeRankList.setCreateDate(rs.getDate("createDate"));
        employeeRankList.setRankOutOfTurn(rs.getString("rankOutOfTurn"));
        employeeRankList.getRank().setName(rs.getString("Rank"));
        employeeRankList.getRank().setSuffix(rs.getString("rankSuffix"));
        employeeRankList.getRank().getRankType().setType(rs.getString("rankType"));
        employeeRankList.getCmbsService().setId(rs.getLong("cmbs_service_id"));
        employeeRankList.getCmbsService().setDescription(rs.getString("description"));
        employeeRankList.setId(rs.getLong("id"));

        return employeeRankList;
    }


}

