package az.akis.dao.Employee;


import az.akis.models.EducationHistory;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EducationHistoryMapper implements RowMapper<EducationHistory> {

    @Override
    public EducationHistory mapRow(ResultSet rs, int i) throws SQLException {

        EducationHistory educationHistory = new EducationHistory();
educationHistory.setId(rs.getLong("id"));
        educationHistory.getEducationType().setDescription(rs.getString("education_type"));
        educationHistory.getEducationType().setId(rs.getLong("EDUCATION_TYPE_ID"));
        educationHistory.getEducationLevel().setDescription(rs.getString("EDUCATION_LEVEL"));
        educationHistory.getEducationLevel().setId(rs.getLong("EDUCATION_LEVEL_ID"));
        educationHistory.getUniversityName().setName(rs.getString("university"));
        educationHistory.getProfession().setProfession(rs.getString("profession"));
        educationHistory.getProfession().setId(rs.getLong("profId"));
        educationHistory.setStartYear(rs.getString("start_year"));
        educationHistory.setEndYear(rs.getString("end_year"));
        educationHistory.getCertificateType().setDescription(rs.getString("certificate_type"));
educationHistory.getEyaniQiyabi().setId(rs.getLong("EYANI_QIYABI_ID"));
        educationHistory.getEyaniQiyabi().setDescription(rs.getString("eyani_qiyabi"));
        educationHistory.getCertificateType().setId(rs.getLong("CERTIFICATE_TYPE_ID"));
        educationHistory.setCertificateGetDate(rs.getDate("certificate_get_date"));
        educationHistory.setSertificateNo(rs.getString("sertificate_no"));
        educationHistory.setStatus(rs.getString("davam"));
        return educationHistory;
    }

}

