package az.akis.dao.Employee;


import az.akis.models.EmployeePosition;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeePositionMapper implements RowMapper<EmployeePosition> {

    @Override
    public EmployeePosition mapRow(ResultSet rs, int i) throws SQLException {

        EmployeePosition empPositionList = new EmployeePosition();
        empPositionList.setId(rs.getLong("id"));
        empPositionList.getLabourCode().setDescription(rs.getString("exit_reason"));
        empPositionList.setFullname(rs.getString("full_name"));
        empPositionList.setEnterOrder(rs.getString("enter_order"));
        empPositionList.setExitOrder(rs.getString("exit_order"));
        empPositionList.setWorkEndDate(rs.getDate("work_end_date"));
        empPositionList.setWorkStartDate(rs.getDate("work_start_date"));
        empPositionList.getStructurePosition().getPositionName().setName(rs.getString("position_name"));

        return empPositionList;

    }


}

