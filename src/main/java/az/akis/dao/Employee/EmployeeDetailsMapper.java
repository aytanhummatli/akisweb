package az.akis.dao.Employee;

import az.akis.models.Employee;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeDetailsMapper implements RowMapper<Employee> {

    @Override
    public Employee mapRow(ResultSet rs, int i) throws SQLException {

        Employee empList = new Employee();

        empList.setId(rs.getLong("id"));

        empList.getPerson().setId(rs.getLong("personId"));

        empList.getPerson().setSurName(rs.getString("surname"));

        empList.getPerson().setMaidenName(rs.getString("maiden_name"));

        empList.getPerson().setName(rs.getString("name"));

        empList.getPerson().setPatronymic(rs.getString("patronymic"));

        empList.getNomenclature().setName(rs.getString("nomenklature"));

        empList.setPersonalMatterNo(rs.getString("personalMatterNo"));

        empList.setPoliceCardNo(rs.getString("policeCardNo"));

        empList.setCivil(rs.getString("civil"));

        empList.getNationality().setDescription(rs.getString("nationality"));

        empList.getNationality().setId(rs.getLong("nationalityId"));

        empList.getPerson().getMaleFemale().setDescription(rs.getString("gender"));

        empList.getPerson().getMaleFemale().setId(rs.getLong("genderId"));

        empList.getPerson().getMarriedStatus().setDescription(rs.getString("familyStatus"));

        empList.getPerson().getMarriedStatus().setId(rs.getLong("familyStatusId"));

        empList.getPerson().setBirthDate(rs.getDate("birth_date"));

        empList.getPerson().getBornAddress().setAdress_fullname(rs.getString("bornAdress"));

        empList.getPassport().getAdress().setAdress_fullname(rs.getString("livingAdress"));

        empList.getPassport().setPassportGetDate(rs.getDate("passportGetDate"));

        empList.getPassport().getPoliceDepartment().setName(rs.getString("passportGivenOrgan"));

        empList.getPassport().getPoliceDepartment().setId(rs.getInt("passportGivenOrganId"));

        empList.getPositionName().setName(rs.getString("positionName"));

        empList.getEmployeePosition().setEnterOrder(rs.getString("enterOrder"));

        empList.getEmployeePosition().setWorkEndDate(rs.getDate("workEndDate"));

        empList.getEmployeePosition().setWorkStartDate(rs.getDate("workStartdate"));

        empList.getEmployeePosition().setOrderDate(rs.getDate("orderDate"));

        empList.getEmployeePosition().getStructurePosition().setFullSttrName(rs.getString("cur_work_str_name"));

        empList.getPassport().setPassportSerialNo(rs.getString("passportSerialNo"));

        return empList;

    }


}

