package az.akis.dao.Employee;

import az.akis.dao.AttestationHistory.AttestationHistoryMapper;
import az.akis.dao.EmployeeInjured.InjuredMapper;
import az.akis.dao.EmployeeVacation.EmployeeVacationMappper;
import az.akis.dao.PrizeOrPunish.PrizeOrPunishMapper;
import az.akis.dao.Relative.RelativeMapper;
import az.akis.dao.WorkActivityBefore.WorkActivityBeforeMapper;
import az.akis.dao.WorkMission.WorkMissionMapper;
import az.akis.models.*;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {
    @Autowired
    private DataSource dataSource;


    public List<Employee> get_employees(Long st_id, String ed_surname, String ed_patron, String ed_name, Long pageno, Long itemsPerPage) {
//
  //    System.out.println("i am in get_Employees method from dao impl");
//
        String defString = "666";

        if (ed_surname.equalsIgnoreCase(defString)) {

            ed_surname = null;
        }

        if (ed_patron.equalsIgnoreCase(defString)) {

            ed_patron = null;
        }
        if (ed_name.equalsIgnoreCase(defString)) {
            //   System.out.println("ednaeeeeeeeeee"+ed_name);
            ed_name = null;
        }
        if (st_id == null) {

            st_id = Long.valueOf(1500000001);

            //   System.out.println("stiddd"+st_id);
        }
     //   System.out.println("edname  " + ed_name);

        Locale.setDefault(Locale.ENGLISH);

        List<Employee> newEmpList = new ArrayList<>();

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {
            // System.out.println("111111111111 :");
            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")
                    .withCatalogName("com_employee").withProcedureName("get_employees");
            // System.out.println("22222222222222 :");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("st_id", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("ed_surname", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("ed_patron", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("ed_name", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("pageno", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("itemsPerPage", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new EmployeeMapper()));
            System.out.println("3333333333333 :");
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("st_id", st_id);

            parameterSource.addValue("ed_surname", ed_surname);

            parameterSource.addValue("ed_patron", ed_patron);

            parameterSource.addValue("ed_name", ed_name);

            parameterSource.addValue("pageno", pageno);

            parameterSource.addValue("itemsPerPage", itemsPerPage);

         //   System.out.println("44444444444444444 : " + parameterSource.getValues().toString());

            Map map = simpleJdbcCall.execute(parameterSource);

            // System.out.println("55555555555555555555 :");

            newEmpList = (List) map.get("cur");

       //     System.out.println("newEmpList :" + newEmpList);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return newEmpList;
    }

    public Employee getEmployeeById(Long empId) {

        System.out.println("i am in daoImpl from getEmployeebyId method");

        Locale.setDefault(Locale.ENGLISH);

        List<Employee> employeeDetailsList = new ArrayList<>();

        List<ContactInformation> contactList = new ArrayList<>();

        Employee employeeDetails = null;

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")
                    .withCatalogName("com_employee").withProcedureName("get_employeeDetails");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("empId", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("employeeDetails", OracleTypes.CURSOR, new EmployeeDetailsMapper()));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("contactList", OracleTypes.CURSOR, new BeanPropertyRowMapper(ContactInformation.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("empId", empId);

            Map map = simpleJdbcCall.execute(parameterSource);

            employeeDetailsList = (List) map.get("employeeDetails");

            contactList = (List) map.get("contactList");

          //  System.out.println("contactList" + contactList.toString());

            for (Employee obj : employeeDetailsList) {

                if (obj.getId().equals(empId)) {

                    obj.getPerson().setContactInformation(contactList);

                    employeeDetails = obj;
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        return employeeDetails;
    }

    public Employee getGeneralIngormationById(Long empId) {


        System.out.println("i am in daoImpl from getGeneralIngormationById method");

        Locale.setDefault(Locale.ENGLISH);
        List<EmployeeMilitaryService> employeeMilitaryList = new ArrayList<>();

        Employee employeeGeneral = new Employee();
        EmployeeMilitaryService employeeMilitary = new EmployeeMilitaryService();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")
                    .withCatalogName("com_employee").withProcedureName("get_general_information");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("empId", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("rankDetails", OracleTypes.CURSOR, new EmployeeRankMapper()));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("serviceDegrees", OracleTypes.CURSOR, new EmployeeSuccessHistoryMapper()));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("militaryService", OracleTypes.CURSOR, new EmployeeMilitaryServiceMapper()));


            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("empId", empId);

            Map map = simpleJdbcCall.execute(parameterSource);

            employeeMilitaryList = (List) map.get("militaryService");

            for (EmployeeMilitaryService emp : employeeMilitaryList) {

                employeeMilitary = emp;

            }

            employeeGeneral.setEmployeeMilitaryService(employeeMilitary);

            employeeGeneral.setSuccessDegreeHistory((List) map.get("serviceDegrees"));

            employeeGeneral.setEmployeeRank((List) map.get("rankDetails"));
        } catch (Exception e) {

            e.printStackTrace();
        }

        return employeeGeneral;


    }

    public Employee getEducationLevel(Long empId) {

        Locale.setDefault(Locale.ENGLISH);

        List<AcademicDegreePosition> academicDegreeList = new ArrayList<>();

        Employee employeeGeneral = new Employee();


        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")
                    .withCatalogName("com_employee").withProcedureName("get_education_level");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("empId", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("langLevel", OracleTypes.CURSOR, new EmployeeLanguageMapper()));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("academicDegree", OracleTypes.CURSOR, new AcademicDegreeMapper()));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("educationHistory", OracleTypes.CURSOR, new EducationHistoryMapper()));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("attestationResult", OracleTypes.CURSOR, new AttestationHistoryMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("empId", empId);

            Map map = simpleJdbcCall.execute(parameterSource);

            academicDegreeList = (List) map.get("academicDegree");

            for (AcademicDegreePosition elem : academicDegreeList)

            {
                System.out.println("elem=" + elem);

                employeeGeneral.setAcademicDegreePosition(elem);

            }
            employeeGeneral.setEmployeeLanguageLevel((List) map.get("langLevel"));

            employeeGeneral.setEducationHistory((List) map.get("educationHistory"));

            employeeGeneral.setAttestationHistory((List) map.get("attestationResult"));

        } catch (Exception e) {

            e.printStackTrace();
        }

        return employeeGeneral;


    }

    public Employee getEmployeeFamily(Long empId) {

        Locale.setDefault(Locale.ENGLISH);


        Employee employeeGeneral = new Employee();

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")
                    .withCatalogName("com_employee").withProcedureName("get_employee_Family");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("empId", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("familyList", OracleTypes.CURSOR, new RelativeMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("empId", empId);

            Map map = simpleJdbcCall.execute(parameterSource);

            employeeGeneral.setRelatives((List) map.get("familyList"));


        } catch (Exception e) {

            e.printStackTrace();
        }

        return employeeGeneral;


    }

    public List<EmployeePosition> getEmployeeDIObyId(Long empId) {

        Locale.setDefault(Locale.ENGLISH);

        List<EmployeePosition> employeeDIOList = new ArrayList<EmployeePosition>();

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {
            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")
                    .withCatalogName("com_employee").withProcedureName("get_employee_DIO");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("empId", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("DIOList", OracleTypes.CURSOR, new EmployeePositionMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("empId", empId);

            Map map = simpleJdbcCall.execute(parameterSource);

            employeeDIOList = (List) map.get("DIOList");

        } catch (Exception e) {

            e.printStackTrace();
        }

        return employeeDIOList;
    }


    public Employee getEmployeeBeforeDIOandEzam(Long empId) {

        Locale.setDefault(Locale.ENGLISH);

        Employee employee = new Employee();

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")
                    .withCatalogName("com_employee").withProcedureName("get_employee_beforeDio_Ezam");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("empId", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("ezamList", OracleTypes.CURSOR, new WorkMissionMapper()));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("beforeDioList", OracleTypes.CURSOR, new WorkActivityBeforeMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("empId", empId);

            Map map = simpleJdbcCall.execute(parameterSource);

            employee.setWorkActivityBefore((List) map.get("beforeDioList"));

            employee.setWorkMissions((List) map.get("ezamList"));


        } catch (Exception e) {

            e.printStackTrace();
        }

        return employee;
    }

    public Employee getEmployeeMezYaralanma(Long empId, Long year) {

        Locale.setDefault(Locale.ENGLISH);

        int activeStatus = 1;

        Employee employee = new Employee();

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")

                    .withCatalogName("com_employee").withProcedureName("get_vacation_and_persent");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("empId", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("year", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("activeStatus", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("vacationList", OracleTypes.CURSOR, new EmployeeVacationMappper()));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("vacationAnaliqList", OracleTypes.CURSOR, new EmployeeVacationMappper()));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("injuredList", OracleTypes.CURSOR, new InjuredMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("empId", empId);

            parameterSource.addValue("year", year);

            parameterSource.addValue("activeStatus", activeStatus);

            Map map = simpleJdbcCall.execute(parameterSource);

             System.out.println("sdfdfdfdf"+parameterSource.getValues());

            employee.setEmployeeVacations((List) map.get("vacationList"));


            System.out.println("vacationList "+employee.getEmployeeVacations().toString());

            employee.setEmployeeVacationMotherhood((List) map.get("vacationAnaliqList"));

            employee.setInjuredList((List) map.get("injuredList"));

            //  System.out.println("inj list" + employee.getInjuredList());

        } catch (Exception e) {

            e.printStackTrace();
        }

        return employee;
    }


    public Employee getPrizeAndPunish(Long empId ) {

        Locale.setDefault(Locale.ENGLISH);

        Employee employee = new Employee();

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")

                    .withCatalogName("com_employee").withProcedureName("get_prize_punish");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("empId", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("medalList", OracleTypes.CURSOR, new PrizeOrPunishMapper()));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("prizeList", OracleTypes.CURSOR, new PrizeOrPunishMapper()));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("DIOPrizeList", OracleTypes.CURSOR, new PrizeOrPunishMapper()));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("punishList", OracleTypes.CURSOR, new PrizeOrPunishMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("empId", empId);

            Map map = simpleJdbcCall.execute(parameterSource);

            employee.setPrize((List) map.get("prizeList"));

            employee.setMedalList((List) map.get("medalList"));

            employee.setDioPrizeList((List) map.get("DIOPrizeList"));

            employee.setPunishlist((List) map.get("punishList"));

//          Employee.setEmployeeVacationMotherhood((List) map.get("vacationAnaliqList"));
//
//          Employee.setInjuredList((List) map.get("injuredList"));
//
//          System.out.println("prizeList" + employee.getInjuredList());

        } catch (Exception e) {

            e.printStackTrace();
        }
        return employee;
    }

    public Employee getOthers(Long empId ) {

        Locale.setDefault(Locale.ENGLISH);

        Employee employee = new Employee();

        List<Employee> employeeList=new ArrayList<Employee>();

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")

                    .withCatalogName("com_employee").withProcedureName("get_others");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("empId", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("infoList", OracleTypes.CURSOR, new EmployeeInfoMapper() ));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("courceList", OracleTypes.CURSOR,new  BeanPropertyRowMapper(QualificationCourse.class)));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("warList", OracleTypes.CURSOR,new BeanPropertyRowMapper(WarActivity.class) ));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("empId", empId);

            Map map = simpleJdbcCall.execute(parameterSource);

            employeeList=(List) map.get("infoList");

           for (Employee elem:employeeList){

                    employee=elem;
            }

            employee.setWarActivities((List) map.get("warList"));

            employee.setQualificationCourses((List) map.get("courceList"));

       //     System.out.println("warlist"+ (List) map.get("warList"));

        } catch (Exception e) {

            e.printStackTrace();
        }

        return employee;
    }

    public Employee getEmployeeInfoPrint(Long EmpId) {
        List<Employee> employeeList = new ArrayList<Employee>();
        List<Employee> employeeinfoList = new ArrayList<Employee>();
        List<EmployeeMilitaryService> employeeMilitaryList = new ArrayList<EmployeeMilitaryService>();
        EmployeeMilitaryService employeeMilitary=new EmployeeMilitaryService();
        List<ContactInformation> contactList = new ArrayList<>();
        Employee employee = new Employee();
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource)
                    .withSchemaName("COMMON_KADR")
                    .withCatalogName("com_employee")
                    .withProcedureName("get_employee_info_print");
            jdbcCall.addDeclaredParameter(new SqlParameter("empId", OracleTypes.NUMBER));

            jdbcCall.addDeclaredParameter(new SqlOutParameter("educationHistory", OracleTypes.CURSOR, new EducationHistoryMapper()));

            jdbcCall.addDeclaredParameter(new SqlOutParameter("prizeList", OracleTypes.CURSOR, new PrizeOrPunishMapper()));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("DIOPrizeList", OracleTypes.CURSOR, new PrizeOrPunishMapper()));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("punishList", OracleTypes.CURSOR, new PrizeOrPunishMapper()));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("DIOList", OracleTypes.CURSOR,new EmployeePositionMapper()));

            jdbcCall.addDeclaredParameter(new SqlOutParameter("familyList", OracleTypes.CURSOR, new RelativeMapper()));

            jdbcCall.addDeclaredParameter(new SqlOutParameter("infoList", OracleTypes.CURSOR, new EmployeeInfoMapper()));

            jdbcCall.addDeclaredParameter(new SqlOutParameter("employeeDetails", OracleTypes.CURSOR, new EmployeeDetailsMapper()));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("contactList", OracleTypes.CURSOR,new BeanPropertyRowMapper<ContactInformation>(ContactInformation.class)));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("rankDetails", OracleTypes.CURSOR, new EmployeeRankMapper()));

            jdbcCall.addDeclaredParameter(new SqlOutParameter("serviceDegrees", OracleTypes.CURSOR, new EmployeeSuccessHistoryMapper()));

            jdbcCall.addDeclaredParameter(new SqlOutParameter("militaryService", OracleTypes.CURSOR, new EmployeeMilitaryServiceMapper()));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("langLevel", OracleTypes.CURSOR, new EmployeeLanguageMapper()));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("ezamList", OracleTypes.CURSOR, new WorkMissionMapper()));

            jdbcCall.addDeclaredParameter(new SqlOutParameter("beforeDioList", OracleTypes.CURSOR, new WorkActivityBeforeMapper()));
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("empId", EmpId);
            Map<String, Object> out = jdbcCall.execute(parameterSource);


            employeeList = (List<Employee>) out.get("employeeDetails");

            for (Employee obj : employeeList) {

                if (obj.getId().equals(EmpId)) {

                    employee = obj;
                }

            }

            employeeinfoList = (List<Employee>) out.get("infoList");

            for (Employee element : employeeinfoList) {

                employee.setPoliceCardNew(element.getPoliceCardNew());

            }
            employeeMilitaryList = (List) out.get("militaryService");

            for (EmployeeMilitaryService emp : employeeMilitaryList) {

                employeeMilitary = emp;

            }
            contactList = (List) out.get("contactList");

            System.out.println("contactList" + contactList.toString());

            for (Employee obj : employeeList) {

                if (obj.getId().equals(EmpId)) {

                    obj.getPerson().setContactInformation(contactList);

                    employee = obj;
                }
            }

            employee.setEmployeeMilitaryService(employeeMilitary);
            employee.setEducationHistory((List<EducationHistory>) out.get("educationHistory"));
            employee.setPrize((List<PrizeOrPunish>) out.get("prizeList"));
            employee.setPunishlist((List<PrizeOrPunish>) out.get("punishList"));
            employee.setRelatives((List<Relative>) out.get("familyList"));
            employee.setDioPrizeList((List<PrizeOrPunish>) out.get("DIOPrizeList"));
            employee.setSuccessDegreeHistory((List<SuccessDegreeHistory>) out.get("serviceDegrees"));
            employee.setEmployeeRank((List<EmployeeRank>) out.get("rankDetails"));
            employee.setDioList((List<EmployeePosition>) out.get("DIOList"));
            employee.setEmployeeLanguageLevel((List) out.get("langLevel"));
            employee.setWorkActivityBefore((List) out.get("beforeDioList"));

            employee.setWorkMissions((List) out.get("ezamList"));

            System.out.println("my employee list   "+employee.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return employee;
    }
}