package az.akis.dao.Employee;

import az.akis.models.EmployeeMilitaryService;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeMilitaryServiceMapper implements RowMapper<EmployeeMilitaryService> {

    @Override
    public EmployeeMilitaryService mapRow(ResultSet rs, int i) throws SQLException {

        EmployeeMilitaryService militaryService = new EmployeeMilitaryService();


        militaryService.setId(rs.getLong("id"));
        militaryService.setArmyPort(rs.getString("army_port"));
        militaryService.setBeginDate(rs.getDate("military_service_begin_date"));
        militaryService.setEndDate(rs.getDate("military_service_end_date"));
        militaryService.getMilitaryDuty().setDescription(rs.getString("military_duty"));
        militaryService.getMilitaryDuty().setId(rs.getLong("military_dutyId"));
        militaryService.setSerialNo(rs.getString("military_service_serialno"));
        militaryService.setIsRegistered(rs.getString("military_service_IS_REGISTERED"));


        return militaryService;
    }


}
