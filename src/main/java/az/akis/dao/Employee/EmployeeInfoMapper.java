package az.akis.dao.Employee;

import az.akis.models.Employee;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeInfoMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet rs, int i) throws SQLException {

        Employee employeeInfo = new Employee();

        employeeInfo.getLastFormFillDate().setFillDate(rs.getDate("form_fill_date"));
        employeeInfo.getEmployeeWorkBook().getNote1().setDescription(rs.getString("note1"));
        employeeInfo.getEmployeeWorkBook().getNote1().setId(rs.getLong("note1Id"));

        employeeInfo.getEmployeeWorkBook().getNote2().setDescription(rs.getString("note2"));

        employeeInfo.getEmployeeWorkBook().getNote2().setId(rs.getLong("note2Id"));

        employeeInfo.getEmployeeWorkBook().setChecked(rs.getString("checked"));
        employeeInfo.getEmployeeWorkBook().setEnvelopNumber(rs.getString("envelop_number"));
        employeeInfo.getEmployeeWorkBook().setSendDate(rs.getDate("send_date"));
        employeeInfo.getEmployeeWorkBook().setIsReceipt(rs.getLong("is_receipt"));

        employeeInfo.getPoliceCardNew().setHeight(rs.getString("height"));
        employeeInfo.getPoliceCardNew().setBlood(rs.getString("blood"));
        employeeInfo.getPoliceCardNew().setEyeColor(rs.getString("EYE_COLOR"));
        employeeInfo.getPoliceCardNew().setWeight(rs.getString("weight"));
        employeeInfo.getPoliceCardNew().setGivenDate(rs.getDate("given_date"));
        employeeInfo.getPoliceCardNew().setValidity(rs.getDate("validity"));
        employeeInfo.getPoliceCardNew().setArmSerialNo(rs.getString("arm_serial_no"));
        employeeInfo.getPoliceCardNew().setArmpermission(rs.getString("arm_permission"));

        return employeeInfo;
    }

}
