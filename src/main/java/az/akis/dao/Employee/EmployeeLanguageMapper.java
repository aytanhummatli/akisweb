package az.akis.dao.Employee;

import  az.akis.models.EmployeeLanguageLevel;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;

import java.sql.SQLException;

public class EmployeeLanguageMapper implements RowMapper<EmployeeLanguageLevel> {

    @Override
    public EmployeeLanguageLevel mapRow(ResultSet rs, int i) throws SQLException {

        EmployeeLanguageLevel languageDegrees = new EmployeeLanguageLevel();


        languageDegrees.setId(rs.getLong("lng_lev_id"));
        languageDegrees.getLanguage().setDescription(rs.getString("language"));
        languageDegrees.getLanguage().setId(rs.getLong("lng_id"));
        languageDegrees.getLanguageLevel().setDescription(rs.getString("lng_level"));
        languageDegrees.getLanguageLevel().setId(rs.getLong("level_id"));
        return languageDegrees;
    }

}

