package az.akis.dao.Employee;

import az.akis.models.SuccessDegreeHistory;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeSuccessHistoryMapper implements RowMapper<SuccessDegreeHistory> {

    @Override
    public SuccessDegreeHistory mapRow(ResultSet rs, int i) throws SQLException {

        SuccessDegreeHistory serviceDegrees = new SuccessDegreeHistory();
        serviceDegrees.setOrderNo(rs.getString("success_order_no"));
        serviceDegrees.setCreateDate(rs.getDate("success_create_date"));
        serviceDegrees.getSuccessDegree().setName(rs.getString("succes_degree"));
           serviceDegrees.setEmpId(rs.getLong("emp_id"));
        serviceDegrees.setId(rs.getLong("id"));

        return serviceDegrees;
    }


}

