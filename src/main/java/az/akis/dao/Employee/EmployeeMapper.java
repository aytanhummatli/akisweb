package az.akis.dao.Employee;

import az.akis.models.Employee;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeMapper implements RowMapper<Employee> {

    @Override
    public Employee mapRow(ResultSet rs, int i) throws SQLException {

        Employee empList = new Employee();

        empList.setId(rs.getLong("empId"));
        empList.setPoliceCardNo(rs.getString("POLICE_CARD_NO"));
        empList.getPerson().setName(rs.getString("NAME"));
        empList.getPerson().setSurName(rs.getString("SURNAME"));
        empList.getPerson().setPatronymic(rs.getString("PATRONYMIC"));
        empList.getRank().setName(rs.getString("rankName"));
        empList.setPersonalMatterNo(rs.getString("PERSONAL_MATTER_NO"));
        empList.getPerson().setBirthDate(rs.getDate("birth_date"));
        empList.getTree().setId(rs.getLong("stId"));
        empList.getTree().setTitle(rs.getString("title"));
        empList.getTree().setText(rs.getString("text"));
        empList.getImage().setImgURL(rs.getString("imgUrl"));
        empList.getImage().setFtpFolder(rs.getString("ftp_folder"));
        empList.getPositionName().setName(rs.getString("cur_work_pos_name"));
        empList.setFullStrname(rs.getString("full_strname"));
  //    System.out.println("mmmmmmmmmmmmmmm :");
        return empList;

    }


}

