package az.akis.dao.nomenclature;

import az.akis.models.Nomenclature;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NomNameByIdMapper implements RowMapper<Nomenclature> {

    @Override
    public Nomenclature mapRow(ResultSet rs, int rowNum) throws SQLException {
        Nomenclature nomMapper=new Nomenclature();
        nomMapper.setId(rs.getBigDecimal("id_nom"));
        nomMapper.setName(rs.getString("name_nom"));
        nomMapper.setStr(rs.getString("str_nom"));
        nomMapper.getNomDependency().setId(rs.getLong("id_dep"));
        return nomMapper;
    }
}
