package az.akis.dao.nomenclature;

import az.akis.models.Nomenclature;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class NomenclatureDaoImpl implements NomenclatureDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Nomenclature> getSearchResults(String elementName) {

        List<Nomenclature> nomenclatureList = new ArrayList<>();

        String defString = "666";

        if (elementName.equalsIgnoreCase(defString)) {

            elementName = null;
        }

        try {

            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)

                    .withSchemaName("HUMRES_KADR")

                    .withCatalogName("PCKG_NOMENCLATURE")

                    .withProcedureName("getSearchResults");

            jdbcCall.addDeclaredParameter(new SqlParameter("elementName", OracleTypes.NVARCHAR));

            jdbcCall.addDeclaredParameter(new SqlOutParameter("nomenList", OracleTypes.CURSOR, new NomenclatureMapper()));

            MapSqlParameterSource source = new MapSqlParameterSource();

            source.addValue("elementName", elementName);

            Map<String, Object> result = jdbcCall.execute(source);

            nomenclatureList = (List<Nomenclature>) result.get("nomenList");

        } catch (Exception e) {

            System.err.println("xeta bash verdi");

            e.printStackTrace();
        }
        return nomenclatureList;
    }

    @Override
    public List<Nomenclature> getNomenkForSelect() {

        List<Nomenclature> nomenclatureList = new ArrayList<>();

        try {

            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_NOMENCLATURE")
                    .withProcedureName("getNomenkForSelect");

            jdbcCall.addDeclaredParameter(new SqlOutParameter("nomenList", OracleTypes.CURSOR, new NomenclatureMapper()));

            MapSqlParameterSource source = new MapSqlParameterSource();

            Map<String, Object> result = jdbcCall.execute(source);

            nomenclatureList = (List<Nomenclature>) result.get("nomenList");

        } catch (Exception e) {

            System.err.println("xeta bash verdi");

            e.printStackTrace();
        }

        return nomenclatureList;

    }

    @Override
    public List<Nomenclature> getNomenclatureList(int rowsPerPage, int pageNumber) {
        List<Nomenclature> nomenclatureList = null;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_NOMENCLATURE")
                    .withProcedureName("get_nomenclatureList");
            jdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new NomenclatureMapper()));
            jdbcCall.addDeclaredParameter(new SqlParameter("n_start", OracleTypes.NUMBER));
            jdbcCall.addDeclaredParameter(new SqlParameter("n_end", OracleTypes.NUMBER));

            MapSqlParameterSource source = new MapSqlParameterSource();
            source.addValue("n_start", ((pageNumber - 1) * rowsPerPage + 1));
            source.addValue("n_end", ((pageNumber - 1) * rowsPerPage + rowsPerPage));
            Map<String, Object> result = jdbcCall.execute(source);
            nomenclatureList = (List<Nomenclature>) result.get("cur");

        } catch (Exception e) {
            System.err.println("xeta nash verdi");
            e.printStackTrace();
        }
        return nomenclatureList;
    }

    @Override
    public List<Nomenclature> getNomListById(Long id) {
        List<Nomenclature> list = null;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_NOMENCLATURE")
                    .withProcedureName("get_nomListById");
            jdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new NomNameByIdMapper()));
            jdbcCall.addDeclaredParameter(new SqlParameter("n_id", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("n_id", id);

            Map<String, Object> out = jdbcCall.execute(parameterSource);
            list = (List<Nomenclature>) out.get("cur");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<Nomenclature> getComboList() {
        List<Nomenclature> comboList = null;
        try {
            SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_NOMENCLATURE")
                    .withProcedureName("get_comboList");

            call.addDeclaredParameter(new SqlOutParameter("cursor", OracleTypes.CURSOR, new NomenclatureMapper()));
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            Map<String, Object> out = call.execute(parameterSource);
            comboList = (List<Nomenclature>) out.get("cursor");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return comboList;
    }

    @Override
    public Nomenclature getNomNameById(BigDecimal id) {
        Nomenclature nomenclature = null;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_NOMENCLATURE")
                    .withProcedureName("get_nomNameById");

            jdbcCall.addDeclaredParameter(new SqlInOutParameter("n_id", OracleTypes.NUMBER));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("n_name", OracleTypes.VARCHAR));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("n_str", OracleTypes.VARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("n_id", id);
            Map<String, Object> out = jdbcCall.execute(parameterSource);
            nomenclature = new Nomenclature();
            nomenclature.setId((BigDecimal) out.get("n_id"));
            nomenclature.setName((String) out.get("n_name"));
            nomenclature.setStr((String) out.get("n_str"));


        } catch (Exception e) {
            e.printStackTrace();
        }
        return nomenclature;
    }

    @Override
    public void deleteNom(Long id) {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_NOMENCLATURE")
                    .withProcedureName("delete_nom");
            jdbcCall.addDeclaredParameter(new SqlParameter("n_id", OracleTypes.NUMBER));
            MapSqlParameterSource source = new MapSqlParameterSource();
            source.addValue("n_id", id);
            jdbcCall.execute(source);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createNom(String name, String str) {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_NOMENCLATURE")
                    .withProcedureName("create_nom");

            jdbcCall.addDeclaredParameter(new SqlParameter("n_name", OracleTypes.NVARCHAR));
            jdbcCall.addDeclaredParameter(new SqlParameter("n_str", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("n_name", name);
            parameterSource.addValue("n_str", str);
            jdbcCall.execute(parameterSource);
            System.out.println("   name: " + name + "   str: " + str);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createChildNom(Nomenclature nom) {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_NOMENCLATURE")
                    .withProcedureName("create_child_nom");
            jdbcCall.addDeclaredParameter(new SqlParameter("dp_n_id", OracleTypes.NUMBER));
            jdbcCall.addDeclaredParameter(new SqlParameter("n_id", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("dp_n_id", nom.getId());
            parameterSource.addValue("n_id", nom.getDepNomId());


            jdbcCall.execute(parameterSource);

            System.out.println("dp_n_id" + nom.getId().toString() + "n_id" + nom.getDepNomId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateNom(Nomenclature nom) {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_NOMENCLATURE")
                    .withProcedureName("update_nom");
            jdbcCall.addDeclaredParameter(new SqlParameter("n_name", OracleTypes.NVARCHAR));
            jdbcCall.addDeclaredParameter(new SqlParameter("n_str", OracleTypes.NVARCHAR));
            jdbcCall.addDeclaredParameter(new SqlParameter("n_id", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("n_name", nom.getName());
            parameterSource.addValue("n_str", nom.getStr());
            parameterSource.addValue("n_id", nom.getId());
            jdbcCall.execute(parameterSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteChildNom(long id) {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_NOMENCLATURE")
                    .withProcedureName("delete_child_nom");
            jdbcCall.addDeclaredParameter(new SqlParameter("dep_id", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("dep_id", id);

            jdbcCall.execute(parameterSource);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
