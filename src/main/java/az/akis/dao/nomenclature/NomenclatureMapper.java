package az.akis.dao.nomenclature;

import az.akis.models.Nomenclature;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NomenclatureMapper implements RowMapper<Nomenclature> {

    @Override
    public Nomenclature mapRow(ResultSet rs, int rowNum) throws SQLException {
        Nomenclature nomMapper=new Nomenclature();
        nomMapper.setId(rs.getBigDecimal("nom_id"));
        nomMapper.setName(rs.getString("nom_name"));
        nomMapper.setStr(rs.getString("nom_str"));
        return nomMapper;
    }
}
