package az.akis.dao.educationHistory;

import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Locale;

@Repository
public class EducationHistoryDaoImpl implements EducationHistoryDao {

    @Autowired
    DataSource dataSource;

    @Override
    public void deleteEducationHistory(Long elementId) {

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)

                    .withSchemaName("COMMON_KADR").withCatalogName("EducationHistory").withProcedureName("deleteEducationHistory");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("elementId", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("elementId", elementId);

            simpleJdbcCall.execute(parameterSource);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
