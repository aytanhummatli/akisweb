package az.akis.dao.employeeRank;

import az.akis.models.Combo;
import az.akis.models.EmployeeRank;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service
public class EmployeeRankDaoImpl implements EmployeeRankDao {
    @Autowired
    private DataSource dataSource;

    public List<Combo> getRankService (){

        Locale.setDefault(Locale.ENGLISH);
        List<Combo> rankServiceList= new ArrayList<Combo>();

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")

                    .withCatalogName("COM_Employee_Rank").withProcedureName("getRankService");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("rankServiceList", OracleTypes.CURSOR,new BeanPropertyRowMapper(Combo.class)));


            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            rankServiceList= (List) map.get("rankServiceList");

        } catch (Exception e) {

            e.printStackTrace();
        }

        return rankServiceList;
    }

    @Override
    public void createEmployeeRank(EmployeeRank employeeRank) {

        Locale.setDefault(Locale.ENGLISH);

        List<Combo> rankServiceList= new ArrayList<Combo>();

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")

                    .withCatalogName("COM_Employee_Rank").withProcedureName("insertEmployeeRank");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("rankOutOfturn", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("rankId", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("orderNo", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("empId", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("createDate", OracleTypes.DATE));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("cmbsServiceId", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("rankOutOfturn",employeeRank.getRankOutOfTurn());

            parameterSource.addValue("rankId",employeeRank.getRank().getId());

            parameterSource.addValue("orderNo",employeeRank.getOrderNo());

            parameterSource.addValue("empId",employeeRank.getEmpId());

            parameterSource.addValue("createDate",employeeRank.getCreateDate());

            parameterSource.addValue("cmbsServiceId",employeeRank.getCmbsService().getId());

             simpleJdbcCall.execute(parameterSource);

        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    public  void deleteEmployeeRank (Long Id){

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")

                    .withCatalogName("COM_Employee_Rank").withProcedureName("deleteEmployeeRank");

            simpleJdbcCall.setAccessCallParameterMetaData(false);


            simpleJdbcCall.addDeclaredParameter(new SqlParameter("Id", OracleTypes.NUMBER));


            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            System.out.println("id for delete = "+Id);

            parameterSource.addValue("Id",Id);

            simpleJdbcCall.execute(parameterSource);

        } catch (Exception e) {

            e.printStackTrace();
        }


    }
   public void updateRank(EmployeeRank employeeRank){


       Locale.setDefault(Locale.ENGLISH);

       JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

       try {

           SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("COMMON_KADR")

                   .withCatalogName("COM_Employee_Rank").withProcedureName("updateEmployeeRank");

           simpleJdbcCall.setAccessCallParameterMetaData(false);

           simpleJdbcCall.addDeclaredParameter(new SqlParameter("elementId", OracleTypes.NUMBER));

           simpleJdbcCall.addDeclaredParameter(new SqlParameter("rankId", OracleTypes.NUMBER));

           simpleJdbcCall.addDeclaredParameter(new SqlParameter("orderNo", OracleTypes.NVARCHAR));

           simpleJdbcCall.addDeclaredParameter(new SqlParameter("createDate", OracleTypes.DATE));

           simpleJdbcCall.addDeclaredParameter(new SqlParameter("serviceId", OracleTypes.NUMBER));

           simpleJdbcCall.addDeclaredParameter(new SqlParameter("rankOutOfTurn", OracleTypes.NVARCHAR));


           MapSqlParameterSource parameterSource = new MapSqlParameterSource();

           parameterSource.addValue("elementId", employeeRank.getId()  );

           parameterSource.addValue("rankId", employeeRank.getRank().getId()  );

           parameterSource.addValue("orderNo",employeeRank.getOrderNo() );

           parameterSource.addValue("createDate",employeeRank.getCreateDate() );

           parameterSource.addValue("serviceId",employeeRank.getCmbsService().getId() );

           parameterSource.addValue("rankOutOfTurn",employeeRank.getRankOutOfTurn() );

          // System.out.println("date"+employeeRank.getCreateDate());

           System.out.println("employee rank  "+employeeRank.getRank().toString());

           System.out.println("employee service  "+employeeRank.getCmbsService().toString());

           System.out.println("valures   "+parameterSource.getValues().toString());

           simpleJdbcCall.execute(parameterSource);

       } catch (Exception e) {

           e.printStackTrace();
       }

   }

}