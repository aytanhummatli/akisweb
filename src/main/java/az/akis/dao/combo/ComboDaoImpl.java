package az.akis.dao.combo;

import az.akis.models.Combo;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Repository
public class ComboDaoImpl implements ComboDao {

    @Autowired
    DataSource dataSource;

    @Override
    public List<Combo> getInfoForCombo(Long comboNo) {

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Combo> infoList = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)

                    .withSchemaName("COMMON_KADR").withCatalogName("com_employee").withProcedureName("getInfoForCombo");

            simpleJdbcCall.setAccessCallParameterMetaData(false);
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("comboNo", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("infoList", OracleTypes.CURSOR, new BeanPropertyRowMapper(Combo.class)));



            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("comboNo", comboNo);

            Map map = simpleJdbcCall.execute(parameterSource);

            infoList = (List<Combo>) map.get("infoList");

        }

        catch (Exception e) {

            e.printStackTrace();
        }

        return infoList;
    }
}
