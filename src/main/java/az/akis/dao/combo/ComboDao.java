package az.akis.dao.combo;

import az.akis.models.Combo;

import java.util.List;

public interface ComboDao {


    public List<Combo> getInfoForCombo(Long comboNo);

}
