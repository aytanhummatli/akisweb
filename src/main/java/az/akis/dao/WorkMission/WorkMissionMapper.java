package az.akis.dao.WorkMission;


import az.akis.models.WorkMission;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WorkMissionMapper implements RowMapper<WorkMission> {


    @Override
    public WorkMission mapRow(ResultSet resultSet, int i) throws SQLException {

       WorkMission missionList=new WorkMission();
       missionList.setId(resultSet.getLong("id"));
       missionList.getStructureName().setFullName(resultSet.getString("full_name"));
       missionList.setMissionOrder(resultSet.getString("MISSION_ORDER"));
       missionList.setMissionOrderDate(resultSet.getDate("MISSION_ORDER_DATE"));
       missionList.setEndDate(resultSet.getDate("END_DATE"));
       missionList.setCreateDate(resultSet.getDate("create_date"));

       return missionList;
    }
}
