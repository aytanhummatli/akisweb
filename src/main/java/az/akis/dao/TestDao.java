package az.akis.dao;

import az.akis.models.User;

import java.util.List;

public interface TestDao {

    User findById(int id);

    User findByName(String name);

    void saveUser(User user);

    void updateUser(User user);

    void deleteUserById(int id);

    List<User> findAllUsers();

    void deleteAllUsers();

    public User testLogin(String username,String password);

    public boolean isUserExist(User user);

    public User findByNameAndPass(String name,String password);
}
