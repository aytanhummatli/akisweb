package az.akis.dao.village;

import az.akis.models.Village;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Repository
public class VillageDaoImpl implements VillageDao {

    @Autowired
    DataSource dataSource;

    @Override
    public List<Village> getVillageList() {

        Locale.setDefault(Locale.ENGLISH);

        List<Village>  villageList=new ArrayList<>();

        try {

            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("COMMON_KADR").withCatalogName("LOCATION").withProcedureName("getVillageList");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("villageList", OracleTypes.CURSOR,new BeanPropertyRowMapper(Village.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            villageList = (List<Village>) map.get("villageList");

        } catch (Exception e) {

            e.printStackTrace();
        }

        return villageList;
    }
}
