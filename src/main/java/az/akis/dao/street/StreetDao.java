package az.akis.dao.street;

import az.akis.models.Street;

import java.util.List;

public interface StreetDao {

    public List<Street> getStreetList();

}
