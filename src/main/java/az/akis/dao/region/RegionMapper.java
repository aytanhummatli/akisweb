package az.akis.dao.region;

import az.akis.models.Region;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RegionMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet rs, int i) throws SQLException {

        Region region=new Region();


        region.setId(rs.getLong(""));
        region.setName(rs.getString(""));
        region.getCountry().setId(rs.getLong(""));
        region.getCountry().getName();
        return null;
    }
}
