package az.akis.dao.region;

import az.akis.models.Region;

import java.util.List;

public interface RegionDao {

    public List<Region> regionList();

}
