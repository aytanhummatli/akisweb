package az.akis.dao.region;

import az.akis.models.Region;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Repository
public class RegionDaoImpl implements RegionDao {

    @Autowired
    DataSource dataSource;

    @Override
    public List<Region> regionList() {


        Locale.setDefault(Locale.ENGLISH);

        List<Region> regionList=new ArrayList<>();

        try {

            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("COMMON_KADR").withCatalogName("LOCATION").withProcedureName("getRegionList");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("regionList", OracleTypes.CURSOR,new BeanPropertyRowMapper(Region.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            regionList = (List<Region>) map.get("regionList");

        } catch (Exception e) {

            e.printStackTrace();
        }

        return regionList;
    }
}
