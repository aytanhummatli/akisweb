package az.akis.dao.WorkActivityBefore;

import az.akis.models.WorkActivityBefore;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WorkActivityBeforeMapper implements RowMapper<WorkActivityBefore > {
    @Override
    public WorkActivityBefore mapRow(ResultSet rs, int i) throws SQLException {

        WorkActivityBefore workActivityBefore = new WorkActivityBefore();

        workActivityBefore.setId(rs.getLong("id"));
        workActivityBefore.getLabourCode().setDescription(rs.getString("exit_reason"));
        workActivityBefore.setExitOrder(rs.getString("exit_order"));
        workActivityBefore.setEnterOrder(rs.getString("enter_order"));
        workActivityBefore.getPositionName().setName(rs.getString("position_name"));
        workActivityBefore.getStructureName().setFullName(rs.getString("full_name"));
        workActivityBefore.setExitDate(rs.getDate("exit_date"));
        workActivityBefore.setEnterDate(rs.getDate("enter_date"));
        workActivityBefore.setEnterDateYear(rs.getLong("enter_date_year"));

        workActivityBefore.setExitDateYear(rs.getLong("enter_date_year"));

        return workActivityBefore;
    }
}
