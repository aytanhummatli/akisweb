package az.akis.dao.RankTypes;

import az.akis.models.RankType;

import java.util.List;

public interface RankTypesDao {

    public List<RankType> findAllRankTypes();

}
