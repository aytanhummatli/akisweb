package az.akis.dao.RankTypes;

import az.akis.models.RankType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Locale;


@Repository
public class RankTypesDaoImpl implements RankTypesDao {

    @Autowired
    private DataSource dataSource;


    List<RankType> rankTypes =null;

    public List<RankType> findAllRankTypes() {


        List<RankType> rankTypes = null;

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {
            String sql = "select ID, Type,Active, CREATE_DATE from HUMRES_KADR.HR_RANK_TYPE where ACTIVE=1";

            rankTypes  = jdbcTemplate.query(sql, new BeanPropertyRowMapper(RankType.class));

           // System.out.println("Rank tiplerinin siyahisi :   " + rankTypes.toString());

                }catch (Exception e){

        e.printStackTrace();
        }
        return rankTypes;
        }


        }
