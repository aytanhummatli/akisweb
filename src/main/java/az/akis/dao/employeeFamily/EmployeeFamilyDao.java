package az.akis.dao.employeeFamily;

import az.akis.models.Combo;

import java.util.List;

public interface EmployeeFamilyDao {

    public List<Combo> getRelativeDegrees();

    public List<Combo> getWorkLocations();


}
