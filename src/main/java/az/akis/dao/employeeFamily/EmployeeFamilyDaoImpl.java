package az.akis.dao.employeeFamily;


import az.akis.models.Combo;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Repository
public class EmployeeFamilyDaoImpl implements EmployeeFamilyDao {


    @Autowired
    DataSource dataSource;

    @Override
    public List<Combo> getRelativeDegrees() {

        Locale.setDefault(Locale.ENGLISH);

        List<Combo> degreeList = new ArrayList<>();

        try {

            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("").withCatalogName("").withProcedureName("getRelativeDegrees");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("degreeList", OracleTypes.CURSOR,new BeanPropertyRowMapper(Combo.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            degreeList = (List<Combo>) map.get("degreeList");

        } catch (Exception e) {

            e.printStackTrace();
        }

        return degreeList;
    }

    @Override
    public List<Combo> getWorkLocations() {

        Locale.setDefault(Locale.ENGLISH);

        List<Combo> workLocationList = new ArrayList<>();

        try {

            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("").withCatalogName("").withProcedureName("getWorkLocations");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("workLocationList", OracleTypes.CURSOR, new BeanPropertyRowMapper(Combo.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            workLocationList = (List<Combo>) map.get("getWorkLocations");

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return workLocationList;
    }

}
