package az.akis.dao.country;

import az.akis.models.Country;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
@Service
public class CountryDaoImpl implements CountryDao {

    @Autowired
    DataSource dataSource;

    @Override
    public List<Country> getCountryList() {

        Locale.setDefault(Locale.ENGLISH);

        List<Country> countryList=new ArrayList<>();

        try {

            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("COMMON_KADR").withCatalogName("LOCATION").withProcedureName("getCountryList");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("countryList", OracleTypes.CURSOR,new BeanPropertyRowMapper(Country.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            countryList = (List<Country>) map.get("countryList");

        } catch (Exception e) {

            e.printStackTrace();
        }

        return countryList;
    }
}
