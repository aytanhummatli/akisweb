package az.akis.dao.country;

import az.akis.models.Country;

import java.util.List;

public interface CountryDao {

    public List<Country> getCountryList();

}
