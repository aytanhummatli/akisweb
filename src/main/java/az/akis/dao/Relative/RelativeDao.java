package az.akis.dao.Relative;

import az.akis.models.Combo;

import java.util.List;

public interface RelativeDao {


    public List<Combo> getRelativeDegreeList();

    public List<Combo> getWorkList();

    public  void deleteRelative(Long elementId);

}