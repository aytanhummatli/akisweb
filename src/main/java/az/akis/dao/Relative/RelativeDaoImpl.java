package az.akis.dao.Relative;

import az.akis.models.Combo;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class RelativeDaoImpl implements RelativeDao {

    @Autowired
    DataSource dataSource;

    @Override
    public List<Combo> getRelativeDegreeList() {

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Combo> relativeDegreeList = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)

                    .withSchemaName("COMMON_KADR").withCatalogName("COM_RELATIVE").withProcedureName("relativeDegreeList");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("relativeDegreeList", OracleTypes.CURSOR, new BeanPropertyRowMapper(Combo.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute();

            relativeDegreeList = (List<Combo>) map.get("relativeDegreeList");

        } catch (Exception e) {

            e.printStackTrace();
        }

        return relativeDegreeList;
    }

    @Override
    public List<Combo> getWorkList() {

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Combo> workList = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)

                    .withSchemaName("COMMON_KADR").withCatalogName("COM_RELATIVE").withProcedureName("getWorkList");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("workList", OracleTypes.CURSOR, new BeanPropertyRowMapper(Combo.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute();

            workList = (List<Combo>) map.get("workList");

        } catch (Exception e) {

            e.printStackTrace();
        }
        return workList;
    }

    @Override
    public void deleteRelative(Long elementId) {

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)

                    .withSchemaName("COMMON_KADR").withCatalogName("RELATIVE").withProcedureName("deleteRelative");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("elementId",OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("elementId",elementId);

           simpleJdbcCall.execute(parameterSource);

        } catch (Exception e) {

            e.printStackTrace();
        }

    }

}
