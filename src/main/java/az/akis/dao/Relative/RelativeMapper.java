package az.akis.dao.Relative;

import az.akis.models.Relative;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RelativeMapper implements RowMapper<Relative> {

    @Override
    public Relative mapRow(ResultSet rs, int i) throws SQLException {

        Relative relativeList = new Relative();
        relativeList.setId(rs.getLong("id"));
        relativeList.getPerson().setName(rs.getString("name"));
        relativeList.getPerson().setSurName(rs.getString("surname"));
        relativeList.getPerson().setPatronymic(rs.getString("partonymic"));
        relativeList.getRelativeDegree().setDescription(rs.getString("relative_degree"));
        relativeList.getRelativeDegree().setId(rs.getLong("relative_degree_id"));
        relativeList.setBornYear(rs.getString("born_year"));
        relativeList.setWorkPlace(rs.getString("work_place"));
        relativeList.getPerson().getBornAddress().setAdress_fullname(rs.getString("bornAdress"));
        relativeList.getLivingAddress().setAdress_fullname(rs.getString("liveAdress"));

        return relativeList;
    }


}

