package az.akis.dao.StructureMap;


import az.akis.models.TreeSimple;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StructureTreeMapper implements RowMapper<TreeSimple> {

    @Override
    public TreeSimple mapRow(ResultSet rs, int i) throws SQLException {

        TreeSimple treeList = new TreeSimple();

          treeList.setTitle(rs.getString("title"));
          treeList.setId(rs.getLong("id"));
          treeList.setParentId(rs.getLong("parentId"));
          treeList.setText(rs.getString("text"));
          treeList.setFullName(rs.getString("fullName"));
          treeList.setChildCount(rs.getLong("CHILDCOUNT"));
          treeList.setnLeft(rs.getLong("nLeft"));
          treeList.setnRight(rs.getLong("nRight"));
          treeList.setnLevel(rs.getLong("nLevel"));
          treeList.setSuffix(rs.getString("suffix"));
          treeList.setUpperStr(rs.getLong("upperStr"));
          treeList.setStructureNameId(rs.getLong("structureNameId"));
          treeList.getOrganType().setId(rs.getLong("organId"));
          treeList.getOrganType().setName(rs.getString("organName"));
        return treeList;
    }


}

