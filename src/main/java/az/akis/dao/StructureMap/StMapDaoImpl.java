package az.akis.dao.StructureMap;

import az.akis.models.OrganType;
import az.akis.models.StuffSchedule;
import az.akis.models.TreeSimple;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Repository
public class StMapDaoImpl implements StMapDao {

    private  static int counter =0;

    @Autowired
    private DataSource dataSource;

//----------------------------new codes--------------------------------------------//

    public List<StuffSchedule>  getEmployees(Long nodeId , Long NomenklaturaID, Long STR_NAME_ID ) {

        List<StuffSchedule>myList = new ArrayList<StuffSchedule>();

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("HUMRES_KADR")
                    .withCatalogName("PKG_STRUCTURE_MAP").withProcedureName("getEmployees");
            simpleJdbcCall.setAccessCallParameterMetaData(false);
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("nodeId",OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("NomenklaturaID",OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("STR_NAME_ID",OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new BeanPropertyRowMapper(StuffSchedule.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("nodeId", nodeId);
            parameterSource.addValue("NomenklaturaID", NomenklaturaID);
            parameterSource.addValue("STR_NAME_ID", STR_NAME_ID);

            Map map = simpleJdbcCall.execute(parameterSource);

            myList = (List) map.get("cur");


        }catch (Exception e){

            e.printStackTrace();
        }
        System.out.println("i am in stdao from getEmployees metod"+myList.toString());

        return myList;
    }

    public TreeSimple getElementById(Long id) {

        TreeSimple myElement = new TreeSimple();

        Locale.setDefault(Locale.ENGLISH);

        List<TreeSimple>nullList = new ArrayList<TreeSimple>();

        //Creating null node list model
        TreeSimple nullchild=new TreeSimple();
        nullchild.setId(Long.valueOf(1));
        nullchild.setTitle("Məlumat yüklənir...");
        nullchild.setParentId(Long.valueOf(3));
        nullchild.setYesChild(true);
        nullList.add(nullchild);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("HUMRES_KADR")
                    .withCatalogName("PKG_STRUCTURE_MAP").withProcedureName("getElementById");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("element_id",OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new  StructureTreeMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("element_id", id);

            Map map = simpleJdbcCall.execute(parameterSource);

            List<TreeSimple> myList=new ArrayList<>();

            myList = (List) map.get("cur");

            for (TreeSimple l:myList){

                myElement= l;

              }

            if ( myElement.getChildCount()!=0 ){

                myElement.setYesChild(true);

                myElement.setNodes(nullList);

            }
            else {
                myElement.setYesChild(false);

            }

        }catch (Exception e){

            e.printStackTrace();
        }
        return myElement;
    }

    public List<TreeSimple>  findChilds(Long id) {

        List<TreeSimple>myList = new ArrayList<TreeSimple>();
        myList=null;


        Locale.setDefault(Locale.ENGLISH);

        List<TreeSimple>nullList = new ArrayList<TreeSimple>();

        //Creating null node list model
        TreeSimple nullchild=new TreeSimple();
        nullchild.setId(Long.valueOf(1));
        nullchild.setTitle("Məlumat yüklənir...");
        nullchild.setParentId(Long.valueOf(3));
        nullchild.setYesChild(true);
        nullList.add(nullchild);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        try {

      SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("HUMRES_KADR")
              .withCatalogName("PKG_STRUCTURE_MAP").withProcedureName("get_ChildList");

       simpleJdbcCall.setAccessCallParameterMetaData(false);

       simpleJdbcCall.addDeclaredParameter(new SqlParameter("child_id",OracleTypes.NUMBER));

       simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new  StructureTreeMapper()));

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("child_id", id);

            Map map = simpleJdbcCall.execute(parameterSource);

            myList = (List) map.get("cur");

            for (TreeSimple myTree:myList)
            {
              //  System.out.println("fullname = "+myTree.getFullName());

                if ( myTree.getChildCount()!=0 ){



                    myTree.setYesChild(true);

                    myTree.setNodes(nullList);

                }
                else {
                    myTree.setYesChild(false);

                }
            }

        }catch (Exception e){

            e.printStackTrace();
        }
        System.out.println("i am in stdao from findparents metod"+myList.toString());
        return myList;
    }



    public List<TreeSimple> searchResults(String searchvalue){

    List<TreeSimple> searchResults=new ArrayList<>();

    Locale.setDefault(Locale.ENGLISH);

    JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

    try {
         SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("HUMRES_KADR")
                    .withCatalogName("PKG_STRUCTURE_MAP").withProcedureName("searchResults");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("searchvalue",OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new  StructureTreeMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("searchvalue", searchvalue);

            Map map = simpleJdbcCall.execute(parameterSource);

            searchResults = (List) map.get("cur");

        System.out.println("searchResults   "+searchResults);
          }catch (Exception e){

        e.printStackTrace();
    }
    return searchResults;
}

    public void updateElement(TreeSimple element) {


        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("HUMRES_KADR")
                    .withCatalogName("PKG_STRUCTURE_MAP").withProcedureName("update_element");

            simpleJdbcCall.setAccessCallParameterMetaData(false);


            simpleJdbcCall.addDeclaredParameter(new SqlParameter("element_id",OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("organTypeId",OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("upperStr",OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("title",OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("text",OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("suffix",OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();


            parameterSource.addValue("element_id", element.getId());

            parameterSource.addValue("organTypeId", element.getOrganType().getId());

            parameterSource.addValue("upperStr", element.getUpperStr());

            parameterSource.addValue("title", element.getTitle());

            parameterSource.addValue("text", element.getText());

            parameterSource.addValue("suffix", element.getSuffix());

            simpleJdbcCall.execute(parameterSource);

        }

        catch (Exception e){

            e.printStackTrace();
        }

    }

    public BigDecimal addElement(TreeSimple myObject )
    {

       TreeSimple  myNode =myObject.getNodes().get(0);

        BigDecimal id=null;

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("HUMRES_KADR")
                    .withCatalogName("PKG_STRUCTURE_MAP").withProcedureName("addElement");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("currentNRight",OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("currentLevel",OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("currentId",OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("organTypeId",OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("upperStr",OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("title",OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("text",OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("suffix",OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("newId", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();


            parameterSource.addValue("currentNRight", myObject.getnRight());

            parameterSource.addValue("currentLevel",myObject.getnLevel());

            parameterSource.addValue("currentId", myObject.getId());

            parameterSource.addValue("organTypeId", myNode.getOrganType().getId());

            parameterSource.addValue("upperStr", myNode.getUpperStr());

            parameterSource.addValue("title", myNode.getTitle());

            parameterSource.addValue("text", myNode.getText());

            parameterSource.addValue("suffix", myNode.getSuffix());

            Map map=simpleJdbcCall.execute(parameterSource);

            id = (BigDecimal) map.get("newId");

        }

        catch (Exception e){

                e.printStackTrace();
                }
          return  id;
      }

    public void removeElement(Long id) {


        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("HUMRES_KADR")
                    .withCatalogName("PKG_STRUCTURE_MAP").withProcedureName("remove_element");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("element_id",OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("element_id", id);

            simpleJdbcCall.execute(parameterSource);

        }catch (Exception e){

            e.printStackTrace();
        }

    }

    public List<OrganType>  getOrgantypes( ) {

        List<OrganType>myList = new ArrayList<OrganType>();

        myList=null;

        Locale.setDefault(Locale.ENGLISH);


        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("HUMRES_KADR")
                    .withCatalogName("PKG_STRUCTURE_MAP").withProcedureName("getOrgantypes");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR,   new BeanPropertyRowMapper(OrganType.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            myList = (List) map.get("cur");


        }catch (Exception e){

            e.printStackTrace();
        }
        return myList;
    }

    public List<OrganType>  getOrgantypeById(Long id) {

        List<OrganType>myList = new ArrayList<OrganType>();

        myList=null;

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("HUMRES_KADR")
                    .withCatalogName("PKG_STRUCTURE_MAP").withProcedureName("getOrgantypeById");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("organ_id",OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR,   new BeanPropertyRowMapper(OrganType.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("organ_id", id);

            Map map = simpleJdbcCall.execute(parameterSource);

            myList = (List) map.get("cur");

        }catch (Exception e){

            e.printStackTrace();
        }
        return myList;
    }

}
