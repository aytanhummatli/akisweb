package az.akis.dao.positionGroup;

import az.akis.models.PositionGroup;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PositionGroupMapper implements RowMapper<PositionGroup> {
    @Override
    public PositionGroup mapRow(ResultSet rs, int rowNum) throws SQLException {
        PositionGroup group=new PositionGroup();
        group.setId(rs.getLong("id"));
        group.setName(rs.getString("name"));
        return group;
    }
}
