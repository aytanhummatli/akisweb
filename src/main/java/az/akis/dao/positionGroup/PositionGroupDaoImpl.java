package az.akis.dao.positionGroup;

import az.akis.models.PositionGroup;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class PositionGroupDaoImpl implements PositionGroupDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public PositionGroup getPositionGroupById (Long id) {
        String findByIdQuery = "select pg.name,pg.id from HUMRES_KADR.HR_POSITION_GROUP pg WHERE ID=?";
        PositionGroup getHpgByid = jdbcTemplate.queryForObject(findByIdQuery, new Object[]{id}, new BeanPropertyRowMapper<PositionGroup>(PositionGroup.class));
        return getHpgByid;
    }


    @Override
    public List<PositionGroup> getPositionGroupList(int itemsPerPage, int pageNumber) {
        List<PositionGroup> groupList = null;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_POSITION_GROUP")
                    .withProcedureName("get_position_groupList");

            jdbcCall.addDeclaredParameter(new SqlOutParameter("cursor", OracleTypes.CURSOR,new PositionGroupMapper()));
            jdbcCall.addDeclaredParameter(new SqlParameter("pg_start", OracleTypes.NUMBER));
            jdbcCall.addDeclaredParameter(new SqlParameter("pg_end", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("pg_start", ((pageNumber - 1) * itemsPerPage + 1));
            parameterSource.addValue("pg_end", ((pageNumber - 1) * itemsPerPage + itemsPerPage));

            Map<String, Object> result = jdbcCall.execute(parameterSource);
            groupList = (List<PositionGroup>) result.get("cursor");
            for (PositionGroup list : groupList) {
                System.out.println("List:   " + list);
            }

        } catch (Exception e) {
            System.err.println("xeta bash verdi");
            e.printStackTrace();
        }
        return groupList;
    }

    @Override
    public void addPositionGroup(String name) {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_POSITION_GROUP")
                    .withProcedureName("add_position_group");

            jdbcCall.addDeclaredParameter(new SqlParameter("pg_name", OracleTypes.VARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("pg_name", name);
            parameterSource.addValue("p_user_id", 0);
            jdbcCall.execute(parameterSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updatePositionGroup(String name, Long id) {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_POSITION_GROUP")
                    .withProcedureName("update_position_group");

            jdbcCall.addDeclaredParameter(new SqlParameter("pg_name", OracleTypes.VARCHAR));
            jdbcCall.addDeclaredParameter(new SqlParameter("pgr_id", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("pg_name", name);
            parameterSource.addValue("pgr_id", id);
            jdbcCall.execute(parameterSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deletePositionGroup(Long id) {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("HUMRES_KADR")
                    .withCatalogName("PCKG_POSITION_GROUP")
                    .withProcedureName("delete_position_group");

            jdbcCall.addDeclaredParameter(new SqlParameter("pg_id", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("pg_id", id);
            jdbcCall.execute(parameterSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
