package az.akis.dao.position;

import az.akis.models.Position;
import az.akis.models.PositionGroup;
import az.akis.models.PositionName;

import java.util.List;

public interface PositionDao {

    List<PositionGroup> getGroupNameList();

    PositionName getPositionNamesById(Long id);

    List<PositionName> getPositionList(int itemsPerPage, int PageNumber);

    void addPosition(String name, Long posGroupId);

    void updatePosition(String name, Long posGroupId, Long id);

    void deletePosition(Long id);
}
