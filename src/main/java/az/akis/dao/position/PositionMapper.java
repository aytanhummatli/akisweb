package az.akis.dao.position;

import az.akis.models.Position;
import az.akis.models.PositionName;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class PositionMapper implements RowMapper<PositionName> {
    @Override
    public PositionName mapRow(ResultSet rs, int rowNum) throws SQLException {
        PositionName positionMapper=new PositionName();
        positionMapper.setId(rs.getLong("c1"));
        positionMapper.setName(rs.getString("c2"));
        positionMapper.getPosition().getPositionGroup().setName(rs.getString("c3"));
        positionMapper.setId(rs.getLong("c4"));
        positionMapper.getPosition().getPositionGroup().setId(rs.getLong("c5"));
        positionMapper.getPosition().setId(rs.getLong("c6"));
        positionMapper.getPosition().getPositionGroup().setId(rs.getLong("c7"));
        return positionMapper;
    }
}
