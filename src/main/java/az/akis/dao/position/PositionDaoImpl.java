package az.akis.dao.position;

import az.akis.models.Position;
import az.akis.models.PositionGroup;
import az.akis.models.PositionName;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class PositionDaoImpl implements PositionDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public List<PositionGroup> getGroupNameList() {
        String query = "select id,name from humres_kadr.hr_position_group";
        List<PositionGroup> list = jdbcTemplate.query(query, new BeanPropertyRowMapper<PositionGroup>(PositionGroup.class));
        return list;
    }

    @Override
    public PositionName getPositionNamesById(Long id) {
        String query = "select pos_id,name from humres_kadr.hr_position_names WHERE pos_id=" + id + "";
        PositionName getHpById = jdbcTemplate.queryForObject(query, new BeanPropertyRowMapper<PositionName>(PositionName.class));
        return getHpById;
    }


    @Override
    public List<PositionName> getPositionList(int itemsPerPage, int pageNumber) {
        List<PositionName> list=null;
        try {
            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("HUMRES_KADR").withCatalogName("PCKG_POSITION").withProcedureName("get_positionList");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new PositionMapper()));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_start", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_end", OracleTypes.NUMBER));

            MapSqlParameterSource param = new MapSqlParameterSource();

            param.addValue("p_start", ((pageNumber - 1) * itemsPerPage + 1));
            param.addValue("p_end", ((pageNumber - 1) * itemsPerPage + itemsPerPage));

            Map<String, Object> map = simpleJdbcCall.execute(param);

            list = (List<PositionName>) map.get("cur");
        } catch (Exception e) {
            System.err.println("xeta bash verdi");
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void addPosition(String name, Long posGroupId) {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("HUMRES_KADR").withCatalogName("PCKG_POSITION").withProcedureName("add_position");
            jdbcCall.addDeclaredParameter(new SqlParameter("p_group_id", OracleTypes.NUMBER));
            jdbcCall.addDeclaredParameter(new SqlParameter("p_name", OracleTypes.VARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("p_group_id", posGroupId);
            parameterSource.addValue("p_name", name);
            parameterSource.addValue("p_user_id", 0);

            jdbcCall.execute(parameterSource);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updatePosition(String name, Long posGroupId,Long id) {

        System.out.println("name : "+name);
        System.out.println("posgroupid : "+posGroupId);
        System.out.println("id:  "+id);
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("HUMRES_KADR").withCatalogName("PCKG_POSITION").withProcedureName("update_position");
            jdbcCall.addDeclaredParameter(new SqlParameter("pn_name", OracleTypes.VARCHAR));
            jdbcCall.addDeclaredParameter(new SqlParameter("pos_gr_id", OracleTypes.NUMBER));
            jdbcCall.addDeclaredParameter(new SqlParameter("p_id",OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("pn_name", name);
            parameterSource.addValue("pos_gr_id", posGroupId);
            parameterSource.addValue("p_id",id);

            jdbcCall.execute(parameterSource);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    ;


    @Override
    public void deletePosition(Long id) {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("HUMRES_KADR").withCatalogName("PCKG_POSITION").withProcedureName("delete_position");
            jdbcCall.addDeclaredParameter(new SqlParameter("p_id", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("p_id", id);

            jdbcCall.execute(parameterSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ;


}
