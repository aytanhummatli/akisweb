package az.akis.dao.AttestationHistory;


import az.akis.models.AttestationHistory;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AttestationHistoryMapper implements RowMapper<AttestationHistory> {

    @Override
    public AttestationHistory mapRow(ResultSet rs, int i) throws SQLException {


        AttestationHistory attestationHistory = new AttestationHistory();

        attestationHistory.setId(rs.getLong("id"));
        attestationHistory.setAtteatationDate(rs.getDate("attestation_date"));
        attestationHistory.getResult().setDescription(rs.getString("result"));

        return attestationHistory;

    }


}

