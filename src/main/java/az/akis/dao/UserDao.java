package az.akis.dao;


import az.akis.models.User;

public interface UserDao {


    public User getUser(String username);


}
