package az.akis.dao.subRegion;

import az.akis.models.SubRegion;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class SubRegionDaoImpl implements SubRegionDao {

    @Autowired
    DataSource dataSource;

    @Override
    public List<SubRegion> getSubRegionList() {

        Locale.setDefault(Locale.ENGLISH);

        List<SubRegion> subRegionList=new ArrayList<>();

        try {

            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("COMMON_KADR").withCatalogName("LOCATION").withProcedureName("getSubRegionList");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("subRegionList", OracleTypes.CURSOR,new BeanPropertyRowMapper(SubRegion.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            subRegionList = (List<SubRegion>) map.get("subRegionList");

           } catch (Exception e) {

            e.printStackTrace();

        }
              return subRegionList;
      }

}