package az.akis.dao.subRegion;

import az.akis.models.SubRegion;

import java.util.List;

public interface SubRegionDao {

    public List<SubRegion> getSubRegionList();

}
