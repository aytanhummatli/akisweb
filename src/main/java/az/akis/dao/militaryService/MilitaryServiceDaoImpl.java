package az.akis.dao.militaryService;

import az.akis.models.Combo;
import az.akis.models.EmployeeMilitaryService;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Repository
public class MilitaryServiceDaoImpl implements MilitaryServiceDao {

    @Autowired
    private DataSource datasource;

    public List<Combo> getHerbiMukellefiyyet() {

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(datasource);

        List<Combo> employeeMilitaryServiceList = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("COMMON_KADR").withCatalogName("COM_MILITARY_SERVICE").withProcedureName("getHerbiMukellefiyyet");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("mukellefiyyetList", OracleTypes.CURSOR, new BeanPropertyRowMapper(Combo.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute();

            employeeMilitaryServiceList = (List<Combo>) map.get("mukellefiyyetList");

        } catch (Exception e) {

            e.printStackTrace();
        }

        System.out.println("employeeMilitaryServiceList"+employeeMilitaryServiceList.toString());

        return employeeMilitaryServiceList;
    }


    public void insertMilitaryService(EmployeeMilitaryService employeeMilitaryService,Long userId) {

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(datasource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("COMMON_KADR").withCatalogName("COM_MILITARY_SERVICE").withProcedureName("insertMilitaryService");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("userId", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("isRegistered", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("serialNo", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("endDate", OracleTypes.DATE));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("empId", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("militaryDutyID", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("startDate", OracleTypes.DATE));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("armyPort", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("userId", userId);
            parameterSource.addValue("isRegistered", employeeMilitaryService.getIsRegistered());
            parameterSource.addValue("serialNo", employeeMilitaryService.getSerialNo());
            parameterSource.addValue("endDate", employeeMilitaryService.getEndDate());
            parameterSource.addValue("empId", employeeMilitaryService.getEmpId());

            parameterSource.addValue("militaryDutyID", employeeMilitaryService.getMilitaryDuty().getId());

            parameterSource.addValue("startDate", employeeMilitaryService.getBeginDate());

            parameterSource.addValue("armyPort", employeeMilitaryService.getArmyPort());


            simpleJdbcCall.execute(parameterSource);

        } catch (Exception e)

        {
            e.printStackTrace();
        }

    }


    public void updateMilitaryService(EmployeeMilitaryService employeeMilitaryService) {

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(datasource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)

                    .withSchemaName("COMMON_KADR").withCatalogName("COM_MILITARY_SERVICE").withProcedureName("updateMilitaryService");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("elementId", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("isRegistered", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("serialNo", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("endDate", OracleTypes.DATE));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("militaryDutyID", OracleTypes.NUMBER));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("startDate", OracleTypes.DATE));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("armyPort", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("elementId", employeeMilitaryService.getId());

            parameterSource.addValue("isRegistered", employeeMilitaryService.getIsRegistered());

            parameterSource.addValue("serialNo", employeeMilitaryService.getSerialNo());

            parameterSource.addValue("endDate", employeeMilitaryService.getEndDate());

            parameterSource.addValue("militaryDutyID", employeeMilitaryService.getMilitaryDuty().getId());

            parameterSource.addValue("startDate", employeeMilitaryService.getBeginDate());

            parameterSource.addValue("armyPort", employeeMilitaryService.getArmyPort());

            System.out.println("parameterSource "+parameterSource.getValues());

            simpleJdbcCall.execute(parameterSource);

        } catch (Exception e)

        {
            e.printStackTrace();
        }
    }

   public void deleteMilitaryService(Long elementId) {

        Locale.setDefault(Locale.ENGLISH);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(datasource);

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("").withCatalogName("").withSchemaName("");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("elementId", OracleTypes.NUMBER));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("elementId", elementId);

            simpleJdbcCall.execute(parameterSource);

        } catch (Exception e)

        {
            e.printStackTrace();
        }
    }
}
