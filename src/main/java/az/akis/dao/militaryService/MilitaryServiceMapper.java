package az.akis.dao.militaryService;

import az.akis.models.EmployeeMilitaryService;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MilitaryServiceMapper implements RowMapper {



    @Override
    public Object mapRow(ResultSet rs, int i) throws SQLException {

        EmployeeMilitaryService employeeMilitaryService=new EmployeeMilitaryService();


        employeeMilitaryService.setArmyPort(rs.getString(""));
        employeeMilitaryService.setBeginDate(rs.getDate(""));
        employeeMilitaryService.setEndDate(rs.getDate(""));
        employeeMilitaryService.setIsRegistered(rs.getString(""));
        employeeMilitaryService.setSerialNo(rs.getString(""));
        employeeMilitaryService.getMilitaryDuty().setDescription(rs.getString(""));
        employeeMilitaryService.getMilitaryDuty().setId(rs.getLong(""));


        return employeeMilitaryService;
    }
}
