package az.akis.services.nomenclature;

import az.akis.dao.nomenclature.NomenclatureDao;
import az.akis.models.Nomenclature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class NomenclatureServiceImpl implements NomenclatureService {



    @Autowired
    private NomenclatureDao nomenclatureDao;

    @Override
    public List<Nomenclature> getSearchResults(String elementName) {

        return nomenclatureDao.getSearchResults(elementName);
    }

    @Override
    public List<Nomenclature> getNomenkForSelect() {

        return nomenclatureDao.getNomenkForSelect();
    }

    @Override
    public List<Nomenclature> getNomenclatureList(int rowsPerPage, int pageNumber) {
        return nomenclatureDao.getNomenclatureList(rowsPerPage, pageNumber);
    }

    @Override
    public List<Nomenclature> getNomListById(Long id) {
        return nomenclatureDao.getNomListById(id);
    }

    @Override
    public List<Nomenclature> getComboList() {
        return nomenclatureDao.getComboList();
    }

    @Override
    public Nomenclature getNomNameById(BigDecimal id) {
        return nomenclatureDao.getNomNameById(id);
    }

    @Override
    public void deleteNom(Long id) {
        nomenclatureDao.deleteNom(id);
    }

    @Override
    public void createNom(String name, String str) {
        nomenclatureDao.createNom(name, str);
    }

    @Override
    public void createChildNom(Nomenclature nom) {
        nomenclatureDao.createChildNom(nom);
    }

    @Override
    public void updateNom(Nomenclature nom) {
        nomenclatureDao.updateNom(nom);
    }

    @Override
    public void deleteChildNom(long id) {
        nomenclatureDao.deleteChildNom(id);
    }
}
