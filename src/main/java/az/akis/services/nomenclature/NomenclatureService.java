package az.akis.services.nomenclature;

import az.akis.models.Nomenclature;

import java.math.BigDecimal;
import java.util.List;

public interface NomenclatureService {

    public List<Nomenclature> getSearchResults(String elementName);

    public  List<Nomenclature> getNomenkForSelect();

    List<Nomenclature> getNomenclatureList(int rowsPerPage, int pageNumber);

    List<Nomenclature> getNomListById(Long id);

    List<Nomenclature> getComboList();

    Nomenclature getNomNameById(BigDecimal id);

    void deleteNom(Long id);

    void createNom(String name, String str);

    void createChildNom(Nomenclature nom);

    void updateNom(Nomenclature nom);

    void deleteChildNom(long id);
}
