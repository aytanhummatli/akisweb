package az.akis.services.region;

import az.akis.dao.region.RegionDao;
import az.akis.models.Region;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionServiceImpl implements RegionService {

    @Autowired
    RegionDao regionDao;

    @Override
    public List<Region> regionList() {

        return regionDao.regionList();
    }

}