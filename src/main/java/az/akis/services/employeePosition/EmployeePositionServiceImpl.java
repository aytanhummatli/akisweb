package az.akis.services.employeePosition;

import az.akis.dao.employeePosition.EmployeePositionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeePositionServiceImpl implements EmployeePositionService {

    @Autowired
    EmployeePositionDao employeePositionDao;

    @Override
    public void deleteEmployeeDio(Long elementId) {
        employeePositionDao.deleteEmployeeDio(elementId);
    }
}
