package az.akis.services;

import az.akis.dao.UserDao;
import az.akis.models.User;

public class UserServiceImpl implements UserService {

    UserDao userDao;

    @Override
    public User getUser(String username) {
        return userDao.getUser(username);
    }
}
