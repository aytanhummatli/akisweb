package az.akis.services.employeeVacation;

import az.akis.dao.EmployeeVacation.EmployeeVacationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeVacationServiceImpl implements EmployeeVacationService {


    @Autowired
    EmployeeVacationDao employeeVacationDao;

    @Override
    public void deleteVacation(Long elementId) {

      employeeVacationDao.deleteVacation(elementId);

    }
}
