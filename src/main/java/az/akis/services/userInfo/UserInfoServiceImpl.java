package az.akis.services.userInfo;

import az.akis.dao.userInfo.UserInfoDao;
import az.akis.models.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class UserInfoServiceImpl implements UserInfoService {
    @Autowired
    private UserInfoDao userInfoDao;
    @Override
    public List<UserInfo> getUserInfoList(int itemsPerPage,int pageNumber) {
        return userInfoDao.getUserInfoList(itemsPerPage,pageNumber);
    }

    @Override
    public List<UserInfo> getUserFullNameCombo(String fullName) {
        return userInfoDao.getUserFullNameCombo(fullName);
    }

    @Override
    public List<UserInfo> getNomNameCombo(String nomName) {
        return userInfoDao.getNomNameCombo(nomName);
    }

    @Override
    public void createUserInfo(UserInfo userInfo) {
        userInfoDao.createUserInfo(userInfo);
    }

    @Override
    public UserInfo getUserNameById(BigDecimal id) {
        return userInfoDao.getUserNameById(id);
    }

    @Override
    public void updateUser(UserInfo userInfo) {
        userInfoDao.updateUser(userInfo);
    }

    @Override
    public void deleteUser(long id) {
        userInfoDao.deleteUser(id);
    }



    @Override
    public String checkUsername(String username) {
        return  userInfoDao.checkUsername(username);
    }

    @Override
    public UserInfo getNomNameByUserId(BigDecimal userId) {
        return userInfoDao.getNomNameByUserId(userId);
    }
}
