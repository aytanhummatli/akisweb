package az.akis.services.userInfo;

import az.akis.models.UserInfo;

import java.math.BigDecimal;
import java.util.List;

public interface UserInfoService {
    List<UserInfo> getUserInfoList(int itemsPerPage, int pageNumber);

    List<UserInfo> getUserFullNameCombo(String fulName);

    List<UserInfo> getNomNameCombo(String nomName);

    void createUserInfo(UserInfo userInfo);

    UserInfo getUserNameById(BigDecimal id);

    void updateUser(UserInfo userInfo);

    void deleteUser(long id);

    String  checkUsername(String username);

    UserInfo getNomNameByUserId(BigDecimal userId);
}
