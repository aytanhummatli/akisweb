package az.akis.services.workActivityBefore;

import az.akis.dao.WorkActivityBefore.WorkActivityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkActivityBeforeServiceImpl implements WorkActivityBeforeService {

    @Autowired
    WorkActivityDao workActivityDao;

    @Override
    public void deleteWorkActivity(Long elementId) {
       workActivityDao.deleteWorkActivity(elementId);
    }
}
