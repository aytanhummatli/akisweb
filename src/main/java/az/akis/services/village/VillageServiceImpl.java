package az.akis.services.village;

import az.akis.dao.village.VillageDao;
import az.akis.models.Village;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class VillageServiceImpl implements VillageService {

    @Autowired
    VillageDao villageDao;

    @Override
    public List<Village> getVillageList() {
        return villageDao.getVillageList();
    }
}
