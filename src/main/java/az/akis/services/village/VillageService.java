package az.akis.services.village;

import az.akis.models.Village;

import java.util.List;

public interface VillageService {

    public List<Village> getVillageList();
}
