package az.akis.services.educationHistoryService;

import az.akis.dao.educationHistory.EducationHistoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EducationHistoryServiceImpl implements EducationHistoryService {

    @Autowired
    EducationHistoryDao educationHistoryDao;
    @Override
    public void deleteEducationHistory(Long elementId) {

        educationHistoryDao.deleteEducationHistory(elementId);
    }
}
