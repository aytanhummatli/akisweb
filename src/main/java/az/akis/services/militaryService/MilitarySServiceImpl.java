package az.akis.services.militaryService;

import az.akis.dao.militaryService.MilitaryServiceDao;
import az.akis.models.Combo;
import az.akis.models.EmployeeMilitaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MilitarySServiceImpl implements MilitarySService {

    @Autowired
    MilitaryServiceDao militaryServiceDao;


    @Override
    public List<Combo> getHerbiMukellefiyyet() {
        return militaryServiceDao.getHerbiMukellefiyyet();
    }

    @Override
    public void insertMilitaryService(EmployeeMilitaryService employeeMilitaryService,Long userId) {
militaryServiceDao.insertMilitaryService(employeeMilitaryService,userId);
    }

    @Override
    public void updateMilitaryService(EmployeeMilitaryService employeeMilitaryService) {
militaryServiceDao.updateMilitaryService(employeeMilitaryService);
    }

    @Override
    public void deleteMilitaryService(Long elementId) {
militaryServiceDao.deleteMilitaryService(elementId);
    }
}
