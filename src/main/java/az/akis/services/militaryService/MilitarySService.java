package az.akis.services.militaryService;

import az.akis.models.Combo;
import az.akis.models.EmployeeMilitaryService;

import java.util.List;

public interface MilitarySService {



    public List<Combo> getHerbiMukellefiyyet();

    public void insertMilitaryService(EmployeeMilitaryService employeeMilitaryService,Long userId);

    public void updateMilitaryService(EmployeeMilitaryService employeeMilitaryService);

    public void deleteMilitaryService(Long elementId);


}
