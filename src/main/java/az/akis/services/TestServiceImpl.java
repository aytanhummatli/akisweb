package az.akis.services;

import az.akis.dao.TestDao;
import az.akis.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private TestDao testDao;

    @Override
    public User findById(int id) {

        return testDao.findById(id);
    }

    @Override
    public User findByName(String name) {

        return testDao.findByName(name);
    }

    @Override
    public void saveUser(User user) {

        testDao.saveUser(user);
    }

    @Override
    public void updateUser(User user) {

        testDao.updateUser(user);
    }

    @Override
    public void deleteUserById(int id) {

        testDao.deleteUserById(id);
    }

    @Override
    public List<User> findAllUsers() {
        return testDao.findAllUsers();
    }

    @Override
    public void deleteAllUsers() {

    }

    @Override
    public boolean isUserExist(User user) {

        return testDao.isUserExist(user);
    }

    @Override
    public User testLogin(String username, String password) {
        return null;
    }

    @Override
    public User findByNameAndPass(String name,String password){


     return  testDao.findByNameAndPass(name,password);
    }

}
