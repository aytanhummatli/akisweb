package az.akis.services.workMission;

import az.akis.dao.WorkMission.WorkMissionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkMissionServiceImpl implements WorkMissionService {

@Autowired
    WorkMissionDao workMissionDao;
    @Override
    public void deleteWorkMission(Long elementId) {
      workMissionDao.deleteWorkMission(elementId);
    }
}
