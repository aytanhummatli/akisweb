package az.akis.services.street;

import az.akis.models.Street;

import java.util.List;

public interface StreetService {

    public List<Street> getStreetList();
}
