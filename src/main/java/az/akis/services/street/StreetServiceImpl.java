package az.akis.services.street;

import az.akis.dao.street.StreetDao;
import az.akis.models.Street;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StreetServiceImpl implements StreetService {
    @Autowired
    StreetDao streetDao;

    @Override
    public List<Street> getStreetList() {
        return streetDao.getStreetList();
    }
}
