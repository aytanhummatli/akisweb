package az.akis.services;

import   az.akis.models.User;

public interface UserService {

    public User getUser(String username);

}
