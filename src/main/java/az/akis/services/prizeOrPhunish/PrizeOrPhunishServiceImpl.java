package az.akis.services.prizeOrPhunish;

import az.akis.dao.PrizeOrPunish.PrizeOrPhunishDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrizeOrPhunishServiceImpl implements PrizeOrPhunishService {


    @Autowired
    PrizeOrPhunishDao prizeOrPhunishDao;


    @Override
    public void deletePrizeOrPunish(Long elementId) {

        prizeOrPhunishDao.deletePrizeOrPunish(elementId);

    }


}
