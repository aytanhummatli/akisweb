package az.akis.services.posititon;

import az.akis.models.Position;
import az.akis.models.PositionGroup;
import az.akis.models.PositionName;

import java.util.List;

public interface PositionService {

    List<PositionGroup> getGroupNameList();

    PositionName getPositionNamesById(Long id);

    void deletePosition(Long id);

    List<PositionName> getPositionList(int itemsPerPage, int pageNumber);

    void addPosition(String name, Long posGroupId);

    void updatePosition(String name, Long posGroupId, Long id);
}
