package az.akis.services.posititon;

import az.akis.dao.position.PositionDao;
import az.akis.models.Position;
import az.akis.models.PositionGroup;
import az.akis.models.PositionName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PositionServiceImpl implements PositionService {
    @Autowired
    private PositionDao positionDao;


    @Override
    public List<PositionGroup> getGroupNameList() {
        List<PositionGroup> list = positionDao.getGroupNameList();
        return list;
    }

    @Override
    public PositionName getPositionNamesById(Long id) {
        return positionDao.getPositionNamesById(id);
    }

    @Override
    public void deletePosition(Long id) {
        positionDao.deletePosition(id);
    }

    @Override
    public List<PositionName> getPositionList(int itemsPerPage, int pageNumber) {
        return positionDao.getPositionList(itemsPerPage, pageNumber);
    }

    @Override
    public void addPosition(String name, Long posGroupId) {
        positionDao.addPosition(name, posGroupId);
    }

    @Override
    public void updatePosition(String name, Long posGroupId, Long id) {
        positionDao.updatePosition(name, posGroupId, id);
    }
}
