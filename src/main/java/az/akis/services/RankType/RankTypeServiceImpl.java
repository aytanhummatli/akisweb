package az.akis.services.RankType;

import az.akis.dao.RankTypes.RankTypesDao;
import az.akis.models.RankType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RankTypeServiceImpl  implements RankTypeService{

  @Autowired
  RankTypesDao rankTypesDao;

    @Override
    public List<RankType> findAllRankTypes() {
        return rankTypesDao.findAllRankTypes();
    }
}
