package az.akis.services.RankType;

import az.akis.models.RankType;

import java.util.List;

public interface RankTypeService {
    public List<RankType> findAllRankTypes();
}
