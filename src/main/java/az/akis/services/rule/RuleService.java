package az.akis.services.rule;

import az.akis.models.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public interface RuleService {
    List<ButtonRule> getButtonRuleList();

    List<Rule> getRuleCombo();

    List<UserRule> getUserRuleListById(BigDecimal userId);

    List<UserButtonRule> getUserButtonRuleListById(BigDecimal userId);

    void createUserRule(ArrayList<Rule> ruleList);

    void createUserButtonRule(ArrayList<ButtonRule> buttonRuleList);

    void updateUserRule(ArrayList<Rule> userRuleList, UserInfo info);

    void updateUserButtonRule(ArrayList<ButtonRule> userButtonRuleList, UserInfo info);
}
