package az.akis.services.rule;

import az.akis.dao.rule.RuleDao;
import az.akis.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class RuleServiceImpl implements RuleService {
    @Autowired
    private RuleDao ruleDao;

    @Override
    public List<ButtonRule> getButtonRuleList() {
        return ruleDao.getButtonRuleList();
    }

    @Override
    public List<Rule> getRuleCombo() {
        return ruleDao.getRuleCombo();
    }

    @Override
    public List<UserRule> getUserRuleListById(BigDecimal userId) {
        return ruleDao.getUserRuleListById(userId);
    }

    @Override
    public List<UserButtonRule> getUserButtonRuleListById(BigDecimal userId) {
        return ruleDao.getUserButtonRuleListById(userId);
    }

    @Override
    public void createUserRule(ArrayList<Rule> ruleList) {
         ruleDao.createUserRule(ruleList);
    }

    @Override
    public void createUserButtonRule(ArrayList<ButtonRule> buttonRuleList) {
         ruleDao.createUserButtonRule(buttonRuleList);
    }

    @Override
    public void updateUserRule(ArrayList<Rule> userRuleList,UserInfo info) {
        ruleDao.updateUserRule(userRuleList,info);
    }

    @Override
    public void updateUserButtonRule(ArrayList<ButtonRule> userButtonRuleList,UserInfo info) {
ruleDao.updateUserButtonRule(userButtonRuleList, info);
    }
}
