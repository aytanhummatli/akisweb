package az.akis.services.subRegion;

import az.akis.dao.subRegion.SubRegionDao;
import az.akis.models.SubRegion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SubRegionServiceImpl implements SubRegionService {

    @Autowired
    SubRegionDao subRegionDao;

    @Override
    public List<SubRegion> getSubRegionList() {

        return subRegionDao.getSubRegionList() ;
    }
}
