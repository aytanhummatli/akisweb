package az.akis.services.Employee;

import az.akis.models.Employee;
import az.akis.models.EmployeePosition;

import java.util.List;

public interface EmployeeService {
    public Employee getEducationLevel(Long empId);
    public List<Employee> get_employees(Long st_id, String ed_surname, String ed_patron, String ed_name, Long pageno, Long itemsPerPage);
    public Employee getEmployeeFamily(Long empId);
    public Employee getEmployeeById(Long empId);
    public Employee getGeneralIngormationById(Long empId);
    public List <EmployeePosition> getEmployeeDIObyId(Long empId);
    public  Employee  getEmployeeBeforeDIOandEzam(Long empId);
    public  Employee  getEmployeeMezYaralanma(Long empId, Long year);

    public Employee getPrizeAndPunish(Long empId);

    public Employee getOthers(Long empId);
    public Employee getEmployeeInfoPrint(Long EmpId);
}
