package az.akis.services.Employee;

import az.akis.models.Employee;
import az.akis.models.EmployeePosition;
import az.akis.dao.Employee.EmployeeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

@Autowired
EmployeeDao employeeDao;

    @Override
    public Employee getEmployeeInfoPrint(Long EmpId) {
        return employeeDao.getEmployeeInfoPrint(EmpId);
    }
    public Employee getGeneralIngormationById(Long empId ){

        return employeeDao.getGeneralIngormationById(empId);

    }

    public List<Employee> get_employees(Long st_id ,String ed_surname,String ed_patron,String ed_name, Long pageno, Long itemsPerPage  ){

        return  employeeDao.get_employees(st_id,ed_surname,ed_patron,ed_name,pageno,itemsPerPage);

    }


    public Employee getEmployeeById( Long empId  ){

        return employeeDao.getEmployeeById(empId);
    }
    public Employee getEducationLevel( Long empId  ){

        return employeeDao.getEducationLevel(empId);
    }

    public Employee getEmployeeFamily(Long empId){

        return employeeDao.getEmployeeFamily(empId);
    }
    public List <EmployeePosition> getEmployeeDIObyId(Long empId){

        return employeeDao.getEmployeeDIObyId(empId);
    }

    public  Employee  getEmployeeBeforeDIOandEzam(Long empId){

        return employeeDao.getEmployeeBeforeDIOandEzam(empId);
    }

    public  Employee  getEmployeeMezYaralanma(Long empId, Long year){

        return employeeDao.getEmployeeMezYaralanma(empId,year);

    }

    public Employee getPrizeAndPunish(Long empId ){

        return employeeDao.getPrizeAndPunish(empId);
    }

    public Employee getOthers(Long empId ){

        return employeeDao.getOthers(empId);
    }
}
