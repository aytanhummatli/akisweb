package az.akis.services.attesstationHistory;

import az.akis.dao.AttestationHistory.AttestationHistoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AttesstationHistoryServiceImpl implements AttesstationHistoryService {

    @Autowired
    AttestationHistoryDao attestationHistoryDao;
    @Override
    public void deleteAttestationHistory(Long elementId) {
        attestationHistoryDao.deleteAttestationHistory(elementId);
    }
}
