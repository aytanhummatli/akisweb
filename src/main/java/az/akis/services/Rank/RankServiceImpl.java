package az.akis.services.Rank;

import az.akis.dao.Rank.RankDao;
import az.akis.models.Rank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
    @Service
    public class RankServiceImpl implements RankService {
        @Autowired
        RankDao rankDao;

        @Override
        public Rank findById(int id) {
            return rankDao.findById(id);
        }

    @Override
    public Rank findByName(String name) {
        return null;
    }

    @Override
    public void saveRank(Rank rank) {

  rankDao.saveRank(rank);
    }

    @Override
    public void updateRank(Rank rank) {
     rankDao.updateRank(rank);
    }

    @Override
    public void deleteRankById(int id) {
            rankDao.deleteRankById(id);
    }

    @Override
    public List<Rank> findAllRanks() {

        return rankDao.findAllRanks();
    }

    @Override
    public void deleteAllRanks() {

    }

    @Override
    public Rank testLogin(String username, String password) {
        return null;
    }

    @Override
    public boolean isRankExist(Rank rank) {
        return rankDao.isRankExist(rank);
    }

    @Override
   public List<Rank> findAllRanksByPage(int pageno, int itemsperpage){return rankDao.findAllRanksByPage( pageno, itemsperpage);}
}
