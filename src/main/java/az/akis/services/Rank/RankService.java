package az.akis.services.Rank;

import az.akis.models.Rank;

import java.util.List;

public interface RankService {
    Rank findById(int id);

    Rank findByName(String name);

    void saveRank(Rank rank);

    void updateRank(Rank rank);

    void deleteRankById(int id);

    List<Rank> findAllRanks();

    List<Rank> findAllRanksByPage(int pageno, int itemsperpage);

    void deleteAllRanks();

    public Rank testLogin(String username, String password);

    public boolean isRankExist(Rank rank);
}
