package az.akis.services.StructureMap;

import az.akis.dao.StructureMap.StMapDao;
import az.akis.models.OrganType;
import az.akis.models.StuffSchedule;
import az.akis.models.TreeSimple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class StServiceImpl implements StService {

    @Autowired
    StMapDao stMapDao;


    public void removeElement(Long id) {stMapDao.removeElement(id);}
    public List<TreeSimple> findChilds(Long id){ return  stMapDao.findChilds(id);}

    public List<TreeSimple> searchResults(String searchvalue){   return stMapDao.searchResults(searchvalue);    }

    public BigDecimal addElement(TreeSimple myObject ){ return stMapDao.addElement( myObject);}

    public void updateElement(TreeSimple element){
        stMapDao.updateElement(element);
    }

    public List<OrganType>  getOrgantypeById(Long id){
        return stMapDao.getOrgantypeById(id);
    }

    public List<OrganType>  getOrgantypes( ){
        return stMapDao.getOrgantypes();
    }

    public  TreeSimple  getElementById(Long id){return stMapDao.getElementById(id);}

    public List<StuffSchedule>  getEmployees(Long nodeId , Long NomenklaturaID, Long STR_NAME_ID ){
        return stMapDao.getEmployees(nodeId,NomenklaturaID,STR_NAME_ID);
    }
}
