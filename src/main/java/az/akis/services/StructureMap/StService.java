package az.akis.services.StructureMap;

import az.akis.models.OrganType;
import az.akis.models.StuffSchedule;
import az.akis.models.TreeSimple;

import java.math.BigDecimal;
import java.util.List;

public interface StService {

    public List<OrganType>  getOrgantypeById(Long id);
    public List<OrganType>  getOrgantypes();
    public List<TreeSimple> findChilds(Long id);
    public  TreeSimple  getElementById(Long id);
    public List<TreeSimple> searchResults(String searchvalue);
    public void removeElement(Long id);
    public BigDecimal addElement(TreeSimple myObject);
    public void updateElement(TreeSimple element);
    public List<StuffSchedule>  getEmployees(Long nodeId, Long NomenklaturaID, Long STR_NAME_ID);
}
