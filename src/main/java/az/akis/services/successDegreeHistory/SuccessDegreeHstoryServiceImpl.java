package az.akis.services.successDegreeHistory;

import az.akis.dao.successDegreeHistory.SuccessdegreeHistoryDao;
import az.akis.models.SuccessDegree;
import az.akis.models.SuccessDegreeHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SuccessDegreeHstoryServiceImpl implements SuccessDegreeHistoryService {

    @Autowired
    private SuccessdegreeHistoryDao successdegreeHistoryDao;

    @Override
    public List<SuccessDegree> getSuccessdegreeList() {


        return successdegreeHistoryDao.getSuccessdegreeList();
    }

    @Override
    public void createSuccessdegreeHist(SuccessDegreeHistory successDegreeHistory) {
        successdegreeHistoryDao.createSuccessdegreeHist(successDegreeHistory);
    }

    @Override
    public void deleteSuccessdegreeHist(Long Id) {
        successdegreeHistoryDao.deleteSuccessdegreeHist(Id);
    }

    @Override
    public void updateSuccessdegreeHist(SuccessDegreeHistory successDegreeHistory) {
        successdegreeHistoryDao.updateSuccessdegreeHist(successDegreeHistory);
    }
}
