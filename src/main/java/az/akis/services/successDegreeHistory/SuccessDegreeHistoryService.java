package az.akis.services.successDegreeHistory;

import az.akis.models.SuccessDegree;
import az.akis.models.SuccessDegreeHistory;

import java.util.List;

public interface SuccessDegreeHistoryService {

    public List<SuccessDegree> getSuccessdegreeList();

    void createSuccessdegreeHist(SuccessDegreeHistory successDegreeHistory);

    public  void deleteSuccessdegreeHist(Long Id);

    void updateSuccessdegreeHist(SuccessDegreeHistory successDegreeHistory);
}
