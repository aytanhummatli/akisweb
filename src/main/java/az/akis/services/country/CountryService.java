package az.akis.services.country;

import az.akis.models.Country;

import java.util.List;

public interface CountryService {

    public List<Country> getCountryList();
}
