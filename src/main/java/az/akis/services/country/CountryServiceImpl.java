package az.akis.services.country;

import az.akis.dao.country.CountryDao;
import az.akis.models.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    CountryDao countryDao;

    @Override
    public List<Country> getCountryList() {

        return countryDao.getCountryList();
    }
}
