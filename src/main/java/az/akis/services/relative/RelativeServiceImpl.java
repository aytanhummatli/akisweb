package az.akis.services.relative;

import az.akis.dao.Relative.RelativeDao;
import az.akis.models.Combo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RelativeServiceImpl implements RelativeService{
@Autowired
    RelativeDao relativeDao;

    @Override
    public List<Combo> getRelativeDegreeList() {
        return relativeDao.getRelativeDegreeList();
    }

    @Override
    public List<Combo> getWorkList() {
        return relativeDao.getWorkList();
    }


    @Override
    public void deleteRelative(Long elementId) {

        relativeDao.deleteRelative(elementId);
    }
}
