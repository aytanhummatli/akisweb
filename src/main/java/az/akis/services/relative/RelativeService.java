package az.akis.services.relative;

import az.akis.models.Combo;

import java.util.List;

public interface RelativeService {

    public List<Combo> getRelativeDegreeList();

    public List<Combo> getWorkList();

    public  void deleteRelative(Long elementId);
}
