package az.akis.services.employeeRank;

import az.akis.models.Combo;
import az.akis.models.EmployeeRank;

import java.util.List;

public interface EmployeeRankService {

    public List<Combo> getRankService ();

    void createEmployeeRank(EmployeeRank employeeRank);
    public  void deleteEmployeeRank (Long Id);

    void updateRank(EmployeeRank employeeRank);
}
