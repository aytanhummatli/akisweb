package az.akis.services.employeeRank;

import az.akis.dao.employeeRank.EmployeeRankDao;
import az.akis.models.Combo;
import az.akis.models.EmployeeRank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeRankServiceImpl implements EmployeeRankService {

    @Autowired EmployeeRankDao employeeRankDao;


    public List<Combo> getRankService (){

 return employeeRankDao.getRankService();
    }

    @Override
    public void createEmployeeRank(EmployeeRank employeeRank) {
         employeeRankDao.createEmployeeRank(employeeRank);
    }


    @Override
    public void deleteEmployeeRank(Long Id) {
        employeeRankDao.deleteEmployeeRank(Id);
    }


    @Override
    public void updateRank(EmployeeRank employeeRank) {
        employeeRankDao.updateRank(employeeRank);
    }
}
