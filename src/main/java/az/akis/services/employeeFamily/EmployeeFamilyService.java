package az.akis.services.employeeFamily;

import az.akis.models.Combo;

import java.util.List;

public interface EmployeeFamilyService {

    public List<Combo> getRelativeDegrees();

    public List<Combo> getWorkLocations();


}
