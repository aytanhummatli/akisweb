package az.akis.services.employeeFamily;

import az.akis.dao.employeeFamily.EmployeeFamilyDao;
import az.akis.models.Combo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class EmployeeFamilyServiceImpl implements EmployeeFamilyService{

@Autowired
EmployeeFamilyDao employeeFamilyDao;

    @Override
    public List<Combo> getRelativeDegrees() {


        return employeeFamilyDao.getRelativeDegrees();

    }

    @Override
    public List<Combo> getWorkLocations() {


        return employeeFamilyDao.getWorkLocations();

    }


}
