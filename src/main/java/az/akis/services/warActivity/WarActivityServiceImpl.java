package az.akis.services.warActivity;

import az.akis.dao.warActivityDao.WarActivityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WarActivityServiceImpl implements WarActivityService {


    @Autowired
    WarActivityDao warActivityDao;

    @Override
    public void deleteWarActivity(Long elementId) {

      warActivityDao.deleteWarActivity(elementId);

    }
}
