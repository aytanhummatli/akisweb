package az.akis.services.employeeInjured;

import az.akis.dao.EmployeeInjured.EmployeeInjuredDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeInjuredServiceImpl implements EmployeeInjuredService {


    @Autowired
    EmployeeInjuredDao employeeInjuredDao;

    @Override
    public void deleteInjured(Long elementId) {

      employeeInjuredDao.deleteInjured(elementId);

    }
}
