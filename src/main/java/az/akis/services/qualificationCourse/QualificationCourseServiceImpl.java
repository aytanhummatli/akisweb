package az.akis.services.qualificationCourse;

import az.akis.dao.qualificationCourse.QualificationCourseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QualificationCourseServiceImpl implements QualificationCourseService {


    @Autowired
    QualificationCourseDao qualificationCourseDao;

    @Override
    public void deleteQualificationCourse(Long elementId) {

      qualificationCourseDao.deleteQualificationCourse(elementId);

    }
}
