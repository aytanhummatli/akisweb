package az.akis.services.languageLevelService;

import az.akis.dao.languageLevel.LanguageLevelDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LanguageLevelServiceImpl implements LanguageLevelService {

@Autowired
    LanguageLevelDao languageLevelDao;
    @Override
    public void deleteLanguageLevel(Long elementId) {
        languageLevelDao.deleteLanguageLevel(elementId);
    }
}
