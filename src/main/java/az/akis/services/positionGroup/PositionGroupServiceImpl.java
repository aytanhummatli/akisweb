package az.akis.services.positionGroup;

import az.akis.dao.positionGroup.PositionGroupDao;
import az.akis.models.PositionGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PositionGroupServiceImpl implements PositionGroupService {
    @Autowired
    private PositionGroupDao positionGroupDao;


    @Override
    public PositionGroup getPositionGroupById(Long id) {
        System.out.println("services dao gethpg by id" + id);
        return positionGroupDao.getPositionGroupById(id);
    }

    @Override
    public List<PositionGroup> getPositionGroupList(int itemsPerPage, int pageNumber) {
        return positionGroupDao.getPositionGroupList(itemsPerPage, pageNumber);
    }

    @Override
    public void addPositionGroup(String name) {
        positionGroupDao.addPositionGroup(name);
    }

    @Override
    public void updatePositionGroup(String name, Long id) {
        positionGroupDao.updatePositionGroup(name, id);
    }

    @Override
    public void deletePositionGroup(Long id) {
        positionGroupDao.deletePositionGroup(id);
    }
}
