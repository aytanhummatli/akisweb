package az.akis.services.positionGroup;

import az.akis.models.PositionGroup;

import java.util.List;


public interface PositionGroupService {

    PositionGroup getPositionGroupById(Long id);

    List<PositionGroup> getPositionGroupList(int itemsPerPage, int pageNumber);

    void addPositionGroup(String name);

    void updatePositionGroup(String name, Long id);

    void deletePositionGroup(Long id);
}
