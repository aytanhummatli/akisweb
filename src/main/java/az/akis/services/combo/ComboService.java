package az.akis.services.combo;

import az.akis.models.Combo;

import java.util.List;

public interface ComboService {

    public List<Combo> getInfoForCombo(Long comboNo);
}
