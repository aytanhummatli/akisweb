package az.akis.services.combo;

import az.akis.dao.combo.ComboDao;
import az.akis.models.Combo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComboServiceImpl implements ComboService {

    @Autowired
    ComboDao comboDao;

    @Override
    public List<Combo> getInfoForCombo(Long comboNo) {

        return comboDao.getInfoForCombo(comboNo);
    }



}
