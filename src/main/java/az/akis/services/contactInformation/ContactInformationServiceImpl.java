package az.akis.services.contactInformation;

import az.akis.dao.contactInformation.ContactInformationDao;
import az.akis.models.ContactInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactInformationServiceImpl implements ContactInformationService{

   @Autowired
   private ContactInformationDao contactInformationDao;

    @Override
    public void addContactInfo(Long personId,ContactInformation contactInformation) {

        contactInformationDao.addContactInfo(personId,contactInformation);
    }

    @Override
    public List<ContactInformation> getAllContacts() {


        return contactInformationDao.getAllContacts();
    }

}
