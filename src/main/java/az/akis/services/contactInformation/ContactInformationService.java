package az.akis.services.contactInformation;

import az.akis.models.ContactInformation;

import java.util.List;


public interface ContactInformationService {

    public void addContactInfo(Long personId,ContactInformation contactInformation);
    public List<ContactInformation> getAllContacts();



}
