package az.akis.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.sql.DataSource;


@Configuration
@EnableWebMvc
@ComponentScan(basePackages ="az.akis.*" )
@PropertySource(value={"classpath:application.properties"})
//@Import({SecurityConfig.class})

public class WebConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private Environment env;


    @Bean
    public InternalResourceViewResolver viewResolver (){
        InternalResourceViewResolver resolver=new InternalResourceViewResolver();
        resolver.setPrefix("/view/pages/");
        resolver.setSuffix(".jsp");
         return resolver;
       }

     @Bean
    public DataSource dataSource(){
    DriverManagerDataSource dataSource=new DriverManagerDataSource();
    dataSource.setDriverClassName(env.getRequiredProperty("akisweb.driver"));
    dataSource.setUrl(env.getRequiredProperty("akisweb.url"));
    dataSource.setUsername(env.getRequiredProperty("akisweb.username"));
    dataSource.setPassword(env.getRequiredProperty("akisweb.password"));

    //System.out.println(dataSource);
    return dataSource;
}

   /* @Bean
    public DataSource dataSource(){
        DriverManagerDataSource dataSource=new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getRequiredProperty("mySql.driver"));
        dataSource.setUrl(env.getRequiredProperty("mySql.url"));
        dataSource.setUsername(env.getRequiredProperty("mySql.username"));
        dataSource.setPassword(env.getRequiredProperty("mySql.password"));

        //System.out.println(dataSource);
        return dataSource;
    }
*/

    @Bean
    public JdbcTemplate jdbcTemplate(){
        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource());
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        return jdbcTemplate;

      }

 /*   @Bean
    public JdbcTemplate OraclejdbcTemplate(){
        JdbcTemplate jdbcTemplate=new JdbcTemplate(DataSourceOracle());
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        return jdbcTemplate;

    }*/

      @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry.addResourceHandler( "/resources/**").addResourceLocations("/resources/");
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
        registry.addResourceHandler("/layout/**").addResourceLocations("/layout/");
        registry.addResourceHandler("/static/resource/**").addResourceLocations("/static/resource/");



         // registry.addResourceHandler("/static/resource/**").addResourceLocations("/static/resource/");
          registry.addResourceHandler("/static/angular/**").addResourceLocations("/static/angular/");
          registry.addResourceHandler("/view/part/**").addResourceLocations("/view/part/");
          registry.addResourceHandler("/view/pages/**").addResourceLocations("/view/pages/");
          registry.addResourceHandler("/select2-develop/select2-develop/**").addResourceLocations("/select2-develop/select2-develop/");


          super.addResourceHandlers(registry);
          // use resource instead of static

}

    @SuppressWarnings("deprecation")
    @Bean
    public static NoOpPasswordEncoder nopass() {
        return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
    }


}