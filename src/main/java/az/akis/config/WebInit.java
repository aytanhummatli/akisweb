package az.akis.config;


import az.akis.config.SecurityLogin.SecurityConfig;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;


public class WebInit extends AbstractAnnotationConfigDispatcherServletInitializer{

   /*
    protected Class<?>[] getRootConfigClasses() { return new Class[]{RootConfig.class};
    }

    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebConfig.class};
    }
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    */

    //

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] { WebConfig.class ,SecurityConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return null;
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }

    @Override
    protected Filter[] getServletFilters() {
       // Filter [] singleton = { new CORSFilter() };
       // return singleton;
        return new Filter[] {new CharacterEncodingFilter()};

    }


}
