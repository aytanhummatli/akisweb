package az.akis.controller;

import az.akis.models.ContactInformation;
import az.akis.services.contactInformation.ContactInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping(value = "/contact")
public class ContactInformationController {

    @Autowired
    private ContactInformationService contactInformationService;

    //-------------------Add info--------------------------------------------------------

   @RequestMapping(value = "/contact/addContactInfo/{personId}", method = RequestMethod.POST)
    public ResponseEntity<Void> createRank(@PathVariable Long personId,@RequestBody ContactInformation contactInformation) {

       System.out.println("empId="+personId+"   continf= "+contactInformation.toString());

        contactInformationService.addContactInfo(personId, contactInformation);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

 //------------------- Update a User --------------------------------------------------------

//    @RequestMapping(value = "/rank/update", method = RequestMethod.PUT)
//    public ResponseEntity<Void> updateRank( @RequestBody Rank rank) {
//
//        System.out.println("i am in UpdateRank method from rankController ");
//
//        rankService.updateRank(rank);
//
//        return new ResponseEntity<Void>(HttpStatus.OK);
//    }
//
//    //------------------- Delete a Rank --------------------------------------------------------
//
//    @RequestMapping(value = "/rank/{id}", method = RequestMethod.DELETE)
//    public ResponseEntity<Void> deleteArticle(@PathVariable Integer id) {
//        //Rank Rank = rankService.findById(id);
//
//        rankService.deleteRankById(id);
//
//        return new ResponseEntity<Void>(HttpStatus.OK);
//    }
//
//
//    //------------------- Delete All Users --------------------------------------------------------
//
//    @RequestMapping(value = "/rank/", method = RequestMethod.DELETE)
//    public ResponseEntity<Rank> deleteAllUsers() {
//        System.out.println("Deleting All Users");
//
//        rankService.deleteAllRanks();
//
//        return new ResponseEntity<Rank>(HttpStatus.NO_CONTENT);
//    }
//
//------------------- Get Contact informations Types --------------------------------------------------------

    @RequestMapping(value = "/contact/getAllContacts", method = RequestMethod.GET)
    public ResponseEntity<List<ContactInformation>> getAllContacts() {

        List<ContactInformation> contactInformationList= contactInformationService.getAllContacts();

        System.out.println("i am contact controller"+contactInformationList);

        if(contactInformationList.isEmpty()){

            System.out.println("list boosdur ");

            return new ResponseEntity<List<ContactInformation>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND

        }
        return new ResponseEntity<List<ContactInformation>>(contactInformationList, HttpStatus.OK);
    }

//  //  --------------------------------------********--------------------------------------------
//    @RequestMapping(value = "/rank/setRankId/{id}", method = RequestMethod.GET)
//    public ResponseEntity<Void> setEditId(@PathVariable Integer id) {
//        System.out.println("setRankId  "+ id);
//
//        rankId = id;
//        return new ResponseEntity<Void>(HttpStatus.OK);
//    }
//
//    //  --------------------------------------********--------------------------------------------
//
//    @RequestMapping(value = "/rank/getRankId", method = RequestMethod.GET)
//    public ResponseEntity<Integer> getEditId() {
//
//        System.out.println("getranlkid=  "+rankId);
//        return new ResponseEntity<Integer>(rankId, HttpStatus.OK);
//    }
////-------------------------------------******PAGINATION*********---------------------------------------------

//    @RequestMapping(value = "/rank/pagination/{itemsPerPage}/{pageno}", method = RequestMethod.GET)
//
//    public ResponseEntity<List<Rank>> listAllRanksn(@PathVariable Integer itemsPerPage ,@PathVariable Integer  pageno ) {
//
//
//        System.out.println("AllRanksByPage ");
//        List<Rank> ranks = rankService.findAllRanksByPage(pageno,itemsPerPage);
//
//      for (Rank rankList:ranks) {
//            System.out.println("Rank siyahi geldi: "+ rankList.getId() +"   "+rankList.getRankType().getType());
//            }
//
//        if(ranks.isEmpty()){
//            System.out.println("list boosdur  ");
//            return new ResponseEntity<List<Rank>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
//
//        }
//        return new ResponseEntity<List<Rank>>(ranks, HttpStatus.OK);
//    }


}

