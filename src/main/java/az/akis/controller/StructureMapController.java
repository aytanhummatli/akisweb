package az.akis.controller;


import az.akis.services.StructureMap.StService;
import az.akis.dao.StructureMap.StMapDao;
import az.akis.models.OrganType;
import az.akis.models.TreeSimple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
public class StructureMapController {

    @Autowired
    private StService stService;

    @Autowired
    private StMapDao stMapDao;


    @RequestMapping(value = "/StructureMap/getParents/{parentId}", method = RequestMethod.GET)
    public ResponseEntity<List<TreeSimple>> listAllParents(@PathVariable("parentId") Long parentId) {
        int id= 1500000000;
        List<TreeSimple> parents=new ArrayList<>();

           parents = stService.findChilds(parentId);

        if(parents.isEmpty()){

            System.out.println("list boosdur ");

            return new ResponseEntity<List<TreeSimple>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND

        }
        return new ResponseEntity<List<TreeSimple>>(parents, HttpStatus.OK);
    }



    @RequestMapping(value = "/StructureMap/getElementById/{id}", method = RequestMethod.GET)

    public ResponseEntity<TreeSimple> getElementById(@PathVariable("id") Long id) {

      TreeSimple results =stService.getElementById(id);

        System.out.println("results from getOrgantypeById ="+results);

        return new ResponseEntity<TreeSimple>(results,HttpStatus.CREATED);
    }

    @RequestMapping(value = "/StructureMap/searchResult/{searchedValue}", method = RequestMethod.GET)
    public ResponseEntity<List<TreeSimple>> checkParents(@PathVariable("searchedValue") String searchedValue) {

     //  System.out.println("i am in st controller from listallparents metood");

        List<TreeSimple> results = stService.searchResults(searchedValue);

        return new  ResponseEntity<List<TreeSimple>> (results, HttpStatus.OK);

    }

    @RequestMapping(value = "/StructureMap/removeElement/{id}", method = RequestMethod.GET)
      public ResponseEntity<Void> removeElement(@PathVariable("id") Long id) {

         stService.removeElement(id);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }


    @RequestMapping(value = "/StructureMap/addElement", method = RequestMethod.POST)
    public ResponseEntity<BigDecimal> addElement(@RequestBody TreeSimple element) {

        BigDecimal x= stService.addElement(element);

        return new ResponseEntity<BigDecimal>(x, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/StructureMap/updateElement", method = RequestMethod.POST)
    public ResponseEntity<Void> updateElement(@RequestBody TreeSimple element) {


        stService.updateElement(element);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }


    @RequestMapping(value = "/StructureMap/getOrgantypeById/{id}", method = RequestMethod.GET)

    public ResponseEntity<List<OrganType>> getOrgantypeById(@PathVariable("id") Long id) {

        List<OrganType> results =stService.getOrgantypeById(id);

        System.out.println("results from getOrgantypeById ="+results);

        return new ResponseEntity<List<OrganType>>(results,HttpStatus.CREATED);
    }

    @RequestMapping(value = "/StructureMap/getOrgantypes", method = RequestMethod.GET)

    public ResponseEntity<List<OrganType>> getOrgantypes( ) {

        List<OrganType> results =stService.getOrgantypes();

        System.out.println("results from getOrgantypeById ="+results);

        return new ResponseEntity<List<OrganType>>(results,HttpStatus.CREATED);
    }


}

