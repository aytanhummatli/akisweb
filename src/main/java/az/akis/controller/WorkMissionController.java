package az.akis.controller;

import az.akis.services.workMission.WorkMissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WorkMissionController {

     @Autowired
     WorkMissionService workMissionService;


    @RequestMapping(value = "/workMission/deleteWorkMission/{elementId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> deleteRelative (@PathVariable Long elementId) {

        workMissionService.deleteWorkMission(elementId);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
