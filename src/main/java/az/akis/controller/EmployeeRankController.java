package az.akis.controller;

import az.akis.models.Combo;
import az.akis.models.EmployeeRank;
import az.akis.services.employeeRank.EmployeeRankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class EmployeeRankController {

    @Autowired
    EmployeeRankService employeeRankService;

    @RequestMapping(value = "/employeeRank/getRankService", method = RequestMethod.GET)
    public ResponseEntity<List<Combo>> getRankServiceAndRanks() {

        List<Combo> rankServiceList = new ArrayList<Combo>();

        rankServiceList = employeeRankService.getRankService();

        return new ResponseEntity<List<Combo>>(rankServiceList, HttpStatus.OK);
    }

    @RequestMapping(value = "/EmployeeRank/createEmployeeRank", method = RequestMethod.POST)
    public ResponseEntity<Void> createEmployeeRank(@RequestBody EmployeeRank employeeRank) {

        System.out.println("employeeRank" + employeeRank);

     // employeeRankService.createEmployeeRank(employeeRank);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }


    @RequestMapping(value = "/EmployeeRank/delete/{Id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> deleteRank(@PathVariable Long Id) {

        System.out.println("i am in delete method from rankController " + Id);

        employeeRankService.deleteEmployeeRank(Id);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/EmployeeRank/update", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateRank(@RequestBody EmployeeRank employeeRank) {

        System.out.println("employeerank for update"+employeeRank);

        employeeRankService.updateRank(employeeRank);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }






}