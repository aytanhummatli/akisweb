package az.akis.controller;

import az.akis.services.employeeInjured.EmployeeInjuredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeInjuredController {

     @Autowired
     EmployeeInjuredService employeeInjuredService;


    @RequestMapping(value = "/employeeInjured/deleteEmployeeInjured/{elementId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> deleteEmployeeInjured (@PathVariable Long elementId) {

        employeeInjuredService.deleteInjured(elementId);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
