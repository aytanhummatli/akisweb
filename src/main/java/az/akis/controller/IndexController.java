package az.akis.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller

public class IndexController {


    @RequestMapping( value="/", method = RequestMethod.GET)
    public String getIndexPage()
    {
        return "login2";
    }

    @RequestMapping(value = "/mainRanks")
    public String mainRank() {

        return "Ranks";
    }

    @RequestMapping(value = "/structureMap")
    public String structureMap() {

        return "StructureMap";
    }

    @RequestMapping(value = "/staffSchedule")
    public String staffSchedule() {

        return "staffSchedule";
    }

    @RequestMapping(value = "/employees")
    public String employees() {

        return "employees";
    }

    @RequestMapping(value = "/employee")
    public String employee() {

        return "employee";
    }

    @RequestMapping(value = "/employeeGeneral")
    public String employeeGeneral() {

        return "employeeGeneral";
    }

    @RequestMapping(value = "/employeeDetails")
    public String employeeDetails() {

        return "employeeDetails";
    }

    @RequestMapping(value = "/employeeEducation")
    public String employeeEducation() {

        return "employeeEducation";
    }

    @RequestMapping(value = "/employeeFamily")
    public String employeeFamily() {

        return "employeeFamily";
    }

    @RequestMapping(value = "/employeeDİO")
    public String employeeDİO() {

        return "employeeDİO";
    }

    @RequestMapping(value = "/employeeEzam")
    public String employeeEzam() {

        return "employeeEzam";
    }

    @RequestMapping(value = "/employeeMukCeza")
    public String employeeMukCeza() {

        return "employeeMukCeza";
    }

    @RequestMapping(value = "/employeeMezYar")
    public String employeeMezYar() {

        return "employeeMezYar";
    }

    @RequestMapping(value = "/employeeOther")
    public String employeeOther() {

        return "employeeOther";
    }

}