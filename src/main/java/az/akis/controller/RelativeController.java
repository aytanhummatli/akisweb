package az.akis.controller;

import az.akis.models.Combo;
import az.akis.services.relative.RelativeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RelativeController {

     @Autowired
    RelativeService relativeService;

    @RequestMapping(value = "/relative/getRelativeDegreeList", method = RequestMethod.GET)
    public ResponseEntity<List<Combo>> getRelativeDegreeList() {
        List<Combo> relativeDegreeList = relativeService.getRelativeDegreeList();
        if(relativeDegreeList.isEmpty()){
            System.out.println("list boosdur  ");
            return new ResponseEntity<List<Combo>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Combo>>(relativeDegreeList, HttpStatus.OK);
    }


    @RequestMapping(value = "/relative/workList", method = RequestMethod.GET)
    public ResponseEntity<List<Combo>> workList() {
        List<Combo> workList = relativeService.getWorkList();
        if(workList.isEmpty()){
            System.out.println("list boosdur  ");
            return new ResponseEntity<List<Combo>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Combo>>(workList, HttpStatus.OK);
    }

    @RequestMapping(value = "/relative/deleteRelative/{elementId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> deleteRelative (@PathVariable Long elementId) {
        System.out.println("relative delete id: "+elementId);
        relativeService.deleteRelative(elementId);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
