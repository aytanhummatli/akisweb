package az.akis.controller;

import az.akis.models.*;
import az.akis.services.rule.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class RuleController {

    @Autowired
    private RuleService ruleService;
    @Autowired
    private UserInfoController userInfoController;

    @RequestMapping(value = "getButtonRuleCombo", method = RequestMethod.GET)
    public ResponseEntity<List<ButtonRule>> getList() {
        List<ButtonRule> buttonRulesList = ruleService.getButtonRuleList();
        if (buttonRulesList.isEmpty()) {
            return new ResponseEntity<List<ButtonRule>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<ButtonRule>>(buttonRulesList, HttpStatus.OK);
    }

    @RequestMapping(value = "getRuleCombo", method = RequestMethod.GET)
    public ResponseEntity<List<Rule>> getCombo() {
        List<Rule> ruleCombo = ruleService.getRuleCombo();
        if (ruleCombo.isEmpty()) {
            return new ResponseEntity<List<Rule>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<Rule>>(ruleCombo, HttpStatus.OK);
    }

    @RequestMapping(value = "getUserRuleListById/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<UserRule>> getRuleList(@PathVariable BigDecimal userId) {
        List<UserRule> list = ruleService.getUserRuleListById(userId);
        if (list.isEmpty()) {
            return new ResponseEntity<List<UserRule>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<UserRule>>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "getUserButtonRuleListById/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<UserButtonRule>> getButtonRuleList(@PathVariable BigDecimal userId) {
        List<UserButtonRule> list = ruleService.getUserButtonRuleListById(userId);
        if (list.isEmpty()) {
            return new ResponseEntity<List<UserButtonRule>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<UserButtonRule>>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "createUserRule/",method = RequestMethod.POST)
    public ResponseEntity<Void> createUserRule(@RequestBody ArrayList<Rule> ruleList){
        for (Rule list:ruleList
             ) {
            System.out.println("userRule list  :  "+list);
        }
        ruleService.createUserRule(ruleList);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "createButtonUserRule/",method = RequestMethod.POST)
    public ResponseEntity<Void> createUserButtonRule(@RequestBody ArrayList<ButtonRule> buttonRuleList){
        for (ButtonRule list:buttonRuleList
             ) {
            System.out.println("Userbutton rule list:    " +list);
        }
        ruleService.createUserButtonRule(buttonRuleList);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "updateUserRule/",method = RequestMethod.POST)
    public ResponseEntity<Void> updateUserRule(@RequestBody ArrayList<Rule> userRuleList){
        UserInfo userInfo=userInfoController.getUserInfo();
        System.out.println("info   "+userInfo);
        ruleService.updateUserRule(userRuleList,userInfo);
        return new  ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "updateUserButtonRule/",method = RequestMethod.POST)
    public ResponseEntity<Void> updateUserButtonRule(@RequestBody ArrayList<ButtonRule> userButtonRuleList){
        UserInfo userInfo=userInfoController.getUserInfo();
        System.out.println("info   "+userInfo);
        ruleService.updateUserButtonRule(userButtonRuleList,userInfo);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
