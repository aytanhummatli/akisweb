package az.akis.controller;

import az.akis.models.Nomenclature;
import az.akis.services.nomenclature.NomenclatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.math.BigDecimal;
import java.util.List;

@Controller
public class NomenclatureController {
    @Autowired
    private NomenclatureService nomenclatureService;

    @RequestMapping(value = "/nomenclature", method = RequestMethod.GET)
    public String nomenclature() {
        return "nomenclature";
    }

    @RequestMapping(value = "/nomenklature/getNomenkForSelect", method = RequestMethod.GET)
    public ResponseEntity<List<Nomenclature>> getNomenkForSelect() {

        List<Nomenclature> nomenList = nomenclatureService.getNomenkForSelect();

        if (nomenList.isEmpty()) {

            return new ResponseEntity<List<Nomenclature>>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<List<Nomenclature>>(nomenList, HttpStatus.OK);
    }

    @RequestMapping(value = "/nomenklature/getSearchResults/{elementName}", method = RequestMethod.GET)
    public ResponseEntity<List<Nomenclature>> getSearchResults(@PathVariable String elementName) {


        List<Nomenclature> nomenList = nomenclatureService.getSearchResults(elementName);

        if (nomenList.isEmpty()) {

            return new ResponseEntity<List<Nomenclature>>(HttpStatus.NOT_FOUND);

        }

        return new ResponseEntity<List<Nomenclature>>(nomenList, HttpStatus.OK);
    }

    @RequestMapping(value = "getNomenclatureList/{rowsPerPage}/{pageNumber}", method = RequestMethod.GET)
    public ResponseEntity<List<Nomenclature>> getList(@PathVariable int rowsPerPage, @PathVariable int pageNumber) {

        List<Nomenclature> nomenList = nomenclatureService.getNomenclatureList(rowsPerPage, pageNumber);

        if (nomenList.isEmpty()) {

            return new ResponseEntity<List<Nomenclature>>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<List<Nomenclature>>(nomenList, HttpStatus.OK);
    }

    @RequestMapping(value = "getNomListById/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Nomenclature>> getListById(@PathVariable Long id) {

        List<Nomenclature> list = nomenclatureService.getNomListById(id);

        if (list.isEmpty()) {
            return new ResponseEntity<List<Nomenclature>>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<List<Nomenclature>>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "getComboList", method = RequestMethod.GET)
    public ResponseEntity<List<Nomenclature>> list() {
        List<Nomenclature> comboList = nomenclatureService.getComboList();
        if (comboList.isEmpty()) {
            return new ResponseEntity<List<Nomenclature>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<Nomenclature>>(comboList, HttpStatus.OK);
    }

    @RequestMapping(value = "getNomNameById/{id}", method = RequestMethod.GET)
    public ResponseEntity<Nomenclature> getById(@PathVariable BigDecimal id) {
        Nomenclature nom = nomenclatureService.getNomNameById(id);
        if (nom == null) {
            return new ResponseEntity<Nomenclature>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Nomenclature>(nom, HttpStatus.OK);
    }

    @RequestMapping(value = "deleteNom/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        nomenclatureService.deleteNom(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }


    @RequestMapping(value = "createNom", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody Nomenclature nom) {
        System.out.println("nom object " + nom.toString());
        nomenclatureService.createNom(nom.getName(), nom.getStr());
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "createChildNom", method = RequestMethod.POST)
    public ResponseEntity<Void> createChild(@RequestBody Nomenclature nom) {
        nomenclatureService.createChildNom(nom);
        System.out.println("child nom  object : " + nom.toString());
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "updateNom", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@RequestBody Nomenclature nom) {
        nomenclatureService.updateNom(nom);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "deleteChildNom/{id}", method = RequestMethod.POST)
    public ResponseEntity<Void> delete(@PathVariable long id) {
        nomenclatureService.deleteChildNom(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
