package az.akis.controller;

import az.akis.services.warActivity.WarActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WarActivityController {

     @Autowired
     WarActivityService warActivityService;

    @RequestMapping(value = "/warActivity/deleteWarActivity/{elementId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> deleteWarActivity (@PathVariable Long elementId) {

        warActivityService.deleteWarActivity(elementId);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
