package az.akis.controller;

import az.akis.services.workActivityBefore.WorkActivityBeforeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WorkActivityBeforeController {

     @Autowired
     WorkActivityBeforeService workActivityBeforeService;


    @RequestMapping(value = "/workActivityBefore/deleteWorkActivityBefore/{elementId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> deleteWorkActivityBefore (@PathVariable Long elementId) {

        workActivityBeforeService.deleteWorkActivity(elementId);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
