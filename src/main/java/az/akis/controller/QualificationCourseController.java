package az.akis.controller;

import az.akis.services.qualificationCourse.QualificationCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QualificationCourseController {


    @Autowired
    QualificationCourseService qualificationCourseService;

    @RequestMapping(value = "/qualificationCourse/deleteQualificationCourse/{elementId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> deleteQualificationCourse (@PathVariable Long elementId) {

        qualificationCourseService.deleteQualificationCourse(elementId);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
