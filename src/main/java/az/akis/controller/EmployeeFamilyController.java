package az.akis.controller;

import az.akis.models.Combo;
import az.akis.services.employeeFamily.EmployeeFamilyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class EmployeeFamilyController {

    @Autowired
    EmployeeFamilyService employeeFamilyService;


    @RequestMapping(value = "/EmployeeFamily/getRelativeDegrees",method = RequestMethod.GET)
    public ResponseEntity<List<Combo>> getRelativeDegrees() {

        List<Combo> relativeDegreeList = new ArrayList<>();

        relativeDegreeList = employeeFamilyService.getRelativeDegrees();


        return new ResponseEntity<List<Combo>>(relativeDegreeList, HttpStatus.OK);
    }

    @RequestMapping(value = "/EmployeeFamily/getWorkLocations",method = RequestMethod.GET)
    public ResponseEntity<List<Combo>> getWorkLocations() {

        List<Combo> getWorkLocationsList = new ArrayList<>();

        getWorkLocationsList = employeeFamilyService.getWorkLocations();


        return new ResponseEntity<List<Combo>>(getWorkLocationsList, HttpStatus.OK);
    }

}
