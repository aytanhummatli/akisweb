package az.akis.controller;

import az.akis.models.Combo;
import az.akis.services.combo.ComboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ComboController {


    @Autowired
    ComboService comboService;

    @RequestMapping(value = "/combo/getInfoForCombo/{comboNo}", method = RequestMethod.GET)
    public ResponseEntity<List<Combo>> getInfoForCombo(@PathVariable Long comboNo) {

        List<Combo> infoList = new ArrayList<>();

        infoList = comboService.getInfoForCombo(comboNo);

        return new ResponseEntity<List<Combo>>(infoList, HttpStatus.OK);
    }

}
