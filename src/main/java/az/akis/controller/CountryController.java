package az.akis.controller;

import az.akis.models.Country;
import az.akis.services.country.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class CountryController {

@Autowired
CountryService countryService;

    @RequestMapping(value = "/country/getCountryList", method = RequestMethod.GET)
    public ResponseEntity<List<Country>> getCountryList() {



        List<Country> countryList = countryService.getCountryList();
        System.out.println("i am in CountryController from getCountryList"+countryList);
          if(countryList.isEmpty()){

            System.out.println("list boosdur  ");

            return new ResponseEntity<List<Country>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Country>>(countryList, HttpStatus.OK);
    }



}
