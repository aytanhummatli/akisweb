package az.akis.controller;

import az.akis.models.SubRegion;
import az.akis.services.subRegion.SubRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class SubRegionController {

@Autowired
SubRegionService subRegionService;

    @RequestMapping(value = "/subRegion/getSubRegionList", method = RequestMethod.GET)
    public ResponseEntity<List<SubRegion>> getRegionList() {

        List<SubRegion> subRegionList = subRegionService.getSubRegionList();

        System.out.println("i am in SubRegionController from getSubRegionList" );

          if(subRegionList.isEmpty()){

            System.out.println("list boosdur  ");

            return new ResponseEntity<List<SubRegion>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<SubRegion>>(subRegionList, HttpStatus.OK);
    }

}
