package az.akis.controller;

import az.akis.models.PositionGroup;
import az.akis.services.positionGroup.PositionGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class PositionGroupController {
    @Autowired
    private PositionGroupService positionGroupService;
    public Long hpgId;

//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public ModelAndView getHrPositionGroup() {
//        ModelAndView modelAndView = new ModelAndView("index");
//        return modelAndView;
//    }

    @RequestMapping(value = "/positionGroup", method = RequestMethod.GET)
    public String positionGroup() {
        return "positionGroup";
    }


    @RequestMapping(value = "getPositionGroupById/{id}", method = RequestMethod.GET)
    public ResponseEntity<PositionGroup> getPositionGroupById(@PathVariable Long id) {
        PositionGroup groupById = positionGroupService.getPositionGroupById(id);
        if (groupById.equals(null)) {
            return new ResponseEntity<PositionGroup>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<PositionGroup>(groupById, HttpStatus.OK);
    }

    @RequestMapping(value = "setEditId/{id}", method = RequestMethod.GET)
    public ResponseEntity<Void> setEditId(@PathVariable Long id) {
        hpgId = id;
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "getEditId", method = RequestMethod.GET)
    public ResponseEntity<Long> getEditId() {
        return new ResponseEntity<Long>(hpgId, HttpStatus.OK);
    }

    @RequestMapping(value = "getPositionGroupList/{itemsPerPage}/{pageNumber}", method = RequestMethod.GET)
    public ResponseEntity<List<PositionGroup>> getGroupList(@PathVariable int itemsPerPage, @PathVariable int pageNumber) {
        List<PositionGroup> groupList = positionGroupService.getPositionGroupList(itemsPerPage, pageNumber);
        if (groupList.isEmpty()) {
            return new ResponseEntity<List<PositionGroup>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<PositionGroup>>(groupList, HttpStatus.OK);
    }

    @RequestMapping(value = "addPositionGroup", method = RequestMethod.POST)
    public ResponseEntity<Void> addPositionGroup(@RequestBody PositionGroup hrPositionGroup) {
        positionGroupService.addPositionGroup(hrPositionGroup.getName());
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "updatePositionGroup", method = RequestMethod.PUT)
    public ResponseEntity<Void> updatePositionGroup(@RequestBody PositionGroup positionGroup) {
        positionGroupService.updatePositionGroup(positionGroup.getName(), positionGroup.getId());
        System.out.println("id :"+positionGroup.getId().toString());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "deletePositionGroup/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> deletePositionGroup(@PathVariable Long id) {
        positionGroupService.deletePositionGroup(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
