package az.akis.controller;

import az.akis.models.Village;
import az.akis.services.village.VillageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class VillageController {

@Autowired
VillageService villageService;

    @RequestMapping(value = "/village/getVillageList", method = RequestMethod.GET)
    public ResponseEntity<List<Village>> getVillageList() {

        List<Village> villageList = villageService.getVillageList();

        System.out.println("i am in RegionController from getRegionList" );

          if(villageList.isEmpty()){

            System.out.println("list boosdur  ");

            return new ResponseEntity<List<Village>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Village>>(villageList, HttpStatus.OK);
    }

}
