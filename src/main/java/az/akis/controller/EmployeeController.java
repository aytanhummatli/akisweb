package az.akis.controller;


import az.akis.services.Employee.EmployeeService;
import az.akis.services.StructureMap.StService;
import az.akis.models.Employee;
import az.akis.models.EmployeePosition;
import az.akis.models.StuffSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController()
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private StService stService;

    public Long employeeId;

    @RequestMapping(value = "/Staff/getEmployees/", method = RequestMethod.POST)
    public ResponseEntity<List<StuffSchedule>> getEmployees(@RequestBody StuffSchedule element) {
        System.out.println("xvcvcvbcb");
        List<StuffSchedule> parents = new ArrayList<>();

        Long num_id = Long.valueOf(1);

        parents = stService.getEmployees(element.getId(), num_id, element.getPos_name_id());

        if (parents.isEmpty()) {

            System.out.println("list boosdur ");

            return new ResponseEntity<List<StuffSchedule>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND

        }
        return new ResponseEntity<List<StuffSchedule>>(parents, HttpStatus.OK);
    }

    @RequestMapping(value = "/Employee/get_employees/{st_id}/{ed_surname}/{ed_patron}/{ed_name}/{pageno}/{itemsPerPage}", method = RequestMethod.GET)
    public ResponseEntity<List<Employee>> get_employees(
            @PathVariable Long st_id,
            @PathVariable String ed_surname,
            @PathVariable String ed_patron,
            @PathVariable String ed_name,
            @PathVariable Long pageno,
            @PathVariable Long itemsPerPage) {

        List<Employee> employeeList = employeeService.get_employees(st_id, ed_surname, ed_patron, ed_name, pageno, itemsPerPage);

        //    System.out.println("i am hereeeeee");

        for (Employee list : employeeList) {

            //  System.out.println("Employee siyahi geldi: "+ list);

            // System.out.println("dateeeeeee"+list.getPerson().getBirthDate());

        }
        if (employeeList.isEmpty()) {

            System.out.println("list boosdur  ");

            return new ResponseEntity<List<Employee>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Employee>>(employeeList, HttpStatus.OK);

    }


    //-------------------Retrieve Selected Employee Details--------------------------------------------------------

    @RequestMapping(value = "/Employee/getEmployeebyId/{empId}", method = RequestMethod.GET)
    public ResponseEntity<Employee> getEmployeebyId(@PathVariable("empId") Long empId) {

        System.out.println("i am in getEmployeebyId method from employeeController");

        Employee employee = employeeService.getEmployeeById(empId);
        //   System.out.println("Employee="+employee);
        if (employee == null) {

            System.out.println("Employee with id " + empId + " not found");

            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }


    @RequestMapping(value = "/Employee/getGeneralIngormationById/{empId}", method = RequestMethod.GET)
    public ResponseEntity<Employee> getGeneralIngormationById(@PathVariable("empId") Long empId) {

        System.out.println("i am in getEmployeebyId method from employeeController");

        Employee employee = employeeService.getGeneralIngormationById(empId);

        // System.out.println("Employee="+employee);

        if (employee == null) {

            System.out.println("Employee with id " + empId + " not found");

            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }


    @RequestMapping(value = "/Employee/getEmployeeEducation/{empId}", method = RequestMethod.GET)
    public ResponseEntity<Employee> getEmployeeEducation(@PathVariable("empId") Long empId) {

        Employee employee = employeeService.getEducationLevel(empId);

        // System.out.println("Employee="+employee);

        if (employee == null) {

            System.out.println("Employee with id " + empId + " not found");

            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }


    @RequestMapping(value = "/Employee/getEmployeeFamilybyId/{empId}", method = RequestMethod.GET)
    public ResponseEntity<Employee> getEmployeeFamilybyId(@PathVariable("empId") Long empId) {

        Employee employee = employeeService.getEmployeeFamily(empId);

        //   System.out.println("getEmployeeFamilybyId="+employee);

        if (employee == null) {

            System.out.println("Employee with id " + empId + " not found");

            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }


    @RequestMapping(value = "/Employee/getEmployeeDIObyId/{empId}", method = RequestMethod.GET)
    public ResponseEntity<List<EmployeePosition>> getEmployeeDIObyId(@PathVariable("empId") Long empId) {

        List<EmployeePosition> employeePositionList = employeeService.getEmployeeDIObyId(empId);

        //     System.out.println("getEmployeeFamilybyId="+employeePositionList);

        if (employeePositionList == null) {

            System.out.println("Employee with id " + empId + " not found");

            return new ResponseEntity<List<EmployeePosition>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<EmployeePosition>>(employeePositionList, HttpStatus.OK);
    }

    @RequestMapping(value = "/Employee/getEmployeePrizeAndPunish/{empId}", method = RequestMethod.GET)
    public ResponseEntity<Employee> getEmployeePrizeAndPunish(@PathVariable("empId") Long empId) {

        Employee employee = employeeService.getPrizeAndPunish(empId);

        //  System.out.println("getEmployeePrizeAndPunish="+employee);

        if (employee == null) {

            System.out.println("Employee with id " + empId + " not found");

            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }

    @RequestMapping(value = "/Employee/getEmployeeBeforeDIOandEzam/{empId}", method = RequestMethod.GET)
    public ResponseEntity<Employee> getEmployeeBeforeDIOandEzam(@PathVariable("empId") Long empId) {

        Employee employee = employeeService.getEmployeeBeforeDIOandEzam(empId);

        System.out.println("getEmployeeFamilybyId=" + employee);

        if (employee == null) {

            System.out.println("Employee with id " + empId + " not found");

            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }


    @RequestMapping(value = "/Employee/getEmployeeMezYar/{empId}/{year}", method = RequestMethod.GET)
    public ResponseEntity<Employee> getEmployeeMezYar(@PathVariable("empId") Long empId, @PathVariable("year") Long year) {

        Employee employee = employeeService.getEmployeeMezYaralanma(empId, year);

        //System.out.println("getEmployeeMezYar="+Employee);

        if (employee == null) {

            System.out.println("Employee with id " + empId + " not found");

            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }


    @RequestMapping(value = "/Employee/getEmployeeOther/{empId}", method = RequestMethod.GET)
    public ResponseEntity<Employee> getEmployeeOther(@PathVariable("empId") Long empId) {

        Employee employee = employeeService.getOthers(empId);

        //     System.out.println("getEmployeeFamilybyId="+employee);

        if (employee == null) {

            System.out.println("Employee with id " + empId + " not found");

            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }


    @RequestMapping(value = "/Employee/getEmployeeInfoPrintById/{empId}", method = RequestMethod.GET)
    public ResponseEntity<Employee> getEmpPrint(@PathVariable("empId") Long empId) {
        Employee employee = employeeService.getEmployeeInfoPrint(empId);
        System.out.println("emp info print object:  " + employee.toString());

        if (employee == null) {
            System.out.println("empid: " + empId);
            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
    }


    @RequestMapping(value = "/Employee/setEmployeeId/{empId}", method = RequestMethod.GET)
    public ResponseEntity<Void> setEmployeeId(@PathVariable("empId") Long empId) {

        employeeId = empId;

        System.out.println("employee id from js: " + employeeId);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/Employee/getEmployeeId", method = RequestMethod.GET)
    public ResponseEntity<Long> getEmployeeId() {

        return new ResponseEntity<Long>(employeeId,HttpStatus.OK);
    }



}

