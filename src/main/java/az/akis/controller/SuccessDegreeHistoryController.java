package az.akis.controller;

import az.akis.models.SuccessDegree;
import az.akis.models.SuccessDegreeHistory;
import az.akis.services.successDegreeHistory.SuccessDegreeHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class SuccessDegreeHistoryController {

    @Autowired
    private SuccessDegreeHistoryService successDegreeHistoryService;

    @RequestMapping(value = "/SuccessDegreeHistory/getSuccessDegreeList", method = RequestMethod.GET)
    public ResponseEntity<List<SuccessDegree>> successDegreeList() {

        List<SuccessDegree> myList = new ArrayList<>();

        myList = successDegreeHistoryService.getSuccessdegreeList();

        return new ResponseEntity<List<SuccessDegree>>(myList, HttpStatus.OK);
    }

    @RequestMapping(value = "/SuccessDegreeHistory/insertSuccessDegreeHistory",method = RequestMethod.POST)
    public ResponseEntity<Void> insertSuccessDegreeHistory(@RequestBody SuccessDegreeHistory successDegreeHistory) {
        System.out.println("i am in success insertttt "+successDegreeHistory);
        successDegreeHistoryService.createSuccessdegreeHist(successDegreeHistory);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/SuccessDegreeHistory/updateSuccessDegreeHistory",method = RequestMethod.PUT)
    public ResponseEntity<Void> updateSuccessDegreeHistory(@RequestBody SuccessDegreeHistory successDegreeHistory) {

        successDegreeHistoryService.updateSuccessdegreeHist(successDegreeHistory);

     return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/SuccessDegreeHistory/deleteSuccessHistory/{elementid}", method = RequestMethod.GET)
    public ResponseEntity<Void> deleteSuccessHistory(@PathVariable Long elementId) {

        successDegreeHistoryService.deleteSuccessdegreeHist(elementId);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }


}
