package az.akis.controller;


import az.akis.services.Rank.RankService;
import az.akis.services.RankType.RankTypeService;
import az.akis.models.Rank;
import  az.akis.models.RankType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
@RestController
public class RankController {

    @Autowired
    private RankService rankService;

    @Autowired
    private RankTypeService rankTypeService;
    private Rank rank;
    private UriComponentsBuilder ucBuilder;
    private Integer rankId;


    @RequestMapping(value = "/rank/", method = RequestMethod.GET)
    public ResponseEntity<List<Rank>> listAllRanks() {

        List<Rank> ranks = rankService.findAllRanks();

        if(ranks.isEmpty()){
            System.out.println("list boosdur  ");
            return new ResponseEntity<List<Rank>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND

        }
        return new ResponseEntity<List<Rank>>(ranks, HttpStatus.OK);
    }

    //-------------------Retrieve Single Rank--------------------------------------------------------

    @RequestMapping(value = "/rank/getRankById/{id}", method = RequestMethod.GET)
    public ResponseEntity<Rank> getRank(@PathVariable("id") int id) {
        System.out.println("Fetching Rank with id " + id);
        Rank rank = rankService.findById(id);

        if (rank == null) {
            System.out.println("Rank with id " + id + " not found");
            return new ResponseEntity<Rank>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Rank>(rank, HttpStatus.OK);
    }

    //-------------------Create a User--------------------------------------------------------

    @RequestMapping(value = "/rank/", method = RequestMethod.POST)
    public ResponseEntity<Void> createRank(@RequestBody Rank rank) {

        this.rank = rank;

        System.out.println("Creating Rank " + rank.getName());

        rankService.saveRank(rank);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }



    //------------------- Update a User --------------------------------------------------------

    @RequestMapping(value = "/rank/update", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateRank( @RequestBody Rank rank) {

        System.out.println("i am in UpdateRank method from rankController ");

        rankService.updateRank(rank);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    //------------------- Delete a Rank --------------------------------------------------------

    @RequestMapping(value = "/rank/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteArticle(@PathVariable Integer id) {
        //Rank Rank = rankService.findById(id);

        rankService.deleteRankById(id);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }







    //------------------- Delete All Users --------------------------------------------------------

    @RequestMapping(value = "/rank/", method = RequestMethod.DELETE)
    public ResponseEntity<Rank> deleteAllUsers() {
        System.out.println("Deleting All Users");

        rankService.deleteAllRanks();

        return new ResponseEntity<Rank>(HttpStatus.NO_CONTENT);
    }

//------------------- Get Rank Types --------------------------------------------------------

    @RequestMapping(value = "/rank/getRankTypes/", method = RequestMethod.GET)
    public ResponseEntity<List<RankType>> getRankTypes() {
        System.out.println("i am in rankcontrller with url=/Rank/getRankTypes /   ");

        List<RankType> rankTypes = rankTypeService.findAllRankTypes();

        System.out.println("1111    "+rankTypes.toString());

        if(rankTypes.isEmpty()){
            System.out.println("list boosdur  ");

            return new ResponseEntity<List<RankType>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND

        }
        return new ResponseEntity<List<RankType>>(rankTypes, HttpStatus.OK);
    }

  //  --------------------------------------********--------------------------------------------
    @RequestMapping(value = "/rank/setRankId/{id}", method = RequestMethod.GET)
    public ResponseEntity<Void> setEditId(@PathVariable Integer id) {
        System.out.println("setRankId  "+ id);

        rankId = id;
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    //  --------------------------------------********--------------------------------------------

    @RequestMapping(value = "/rank/getRankId", method = RequestMethod.GET)
    public ResponseEntity<Integer> getEditId() {

        System.out.println("getranlkid=  "+rankId);
        return new ResponseEntity<Integer>(rankId, HttpStatus.OK);
    }
//-------------------------------------******PAGINATION*********---------------------------------------------


    @RequestMapping(value = "/rank/pagination/{itemsPerPage}/{pageno}", method = RequestMethod.GET)

    public ResponseEntity<List<Rank>> listAllRanksn(@PathVariable Integer itemsPerPage ,@PathVariable Integer  pageno ) {


        System.out.println("AllRanksByPage ");
        List<Rank> ranks = rankService.findAllRanksByPage(pageno,itemsPerPage);

      for (Rank rankList:ranks) {
            System.out.println("Rank siyahi geldi: "+ rankList.getId() +"   "+rankList.getRankType().getType());
            }

        if(ranks.isEmpty()){
            System.out.println("list boosdur  ");
            return new ResponseEntity<List<Rank>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND

        }
        return new ResponseEntity<List<Rank>>(ranks, HttpStatus.OK);
    }


}

