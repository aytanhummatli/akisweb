package az.akis.controller;

import az.akis.models.Combo;
import az.akis.models.EmployeeMilitaryService;
import az.akis.services.militaryService.MilitarySService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MilitaryServiceController {


    @Autowired
    MilitarySService militarySService;

public Long userId= Long.valueOf(1);

    @RequestMapping(value = "/MilitaryService/getHerbiMukellefiyyetList", method = RequestMethod.GET)
    public ResponseEntity<List<Combo>> getHerbiMukellefiyyetList() {

        List<Combo> employeeMilitaryServiceList = new ArrayList<>();

        employeeMilitaryServiceList = militarySService.getHerbiMukellefiyyet();

        return new ResponseEntity<List<Combo>>(employeeMilitaryServiceList, HttpStatus.OK);

    }

    @RequestMapping(value = "/MilitaryService/insertHerbiMukellefiyyet", method = RequestMethod.POST
    )
    public ResponseEntity<Void> insertHerbiMukellefiyyet(@RequestBody EmployeeMilitaryService element) {

        System.out.println("military insert "+userId);

        militarySService.insertMilitaryService(element,userId);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }


    @RequestMapping(value = "/MilitaryService/updateHerbiMukellefiyyet", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateHerbiMukellefiyyet(@RequestBody EmployeeMilitaryService element) {

        System.out.println("id="+element.getId());

        militarySService.updateMilitaryService(element);

        System.out.println("military update  "+element);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}


