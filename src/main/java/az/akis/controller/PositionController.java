package az.akis.controller;

import az.akis.models.Position;
import az.akis.models.PositionGroup;
import az.akis.models.PositionName;
import az.akis.services.positionGroup.PositionGroupService;
import az.akis.services.posititon.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class PositionController {
    @Autowired
    PositionGroupService positionGroupService;
    @Autowired
    private PositionService positionService;
    @RequestMapping(value = "/position", method = RequestMethod.GET)
    public String position() {

        return "position";
    }

    @RequestMapping(value = "getPositionList/{itemsPerPage}/{pageNumber}", method = RequestMethod.GET)
    public ResponseEntity<List<PositionName>> getHrPositionList(@PathVariable int itemsPerPage, @PathVariable int pageNumber) {
        List<PositionName> getList = positionService.getPositionList(itemsPerPage, pageNumber);
        System.out.println("list:  "+getList);
        if (getList == null) {
            return new ResponseEntity<List<PositionName>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<PositionName>>(getList, HttpStatus.OK);
    }

    @RequestMapping(value = "addPosition", method = RequestMethod.POST)
    public ResponseEntity<Void> addHrPosition(@RequestBody PositionName positionName) {
        positionService.addPosition(positionName.getName(), positionName.getPosition().getPositionGroup().getId());
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "getPositionGroupNameList", method = RequestMethod.GET)
    public ResponseEntity<List<PositionGroup>> list() {
        List<PositionGroup> nameList = positionService.getGroupNameList();
        if (nameList.isEmpty()) {
            return new ResponseEntity<List<PositionGroup>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<PositionGroup>>(nameList, HttpStatus.OK);
    }

    @RequestMapping(value = "getPositionNamesById/{id}", method = RequestMethod.GET)
    public ResponseEntity<PositionName> getHpyId(@PathVariable Long id) {
        PositionName hpById = positionService.getPositionNamesById(id);
        if (hpById.equals(null)) {
            new ResponseEntity<PositionName>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<PositionName>(hpById, HttpStatus.OK);
    }

    @RequestMapping(value = "updatePosition", method = RequestMethod.PUT)
    public ResponseEntity<Void> updatePosition(@RequestBody PositionName positionName) {
        positionService.updatePosition(positionName.getName(),
                positionName.getPosition().getPositionGroup().getId(),
                positionName.getPosition().getId());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "deletePosition/{id}", method = RequestMethod.POST)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        positionService.deletePosition(id);
        System.out.println("position id :" + id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }


}
