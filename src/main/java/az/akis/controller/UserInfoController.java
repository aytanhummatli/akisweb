package az.akis.controller;

import az.akis.models.UserInfo;
import az.akis.services.userInfo.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@Controller
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;
private UserInfo userInfo;
    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    public String userInfo() {
        return "userInfo";
    }

    @RequestMapping(value = "getUserInfoList/{itemsPerPage}/{pageNumber}", method = RequestMethod.GET)
    public ResponseEntity<List<UserInfo>> getList(@PathVariable int itemsPerPage, @PathVariable int pageNumber) {
        List<UserInfo> list = userInfoService.getUserInfoList(itemsPerPage,pageNumber);
        if (list.isEmpty()) {
            return new ResponseEntity<List<UserInfo>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<UserInfo>>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "searchUserFullName/{fullName}",method = RequestMethod.GET)
    public ResponseEntity<List<UserInfo>> searchUserFullName(@PathVariable String fullName){
        List<UserInfo> combo=userInfoService.getUserFullNameCombo(fullName);
        if(combo.isEmpty()){
          //  return new ResponseEntity<List<UserInfo>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<UserInfo>>(combo,HttpStatus.OK);
    }

    @RequestMapping(value = "searchNomName/{nomName}",method = RequestMethod.GET)
    public  ResponseEntity<List<UserInfo>> searchNomName(@PathVariable String nomName){
        List<UserInfo> combo=userInfoService.getNomNameCombo(nomName);
        if(combo.isEmpty()){
            //return new ResponseEntity<List<UserInfo>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<UserInfo>>(combo,HttpStatus.OK);
    }

    @RequestMapping(value = "createUserInfo/",method = RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody UserInfo userInfo){
        userInfoService.createUserInfo(userInfo);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
    @RequestMapping(value = "getUserNameById/{id}",method = RequestMethod.GET)
    public ResponseEntity<UserInfo> getUserNameById(@PathVariable BigDecimal id){
        UserInfo userInfo=userInfoService.getUserNameById(id);
        if(userInfo == null){
            return new ResponseEntity<UserInfo>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<UserInfo>(userInfo,HttpStatus.OK);
    }

    @RequestMapping(value = "updateUser",method = RequestMethod.PUT)
    public ResponseEntity<Void> updateUser(@RequestBody UserInfo userInfos){
        userInfo=userInfos;
        userInfoService.updateUser(userInfos);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "deleteUser/{id}",method = RequestMethod.PUT)
    public ResponseEntity<Void> deleteUser(@PathVariable long id){
        userInfoService.deleteUser(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }


    @RequestMapping(value = "checkUsername/{username}",method = RequestMethod.POST)
    public ResponseEntity<String> checkUsername(@PathVariable String username){
        String check=userInfoService.checkUsername(username);
        return new ResponseEntity<String>(check,HttpStatus.OK);
    }
@RequestMapping(value = "getNomNameByUserId/{userId}",method = RequestMethod.GET)
    public ResponseEntity<UserInfo> getNomName(@PathVariable BigDecimal userId){
        UserInfo userInfo=userInfoService.getNomNameByUserId(userId);
        if(userInfo==null){
            return new ResponseEntity<UserInfo>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<UserInfo>(userInfo,HttpStatus.OK);
}

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }


}
