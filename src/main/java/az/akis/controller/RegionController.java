package az.akis.controller;

import az.akis.models.Region;
import az.akis.services.region.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class RegionController {

@Autowired
RegionService regionService;

    @RequestMapping(value = "/region/getRegionList", method = RequestMethod.GET)
    public ResponseEntity<List<Region>> getRegionList() {

        List<Region> regionList = regionService.regionList();

        System.out.println("i am in RegionController from getRegionList" );

          if(regionList.isEmpty()){

            System.out.println("list boosdur  ");

            return new ResponseEntity<List<Region>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Region>>(regionList, HttpStatus.OK);
    }

}
