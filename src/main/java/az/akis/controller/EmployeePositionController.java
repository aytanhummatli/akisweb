package az.akis.controller;

import az.akis.services.employeePosition.EmployeePositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeePositionController {

     @Autowired
     EmployeePositionService employeePositionService;


    @RequestMapping(value = "/employeePosition/deleteEmployeePosition/{elementId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> deleteRelative (@PathVariable Long elementId) {

        employeePositionService.deleteEmployeeDio(elementId);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
