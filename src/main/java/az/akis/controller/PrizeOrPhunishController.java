package az.akis.controller;

import az.akis.services.prizeOrPhunish.PrizeOrPhunishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrizeOrPhunishController {

    @Autowired
    PrizeOrPhunishService prizeOrPhunishService;

    @RequestMapping(value = "/prizeOrPhunish/deletePrizeOrPhunish/{elementId}", method = RequestMethod.PUT)
     public ResponseEntity<Void> deletePrizeOrPhunish (@PathVariable Long elementId) {

        prizeOrPhunishService.deletePrizeOrPunish(elementId);

        return new ResponseEntity<Void>(HttpStatus.OK);
     }


}
