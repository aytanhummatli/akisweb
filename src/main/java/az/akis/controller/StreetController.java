package az.akis.controller;

import az.akis.models.Street;
import az.akis.services.street.StreetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class StreetController {

@Autowired
StreetService streetService;

    @RequestMapping(value = "/street/getStreetList", method = RequestMethod.GET)
    public ResponseEntity<List<Street>> getStreetList() {

        List<Street> streetList = streetService.getStreetList();

        System.out.println("i am in StreetController from getStreetList" );

          if(streetList.isEmpty()){

            System.out.println("list boosdur  ");

            return new ResponseEntity<List<Street>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Street>>(streetList, HttpStatus.OK);
    }

}
