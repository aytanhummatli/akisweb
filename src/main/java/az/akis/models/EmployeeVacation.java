package az.akis.models;

import java.util.Date;

public class EmployeeVacation {

    private String withFamily;
    private String vacationNote;
    private String VacationCardNo;
    private Long id;
    private Date endDate;
    private Date createDate;
    private Combo vacationType;
    private String addressNote;
    private Address address;
    private Long dayCount;


    public EmployeeVacation( ) {
        this.vacationType = vacationType;
        this.address = address;
        this.vacationType=new Combo();
    }

    public String getWithFamily() {
        return withFamily;
    }

    public Long getDayCount() {
        return dayCount;
    }

    public void setDayCount(Long dayCount) {
        this.dayCount = dayCount;
    }

    public void setWithFamily(String withFamily) {
        this.withFamily = withFamily;
    }

    public String getVacationNote() {
        return vacationNote;
    }

    public void setVacationNote(String vacationNote) {
        this.vacationNote = vacationNote;
    }

    public String getVacationCardNo() {
        return VacationCardNo;
    }

    public void setVacationCardNo(String vacationCardNo) {
        VacationCardNo = vacationCardNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Combo getVacationType() {
        return vacationType;
    }

    public void setVacationType(Combo vacationType) {
        this.vacationType = vacationType;
    }

    public String getAddressNote() {
        return addressNote;
    }

    public void setAddressNote(String addressNote) {
        this.addressNote = addressNote;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "EmployeeVacation{" +
                "withFamily='" + withFamily + '\'' +
                ", vacationNote='" + vacationNote + '\'' +
                ", VacationCardNo=" + VacationCardNo +
                ", id=" + id +
                ", endDate=" + endDate +
                ", createDate=" + createDate +
                ", vacationType=" + vacationType +
                ", addressNote='" + addressNote + '\'' +
                ", address=" + address +
                ", dayCount=" + dayCount +
                '}';
    }
}
