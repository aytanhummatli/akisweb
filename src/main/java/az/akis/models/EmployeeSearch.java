package az.akis.models;

import java.util.ArrayList;

import java.util.List;

public class EmployeeSearch {

    private  PaginationModel paging;

    private List<Employee> employee;

    public EmployeeSearch() {

     this.employee=new ArrayList<Employee>() ;

     this.paging=new PaginationModel();
    }

    public PaginationModel getPaging() {
        return paging;
    }

    public void setPaging(PaginationModel paging) {
        this.paging = paging;
    }

    public List<Employee> getEmployee() {
        return employee;
    }

    public void setEmployee(List<Employee> employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "EmployeeSearch{" +
                "paging=" + paging +
                ", Employee=" + employee +
                '}';
    }

}
