package az.akis.models;

import java.math.BigDecimal;
import java.util.Date;

public class User {
    private BigDecimal id;
    private String userName;
    private long employeeId;
    private Date createDate;
    private Date endDate;
    private String active;
    private long ident;
    private long nomenklaturaId;
    private String doupdate;

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public long getIdent() {
        return ident;
    }

    public void setIdent(long ident) {
        this.ident = ident;
    }

    public long getNomenklaturaId() {
        return nomenklaturaId;
    }

    public void setNomenklaturaId(long nomenklaturaId) {
        this.nomenklaturaId = nomenklaturaId;
    }

    public String getDoupdate() {
        return doupdate;
    }

    public void setDoupdate(String doupdate) {
        this.doupdate = doupdate;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", employeeId=" + employeeId +
                ", createDate=" + createDate +
                ", endDate=" + endDate +
                ", active='" + active + '\'' +
                ", ident=" + ident +
                ", nomenklaturaId=" + nomenklaturaId +
                ", doupdate='" + doupdate + '\'' +
                '}';
    }
}
