package az.akis.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class SuccessDegreeHistory {

 private Long id;

private Long empId;

private SuccessDegree successDegree;



    @JsonFormat(pattern = "MM.dd.yyyy",shape = JsonFormat.Shape.STRING )
private Date createDate;

private String orderNo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SuccessDegreeHistory() {
        this.successDegree = new SuccessDegree();
    }

    public Long getEmpId() {
        return empId;
    }

    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    public SuccessDegree getSuccessDegree() {
        return successDegree;
    }

    public void setSuccessDegree(SuccessDegree successDegree) {
        this.successDegree = successDegree;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    @Override
    public String toString() {
        return "SuccessDegreeHistory{" +
                "empId=" + empId +
                ", successDegree=" + successDegree +
                ", createDate=" + createDate +
                ", orderNo='" + orderNo + '\'' +
                '}';
    }


}
