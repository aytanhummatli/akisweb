package az.akis.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class PrizeOrPunish {

    private PrizePunishReason reason;

    private PrizePunishReason punishEndReason;

    private String punishEndOrder;

    @JsonFormat(pattern = "MM.dd.yyyy",shape = JsonFormat.Shape.STRING )
    private Date punishEndDate;

    private Long OrganFullNameId;

    private StructureName orderOrgan;

    private String orderNo;

    private Long id;

    @JsonFormat(pattern = "MM.dd.yyyy",shape = JsonFormat.Shape.STRING )
    private Date getDate;

    private PrizeAndPunishment  prizeType;

    public PrizeOrPunish(  ) {
        this.reason =  new PrizePunishReason();
        this.punishEndReason = new PrizePunishReason();
        this.prizeType = new PrizeAndPunishment();
        this.orderOrgan=new StructureName();
    }

    public PrizePunishReason getReason() {
        return reason;
    }

    public void setReason(PrizePunishReason reason) {
        this.reason = reason;
    }

    public PrizePunishReason getPunishEndReason() {
        return punishEndReason;
    }

    public void setPunishEndReason(PrizePunishReason punishEndReason) {
        this.punishEndReason = punishEndReason;
    }

    public String getPunishEndOrder() {
        return punishEndOrder;
    }

    public void setPunishEndOrder(String punishEndOrder) {
        this.punishEndOrder = punishEndOrder;
    }

    public Date getPunishEndDate() {
        return punishEndDate;
    }

    public void setPunishEndDate(Date punishEndDate) {
        this.punishEndDate = punishEndDate;
    }

    public Long getOrganFullNameId() {
        return OrganFullNameId;
    }

    public void setOrganFullNameId(Long organFullNameId) {
        OrganFullNameId = organFullNameId;
    }

    public StructureName getOrderOrgan() {
        return orderOrgan;
    }

    public void setOrderOrgan(StructureName orderOrgan) {
        this.orderOrgan = orderOrgan;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGetDate() {
        return getDate;
    }

    public void setGetDate(Date getDate) {
        this.getDate = getDate;
    }

    public PrizeAndPunishment  getPrizeType() {
        return prizeType;
    }

    public void setPrizeType(PrizeAndPunishment  prizeType) {
        this.prizeType = prizeType;
    }

    @Override
    public String toString() {
        return "PrizeOrPunish{" +
                "reason=" + reason +
                ", punishEndReason=" + punishEndReason +
                ", punishEndOrder='" + punishEndOrder + '\'' +
                ", punishEndDate=" + punishEndDate +
                ", OrganFullNameId=" + OrganFullNameId +
                ", orderOrgan=" + orderOrgan +
                ", orderNo='" + orderNo + '\'' +
                ", id=" + id +
                ", getDate=" + getDate +
                ", prizeType=" + prizeType +
                '}';
    }
}
