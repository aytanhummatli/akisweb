package az.akis.models;

import java.util.Date;

public class StructurePosition {

    private String fullSttrName;
    private Long id;
    private StructureName structureName;
    private PositionName positionName;

    private Long RankLimit;

    private Long partTime;
    private String isEmpty;

    private Date endDate;
    private Date createDate;
    private Combo ppxDyp;
    private Long budget;


    public StructurePosition() {
        this.structureName = new StructureName();
        this.positionName = new PositionName();
        this.ppxDyp = new Combo();
    }

    public Long getPartTime() {
        return partTime;
    }

    public void setPartTime(Long partTime) {
        this.partTime = partTime;
    }

    public String getIsEmpty() {
        return isEmpty;
    }

    public void setIsEmpty(String isEmpty) {
        this.isEmpty = isEmpty;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Combo getPpxDyp() {
        return ppxDyp;
    }

    public void setPpxDyp(Combo ppxDyp) {
        this.ppxDyp = ppxDyp;
    }

    public Long getBudget() {
        return budget;
    }

    public void setBudget(Long budget) {
        this.budget = budget;
    }

    public String getFullSttrName() {
        return fullSttrName;
    }

    public void setFullSttrName(String fullSttrName) {
        this.fullSttrName = fullSttrName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StructureName getStructureName() {
        return structureName;
    }

    public void setStructureName(StructureName structureName) {
        this.structureName = structureName;
    }

    public PositionName getPositionName() {
        return positionName;
    }

    public void setPositionName(PositionName positionName) {
        this.positionName = positionName;
    }

    public Long getRankLimit() {
        return RankLimit;
    }

    public void setRankLimit(Long rankLimit) {
        RankLimit = rankLimit;
    }


    @Override
    public String toString() {
        return "StructurePosition{" +
                "fullSttrName='" + fullSttrName + '\'' +
                ", id=" + id +
                ", structureName=" + structureName +
                ", positionName=" + positionName +
                ", RankLimit=" + RankLimit +
                ", partTime=" + partTime +
                ", isEmpty='" + isEmpty + '\'' +
                ", endDate=" + endDate +
                ", createDate=" + createDate +
                ", ppxDyp=" + ppxDyp +
                ", budget=" + budget +
                '}';
    }
}
