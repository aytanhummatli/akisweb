package az.akis.models;

public class PaginationModel {

    private Long pageno;

    private Long itemsPerPage;

    private Long totalCount;

    public PaginationModel() {
    }

    public Long getPageno() {
        return pageno;
    }

    public void setPageno(Long pageno) {
        this.pageno = pageno;
    }

    public Long getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(Long itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    @Override
    public String toString() {
        return "PaginationModel{" +
                "pageno=" + pageno +
                ", itemsPerPage=" + itemsPerPage +
                ", totalCount=" + totalCount +
                '}';
    }
}
