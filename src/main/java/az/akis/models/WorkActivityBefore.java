package az.akis.models;

import java.util.Date;

public class WorkActivityBefore {

    private Date workStartDate;
    private Date workEndDate;
    private StructureName structureName;
    private PositionName positionName;
    private String otherForce;
    private Long id;
    private String fullStrName;
    private String enterOrder;
    private Long enterDateYear;
    private Date enterDate;
    private String exitOrder;
    private Long exitDateYear;
    private Date exitDate;
    private Combo labourCode;


    public WorkActivityBefore() {
        this.structureName = new StructureName();
        this.positionName = new PositionName();
        this.labourCode = new Combo();
    }

    public Date getWorkStartDate() {
        return workStartDate;
    }

    public void setWorkStartDate(Date workStartDate) {
        this.workStartDate = workStartDate;
    }

    public Date getWorkEndDate() {
        return workEndDate;
    }

    public void setWorkEndDate(Date workEndDate) {
        this.workEndDate = workEndDate;
    }

    public StructureName getStructureName() {
        return structureName;
    }

    public void setStructureName(StructureName structureName) {
        this.structureName = structureName;
    }

    public PositionName getPositionName() {
        return positionName;
    }

    public void setPositionName(PositionName positionName) {
        this.positionName = positionName;
    }

    public String getOtherForce() {
        return otherForce;
    }

    public void setOtherForce(String otherForce) {
        this.otherForce = otherForce;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullStrName() {
        return fullStrName;
    }

    public void setFullStrName(String fullStrName) {
        this.fullStrName = fullStrName;
    }

    public String getEnterOrder() {
        return enterOrder;
    }

    public void setEnterOrder(String enterOrder) {
        this.enterOrder = enterOrder;
    }

    public Long getExitDateYear() {
        return exitDateYear;
    }

    public void setExitDateYear(Long exitDateYear) {
        this.exitDateYear = exitDateYear;
    }

    public Long getEnterDateYear() {
        return enterDateYear;
    }

    public void setEnterDateYear(Long enterDateYear) {
        this.enterDateYear = enterDateYear;
    }

    public Date getEnterDate() {
        return enterDate;
    }

    public void setEnterDate(Date enterDate) {
        this.enterDate = enterDate;
    }

    public String getExitOrder() {
        return exitOrder;
    }

    public void setExitOrder(String exitOrder) {
        this.exitOrder = exitOrder;
    }

    public Date getExitDate() {
        return exitDate;
    }

    public void setExitDate(Date exitDate) {
        this.exitDate = exitDate;
    }

    public Combo getLabourCode() {
        return labourCode;
    }

    public void setLabourCode(Combo labourCode) {
        this.labourCode = labourCode;
    }

    @Override
    public String toString() {
        return "WorkActivityBefore{" +
                "workStartDate=" + workStartDate +
                ", workEndDate=" + workEndDate +
                ", structureName=" + structureName +
                ", positionName=" + positionName +
                ", otherForce='" + otherForce + '\'' +
                ", id=" + id +
                ", fullStrName='" + fullStrName + '\'' +
                ", enterOrder='" + enterOrder + '\'' +
                ", enterDateYear=" + enterDateYear +
                ", enterDate=" + enterDate +
                ", exitOrder='" + exitOrder + '\'' +
                ", exitDateYear=" + exitDateYear +
                ", exitDate=" + exitDate +
                ", labourCode=" + labourCode +
                '}';
    }
}
