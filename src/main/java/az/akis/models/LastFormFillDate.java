package az.akis.models;

import java.util.Date;

public class LastFormFillDate
{
private Long id;
private Date fillDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFillDate() {
        return fillDate;
    }

    public void setFillDate(Date fillDate) {
        this.fillDate = fillDate;
    }

    @Override
    public String toString() {
        return "LastFormFillDate{" +
                "id=" + id +
                ", fillDate=" + fillDate +
                '}';
    }
}
