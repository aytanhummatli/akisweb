package az.akis.models;

import java.util.Date;

public class EducationHistory {

    private UniversityName universityName;
    private String startYear;
    private String sertificateNo;
    private Profession profession;
    private String main;
    private Long id;
    private String endYear;
    private Date endDate;
    private Date createDate;
    private Combo eyaniQiyabi;
    private Combo educationType;
    private Combo educationLevel;
    private Combo certificateType;
    private Date certificateGetDate;
    private String status;

    public EducationHistory( ) {
        this.universityName = new UniversityName();
        this.profession = new Profession();
        this.eyaniQiyabi = new Combo();
        this.educationType = new Combo();
        this.educationLevel = new Combo();
        this.certificateType = new Combo();
    }

    public UniversityName getUniversityName() {
        return universityName;
    }

    public void setUniversityName(UniversityName universityName) {
        this.universityName = universityName;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getSertificateNo() {
        return sertificateNo;
    }

    public void setSertificateNo(String sertificateNo) {
        this.sertificateNo = sertificateNo;
    }

    public Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Combo getEyaniQiyabi() {
        return eyaniQiyabi;
    }

    public void setEyaniQiyabi(Combo eyaniQiyabi) {
        this.eyaniQiyabi = eyaniQiyabi;
    }

    public Combo getEducationType() {
        return educationType;
    }

    public void setEducationType(Combo educationType) {
        this.educationType = educationType;
    }

    public Combo getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(Combo educationLevel) {
        this.educationLevel = educationLevel;
    }

    public Combo getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(Combo certificateType) {
        this.certificateType = certificateType;
    }

    public Date getCertificateGetDate() {
        return certificateGetDate;
    }

    public void setCertificateGetDate(Date sertificateGetDate) {
        this.certificateGetDate = sertificateGetDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "EducationHistory{" +
                "universityName=" + universityName +
                ", startYear='" + startYear + '\'' +
                ", sertificateNo='" + sertificateNo + '\'' +
                ", profession=" + profession +
                ", main='" + main + '\'' +
                ", id=" + id +
                ", endYear='" + endYear + '\'' +
                ", endDate=" + endDate +
                ", createDate=" + createDate +
                ", eyaniQiyabi=" + eyaniQiyabi +
                ", educationType=" + educationType +
                ", educationLevel=" + educationLevel +
                ", certificateType=" + certificateType +
                ", certificateGetDate=" + certificateGetDate +
                ", status='" + status + '\'' +
                '}';
    }
}
