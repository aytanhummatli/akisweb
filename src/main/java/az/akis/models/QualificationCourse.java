package az.akis.models;

import java.util.Date;

public class QualificationCourse {

    private Long id;

    private Date finishDate;

    private String courseName;

    private Date beginDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    @Override
    public String toString() {
        return "QualificationCourse{" +
                "id=" + id +
                ", finishDate=" + finishDate +
                ", courseName='" + courseName + '\'' +
                ", beginDate=" + beginDate +
                '}';

    }
}
