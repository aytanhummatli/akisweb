package az.akis.models;

import java.util.Date;

public class AttestationHistory {
private Long id;
    private Date atteatationDate;
    private Combo result;

    public AttestationHistory( ) {
        this.result = new Combo();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getAtteatationDate() {
        return atteatationDate;
    }

    public void setAtteatationDate(Date atteatationDate) {
        this.atteatationDate = atteatationDate;
    }

    public Combo getResult() {
        return result;
    }

    public void setResult(Combo result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "AttestationHistory{" +
                "atteatationDate=" + atteatationDate +
                ", result=" + result +
                '}';
    }
}
