package az.akis.models;

public class SubRegion {

    private Long id;
    private Region region;
    private String name;

    public SubRegion() {

        this.region=new Region();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SubRegion{" +
                "id=" + id +
                ", region=" + region +
                ", name='" + name + '\'' +
                '}';
    }
}
