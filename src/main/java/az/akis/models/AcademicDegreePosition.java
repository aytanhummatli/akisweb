package az.akis.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class AcademicDegreePosition {

    private Long id;

    private Combo academicDegree;
    @JsonFormat(pattern = "MM.dd.yyyy",shape = JsonFormat.Shape.STRING )
    private Date getDate;

    public AcademicDegreePosition() {

        this.academicDegree = new Combo();

    }

    public Date getGetDate() {
        return getDate;
    }

    public void setGetDate(Date getDate) {
        this.getDate = getDate;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public Combo getAcademicDegree() {

        return academicDegree;
    }

    public void setAcademicDegree(Combo academicDegree) {

        this.academicDegree = academicDegree;
    }

    @Override
    public String toString() {
        return "AcademicDegreePosition{" +
                "id=" + id +
                ", academicDegree=" + academicDegree +
                ", getDate=" + getDate +
                '}';
    }
}
