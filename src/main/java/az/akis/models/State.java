package az.akis.models;

public class State
{
    public boolean checked;

    public boolean expanded=false;

    public boolean  selected;

    public boolean selectable;


    public State() {

    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelectable() {
        return selectable;
    }

    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }

    @Override
    public String toString() {
        return "State{" +
                "checked=" + checked +
                ", expanded=" + expanded +
                ", selected=" + selected +
                ", selectable=" + selectable +
                '}';
    }
}
