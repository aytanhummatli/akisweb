package az.akis.models;

import java.util.Date;

public class WorkMission {

    private Long id;
    private StructureName structureName;
    private Date missionOrderDate;
    private String missionOrder;
    private Date endDate;
    private Date createDate;
    private String missionTermLess;

    public WorkMission() {
        this.structureName = new StructureName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StructureName getStructureName() {
        return structureName;
    }

    public void setStructureName(StructureName structureName) {
        this.structureName = structureName;
    }

    public Date getMissionOrderDate() {
        return missionOrderDate;
    }

    public void setMissionOrderDate(Date missionOrderDate) {
        this.missionOrderDate = missionOrderDate;
    }

    public String getMissionOrder() {
        return missionOrder;
    }

    public void setMissionOrder(String missionOrder) {
        this.missionOrder = missionOrder;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getMissionTermLess() {
        return missionTermLess;
    }

    public void setMissionTermLess(String missionTermLess) {
        this.missionTermLess = missionTermLess;
    }

    @Override
    public String toString() {
        return "WorkMission{" +
                "id=" + id +
                ", structureName=" + structureName +
                ", missionOrderDate=" + missionOrderDate +
                ", missionOrder='" + missionOrder + '\'' +
                ", endDate=" + endDate +
                ", createDate=" + createDate +
                ", missionTermLess='" + missionTermLess + '\'' +
                '}';
    }
}

