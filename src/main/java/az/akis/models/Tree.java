package az.akis.models;

public class Tree {

    private StructureName structureName;
    private Long childCount;
    private StructureTree structureTree;


    public Tree( ) {
        this.structureName = new StructureName();
        this.structureTree = new StructureTree();
    }

    @Override
    public String toString() {
        return "Tree{" +
                "structureName=" + structureName +
                ", childCount=" + childCount +
                ", structureTree=" + structureTree +
                '}';
    }

    public StructureTree getStructureTree() {
        return structureTree;
    }

    public void setStructureTree(StructureTree structureTree) {
        this.structureTree = structureTree;
    }

    public StructureName getStructureName() {
        return structureName;
    }

    public void setStructureName(StructureName structureName) {
        this.structureName = structureName;
    }

    public Long getChildCount() {
        return childCount;
    }

    public void setChildCount(Long childCount) {
        this.childCount = childCount;
    }


}
