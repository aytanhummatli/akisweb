package az.akis.models;
import java.util.Date;
public class StructureTree {
    private Long id;
    private Long nLeft;
    private Long nRight;
    private Long nLevel;
    private Date createDate;
    private Date endDate;

    public StructureTree() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getnLeft() {
        return nLeft;
    }

    public void setnLeft(Long nLeft) {
        this.nLeft = nLeft;
    }

    public Long getnRight() {
        return nRight;
    }

    public void setnRight(Long nRight) {
        this.nRight = nRight;
    }

    public Long getnLevel() {
        return nLevel;
    }

    public void setnLevel(Long nLevel) {
        this.nLevel = nLevel;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "StructureTree{" +
                "id=" + id +
                ", nLeft=" + nLeft +
                ", nRight=" + nRight +
                ", nLevel=" + nLevel +
                ", createDate=" + createDate +
                ", endDate=" + endDate +
                '}';
    }
}


