package az.akis.models;

public class UserInfo {
    private User user;
    private Employee employee;
    private Person person;
    private Card card;
    private Nomenclature nomenclature;
    private EmpImageHistory imgHistory;
    private Password password;
//    private UserRule userRule;
//    private UserButtonRule userButtonRule;
//    private Rule rule;
//    private ButtonRule buttonRule;

    public UserInfo() {
        this.user = new User();
       this.employee = new Employee();
        this.person = new Person();
        this.card = new Card();
        this.nomenclature = new Nomenclature();
        this.imgHistory = new EmpImageHistory();
        this.password=new Password();
//        this.userRule=new UserRule();
//        this.userButtonRule=new UserButtonRule();
//        this.rule=new Rule();
//        this.buttonRule=new ButtonRule();
   }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Nomenclature getNomenclature() {
        return nomenclature;
    }

    public void setNomenclature(Nomenclature nomenclature) {
        this.nomenclature = nomenclature;
    }

    public EmpImageHistory getImgHistory() {
        return imgHistory;
    }

    public void setImgHistory(EmpImageHistory imgHistory) {
        this.imgHistory = imgHistory;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "user=" + user +
                ", person=" + person +
                ", card=" + card +
                ", nomenclature=" + nomenclature +
                ", imgHistory=" + imgHistory +
                ", password=" + password +
                '}';
    }
}

