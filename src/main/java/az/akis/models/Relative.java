package az.akis.models;

public class Relative {

    private Long id;

    private String workPlace;

    private Person person;

    private String livingAddressNote;

    private Address livingAddress;

    private Combo relativeDegree;

    private String bornYear;

    public Relative( ) {

        this.person = new Person();

        this.livingAddress = new Address();

        this.relativeDegree = new Combo();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWorkPlace() {
        return workPlace;
    }

    public void setWorkPlace(String workPlace) {
        this.workPlace = workPlace;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getLivingAddressNote() {
        return livingAddressNote;
    }

    public void setLivingAddressNote(String livingAddressNote) {
        this.livingAddressNote = livingAddressNote;
    }

    public Address getLivingAddress() {
        return livingAddress;
    }

    public void setLivingAddress(Address livingAddress) {
        this.livingAddress = livingAddress;
    }

    public Combo getRelativeDegree() {
        return relativeDegree;
    }

    public void setRelativeDegree(Combo relativeDegree) {
        this.relativeDegree = relativeDegree;
    }

    public String getBornYear() {
        return bornYear;
    }

    public void setBornYear(String bornYear) {
        this.bornYear = bornYear;
    }

    @Override
    public String toString() {
        return "Relative{" +
                "id=" + id +
                ", workPlace='" + workPlace + '\'' +
                ", person=" + person +
                ", livingAddressNote='" + livingAddressNote + '\'' +
                ", livingAddress=" + livingAddress +
                ", relativeDegree=" + relativeDegree +
                ", bornYear='" + bornYear + '\'' +
                '}';
    }
}
