package az.akis.models;

public class Profession {

private String profession;
private Combo professionType;

private Long id;

    public Profession() {
        this.professionType = new Combo();
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Combo getProfessionType() {
        return professionType;
    }

    public void setProfessionType(Combo professionType) {
        this.professionType = professionType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Profession{" +
                "profession='" + profession + '\'' +
                ", professionType=" + professionType +
                ", id=" + id +
                '}';
    }
}
