package az.akis.models;

public class CardSettings {

    private Long id;
    private Long strPosFontSize;
    private Long posFontSize;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStrPosFontSize() {
        return strPosFontSize;
    }

    public void setStrPosFontSize(Long strPosFontSize) {
        this.strPosFontSize = strPosFontSize;
    }

    public Long getPosFontSize() {
        return posFontSize;
    }

    public void setPosFontSize(Long posFontSize) {
        this.posFontSize = posFontSize;
    }

    @Override
    public String toString() {
        return "CardSettings{" +
                "id=" + id +
                ", strPosFontSize=" + strPosFontSize +
                ", posFontSize=" + posFontSize +
                '}';
    }
}
