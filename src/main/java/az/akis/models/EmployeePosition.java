package az.akis.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class EmployeePosition {

    private Long id;
    private StructurePosition structurePosition;
    @JsonFormat(pattern = "MM.dd.yyyy",shape = JsonFormat.Shape.STRING )
    private Date workStartDate;
    private Date workEndDate;
    @JsonFormat(pattern = "MM.dd.yyyy",shape = JsonFormat.Shape.STRING )
    private Date orderDate;
    private String enterOrder;
    private Date endDate;
    private String exitOrder;
    private Date createDate;
    private Combo enterReason;
    private Combo labourCode;
    private String fullname;

    public EmployeePosition() {

        this.structurePosition = new StructurePosition();

        this.enterReason = new Combo();

        this.labourCode = new Combo();
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Combo getLabourCode() {
        return labourCode;
    }

    public void setLabourCode(Combo labourCode) {
        this.labourCode = labourCode;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getExitOrder() {
        return exitOrder;
    }

    public void setExitOrder(String exitOrder) {
        this.exitOrder = exitOrder;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Combo getEnterReason() {
        return enterReason;
    }

    public void setEnterReason(Combo enterReason) {
        this.enterReason = enterReason;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public StructurePosition getStructurePosition() {
        return structurePosition;
    }

    public void setStructurePosition(StructurePosition structurePosition) {
        this.structurePosition = structurePosition;
    }

    public Date getWorkStartDate() {
        return workStartDate;
    }

    public void setWorkStartDate(Date workStartDate) {
        this.workStartDate = workStartDate;
    }

    public Date getWorkEndDate() {
        return workEndDate;
    }

    public void setWorkEndDate(Date workEndDate) {
        this.workEndDate = workEndDate;
    }

    public String getEnterOrder() {
        return enterOrder;
    }

    public void setEnterOrder(String enterOrder) {
        this.enterOrder = enterOrder;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    @Override
    public String toString() {
        return "EmployeePosition{" +
                "id=" + id +
                ", structurePosition=" + structurePosition +
                ", workStartDate=" + workStartDate +
                ", workEndDate=" + workEndDate +
                ", orderDate=" + orderDate +
                ", enterOrder='" + enterOrder + '\'' +
                ", endDate=" + endDate +
                ", exitOrder='" + exitOrder + '\'' +
                ", createDate=" + createDate +
                ", enterReason=" + enterReason +
                ", labourCode=" + labourCode +
                '}';
    }
}
