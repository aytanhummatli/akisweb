package az.akis.models;

import java.util.Date;


public class EmployeeInjured {

    private PrizePunishReason prizePhunishReason;

    private Long id;

    private Date createDate;

    public EmployeeInjured(  ) {
        this.prizePhunishReason = new PrizePunishReason();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public PrizePunishReason getPrizePhunishReason() {
        return prizePhunishReason;
    }

    public void setPrizePhunishReason(PrizePunishReason prizePhunishReason) {
        this.prizePhunishReason = prizePhunishReason;
    }

    @Override
    public String toString() {
        return "EmployeeInjured{" +
                "prizePhunishReason=" + prizePhunishReason +
                ", id=" + id +
                ", createDate=" + createDate +
                '}';
    }

}
