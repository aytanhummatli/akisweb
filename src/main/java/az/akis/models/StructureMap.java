package az.akis.models;
import java.util.ArrayList;
import java.util.List;


public class StructureMap<T> {

    private T data = null;
    private List<StructureMap> children = new ArrayList<>();
    private StructureMap parent = null;

    public StructureMap(T data) {
        this.data = data;
    }

    public void addChild(StructureMap child) {
        child.setParent(this);
        this.children.add(child);
    }

    public void addChild(T data) {
        StructureMap<T> newChild = new StructureMap<>(data);
        this.addChild(newChild);
    }

    public void addChildren(List<StructureMap> children) {
        for(StructureMap t : children) {
            t.setParent(this);
        }
        this.children.addAll(children);
    }

    public List<StructureMap> getChildren() {
        return children;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    private void setParent(StructureMap parent) {
        this.parent = parent;
    }

    public StructureMap getParent() {
        return parent;
    }
}
