package az.akis.models;

public class StructureName {


   private int id;
   private String achronym;
   private String suffix;
   private String name;
   private StructureTree structureTree;
   private String fullName;


    public StructureName() {

        this.structureTree=new StructureTree();
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAchronym() {
        return achronym;
    }

    public void setAchronym(String achronym) {
        this.achronym = achronym;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public StructureTree getStructureTree() {
        return structureTree;
    }

    public void setStructureTree(StructureTree structureTree) {
        this.structureTree = structureTree;
    }



    @Override
    public String toString() {
        return "StructureName{" +

                ", achronym='" + achronym + '\'' +
                ", suffix='" + suffix + '\'' +
                ", name='" + name + '\'' +
                ", structureTree=" + structureTree +
                '}';
    }
}
