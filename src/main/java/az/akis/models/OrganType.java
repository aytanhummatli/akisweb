package az.akis.models;

public class OrganType {

public String name;
public Long id;
private String suffix;

    public OrganType() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public String toString() {
        return "OrganType{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", suffix='" + suffix + '\'' +
                '}';
    }
}
