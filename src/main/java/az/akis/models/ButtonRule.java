package az.akis.models;

import java.math.BigDecimal;
import java.util.Date;

public class ButtonRule {
    private BigDecimal id;
    private BigDecimal createByUserId ;
    private String ruleNames;
    private Date createDate;
    private Date endDate;
    private String active;
    private UserButtonRule userButtonRule;
public ButtonRule(){
    this.userButtonRule=new UserButtonRule();
}
    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getCreateByUserId() {
        return createByUserId;
    }

    public void setCreateByUserId(BigDecimal createByUserId) {
        this.createByUserId = createByUserId;
    }

    public String getRuleNames() {
        return ruleNames;
    }

    public void setRuleNames(String ruleNames) {
        this.ruleNames = ruleNames;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public UserButtonRule getUserButtonRule() {
        return userButtonRule;
    }

    public void setUserButtonRule(UserButtonRule userButtonRule) {
        this.userButtonRule = userButtonRule;
    }

    @Override
    public String toString() {
        return "ButtonRule{" +
                "id=" + id +
                ", createByUserId=" + createByUserId +
                ", ruleNames='" + ruleNames + '\'' +
                ", createDate=" + createDate +
                ", endDate=" + endDate +
                ", active='" + active + '\'' +
                ", userButtonRule=" + userButtonRule +
                '}';
    }
}
