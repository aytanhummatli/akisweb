package az.akis.models;

public class Street {

    private Long id;

    private Village village;

    private String name;

    public Street() {

        this.village = new Village();

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Village getVillage() {
        return village;
    }

    public void setVillage(Village village) {
        this.village = village;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Street{" +
                "id=" + id +
                ", village=" + village +
                ", name='" + name + '\'' +
                '}';
    }
}
