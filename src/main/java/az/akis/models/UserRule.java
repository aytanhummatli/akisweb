package az.akis.models;

import java.math.BigDecimal;
import java.util.Date;

public class UserRule {
    private BigDecimal id;
    private BigDecimal userId;
    private BigDecimal rulesId;
    private BigDecimal createByUserId;
    private Date createDate;
    private Date endDate;
    private String active;


    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getUserId() {
        return userId;
    }

    public void setUserId(BigDecimal userId) {
        this.userId = userId;
    }





    public BigDecimal getCreateByUserId() {
        return createByUserId;
    }

    public void setCreateByUserId(BigDecimal createByUserId) {
        this.createByUserId = createByUserId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }


    @Override
    public String toString() {
        return "UserRule{" +
                "id=" + id +
                ", userId=" + userId +
                ", createByUserId=" + createByUserId +
                ", createDate=" + createDate +
                ", endDate=" + endDate +
                ", active='" + active + '\'' +
                '}';
    }
}
