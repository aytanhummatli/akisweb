package az.akis.models;

public class UniversityName {

    private University university;
    private Long id;
    private String name;


    public UniversityName() {
        this.university = new  University();
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UniversityName{" +
                "university=" + university +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
