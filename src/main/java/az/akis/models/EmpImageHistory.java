package az.akis.models;

import java.util.Date;

public class EmpImageHistory {

    private Long id;
    private Employee employee;
    private Date addDate;
    private String imgURL;
    private String ftpFolder;

    public EmpImageHistory() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Date getAddDate() {
        return addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getFtpFolder() {
        return ftpFolder;
    }

    public void setFtpFolder(String ftpFolder) {
        this.ftpFolder = ftpFolder;
    }

    @Override
    public String toString() {
        return "EmpImageHistory{" +
                "id=" + id +
                ", Employee=" + employee +
                ", addDate=" + addDate +
                ", imgURL='" + imgURL + '\'' +
                ", ftpFoder='" + ftpFolder + '\'' +
                '}';
    }
}
