package az.akis.models;

public class Combo {

    private String name;

    private Long no;

    private long id;

    private String description;


    public Combo() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getNo() {
        return no;
    }

    public void setNo(Long no) {
        this.no = no;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Combo{" +
                "name='" + name + '\'' +
                ", no=" + no +
                ", id=" + id +
                ", description='" + description + '\'' +
                '}';
    }
}
