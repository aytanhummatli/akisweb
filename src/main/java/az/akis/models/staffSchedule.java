package az.akis.models;

public class staffSchedule {

private Long id;
private String pos_name;
private String rank_name;
private String full_name;
private String emp_rank;
private Long emp_id;
private Long pos_gr_id;
private Long nomenklatura_id;
private Long CMBS_PPX_DYP_ID;
private boolean IS_EMPTY;
private Long  pos_name_id;
private String    str_name;
private Long  RANK_TYPE_ID;

    public staffSchedule() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPos_name() {
        return pos_name;
    }

    public void setPos_name(String pos_name) {
        this.pos_name = pos_name;
    }

    public String getRank_name() {
        return rank_name;
    }

    public void setRank_name(String rank_name) {
        this.rank_name = rank_name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmp_rank() {
        return emp_rank;
    }

    public void setEmp_rank(String emp_rank) {
        this.emp_rank = emp_rank;
    }

    public Long getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(Long emp_id) {
        this.emp_id = emp_id;
    }

    public Long getPos_gr_id() {
        return pos_gr_id;
    }

    public void setPos_gr_id(Long pos_gr_id) {
        this.pos_gr_id = pos_gr_id;
    }

    public Long getNomenklatura_id() {
        return nomenklatura_id;
    }

    public void setNomenklatura_id(Long nomenklatura_id) {
        this.nomenklatura_id = nomenklatura_id;
    }

    public Long getCMBS_PPX_DYP_ID() {
        return CMBS_PPX_DYP_ID;
    }

    public void setCMBS_PPX_DYP_ID(Long CMBS_PPX_DYP_ID) {
        this.CMBS_PPX_DYP_ID = CMBS_PPX_DYP_ID;
    }

    public boolean isIS_EMPTY() {
        return IS_EMPTY;
    }

    public void setIS_EMPTY(boolean IS_EMPTY) {
        this.IS_EMPTY = IS_EMPTY;
    }

    public Long getPos_name_id() {
        return pos_name_id;
    }

    public void setPos_name_id(Long pos_name_id) {
        this.pos_name_id = pos_name_id;
    }

    public String getStr_name() {
        return str_name;
    }

    public void setStr_name(String str_name) {
        this.str_name = str_name;
    }

    public Long getRANK_TYPE_ID() {
        return RANK_TYPE_ID;
    }

    public void setRANK_TYPE_ID(Long RANK_TYPE_ID) {
        this.RANK_TYPE_ID = RANK_TYPE_ID;
    }

    @Override
    public String toString() {
        return "staffSchedule{" +
                "id=" + id +
                ", pos_name='" + pos_name + '\'' +
                ", rank_name='" + rank_name + '\'' +
                ", full_name='" + full_name + '\'' +
                ", emp_rank='" + emp_rank + '\'' +
                ", emp_id=" + emp_id +
                ", pos_gr_id=" + pos_gr_id +
                ", nomenklatura_id=" + nomenklatura_id +
                ", CMBS_PPX_DYP_ID=" + CMBS_PPX_DYP_ID +
                ", IS_EMPTY=" + IS_EMPTY +
                ", pos_name_id=" + pos_name_id +
                ", str_name='" + str_name + '\'' +
                ", RANK_TYPE_ID=" + RANK_TYPE_ID +
                '}';
    }

}
