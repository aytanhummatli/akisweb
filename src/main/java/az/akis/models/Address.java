package az.akis.models;

public class Address {

    private Long id;
    private String country;
    private String region;
    private String street;
    private String subRegion;
    private String village;
    private String adress_fullname;

    public Address() {

    }

    public Long getId() {
        return id;
    }

    public String getAdress_fullname() {
        return adress_fullname;
    }

    public void setAdress_fullname(String adress_fullname) {
        this.adress_fullname = adress_fullname;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSubRegion() {
        return subRegion;
    }

    public void setSubRegion(String subRegion) {
        this.subRegion = subRegion;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", region='" + region + '\'' +
                ", street='" + street + '\'' +
                ", subRegion='" + subRegion + '\'' +
                ", village='" + village + '\'' +
                ", adress_fullname='" + adress_fullname + '\'' +
                '}';
    }
}
