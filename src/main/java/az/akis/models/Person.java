package az.akis.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Person {

    private Long id;
    private String name;
    private String surName;
    private String patronymic;
    @JsonFormat(pattern = "MM.dd.yyyy",shape = JsonFormat.Shape.STRING )
    private Date birthDate;
    private Combo marriedStatus;
    private Address bornAddress;
    private String bornAddressNote;
    private String fullName;
    private String maidenName;
    private Combo maleFemale;
    private List<Relative> relatives;
    private List<ContactInformation> contactInformation;

    public Person() {
        this.relatives = new ArrayList<Relative>();

        this.maleFemale=new Combo();

        this.marriedStatus=new Combo();

        this.bornAddress=new Address();

        this.contactInformation=new ArrayList<ContactInformation>();
      //  this.contactInformation= (List<ContactInformation>) new ContactInformation();

    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public List<ContactInformation> getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(List<ContactInformation> contactInformation) {
        this.contactInformation = contactInformation;
    }

    public String getBornAddressNote() {
        return bornAddressNote;
    }

    public void setBornAddressNote(String bornAddressNote) {
        this.bornAddressNote = bornAddressNote;
    }


    public Address getBornAddress() {
        return bornAddress;
    }

    public void setBornAddress(Address bornAddress) {
        this.bornAddress = bornAddress;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Combo getMarriedStatus() {
        return marriedStatus;
    }

    public void setMarriedStatus(Combo marriedStatus) {
        this.marriedStatus = marriedStatus;
    }

    public Combo getMaleFemale() {
        return maleFemale;
    }

    public void setMaleFemale(Combo maleFemale) {
        this.maleFemale = maleFemale;
    }

    public List<Relative> getRelatives() {
        return relatives;
    }

    public void setRelatives(List<Relative> relatives) {
        this.relatives = relatives;
    }

    public String getMaidenName() {
        return maidenName;
    }

    public void setMaidenName(String maidenName) {
        this.maidenName = maidenName;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", birthDate=" + birthDate +
                ", marriedStatus=" + marriedStatus +
                ", bornAddress=" + bornAddress +
                ", bornAddressNote='" + bornAddressNote + '\'' +
                ", fullName='" + fullName + '\'' +
                ", maidenName='" + maidenName + '\'' +
                ", maleFemale=" + maleFemale +
                ", relatives=" + relatives +
                ", contactInformation=" + contactInformation +
                '}';
    }
}
