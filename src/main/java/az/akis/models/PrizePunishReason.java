package az.akis.models;

public class PrizePunishReason {


    private Long id;
    private String name;
    private Combo type;

    public PrizePunishReason( ) {
        this.type = new Combo();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Combo getType() {
        return type;
    }

    public void setType(Combo type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "PrizePunishReason{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                '}';
    }

}
