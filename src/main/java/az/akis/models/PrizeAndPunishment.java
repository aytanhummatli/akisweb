package az.akis.models;

public class PrizeAndPunishment {

    private String name;
    private Long id;
    private Combo combo;

    public PrizeAndPunishment( ) {
        this.combo = new Combo();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Combo getCombo() {
        return combo;
    }

    public void setCombo(Combo combo) {
        this.combo = combo;
    }

    @Override
    public String toString() {
        return "PrizeAndPunishment{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", combo=" + combo +
                '}';
    }
}
