package az.akis.models;


import java.util.Date;

public class PositionGroup {

private Long id;
private Date createDate;
private Date endDate;

private String name;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "PositionGroup{" +
                "id=" + id +
                ", createDate=" + createDate +
                ", endDate=" + endDate +
                ", name='" + name + '\'' +
                '}';
    }
}
