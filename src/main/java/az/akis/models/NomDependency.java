package az.akis.models;

public class NomDependency {

    private Long id;
    private Long nomId;
    private Long depNomId;

    public NomDependency() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNomId() {
        return nomId;
    }

    public void setNomId(Long nomId) {
        this.nomId = nomId;
    }

    public Long getDepNomId() {
        return depNomId;
    }

    public void setDepNomId(Long depNomId) {
        this.depNomId = depNomId;
    }

    @Override
    public String toString() {
        return "NomDependency{" +
                "id=" + id +
                ", nomId=" + nomId +
                ", depNomId=" + depNomId +
                '}';
    }
}
