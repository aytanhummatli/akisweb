package az.akis.models;

public class EmployeeLanguageLevel {

    private Long id;
    private Combo language;
    private Combo languageLevel;

    public EmployeeLanguageLevel( ) {
        this.language = new Combo() ;
        this.languageLevel =new Combo();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Combo getLanguage() {
        return language;
    }

    public void setLanguage(Combo language) {
        this.language = language;
    }

    public Combo getLanguageLevel() {
        return languageLevel;
    }

    public void setLanguageLevel(Combo languageLevel) {
        this.languageLevel = languageLevel;
    }

    @Override
    public String toString() {
        return "EmployeeLanguageLevel{" +
                "id=" + id +
                ", language=" + language +
                ", languageLevel=" + languageLevel +
                '}';
    }
}
