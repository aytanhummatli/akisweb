package az.akis.models;

import java.util.Date;

public class WarActivity {

    private String warName;

    private Long id;

    private Date endDate;

    private Date beginDate;


    public String getWarName() {
        return warName;
    }

    public void setWarName(String warName) {
        this.warName = warName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    @Override
    public String toString() {
        return "WarActivity{" +
                "warName='" + warName + '\'' +
                ", id=" + id +
                ", endDate=" + endDate +
                ", beginDate=" + beginDate +
                '}';
    }

}
