package az.akis.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Employee {

    private Long id;
    private Person person;

    private Date createDate;
    private Date endDate;
    private String personalMatterNo;
    private String policeCardNo;
    private Combo nationality;
    private String civil;
    private String ok;
    private String cardMustBeChanged;
    private Long cardReasonId;
    private Rank rank;
    private EmpImageHistory image;
    private PositionName positionName;
    private String fullStrname;
    private TreeSimple tree;
    private Nomenclature nomenclature;
    private Passport passport;
    private EmployeePosition employeePosition;
    private List<EmployeeRank> employeeRank;
    private List<SuccessDegreeHistory> successDegreeHistory;
    private EmployeeMilitaryService employeeMilitaryService;
    private List<EmployeeLanguageLevel> employeeLanguageLevel;
    private AcademicDegreePosition academicDegreePosition;
    private List<EducationHistory> educationHistory;
    private List<AttestationHistory> attestationHistory;

    private List<Relative> relatives;

    private List<WorkMission> workMissions;

    private List<WorkActivityBefore> workActivityBefore;

    private List<EmployeeVacation> employeeVacations;

    private List<EmployeeVacation> employeeVacationMotherhood;

    private List<EmployeeInjured> injuredList;

    private List<PrizeOrPunish> prize;

    private List<PrizeOrPunish> punishlist;

    private List<PrizeOrPunish> medalList;

    private List<PrizeOrPunish> dioPrizeList;

    private EmployeeWorkBook employeeWorkBook;

    private LastFormFillDate lastFormFillDate;

    private List<QualificationCourse> qualificationCourses;

    private List<WarActivity> warActivities;

    private List<EmployeePosition> dioList;

    private PoliceCardNew policeCardNew;




    public Employee() {

        this.policeCardNew = new PoliceCardNew();

        this.lastFormFillDate = new LastFormFillDate();

        this.employeeWorkBook = new EmployeeWorkBook();

        this.qualificationCourses = new ArrayList<QualificationCourse>();

        this.warActivities = new ArrayList<WarActivity>();

        this.prize = new ArrayList<PrizeOrPunish>();

        this.punishlist = new ArrayList<PrizeOrPunish>();

        this.medalList = new ArrayList<PrizeOrPunish>();

        this.dioPrizeList = new ArrayList<PrizeOrPunish>();

        this.injuredList = new ArrayList<EmployeeInjured>();

        this.employeeMilitaryService = new EmployeeMilitaryService();

        this.employeeRank = new ArrayList<EmployeeRank>();

        this.successDegreeHistory = new ArrayList<SuccessDegreeHistory>();

        this.employeeLanguageLevel = new ArrayList<EmployeeLanguageLevel>();

        this.academicDegreePosition = new AcademicDegreePosition();

        this.person = new Person();

        this.rank = new Rank();

        this.image = new EmpImageHistory();

        this.positionName = new PositionName();

        this.tree = new TreeSimple();

        this.nomenclature = new Nomenclature();

        this.nationality = new Combo();

        this.passport = new Passport();

        this.employeePosition = new EmployeePosition();

        this.educationHistory = new ArrayList<EducationHistory>();

        this.attestationHistory = new ArrayList<AttestationHistory>();

        this.relatives = new ArrayList<Relative>();

        this.workMissions = new ArrayList<WorkMission>();

        this.workActivityBefore = new ArrayList<WorkActivityBefore>();

        this.employeeVacations = new ArrayList<EmployeeVacation>();

        this.employeeVacationMotherhood = new ArrayList<EmployeeVacation>();

        this.dioList=new ArrayList<EmployeePosition>();
    }

    public List<EmployeePosition> getDioList() {
        return dioList;
    }

    public void setDioList(List<EmployeePosition> dioList) {
        this.dioList = dioList;
    }

    public PoliceCardNew getPoliceCardNew() {
        return policeCardNew;
    }

    public void setPoliceCardNew(PoliceCardNew policeCardNew) {
        this.policeCardNew = policeCardNew;
    }

    public EmployeeWorkBook getEmployeeWorkBook() {
        return employeeWorkBook;
    }

    public void setEmployeeWorkBook(EmployeeWorkBook employeeWorkBook) {
        this.employeeWorkBook = employeeWorkBook;
    }

    public LastFormFillDate getLastFormFillDate() {
        return lastFormFillDate;
    }

    public void setLastFormFillDate(LastFormFillDate lastFormFillDate) {
        this.lastFormFillDate = lastFormFillDate;
    }

    public List<QualificationCourse> getQualificationCourses() {
        return qualificationCourses;
    }

    public void setQualificationCourses(List<QualificationCourse> qualificationCourses) {
        this.qualificationCourses = qualificationCourses;
    }

    public List<WarActivity> getWarActivities() {
        return warActivities;
    }

    public void setWarActivities(List<WarActivity> warActivities) {
        this.warActivities = warActivities;
    }

    public List<PrizeOrPunish> getPunishlist() {
        return punishlist;
    }

    public void setPunishlist(List<PrizeOrPunish> punishlist) {
        this.punishlist = punishlist;
    }

    public List<PrizeOrPunish> getMedalList() {
        return medalList;
    }

    public void setMedalList(List<PrizeOrPunish> medalList) {
        this.medalList = medalList;
    }

    public List<PrizeOrPunish> getDioPrizeList() {
        return dioPrizeList;
    }

    public void setDioPrizeList(List<PrizeOrPunish> dioPrizeList) {
        this.dioPrizeList = dioPrizeList;
    }

    public List<PrizeOrPunish> getPrize() {
        return prize;
    }

    public void setPrize(List<PrizeOrPunish> prize) {
        this.prize = prize;
    }

    public List<EmployeeInjured> getInjuredList() {
        return injuredList;
    }

    public void setInjuredList(List<EmployeeInjured> injuredList) {
        this.injuredList = injuredList;
    }

    public List<EmployeeVacation> getEmployeeVacationMotherhood() {
        return employeeVacationMotherhood;
    }

    public void setEmployeeVacationMotherhood(List<EmployeeVacation> employeeVacationMotherhood) {
        this.employeeVacationMotherhood = employeeVacationMotherhood;
    }

    public List<EmployeeVacation> getEmployeeVacations() {
        return employeeVacations;
    }

    public void setEmployeeVacations(List<EmployeeVacation> employeeVacations) {
        this.employeeVacations = employeeVacations;
    }

    public List<WorkActivityBefore> getWorkActivityBefore() {
        return workActivityBefore;
    }

    public void setWorkActivityBefore(List<WorkActivityBefore> workActivityBefore) {
        this.workActivityBefore = workActivityBefore;
    }

    public List<WorkMission> getWorkMissions() {
        return workMissions;
    }

    public void setWorkMissions(List<WorkMission> workMissions) {
        this.workMissions = workMissions;
    }

    public AcademicDegreePosition getAcademicDegreePosition() {
        return academicDegreePosition;
    }

    public void setAcademicDegreePosition(AcademicDegreePosition academicDegreePosition) {
        this.academicDegreePosition = academicDegreePosition;
    }

    public List<EmployeeLanguageLevel> getEmployeeLanguageLevel() {
        return employeeLanguageLevel;
    }

    public void setEmployeeLanguageLevel(List<EmployeeLanguageLevel> employeeLanguageLevel) {
        this.employeeLanguageLevel = employeeLanguageLevel;
    }

    public List<EmployeeRank> getEmployeeRank() {
        return employeeRank;
    }

    public void setEmployeeRank(List<EmployeeRank> employeeRank) {
        this.employeeRank = employeeRank;
    }

    public List<SuccessDegreeHistory> getSuccessDegreeHistory() {
        return successDegreeHistory;
    }

    public void setSuccessDegreeHistory(List<SuccessDegreeHistory> successDegreeHistory) {
        this.successDegreeHistory = successDegreeHistory;
    }

    public EmployeeMilitaryService getEmployeeMilitaryService() {
        return employeeMilitaryService;
    }

    public void setEmployeeMilitaryService(EmployeeMilitaryService employeeMilitaryService) {

        this.employeeMilitaryService = employeeMilitaryService;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPersonalMatterNo() {
        return personalMatterNo;
    }

    public void setPersonalMatterNo(String personalMatterNo) {
        this.personalMatterNo = personalMatterNo;
    }

    public String getPoliceCardNo() {
        return policeCardNo;
    }

    public void setPoliceCardNo(String policeCardNo) {
        this.policeCardNo = policeCardNo;
    }

    public Combo getNationality() {
        return nationality;
    }

    public void setNationality(Combo nationality) {
        this.nationality = nationality;
    }

    public String getCivil() {
        return civil;
    }

    public void setCivil(String civil) {
        this.civil = civil;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public String getCardMustBeChanged() {
        return cardMustBeChanged;
    }

    public void setCardMustBeChanged(String cardMustBeChanged) {
        this.cardMustBeChanged = cardMustBeChanged;
    }

    public Long getCardReasonId() {
        return cardReasonId;
    }

    public void setCardReasonId(Long cardReasonId) {
        this.cardReasonId = cardReasonId;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public EmpImageHistory getImage() {
        return image;
    }

    public void setImage(EmpImageHistory image) {
        this.image = image;
    }

    public PositionName getPositionName() {
        return positionName;
    }

    public void setPositionName(PositionName positionName) {
        this.positionName = positionName;
    }

    public String getFullStrname() {
        return fullStrname;
    }

    public void setFullStrname(String fullStrname) {
        this.fullStrname = fullStrname;
    }

    public TreeSimple getTree() {
        return tree;
    }

    public void setTree(TreeSimple tree) {
        this.tree = tree;
    }

    public Nomenclature getNomenclature() {
        return nomenclature;
    }

    public void setNomenclature(Nomenclature nomenclature) {
        this.nomenclature = nomenclature;
    }

    public Passport getPassport() {
        return passport;
    }

    public void setPassport(Passport passport) {
        this.passport = passport;
    }

    public EmployeePosition getEmployeePosition() {
        return employeePosition;
    }

    public void setEmployeePosition(EmployeePosition employeePosition) {
        this.employeePosition = employeePosition;
    }

    public List<EducationHistory> getEducationHistory() {
        return educationHistory;
    }

    public void setEducationHistory(List<EducationHistory> educationHistory) {
        this.educationHistory = educationHistory;
    }

    public List<AttestationHistory> getAttestationHistory() {
        return attestationHistory;
    }

    public void setAttestationHistory(List<AttestationHistory> attestationHistory) {
        this.attestationHistory = attestationHistory;
    }

    public List<Relative> getRelatives() {
        return relatives;
    }

    public void setRelatives(List<Relative> relatives) {
        this.relatives = relatives;
    }

    @Override
    public String toString() {

        return "Employee{" +
                "id=" + id +
                ", person=" + person +
                ", createDate=" + createDate +
                ", endDate=" + endDate +
                ", personalMatterNo='" + personalMatterNo + '\'' +
                ", policeCardNo='" + policeCardNo + '\'' +
                ", nationality=" + nationality +
                ", civil='" + civil + '\'' +
                ", ok='" + ok + '\'' +
                ", cardMustBeChanged='" + cardMustBeChanged + '\'' +
                ", cardReasonId=" + cardReasonId +
                ", Rank=" + rank +
                ", image=" + image +
                ", positionName=" + positionName +
                ", fullStrname='" + fullStrname + '\'' +
                ", tree=" + tree +
                ", nomenclature=" + nomenclature +
                ", passport=" + passport +
                ", employeePosition=" + employeePosition +
                ", employeeRank=" + employeeRank +
                ", successDegreeHistory=" + successDegreeHistory +
                ", employeeMilitaryService=" + employeeMilitaryService +
                ", employeeLanguageLevel=" + employeeLanguageLevel +
                ", academicDegreePosition=" + academicDegreePosition +
                ", educationHistory=" + educationHistory +
                ", attestationHistory=" + attestationHistory +
                ", relatives=" + relatives +
                ", workMissions=" + workMissions +
                ", workActivityBefore=" + workActivityBefore +
                ", employeeVacations=" + employeeVacations +
                ", employeeVacationMotherhood=" + employeeVacationMotherhood +
                '}';
    }

}
