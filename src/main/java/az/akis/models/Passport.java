package az.akis.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class Passport {
    private String addressNote;
    private Address adress;
    @JsonFormat(pattern = "MM.dd.yyyy",shape = JsonFormat.Shape.STRING )
    private Date passportGetDate;
    private String passportSerialNo;
    private StructureName policeDepartment;

    public Passport() {

        this.adress=new Address();

        this.policeDepartment=new StructureName();
    }

    public String getAddressNote() {
        return addressNote;
    }

    public void setAddressNote(String addressNote) {
        this.addressNote = addressNote;
    }

    public Address getAdress() {
        return adress;
    }

    public void setAdress(Address adress) {
        this.adress = adress;
    }

    public Date getPassportGetDate() {
        return passportGetDate;
    }

    public void setPassportGetDate(Date passportGetDate) {
        this.passportGetDate = passportGetDate;
    }

    public String getPassportSerialNo() {
        return passportSerialNo;
    }

    public void setPassportSerialNo(String passportSerialNo) {
        this.passportSerialNo = passportSerialNo;
    }

    public StructureName getPoliceDepartment() {
        return policeDepartment;
    }

    public void setPoliceDepartment(StructureName policeDepartment) {
        this.policeDepartment = policeDepartment;
    }

    @Override
    public String toString() {
        return "Passport{" +
                "addressNote='" + addressNote + '\'' +
                ", adress=" + adress +
                ", passportGetDate=" + passportGetDate +
                ", passportSerialNo=" + passportSerialNo +
                ", policeDepartment=" + policeDepartment +
                '}';
    }
}
