package az.akis.models;

import java.util.List;

public class TreeSimple {


   public Long id ;
   private Long parentId ;
   private String text ;
   private String title ;
   public List<TreeSimple> nodes;
   public Long childCount;
   private  String fullName;
   private Boolean yesChild;
   private Long nLeft;
   private Long nRight;
   private Long nLevel;
   private String shortName ;
   private String suffix;
   private Long upperStr;
   private Long structureNameId;
   private boolean selected=false;
   private OrganType organType;


    public TreeSimple() {

        this.organType = new OrganType();

    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Long getStructureNameId() {
        return structureNameId;
    }

    public void setStructureNameId(Long structureNameId) {
        this.structureNameId = structureNameId;
    }

    public OrganType getOrganType() {
        return organType;
    }

    public void setOrganType(OrganType organType) {
        this.organType = organType;
    }

    public Long getnLeft() {
        return nLeft;
    }

    public void setnLeft(Long nLeft) {
        this.nLeft = nLeft;
    }

    public Long getnRight() {
        return nRight;
    }

    public void setnRight(Long nRight) {
        this.nRight = nRight;
    }

    public Long getnLevel() {
        return nLevel;
    }

    public void setnLevel(Long nLevel) {
        this.nLevel = nLevel;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public Long getUpperStr() {
        return upperStr;
    }

    public void setUpperStr(Long upperStr) {
        this.upperStr = upperStr;
    }


    public Boolean getYesChild() {
        return yesChild;
    }

    public void setYesChild(Boolean yesChild) {
        this.yesChild = yesChild;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Long getChildCount() {
        return childCount;
    }

    public void setChildCount(Long childCount) {
        this.childCount = childCount;
    }

    public List<TreeSimple> getNodes() {
        return nodes;
    }



    public void setNodes(List<TreeSimple> nodes) {
        this.nodes = nodes;
    }


    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "TreeSimple{" +
                "id=" + id +
                ", parentId=" + parentId +
                ", text='" + text + '\'' +
                ", title='" + title + '\'' +
                ", nodes=" + nodes +
                ", childCount=" + childCount +
                ", fullName='" + fullName + '\'' +
                ", yesChild=" + yesChild +
                ", nLeft=" + nLeft +
                ", nRight=" + nRight +
                ", nLevel=" + nLevel +
                ", shortName='" + shortName + '\'' +
                ", suffix='" + suffix + '\'' +
                ", upperStr=" + upperStr +
                ", structureNameId=" + structureNameId +
                ", selected=" + selected +
                ", organType=" + organType +
                '}';
    }
}
