package az.akis.models;

import java.math.BigDecimal;
import java.util.Date;

public class Nomenclature {


    private BigDecimal id;
    private String name;
    private Date createDate;
    private Date endDate;
    private String active;
    private String str;
    private long strTreeId;
    private long depNomId;
    private NomDependency nomDependency;

    public Nomenclature() {
        this.nomDependency = new NomDependency();
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public long getStrTreeId() {
        return strTreeId;
    }

    public void setStrTreeId(long strTreeId) {
        this.strTreeId = strTreeId;
    }

    public long getDepNomId() {
        return depNomId;
    }

    public void setDepNomId(long depNomId) {
        this.depNomId = depNomId;
    }

    public NomDependency getNomDependency() {
        return nomDependency;
    }

    public void setNomDependency(NomDependency nomDependency) {
        this.nomDependency = nomDependency;
    }

    @Override
    public String toString() {
        return "Nomenclature{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", createDate=" + createDate +
                ", endDate=" + endDate +
                ", active='" + active + '\'' +
                ", str='" + str + '\'' +
                ", strTreeId=" + strTreeId +
                ", depNomId=" + depNomId +
                ", nomDependency=" + nomDependency +
                '}';
    }
}
