package az.akis.models;

import java.util.Date;

public class PoliceCardNew {

    private String weight;
    private Date validity;
    //private signAuthor;
    private String showGender;
    private CardSettings settings;
    private  Combo language;
    private Long id;
    private String height;
    private Date givenDate;
    private String eyeColor;
    private String blood;
    private String armSerialNo;
    private String armpermission;

    public PoliceCardNew( ) {
        this.settings = new CardSettings();
        this.language = new Combo();
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public Date getValidity() {
        return validity;
    }

    public void setValidity(Date validity) {
        this.validity = validity;
    }

    public String getShowGender() {
        return showGender;
    }

    public void setShowGender(String showGender) {
        this.showGender = showGender;
    }

    public CardSettings getSettings() {
        return settings;
    }

    public void setSettings(CardSettings settings) {
        this.settings = settings;
    }

    public Combo getLanguage() {
        return language;
    }

    public void setLanguage(Combo language) {
        this.language = language;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public Date getGivenDate() {
        return givenDate;
    }

    public void setGivenDate(Date givenDate) {
        this.givenDate = givenDate;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    public String getArmSerialNo() {
        return armSerialNo;
    }

    public void setArmSerialNo(String armSerialNo) {
        this.armSerialNo = armSerialNo;
    }

    public String getArmpermission() {
        return armpermission;
    }

    public void setArmpermission(String armpermission) {
        this.armpermission = armpermission;
    }
}
