package az.akis.models;
import java.util.Date;

public class Rank {

    private Long id;
        private String name;
       //private Long rankTypeId;
        private String active;
        private Date createDate;
        private Date endDate;
        private Long nextRankPeriod;
        private String suffix;
        private Long orderP;
        private RankType rankType;

    public Rank() {

        this.rankType = new RankType();

    }

    public Date getCreateDate() {
            return createDate;
        }

        public void setCreateDate(Date createDate) {
            this.createDate = createDate;
        }

        public Date getEndDate() {
            return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RankType getRankType() {
        return rankType;
    }

    public void setRankType(RankType rankType) {
        this.rankType = rankType;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }


    public Long getNextRankPeriod() {
        return nextRankPeriod;
    }

    public void setNextRankPeriod(Long nextRankPeriod) {
        this.nextRankPeriod = nextRankPeriod;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public Long getOrderP() {
        return orderP;
    }

    public void setOrderP(Long orderP) {
        this.orderP = orderP;
    }

    @Override
    public String toString() {
        return "Rank{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", active='" + active + '\'' +
                ", createDate=" + createDate +
                ", endDate=" + endDate +
                ", nextRankPeriod=" + nextRankPeriod +
                ", suffix='" + suffix + '\'' +
                ", orderP=" + orderP +
                ", rankType=" + rankType +
                '}';
    }
}