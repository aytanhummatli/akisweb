package az.akis.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class EmployeeRank {

   private Long id;
   private Employee empId;
   private Rank rank;

   @JsonFormat(pattern = "MM.dd.yyyy",shape = JsonFormat.Shape.STRING )
   private Date createDate;

   private String rankOutOfTurn;

   private String orderNo;

   private  Combo cmbsService;

    public EmployeeRank() {
        this.rank = new Rank();
        this.cmbsService=new Combo();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Combo getCmbsService() {
        return cmbsService;
    }

    public void setCmbsService(Combo cmbsService) {
        this.cmbsService = cmbsService;
    }

    public Employee getEmpId() {
        return empId;
    }

    public void setEmpId(Employee empId) {
        this.empId = empId;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getRankOutOfTurn() {
        return rankOutOfTurn;
    }

    public void setRankOutOfTurn(String rankOutOfTurn) {
        this.rankOutOfTurn = rankOutOfTurn;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    @Override
    public String toString() {
        return "EmployeeRank{" +
                "empId=" + empId +
                ", Rank=" + rank +
                ", createDate=" + createDate +
                ", rankOutOfTurn='" + rankOutOfTurn + '\'' +
                ", orderNo='" + orderNo + '\'' +
                '}';
    }
}
