package az.akis.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class EmployeeMilitaryService {


    private Long id;
    private String armyPort;
    @JsonFormat(pattern = "MM.dd.yyyy",shape = JsonFormat.Shape.STRING )
    private Date beginDate;
    @JsonFormat(pattern = "MM.dd.yyyy",shape = JsonFormat.Shape.STRING )
    private Date endDate;
    private Combo militaryDuty;
    private String isRegistered;
    private String serialNo;
    private Long empId;

    public EmployeeMilitaryService() {
        militaryDuty=new Combo();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEmpId() {
        return empId;
    }

    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    public String getArmyPort() {
        return armyPort;
    }

    public void setArmyPort(String armyPort) {
        this.armyPort = armyPort;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Combo getMilitaryDuty() {
        return militaryDuty;
    }

    public void setMilitaryDuty(Combo militaryDuty) {
        this.militaryDuty = militaryDuty;
    }

    public String getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(String isRegistered) {
        this.isRegistered = isRegistered;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    @Override
    public String toString() {
        return "EmployeeMilitaryService{" +
                "armyPort='" + armyPort + '\'' +
                ", beginDate=" + beginDate +
                ", endDate=" + endDate +
                ", militaryDuty=" + militaryDuty +
                ", isRegistered='" + isRegistered + '\'' +
                ", serialNo='" + serialNo + '\'' +
                '}';
    }
}
