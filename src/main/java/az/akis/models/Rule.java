package az.akis.models;

import java.math.BigDecimal;
import java.util.Date;

public class Rule {
    private BigDecimal id;
    private BigDecimal createByUserId;
    private String ruleName;
    private Date createDate;
    private Date endDate;
    private String active;
    private UserRule userRule;
    public Rule(){
        this.userRule=new UserRule();
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getCreateByUserId() {
        return createByUserId;
    }

    public void setCreateByUserId(BigDecimal createByUserId) {
        this.createByUserId = createByUserId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public UserRule getUserRule() {
        return userRule;
    }

    public void setUserRule(UserRule userRule) {
        this.userRule = userRule;
    }

    @Override
    public String toString() {
        return "Rule{" +
                "id=" + id +
                ", createByUserId=" + createByUserId +
                ", ruleName='" + ruleName + '\'' +
                ", createDate=" + createDate +
                ", endDate=" + endDate +
                ", active='" + active + '\'' +
                ", userRule=" + userRule +
                '}';
    }
}
