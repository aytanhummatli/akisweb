package az.akis.models;

public class University {
    private Long id;
    private Country country;
    private Combo eduStructureType;


    public University( ) {
        this.country =new Country();
        this.eduStructureType = new Combo();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Combo getEduStructureType() {
        return eduStructureType;
    }

    public void setEduStructureType(Combo eduStructureType) {
        this.eduStructureType = eduStructureType;
    }

    @Override
    public String toString() {
        return "University{" +
                "id=" + id +
                ", country=" + country +
                ", eduStructureType=" + eduStructureType +
                '}';
    }
}
