package az.akis.models;

import java.util.Date;

public class Position {

    public Long id;
    private String active;
    private Date createDate;
    private Long posGroupId;
    private Date endDate;

    private PositionGroup positionGroup;
   // private PositionName positionName;

    public Position() {
        this.positionGroup = new PositionGroup();
       // this.positionName = new PositionName();
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getPosGroupId() {
        return posGroupId;
    }

    public void setPosGroupId(Long posGroupId) {
        this.posGroupId = posGroupId;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public PositionGroup getPositionGroup() {
        return positionGroup;
    }

    public void setPositionGroup(PositionGroup positionGroup) {
        this.positionGroup = positionGroup;
    }

//    public PositionName getPositionName() {
//        return positionName;
//    }
//
//    public void setPositionName(PositionName positionNames) {
//        this.positionName = positionNames;
//    }


    @Override
    public String toString() {
        return "Position{" +
                "id=" + id +
                ", active='" + active + '\'' +
                ", createDate=" + createDate +
                ", posGroupId=" + posGroupId +
                ", endDate=" + endDate +
                ", hrPositionGroup=" + positionGroup +
              //  ", hrPositionNames=" + positionName +
                '}';
    }

}
