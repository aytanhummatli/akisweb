package az.akis.models;

public class PositionName
{
    private Long posId;
    private Long id;
    private String name;
    private String active;
    private Position position;

    public PositionName() {
        this.position=new Position();
    }

    public Long getPosId() {
        return posId;
    }

    public void setPosId(Long posId) {
        this.posId = posId;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "PositionNames{" +
                "id=" + id +
                ", posId=" + posId +
                ", name='" + name + '\'' +
                ", active='" + active + '\'' +
                '}';
    }
}
