package az.akis.models;

import java.util.Date;

public class EmployeeWorkBook {

    private Date sendDate;
    private Combo note2;
    private Combo note1;
    private Long isReceipt;
    private String envelopNumber;
    private String checked;

    public EmployeeWorkBook() {

        this.note1=new Combo();

        this.note2=new Combo();
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Combo getNote2() {
        return note2;
    }

    public void setNote2(Combo note2) {
        this.note2 = note2;
    }

    public Combo getNote1() {
        return note1;
    }

    public void setNote1(Combo note1) {
        this.note1 = note1;
    }

    public Long getIsReceipt() {
        return isReceipt;
    }

    public void setIsReceipt(Long isReceipt) {
        this.isReceipt = isReceipt;
    }

    public String getEnvelopNumber() {
        return envelopNumber;
    }

    public void setEnvelopNumber(String envelopNumber) {
        this.envelopNumber = envelopNumber;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    @Override
    public String toString() {
        return "EmployeeWorkBook{" +
                "sendDate=" + sendDate +
                ", note2=" + note2 +
                ", note1=" + note1 +
                ", isReceipt=" + isReceipt +
                ", envelopNumber='" + envelopNumber + '\'' +
                ", checked='" + checked + '\'' +
                '}';
    }
}
