package az.akis.models;

import java.util.Date;

public class Password {
    private Long id;
    private Long userId;
    private String password;
    private Date createDate;
    private Date endDate;
    private String active;
    private String newPassword;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public String toString() {
        return "Password{" +
                "id=" + id +
                ", userId=" + userId +
                ", password='" + password + '\'' +
                ", createDate=" + createDate +
                ", endDate=" + endDate +
                ", active='" + active + '\'' +
                ", newPassword='" + newPassword + '\'' +
                '}';
    }
}
