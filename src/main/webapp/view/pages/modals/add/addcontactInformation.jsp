<%--
  Created by IntelliJ IDEA.
  User: Moonlight
  Date: 05.04.2019
  Time: 12:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/ng-template" id="addContactInformation.html"   >

    <div class="modal-header">

        <h4>Yeni əlaqə məlumatlarının əlavə edilməsi</h4>

    </div>

    <div class="modal-body">

        <div class="formcontainer">

            <form ng-submit="submit()" name="myForm" class="form-horizontal">

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable" for="contactInformation">Əlaqə növü</label>
                        <div class="col-md-7">
                            <select name="contactInformation"  id="contactInformation" ng-model="contactInformation"

                                    ng-options="contactInformation.name for contactInformation in contactInformations track by contactInformation.id" class="field form-control input-sm"  required  >

                                <option   value="">Seçim et </option>
                            </select>

                            <div class="has-error" ng-show="myForm.$dirty">
                                <%--  <span ng-show="myForm.$error.required">kategoryani daxil edin</span>
                                  <span ng-show="myForm.technicalFailure.id.$invalid">kategorya düzgün daxil edilməyib </span> --%>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="form-group col-md-12">

                        <label class="col-md-4 control-lable" for="value">Adı</label>

                        <div class="col-md-7">

                            <input type="text" ng-model="contactInformation.value"   name="value"

                                   id="value" class="field form-control input-sm" placeholder="Qısa adı daxil edin"/>

                            <div class="has-error" ng-show="myForm.$dirty">

                                <span ng-show="myForm.aShortName.$error.required">Adı daxil edin</span>

                                <%--  <span ng-show="myForm.name.$error.minlength">Başlığı 7 sinvoldan ibarət olmaldi</span>  --%>

                                <span ng-show="myForm.aShortName.$invalid">Daxil edilən məlumatlar düzgün deyil </span>

                            </div>

                        </div>

                    </div>

                </div>

                <br>

                <div class="row" style="position: absolute; bottom:5px;right: 20px">

                    <div>

                        <input type="submit"  value="Əlavə et" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">

                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>

                    </div>

                </div>

            </form>

        </div>

    </div>

</script>


