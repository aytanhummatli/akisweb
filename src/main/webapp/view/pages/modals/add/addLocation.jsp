<%--
  Created by IntelliJ IDEA.
  User: Moonlight
  Date: 05.04.2019
  Time: 12:55
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/ng-template" id="addLocation.html">

    <div class="modal-header">

        <h4>Ünvan</h4>

    </div>

    <div class="modal-body">

        <div class="formcontainer">

            <form ng-submit="submit()" name="myForm" class="form-horizontal">

                <div class="row">

                    <div class="col-md-6">

                        <div class="panel panel-default" style="height: 170px">

                            <div class="panel-body">

                                <div class="radio">

                                    <label><input ng-click="getCountryList()"   name="location" type="radio"> Ölkə </label>

                                </div>

                                <div class="radio">

                                    <label><input  ng-click="getRegionList()"  type="radio" name="location" >Region</label>

                                </div>

                                <div class="radio">

                                    <label><input ng-click="getSubRegionList()" type="radio"    name="location" >Altregion</label>

                                </div>


                                <div class="radio">

                                    <label><input ng-click="getVillageList()"   type="radio" name="location" >Kənd/Qəsəbə</label>

                                </div>

                                <div class="radio">

                                    <label><input ng-click="getStreetList()"  type="radio" name="location"  >Küçə</label>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="panel panel-default" style="height: 170px">

                            <div class="panel-body">

                                <ul style="overflow: auto;height: 140px" >

                                    <li  ng-repeat="elem in mylisyList"> {{elem.name}} </li>

                                </ul>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-6">

                        <label > Qeyd: </label>

                        <input   type="text" class="form-control"   placeholder="Qeyd">

                    </div>

                    <div class="col-md-6">

                        <label > Axtarış </label>

                        <input    type="text" class="form-control"     placeholder="Axtarış">

                    </div>

                </div>

                <br>
                <br>
                <br>
                <div class="row" style="position: absolute; bottom:1px; right: 20px; ">

                    <div>

                        <input type="submit" value="Əlavə et" class="btn btn-primary btn-sm"

                               ng-disabled="myForm.$invalid">

                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>

                    </div>

                </div>

            </form>

        </div>

    </div>

</script>