<%--
  Created by IntelliJ IDEA.
  User: Moonlight
  Date: 05.04.2019
  Time: 12:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/ng-template" id="addEmployeeRank.html"   >

    <div class="modal-header">

        <h4>Yeni rütbə məlumatlarının əlavə edilməsi</h4>

    </div>

    <div class="modal-body">

        <div class="formcontainer">

            <form ng-submit="submit()" name="myForm" class="form-horizontal">

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable" for="cmbsService">Rütbə növü</label>
                        <div class="col-md-7">
                            <select name="cmbsService"  id="cmbsService" ng-model="cmbsService"

                                    ng-options="cmbsService.description for cmbsService in cmbsServiceList track by cmbsService.id" class="field form-control input-sm"  required  >

                                <option   value="">Seçim et </option>

                            </select>

                            <div class="has-error" ng-show="myForm.$dirty">

                                <%--  <span ng-show="myForm.$error.required">kategoryani daxil edin</span>
                                  <span ng-show="myForm.technicalFailure.id.$invalid">kategorya düzgün daxil edilməyib </span> --%>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable" for="rank">Rütbəsi</label>
                        <div class="col-md-7">
                            <select name="rank"  id="rank" ng-model="rank"

                                    ng-options="rank.name for rank in rankList track by rank.id" class="field form-control input-sm"  required  >

                                <option   value="">Seçim et </option>
                            </select>

                            <div class="has-error" ng-show="myForm.$dirty">
                                <%--  <span ng-show="myForm.$error.required">kategoryani daxil edin</span>
                                  <span ng-show="myForm.technicalFailure.id.$invalid">kategorya düzgün daxil edilməyib </span> --%>
                            </div>
                        </div>
                    </div>
                </div>

                <%--<div class="row">--%>

                    <%--<div class="form-group col-md-12">--%>

                        <%--<label class="col-md-4 control-lable" for="createDate">Aldığı tarix</label>--%>

                        <%--<div class="col-md-7">--%>

                            <%--<input type="date" ng-model="employeeRank.createDate"   name="createDate"--%>

                                   <%--id="createDate" class="field form-control input-sm" placeholder="Aldığı tarix"/>--%>

                            <%--<div class="has-error" ng-show="myForm.$dirty">--%>

                                <%--<span ng-show="myForm.getDate.$error.required">Tarixi daxil edin</span>--%>

                                <%--&lt;%&ndash;  <span ng-show="myForm.name.$error.minlength">Başlığı 7 sinvoldan ibarət olmaldi</span>  &ndash;%&gt;--%>

                                <%--<span ng-show="myForm.getDate.$invalid">Daxil edilən məlumatlar düzgün deyil </span>--%>

                            <%--</div>--%>

                        <%--</div>--%>

                    <%--</div>--%>

                <%--</div>--%>

                <div class="row">

                    <div class="form-group col-md-12">

                        <label class="col-md-4 control-lable" for="orderNo">Əmr nömrəsi</label>

                        <div class="col-md-7">

                            <input type="text" ng-model="employeeRank.orderNo"   name="orderNo"

                                   id="orderNo" class="field form-control input-sm" placeholder="Əmr nömrəsi"/>

                            <div class="has-error" ng-show="myForm.$dirty">

                                <span ng-show="myForm.orderNo.$error.required">Adı daxil edin</span>

                                <%--  <span ng-show="myForm.name.$error.minlength">Başlığı 7 sinvoldan ibarət olmaldi</span>  --%>

                                <span ng-show="myForm.orderNo.$invalid">Daxil edilən məlumatlar düzgün deyil </span>

                            </div>

                        </div>

                    </div>

                </div>


                <%--<div class="row">--%>
                    <%--<div class="form-group col-md-4">--%>
                    <%--</div>--%>
                    <%--<div class="form-group col-md-7">--%>

                    <%--<div class="checkbox">--%>

                        <%--<label > <input  id="sonuncu" name="Sonuncu"   type="checkbox" value=""  >Sonuncu </label>--%>

                    <%--</div>--%>

                <%--</div>--%>

                <%--</div>--%>

                <div class="row">

                    <div class="form-group col-md-4">
                    </div>
                    <div class="form-group col-md-7">

                        <div class="checkbox">

                            <label > <input id="novbedenKenar" name="rankOutOfTurn" ng-model="rankOutOfTurn" type="checkbox" value=""  >Növbədən kənar rütbə artımı </label>

                        </div>

                    </div>
                </div>

                <br>

                <div class="row" style="position: absolute; bottom:5px;right: 20px">

                    <div>

                        <input type="submit"  value="Əlavə et" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">

                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>

                    </div>

                </div>

            </form>

        </div>

    </div>

</script>


