<%--

  Created by IntelliJ IDEA.
  User: Moonlight
  Date: 05.04.2019
  Time: 12:55
  To change this template use File | Settings | File Templates.

--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/ng-template" id="modalNomenclature.html">

    <div class="modal-header">

        <h4>Nomenklatura</h4>

    </div>

    <div class="modal-body">

        <div class="formcontainer">

            <form ng-submit="submit()" name="myForm" class="form-horizontal">

                <div class="row">

                    <%--<div class="form-group col-md-12">--%>


                        <div class="col-md-11">

                            <input type="text" name="search"

                                ng-model="search"   id="search" class="field form-control input-sm"

                                   placeholder="axtarış mətnini daxil edin"/>

                        </div>

                        <div class="col-md-1">

                            <a class="  btn btn-success btn-sm"
                               ng-click="getSearchResults()"><span
                                    class="glyphicon glyphicon-search"></span></a>
                        </div>

                    <%--</div>--%>

                </div>

                <br>

                <div class="row">

                    <div class="col-md-12">

                        <div class="panel panel-default">

                            <div class="table-wrapper-scroll-y">

                                <table id="contdactjd" class="table table-hover"

                                       style="font-size: small; border: 1px solid #ddd !important; ">

                                    <thead>

                                    <tr class="headings">

                                        <th class="column-title"><label>Id</label></th>

                                        <th class="column-title"><label>Adı</label></th>

                                    </tr>

                                    </thead>

                                    <tbody>

                                    <tr  ng-dblclick="selectNomenklature(this)" ng-repeat="info in nomenkList">

                                        <td  class=" "><span ng-bind="info.id"> </span></td>

                                        <td class=" "><span ng-bind="info.name"> </span></td>

                                    </tr>

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

                <br>

                <div class="row">

                    <div class="col-md-12">

                        <div class="panel panel-default">

                            <label style="margin-left: 50%;"> Tarixinə qədər</label>

                            <div class="checkbox">

                                <input  style="margin-left: 30px; " name="DIN" type="checkbox" value="" data-parsley-required="false">

                                <label  style="margin-left: 30px; ">  Müvəqqəti </label>

                            </div>
                            <br>
                        </div>

                    </div>

                </div>

<br>
                <div class="row" style="position: absolute; bottom:5px;right: 20px">

                    <div>

                        <input type="submit" value="Əlavə et" class="btn btn-primary btn-sm"

                               ng-disabled="myForm.$invalid">

                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>

                    </div>

                </div>

            </form>

        </div>

    </div>

</script>