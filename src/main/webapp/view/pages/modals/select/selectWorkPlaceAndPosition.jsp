<%--
  Created by IntelliJ IDEA.
  User: Moonlight
  Date: 05.04.2019
  Time: 12:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/ng-template" id="modalWorkPlaceAndPosition.html">

    <div class="modal-header">

        <h4>İş yeri və vəzifəsi</h4>

    </div>

    <div class="modal-body">

        <div class="formcontainer">
            <ul class="nav nav-tabs" id="tabContent">
                <li class="active"><a href="#firstTab" data-toggle="tab">DİN Strukturu</a></li>
                <li><a href="#secondTab" data-toggle="tab">Qurum Sərəncamında</a></li>
                <li><a href="#thirdTab" data-toggle="tab">Xaricdə</a></li>
            </ul>

            <%--din strukturu --%>
            <div class="tab-content">
                <div class="tab-pane active" id="firstTab">
                    <div class="row">

                        <div class="col-md-4" style="border-right: 1px;">
                            <div class="panel panel-default" style="height: 400px;width: 250px">
                                <div style="height: 20px;border-bottom:1px solid #BAB8B8">
                                    <i class="fas fa-home"></i>
                                    <i class="far fa-file"></i>
                                    <i class="fas fa-sync-alt"></i>
                                </div>
                                <div class="panel-body" style="height: 400px;">
                                    sol panel
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 control-lable">Axtarış:</label>
                                <div class="col-sm-3">
                                    <input type="text" class="field form-control input-sm"
                                           style="width: 150px;height: 15px"/>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-8">
                            <div class="panel panel-default" style="height: 400px;width: auto">
                                <div style="height: 20px;width: auto;border-bottom: 1px solid #BAB8B8;">
                                   <i class="fas fa-home"></i>
                                  <i class="far fa-file"></i>
                                  <i class="fas fa-sync-alt"></i>
                                </div>
                                <table border="1px solid #BAB8B8">
                                    <thead>
                                    <tr>
                                        <th>A</th>
                                        <th>B</th>
                                        <th>C</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>a</td>
                                        <td>b</td>
                                        <td>c</td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>

                </div>
<%--qurum serencaminda--%>
                <div class="tab-pane" id="secondTab">
                    <div class="panel panel-default" style="height: 22px;width: auto">
                        <div style="height: 22px;border:1px solid #BAB8B8">
                          <i class="fas fa-home"></i>
                            <i class="far fa-file"></i>
                            <i class="far fa-folder-open"></i>
                          <i class="fas fa-sync-alt"></i>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default" style="height: 400px;width: auto">

                        </div>

                                <input type="text" class="field form-control input-sm"
                                       style="width: 265px;height: 15px"/>

                    </div>
                    <div class="col-md-6">
                        <%--<div class="panel panel-default" style="height: 20px;width: auto">--%>

                        <%--</div>--%>
                            <div class="row">
                                <label class="col-sm-3 control-lable">Axtarish:</label>
                                <div class="col-sm-3">
                                    <input type="text" class="field form-control input-sm"
                                           style="width: 193px;height: 15px"/>
                                </div>
                            </div>

                        <div class="panel panel-default" style="height: 415px;width: auto">

                        </div>
                    </div>

                    <div class="row">

                        <div style="height:500px">

                            <input style="position: absolute; bottom:5px;left: 150px" type="submit" value="Təsdiq et"
                                   class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">

                            <button style="position: absolute; bottom:5px;right: 150px" type="button"
                                    ng-click="cancel()" class="btn btn-warning btn-sm">Bağla
                            </button>

                        </div>

                    </div>

                </div>

<%--xaricde--%>
                <div class="tab-pane" id="thirdTab">
                    <div class="row">
                        <div class="col-md-7" style="border-right: 1px;">
                            <div class="panel panel-default" style="height: 22px;width:350px">
                                <div style="height: 22px;border:1px solid #BAB8B8">
                                    <i class="fas fa-home" ng-click="op"></i>
                                    <i class="far fa-file"></i></button>
                                    <i class="fas fa-sync-alt"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5" style="border-right: 1px;">
                            <div class="panel panel-default" style="height: 22px;width: auto">
                                <div style="height: 22px;border:1px solid #BAB8B8">
                                    <i class="far fa-file"></i>
                                    <i class="far fa-file"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7" style="border-right: 1px;">
                        <div class="row">
                            <label class="col-sm-3 control-lable">Axtarış:</label>
                            <div class="col-sm-3">
                                <input type="text" class="field form-control input-sm"
                                       style="width: 208px;height: 15px"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="panel panel-default" style="height: 400px;width: 300px">

                                <div class="panel-body" style="height: 400px;width: 300px">
                                    sol panel
                                </div>
                            </div>
                        </div>

                        <div class="row">
                                <input type="text" class="field form-control input-sm"
                                       style="width: 300px;height: 15px"/>
                        </div>
                    </div>
                    <div class="col-md-5" style="border-right: 1px;">

                        <div class="row">
                            <p style="text-align: center">Vezife qruplari</p>
                           <label class="col-sm-4 control-lable"> <input type="search" class="field form-control input-sm" style="height: 15px;width: 20px"/></label>
                            <select  class="col-sm-8"style="width:155px;height: 15px">
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="row">
                            <p style="text-align: center;height: 15px">Vezifeler</p>
                            <label class="col-sm-3 control-lable">Axtarış:</label>
                            <div class="col-sm-3">
                                <input type="text" class="field form-control input-sm"
                                       style="width: 167px;height: 15px"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="panel panel-default" style="height: 325px;width: auto">

                                <div class="panel-body" style="height: 325px;width:auto">
                                    sag panel
                                </div>
                            </div>
                        </div>

                        <div class="row">
                                <input type="text" class="field form-control input-sm"
                                       style="width: auto;height: 15px"/>
                        </div>

                    </div>


                    <div class="row">

                        <div style="height:500px">

                            <input style="position: absolute; bottom:5px;left: 150px" type="submit" value="Təsdiq et"
                                   class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">

                            <button style="position: absolute; bottom:5px;right: 150px" type="button"
                                    ng-click="cancel()" class="btn btn-warning btn-sm">Bağla
                            </button>

                        </div>

                    </div>

                </div>
            </div>

        </div>

    </div>


</script>


