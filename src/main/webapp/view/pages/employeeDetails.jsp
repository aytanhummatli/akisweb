<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Şəxsiyyət məlumatları </title>

    <jsp:include page="../layout/rakCss.jsp"/>

    <!-- bootstrap-wysiwyg -->
    <link href="../static/resource/css/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../static/resource/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../static/resource/css/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="../static/resource/css/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../static/resource/css/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../static/resource/css/custom.min.css" rel="stylesheet">
    <%--datepicker css--%>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <style>
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
        .table-wrapper-scroll-y {
            display: block;
            max-height: 300px;
            overflow-y: auto;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }
    </style>

</head>

<body class="nav-md" ng-app="appEmployeeDetails">

<div class="container body" ng-controller="employeeDetailsController as empCtrl">

    <div class="main_container">

        <div class="col-md-3 left_col">

            <div class="left_col scroll-view">

                <br>

                <!-- sidebar menu -->
                <jsp:include page="../layout/sidebarMenuForEmployeeDetails.jsp"/>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">

                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>

                </div>
                <!-- /menu footer buttons -->
            </div>

        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="images/img.jpg" alt="">John Doe
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;"> Profile</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li><a href="javascript:;">Help</a></li>
                                <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">

                <div class="page-title">

                </div>

                <div class="clearfix"></div>

                <div  class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Şəxsi məlumatlar

                                </h2>
                                <ul class="nav navbar-right panel_toolbox">

                                    <li>
                                  <a href="#">Təsdiqlə</a>
                                    </li>

                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>

                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form id="form5" data-parsley-validate class="form-horizontal form-label-left">

                                    <%--  Soyad, şəxsi iş, şəxsi işi tamdır   --%>
                                    <div class="row">

                                        <div class="form-group col-md-5">

                                            <div class="row">

                                                <div class="form-group">

                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Soyadı</label>

                                                    <div class="col-md-9 col-sm-9 col-xs-12">

                                                        <input type="text" ng-model="employee.person.surName" class="form-control" placeholder="Soyadı">

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Adı</label>

                                                    <div class="col-md-9 col-sm-9 col-xs-12">

                                                        <input type="text" ng-model="employee.person.name" class="form-control" placeholder="Adı">

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Ata adı</label>

                                                    <div class="col-md-9 col-sm-9 col-xs-12">

                                                        <input type="text" ng-model="employee.person.patronymic " class="form-control" placeholder="Ata Adı">

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <label   class="col-md-3 control-lable" for="location"> Hal-hazırki
                                                        ünvanı</label>

                                                    <div class="col-md-8">

                                                        <input  type="text" ng-model="employee.location" name="location"
                                                               id="location"
                                                               class="field form-control input-sm"
                                                               placeholder="Ünvanı daxil edin"/>

                                                        <div class="has-error" ng-show="myForm.$dirty">

                                                        </div>

                                                    </div>

                                                    <div class="col-md-1 col -sm-1 col-xs-12">

                                                        <a style="height: 30px" class="  btn btn-default btn-xs"
                                                           data-nodrag

                                                           ng-click="selectTree(this)"><span
                                                                class="glyphicon glyphicon-option-horizontal"></span></a>

                                                    </div>

                                                </div>

                                                <div class="form-group  ">

                                                            <label class="col-md-3 control-lable" for="location"> Əlaqə məlumatları</label>

                                                            <div class="col-md-9 col -sm-9 col-xs-9">

                                                                <table id="contact" class="table table-hover"
                                                                       style="font-size: small; border: 1px solid #ddd !important;">
                                                                    <thead>
                                                                    <tr class="headings">
                                                                        <th class="column-title"><label> Əlaqə növü </label> </th>

                                                                        <th class="column-title"><label>Adı</label></th>

                                                                        <th>

                                                                            <a class="  btn btn-default btn-xs"
                                                                               ng-click="addContactInfo()"><span
                                                                                    class="glyphicon glyphicon-plus">  </span></a>
                                                                        </th>
                                                                        <th>

                                                                        </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    <tr  ng-repeat="inf in employee.person.contactInformation" >
                                                                        <td class=" "><span ng-bind="inf.name"></span></td>
                                                                        <td class=" "><span ng-bind="inf.value"></span></td>
                                                                        <td class=" ">

                                                                            <button type="button" ng-click="editContactInfo(this)"
                                                                                    class="  btn btn-default btn-xs"  >Redaktə
                                                                            </button>
                                                                        </td>
                                                                        <td class=" ">
                                                                            <a class="  btn btn-default btn-xs"
                                                                               ng-click="deleteContactInfo(this)"><span
                                                                                    class="glyphicon glyphicon-trash">  </span></a>
                                                                        </td>

                                                                    </tr>


                                                                    </tbody>

                                                                </table>


                                                            </div>


                                                        </div>

                                                <div class="form-group">

                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nomenklatura</label>

                                                    <div class="col-md-8 col-sm-8 col-xs-12">

                                                        <input  ng-model="employee.nomenclature.name" type="text" class="form-control"
                                                               placeholder="Nomenklatura">

                                                    </div>

                                                    <div class="col-md-1 col -sm-1 col-xs-12">


                                                        <a style="height: 30px" class="  btn btn-default btn-xs"
                                                           data-nodrag
                                                           ng-click="selectNomenclature(this)"><span
                                                                class="glyphicon glyphicon-option-horizontal"></span></a>
                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                                   <div class="checkbox">

                                                        <input  name="DIN" type="checkbox" value="" data-parsley-required="false" > <label> Daxili İşlər Nazirliyi orqanlarından xidmətdən xaric olunub </label>

                                                    </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                        <div class="form-group col-md-4">

                                            <div class="form-group">

                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Şəxsi iş
                                                    nömrəsi</label>

                                                <div class="col-md-9 col-sm-9 col-xs-12">

                                                    <input ng-model="employee.personalMatterNo" type="text" class="form-control"
                                                           placeholder="Şəxsi iş nömrəsi">

                                                </div>

                                            </div>

                                            <div class="form-group">

                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Kimlik
                                                    nömrəsi</label>

                                                <div class="col-md-9 col-sm-9 col-xs-12">

                                                    <input ng-model="employee.policeCardNo" type="text" class="form-control"
                                                           placeholder="Kimlik nömrəsi">

                                                </div>

                                            </div>

                                            <div class="form-group ">
                                                <%--Qızlıq soyadı--%>

                                                    <div class="col-md-2 col-sm-2 col-xs-2">

                                                    </div>

                                                    <div class="col-md-10 col-sm-10 col-xs-10">

                                                <div class="checkbox">

                                                    <label> <input name="QSA"  type="checkbox" value="" data-parsley-required="false"> Qızlıq soyadı   </label>

                                                </div>

                                                    </div>

                                            </div>

                                        </div>

                                        <div class="form-group col-md-3">

                                            <div class="form-group ">
                                                <%--Şəxsi işi tamdır--%>
                                                <div class="checkbox">

                                                    <label> <input name="SIT" type="checkbox" value="" data-parsley-required="false"> Şəxsi işi tamdır   </label>

                                                </div>

                                                <%-- Mülki əməkdaş--%>
                                                <div class="checkbox">

                                                    <label> <input name="ME" type="checkbox" value="" ng-model="employee.civil" data-parsley-required="false"> Mülki əməkdaş </label>

                                                </div>

                                            </div>

                                            <div class="form-group  ">
                                                <%--<label>Şəkil axtar </label>--%>

                                                <input type="text" ng-model="search" name="search" id="search"

                                                       class="field form-control input-sm"

                                                       placeholder="Şəkil axtar"/>

                                            </div>

                                            <div class="form-group">

                                                <label class="btn btn-default btn-sm " >
                                                    Şəkil seç<input type="file" style="display: none;" >
                                                </label>

                                            </div>

                                        </div>

                                        </div>

                                </form>

                            </div>


                <div class="row">

                    <div class="col-md-6 col-xs-12">

                        <div class="x_panel">
                            <div class="x_title">
                                <h2>İş yeri
                                </h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false"><i class="fa fa-wrench"></i></a>


                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form class="form-horizontal form-label-left input_mask">

                                    <div class="form-group">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-" for="work">  İş yeri və vəzifəsi
                                             </label>

                                        <div class="col-md-8">

                                            <input type="text" ng-model="employee.employeePosition.structurePosition.fullSttrName" name="location"
                                                   id="work"
                                                   class="field form-control input-sm"
                                                   placeholder="Iş yerini daxil edin"/>

                                            <div class="has-error" ng-show="myForm.$dirty">

                                            </div>

                                        </div>

                                        <div class="col-md-1 col -sm-1 col-xs-12">

                                            <a style="height: 30px" class="  btn btn-default btn-xs"
                                               data-nodrag

                                               ng-click="workPlaceAndPosition(this)"><span
                                                    class="glyphicon glyphicon-option-horizontal"></span></a>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Əmr
                                            nömrəsi</label>

                                        <div class="col-md-9 col-sm-9 col-xs-12">

                                            <input  ng-model="employee.employeePosition.enterOrder" type="text" class="form-control"
                                                   placeholder="Əmr nömrəsi">
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Qəbul tarixi</label>

                                        <div class="col-md-9 col-sm-9 col-xs-12">

                                            <%--<input ng-model="Employee.employeePosition.orderDate | date:'dd.MM.yyyy'" type="date:'dd.MM.yyyy'" class="form-control"--%>
                                            <%--<input ng-model="employee.employeePosition.orderDate  "  class="form-control"--%>
                                                   <%--placeholder="Qəbul tarixi">--%>

                                                    <br />


                                                                <div class='input-group date' id='datetimepicker1'>
                                                                    <input type='text' class="form-control" ng-model="employee.employeePosition.orderDate" />
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                                                </div>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Təyin olunma tarixi</label>

                                        <div class="col-md-9 col-sm-9 col-xs-12">

                                            <%--<input ng-model="employee.employeePosition.workStartDate  "  type="text" class="form-control"--%>
                                                   <%--placeholder="Təyin olunma tarixi">--%>
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input type='text' class="form-control" ng-model="employee.employeePosition.workStartDate" />
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </div>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                    <div class="checkbox">

                                        <label style="margin-left: 20%"> <input name="SEBEB" type="checkbox" value="" data-parsley-required="false">Səbəb </label>

                                    </div>

                                </div>

                                </form>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-6 col-xs-12">

                        <div class="x_panel">

                            <div class="x_title">

                                <h2>Şəxsiyyət vəsiqəsi məlumatları

                                </h2>

                                <ul class="nav navbar-right panel_toolbox">

                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>

                                    </li>

                                    <li class="dropdown">

                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings ,klkl1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form class="form-horizontal form-label-left">

                                    <div class="form-group ">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="nationality"> Etnik
                                            mənsubiyyəti </label>

                                        <div class="col-md-9 col-sm-9 col-xs-9">

                                            <select name="nationality" id="nationality" ng-model="nationality"

                                                    ng-options="nationality.description for nationality in nationalities track by nationality.id"

                                                    class="field form-control input-sm" required>

                                            </select>

                                        </div>

                                    </div>

                                    <div class="form-group ">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="nationality"> Ailə vəziyyəti</label>

                                        <div class="col-md-9 col-sm-9 col-xs-9">

                                            <select name="marriedStatus" id="marriedStatus" ng-model="marriedStatus"

                                                    ng-options="marriedStatus.description for marriedStatus in marriedStatusList track by marriedStatus.id"

                                                    class="field form-control input-sm" required>

                                            </select>

                                    </div>

                                    </div>

                                    <div class="form-group ">

                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="maleFemale"> Cinsi</label>

                                            <div class="col-md-9 col-sm-9 col-xs-9">

                                                <select name="maleFemale" id="maleFemale" ng-model="maleFemale"

                                                        ng-options="maleFemale.description for maleFemale in maleFemaleList track by maleFemale.id"

                                                        class="field form-control input-sm" required>

                                                </select>

                                            </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Doğulduğu yer</label>

                                        <div class="col-md-7 col-sm-7 col-xs-7">

                                            <input ng-model="employee.person.bornAddress.adress_fullname" type="text" class="form-control"

                                                   placeholder="Doğulduğu yer">
                                        </div>

                                        <div class="col-md-1 col -sm-1 col-xs-1">

                                            <a style="height: 30px" class="  btn btn-default btn-xs"
                                               data-nodrag
                                               ng-click="selectTree(this)"><span
                                                    class="glyphicon glyphicon-option-horizontal"></span></a>

                                        </div>
                                        <div class="col-md-1 col -sm-1 col-xs-1">

                                            <a style="height: 25px" class="  btn btn-default btn-xs" data-nodrag
                                               ng-click="selectTree(this)"><span
                                                    class="glyphicon glyphicon-chevron-right"></span></a>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-3"> Vəsiqəni verən orqan </label>

                                        <div class="col-md-7 col-sm-7 col-xs-7">

                                            <input  ng-model="employee.passport.policeDepartment.name" type="text" class="form-control"
                                                   placeholder="Vəsiqəni verən orqan">
                                        </div>

                                        <div class="col-md-1 col -sm-1 col-xs-1">

                                            <a style="height: 30px" class="  btn btn-default btn-xs"
                                               data-nodrag
                                               ng-click="selectTree(this)"><span
                                                    class="glyphicon glyphicon-option-horizontal"></span></a>

                                        </div>
                                        <div class="col-md-1 col -sm-1 col-xs-1">

                                            <a style="height: 25px" class="  btn btn-default btn-xs" data-nodrag
                                               ng-click="selectTree(this)"><span
                                                    class="glyphicon glyphicon-remove"></span></a>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Yaşayış yeri</label>

                                        <div class="col-md-7 col-sm-7 col-xs-7">

                                            <input ng-model="employee.passport.adress.adress_fullname" type="text" class="form-control"

                                                   placeholder="Yaşayış yeri">
                                        </div>

                                        <div class="col-md-1 col -sm-1 col-xs-1">

                                            <a style="height: 30px" class="  btn btn-default btn-xs"
                                               data-nodrag
                                               ng-click="selectTree(this)"><span
                                                    class="glyphicon glyphicon-option-horizontal"></span></a>

                                        </div>
                                        <div class="col-md-1 col -sm-1 col-xs-1">

                                            <a style="height: 25px" class="  btn btn-default btn-xs" data-nodrag
                                               ng-click="selectTree(this)"><span
                                                    class="glyphicon glyphicon-chevron-right"></span></a>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Doğum tarixi</label>

                                        <div class="col-md-9 col-sm-9 col-xs-12">

                                            <%--<input ng-model="employee.person.birthDate  " type="text" class="form-control"--%>
                                                   <%--placeholder="Doğum tarixi">--%>
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input type='text' class="form-control" ng-model="employee.person.birthDate" />
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </div>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Verilmə tarixi</label>

                                        <div class="col-md-9 col-sm-9 col-xs-12">

                                            <%--<input ng-model="employee.passport.getPassport  "   class="form-control"--%>
                                                   <%--placeholder="Verilmə tarixi">--%>
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input type='text' class="form-control" ng-model="employee.passport.passportGetDate" />
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </div>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="passportSerialNo"> Vəsiqənin
                                                seriyası </label>

                                            <div class="col-md-8">

                                                <input  style="background-color: #a80f00;   " type="text"
                                                       ng-model="employee.pasport.passportSerialNo" name="passportSerialNo"
                                                       id="passportSerialNo"
                                                       class="field form-control input-sm"
                                                       placeholder=" AZE boşluq sonra nömrə yazın"/>

                                                <div class="has-error" ng-show="myForm.$dirty">

                                                </div>

                                            </div>

                                            <div class="col-md-1">

                                                <a class="  btn btn-default btn-sm"
                                                   ng-click="collectData(1)"><span
                                                        class="glyphicon glyphicon-search"> </span></a>
                                            </div>

                                        </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">İstifadəçi üçün bu şəxsi iş haqqında xüsusi qeydlər</label>

                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <textarea class="resizable_textarea form-control" placeholder="  "></textarea>
                                        </div>
                                    </div>
                                </form>

                            </div>

                        </div>

                    </div>


                </div>



            </div>

        </div>

        <!-- /page content -->

        <!-- footer content -->

        <footer>
            <div class="pull-right">
                Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
            </div>
            <div class="clearfix"></div>
        </footer>

        <!-- /footer content -->

    </div>
</div>
        </div>
    </div>
</div>


<jsp:include page="../pages/modals/add/addcontactInformation.jsp" />

<jsp:include page="../pages/modals/select/selectNomenclature.jsp" />
<jsp:include page="../pages/modals/select/selectWorkPlaceAndPosition.jsp" />
<jsp:include page="../pages/modals/update/updateContactInformation.jsp" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.1/angular.js"></script>

<script src="<c:url value='/static/resource/js/ui-bootstrap-tpls-2.5.0.min.js' />"></script>

<script src="/static/angular/Employees/EmployeeDetails/appEmployeeDetails.js"></script>

<script src="<c:url value='/static/angular/Employees/EmployeeDetails/employeeDetailsController.js' />"></script>

<script src="<c:url value='/static/angular/Employees/EmployeeDetails/employeeDetailsService.js' />"></script>

<script type="text/javascript" src="/static/resource/js/angular-ui-tree.js"></script>

<script src="<c:url value='/static/angular/Employees/EmployeeDetails/modals/modalContactInformation.js' />"></script>

<script src="<c:url value='/static/angular/Employees/EmployeeDetails/modals/modalNomenclature.js' />"></script>

<script src="<c:url value='/static/angular/Employees/EmployeeDetails/modals/modalWorkPlaceAndPosition.js' />"></script>
<script src="../../static/resource/js/jquery.min.js"></script>
<%--datepicker js--%>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>

<!-- Bootstrap -->
<script src="../static/resource/js/bootstrap.min.js"></script>
<%----%>
<!-- FastClick -->
<script src="../static/resource/js/fastclick.js"></script>

<!-- NProgress -->
<script src="../static/resource/js/nprogress.js"></script>

<!-- iCheck -->
<script src="../static/resource/js/icheck.min.js"></script>

<script src="../static/resource/js/bootstrap-progressbar.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../static/resource/js/moment.min.js"></script>
<script src="../static/resource/js/daterangepicker.js"></script>

<!-- bootstrap-wysiwyg -->
<script src="../static/resource/js/bootstrap-wysiwyg.min.js"></script>
<script src="../static/resource/js/jquery.hotkeys.js"></script>
<script src="../static/resource/js/prettify.js"></script>

<!-- jQuery Tags Input -->
<script src="../static/resource/js/jquery.tagsinput.js"></script>

<!-- Switchery -->
<script src="../static/resource/js/switchery.min.js"></script>

<!-- Select2 -->
<script src="../static/resource/js/select2.full.min.js"></script>

<!-- Parsley -->
<script src="../static/resource/js/parsley.min.js"></script>

<!-- Autosize -->
<script src="../static/resource/js/autosize.min.js"></script>

<!-- jQuery autocomplete -->
<script src="../static/resource/js/jquery.autocomplete.min.js"></script>

<!-- starrr -->
<script src="../static/resource/js/starrr.js"></script>

<!-- Custom Theme Scripts -->
<script src="../static/resource/js/custom.min.js"></script>




</body>






</html>
