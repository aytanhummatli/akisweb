<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Digər </title>

    <jsp:include page="../layout/rakCss.jsp"/>

    <!-- bootstrap-wysiwyg -->
    <link href="../../static/resource/css/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../../static/resource/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../../static/resource/css/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="../../static/resource/css/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../../static/resource/css/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../static/resource/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">

    <style>
        fieldset.scheduler-border {
            border: 1px groove #ddd !important;
            padding: 0 1.4em 1.4em 1.4em !important;
            margin: 0 0 1.5em 0 !important;
            -webkit-box-shadow: 0px 0px 0px 0px #000;
            box-shadow: 0px 0px 0px 0px #000;
        }

        legend.scheduler-border {
            width: inherit; /* Or auto */
            padding: 0 10px; /* To give a bit of padding on the left and right */
            border-bottom: none;
        }


    </style>

</head>

<body class="nav-md" ng-app="appEmployeeOther">
<div class="container body" ng-controller="employeeOtherController as empCtrl">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <br>
                <!-- sidebar menu -->
                <jsp:include page="../layout/sidebarMenuForEmployeeDetails.jsp"/>
                <!-- /sidebar menu -->
                <!-- /menu footer buttons -->

                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>

                <!-- /menu footer buttons -->

            </div>

        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>

                    <div class="nav toggle">

                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>

                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="images/img.jpg" alt="">John Doe
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;"> Profile</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li><a href="javascript:;">Help</a></li>
                                <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">

                <div class="page-title">

                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form id="formAnket" data-parsley-validate class="form-horizontal form-label-left">

                                    <div class="col-md-4 col-sm-4 col-xs-4">

                                        <fieldset>
                                            <legend>Anketin doldurulma tarixi</legend>

                                            <label>Sonuncu doldurulma tarixi</label>

                                            <input class="form-control"
                                                   ng-model="employee.lastFormFillDate.fillDate "
                                                   placeholder="Sonuncu doldurulma tarixi">
                                        </fieldset>

                                    </div>

                                    <div class="col-md-8 col-sm-8 col-xs-8">

                                        <fieldset>

                                            <legend>Əmək kitabçası</legend>

                                            <div class="col-md-3 col-sm-3 col-xs-3">

                                                <select name="note1" id="note1"

                                                        ng-model="note1"

                                                        ng-options="note1.description for note1 in note1List track by note1.id"

                                                        class="field form-control input-sm" required>

                                                </select>

                                                <br>

                                                <select name="note2" id="note2"

                                                        ng-model="note2"    ng-options="note2.description for note2 in note2List track by note2.id"

                                                        class="field form-control input-sm" required>


                                                </select>

                                            </div>

                                            <div class="col-md-1 col-sm-1 col-xs-1">

                                                <div class="form-group">

                                                    <div class="checkbox">

                                                        <label style="margin-left: 20%"> <input name="yes"

                                                         ng-model="checked"  type="checkbox" value="">Hə </label>

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-6">

                                                <div class="form-group">

                                                    <label class="control-label col-md-4 col-sm-4 col-xs-4"> Məktub
                                                        nömrəsi</label>

                                                    <div class="col-md-8 col-sm-8 col-xs-8">

                                                        <input ng-model="employee.employeeWorkBook.envelopNumber"
                                                               class="form-control"
                                                               placeholder="Məktub nömrəsi">
                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <label class="control-label col-md-4 col-sm-4 col-xs-4">Məktub
                                                        tarixi</label>

                                                    <div class="col-md-8 col-sm-8 col-xs-8">

                                                        <input ng-model="employee.employeeWorkBook.sendDate"
                                                               class="form-control"
                                                               placeholder="Məktub tarixi">

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="col-md-2 col-sm-2 col-xs-2">

                                                <div class="form-group">

                                                    <div class="checkbox">

                                                        <label style="margin-left: 20%"> <input   ng-model="isReceipt" type="checkbox"
                                                                                                value="">Qəbzlə </label>
                                                    </div>

                                                </div>

                                            </div>

                                        </fieldset>

                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2> Kurslar

                                </h2>

                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form id="formCource" data-parsley-validate class="form-horizontal form-label-left">

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="x_content">

                                                <table id="cource" class="table table-hover"
                                                       style="font-size: small; border: 1px solid #ddd !important;">
                                                    <thead>
                                                    <tr class="headings">
                                                        <th class="column-title"><label> Kursun adı </label></th>

                                                        <th class="column-title"><label>Başlama tarixi</label></th>

                                                        <th class="column-title"><label> Bitmə tarixi </label></th>

                                                        <th class="column-title">

                                                            <button style="float: right" type="button"
                                                                    ng-click="rankCtrl.add(0)"
                                                                    class="btn btn-success  "> Əlavə et
                                                            </button>

                                                        </th>

                                                    </tr>

                                                    </thead>

                                                    <tbody>

                                                    <tr ng-repeat="inf in employee.qualificationCourses">

                                                        <td class=" "><label><span ng-bind="inf.courseName"> </span>
                                                        </label></td>

                                                        <td class=" "><label><span ng-bind="inf.beginDate"> </span>
                                                        </label></td>

                                                        <td class=" "><label><span ng-bind="inf.finishDate"> </span>
                                                        </label></td>

                                                        <td class="pull-right">
                                                            <button type="button" ng-click="rankCtrl.edit(r.id)"
                                                                    class="btn btn-success custom-width">Redaktə
                                                            </button>
                                                            <%--<button type="button" ng-click="rankCtrl.remove(r.id)" class="btn btn-danger custom-width">Sil</button>--%>
                                                            <button type="button"
                                                                    ng-click="removeQualificationCourse(inf.id)"
                                                                    class="btn btn-danger custom-width">Sil
                                                            </button>
                                                        </td>

                                                    </tr>


                                                    </tbody>

                                                </table>

                                            </div>

                                        </div>


                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2> Müharibədə iştirak

                                </h2>

                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form id="demo-form6" data-parsley-validate class="form-horizontal form-label-left">
                                    <%--  Soyad, şəxsi iş, şəxsi işi tamdır   --%>
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="x_content">

                                                <table id="contdact" class="table table-hover"
                                                       style="font-size: small;  border: 1px solid #ddd !important;">
                                                    <thead>
                                                    <tr class="headings">
                                                        <th class="column-title"><label> Müharibənin
                                                            adı </label></th>

                                                        <th class="column-title"><label>Başlama tarixi</label>
                                                        </th>

                                                        <th class="column-title"><label>Bitmə tarixi</label>
                                                        <th>

                                                            <button style="float: right"
                                                                    type="button" ng-click="rankCtrl.add(0)"
                                                                    class="btn btn-success  "> Əlavə et
                                                            </button>
                                                        </th>
                                                    </tr>

                                                    </thead>

                                                    <tbody>

                                                    <tr ng-repeat="inf in employee.warActivities">

                                                        <td class=" "><span ng-bind="inf.warName"> </span></td>

                                                        <td class=" "><span ng-bind="inf.beginDate"> </span></td>

                                                        <td class=" "><span ng-bind="inf.endDate"> </span></td>

                                                        <td class="pull-right">
                                                            <button type="button" ng-click="rankCtrl.edit(r.id)"
                                                                    class="btn btn-success custom-width">Redaktə
                                                            </button>
                                                            <%--<button type="button" ng-click="rankCtrl.remove(r.id)" class="btn btn-danger custom-width">Sil</button>--%>
                                                            <button type="button"
                                                                    ng-click="removeWarActivity(inf.id)"
                                                                    class="btn btn-danger custom-width">Sil
                                                            </button>
                                                        </td>

                                                    </tr>

                                                    </tbody>

                                                </table>

                                            </div>
                                        </div>


                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Vəsiqə məlumatları
                                </h2>

                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form id="demo-formg6" data-parsley-validate class="form-horizontal form-label-left">

                                    <div class="col-md-5 col-sm-5 col-xs-5">

                                        <div class="form-group">

                                            <label class="control-label col-md-3 col-sm-3 col-xs-3">Boyu</label>

                                            <div class="col-md-9 col-sm-9 col-xs-9">

                                                <input ng-model="employee.policeCardNew.height" class="form-control"
                                                       placeholder="Boyu">

                                            </div>

                                        </div>


                                        <div class="form-group ">

                                            <%--<div class="col-md-8 col-sm-8 col-xs-8">--%>

                                            <label class="control-label col-md-3 col-sm-3 col-xs-3"

                                                   for="blood"> Qan qrupu</label>

                                            <div class="col-md-9 col-sm-9 col-xs-9">

                                                <select name="blood" id="blood"

                                                        ng-model="blood"

                                                        ng-options="blood.description for blood in bloodGroupList track by blood.id"

                                                        class="field form-control input-sm" required>

                                                    <option ng-bind="employee.policeCardNew.blood"></option>

                                                </select>

                                            </div>


                                        </div>


                                        <div class="form-group">

                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="eyeColor">
                                                Gözlərinin rəngi</label>

                                            <div class="col-md-9 col-sm-9 col-xs-9">

                                                <select name="eyeColor" id="eyeColor"

                                                        ng-model="eyeColor"

                                                        ng-options="eyeColor.description for eyeColor in eyeColorList track by eyeColor.id"

                                                        class="field form-control input-sm" required>

                                                    <option ng-bind="employee.policeCardNew.eyeColor"></option>

                                                </select>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-md-7 col-sm-6 col-xs-6">

                                        <div class="form-group">

                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="weight">Çəkisi
                                            </label>

                                            <div class="col-md-9">

                                                <input ng-model="employee.policeCardNew.weight" type="text"
                                                       class="form-control" id="weight"
                                                       placeholder="Çəkisi">


                                            </div>

                                        </div>

                                        <div class="form-group">

                                            <label class="control-label col-md-3 col-sm-3 col-xs-3 ">

                                                Tabel silahı №

                                            </label>

                                            <%--<div class="col-md-2 col-sm-2 xs-2">--%>

                                            <%--<div class="checkbox">--%>

                                            <%--<input  ng-model="cc"  type="checkbox" value="" >--%>

                                            <%--</div>--%>

                                            <%--</div>--%>

                                            <div class="col-md-9 col-sm-9 xs-9">

                                                <input id="armNo" type="text" class="form-control"
                                                       placeholder=" Tabel silahı № ">

                                            </div>

                                        </div>

                                        <div class="form-group">

                                            <label class="control-label col-md-3 col-sm-3 col-xs-3" for="givenDate">
                                                Verilmə tarixi
                                            </label>

                                            <div class="col-md-3">

                                                <input ng-model="employee.policeCardNew.givenDate  "
                                                       id="givenDate" class="form-control">
                                            </div>

                                            <label class="control-label col-md-2 col-sm-2 col-xs-2" for="validate">
                                                Etibarlıdır
                                            </label>

                                            <div class="col-md-4">

                                                <input ng-model="employee.policeCardNew.validity  "
                                                       id="validate" class="form-control">

                                            </div>

                                        </div>

                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <!-- /page content -->

        <!-- footer content -->

        <footer>
            <div class="pull-right">
                Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
            </div>
            <div class="clearfix"></div>
        </footer>

        <!-- /footer content -->

    </div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.1/angular.js"></script>

<script src="<c:url value='/static/resource/js/ui-bootstrap-tpls-2.5.0.min.js' />"></script>

<script src="<c:url value='/static/angular/Employees/EmployeeOther/appEmployeeOther.js' />"></script>

<script src="/static/angular/Employees/EmployeeOther/EmployeeOtherController.js"></script>

<script src="<c:url value='/static/angular/Employees/EmployeeOther/EmployeeOtherService.js' />"></script>

<script src="../../static/resource/js/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="../../static/resource/js/bootstrap.min.js"></script>

<!-- FastClick -->
<script src="../../static/resource/js/fastclick.js"></script>

<!-- NProgress -->
<script src="../../static/resource/js/nprogress.js"></script>

<!-- iCheck -->
<script src="../../static/resource/js/icheck.min.js"></script>

<script src="../../static/resource/js/bootstrap-progressbar.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../../static/resource/js/moment.min.js"></script>

<script src="../../static/resource/js/daterangepicker.js"></script>

<!-- bootstrap-wysiwyg -->
<script src="../../static/resource/js/bootstrap-wysiwyg.min.js"></script>

<script src="../../static/resource/js/jquery.hotkeys.js"></script>

<script src="../../static/resource/js/prettify.js"></script>

<!-- jQuery Tags Input -->
<script src="../../static/resource/js/jquery.tagsinput.js"></script>

<!-- Switchery -->
<script src="../../static/resource/js/switchery.min.js"></script>

<!-- Select2 -->
<script src="../../static/resource/js/select2.full.min.js"></script>

<!-- Parsley -->
<script src="../../static/resource/js/parsley.min.js"></script>

<!-- Autosize -->
<script src="../../static/resource/js/autosize.min.js"></script>

<!-- jQuery autocomplete -->
<script src="../../static/resource/js/jquery.autocomplete.min.js"></script>

<!-- starrr -->
<script src="../../static/resource/js/starrr.js"></script>

<!-- Custom Theme Scripts -->
<script src="../../static/resource/js/custom.min.js"></script>

</body>

</html>
