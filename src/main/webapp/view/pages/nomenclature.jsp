<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 2/20/2019
  Time: 6:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <jsp:include page="/static/resource/jsp/resourcesCss.jsp"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="/index" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
                </div>

                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <jsp:include page="/view/layout/menuProfile.jsp"/>
                <!-- /menu profile quick info -->

                <!-- sidebar menu -->
                <jsp:include page="/view/layout/sidebarMenu.jsp"/>
                <!-- sidebar menu-->

                <!-- /menu footer buttons -->
                <jsp:include page="/view/layout/menuFooter.jsp"/>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <jsp:include page="/view/layout/header.jsp"/>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main" ng-app="nomApp" ng-controller="NomController">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Tables
                            <small>Some examples to get you started</small>
                        </h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix">

                </div>

                <div class="row">
                    <!-- table panel-->
                    <div class="col-md-7 col-sm-7 col-xs-7">
                        <div class="x_panel">
                            <div class="x_content">
                                <div class="table-responsive">
                                    <%--Nomenclature  Table--%>
                                    <jsp:include page="/view/part/nomenTable.jsp"/>
                                    <%--Nomenclature  Table--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Textarea panel-->
                    <div class="col-md-5 col-sm-5 col-xs-5">
                        <div class="x_panel">
                            <div class="x_content">
                                <div class="textarea">
                                    <%--Nomenclature  textarea--%>
                                    <%--<div ng-repeat="nomNameById in nomListById">--%>
                                        <%--<textarea rows="3" cols="16" ng-models="nomNameById.NAME"--%>
                                                  <%--style="overflow: auto;resize: none;width:100%;height: auto"></textarea>--%>
                                    <%--</div>--%>

                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                        <tr class="headings">
                                            <th class="column-title">Tərkibi qurum</th>
                                            <th class="column-title">Dep ID</th>
                                            <th class="column-title">Sil</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr ng-show="nomList.length <= 0">
                                            <td colspan="5" style="text-align:center;">Loading new data!!</td>
                                        </tr>
                                        <tr class="odd pointer" ng-repeat="n in nomListById">
                                            <td class=" "><span ng-bind="n.name"></span></td>
                                            <td class=" "><span ng-bind="n.nomDependency.id"></span></td>
                                            <td>
                                                <button ng-click="removeChild(n.nomDependency.id)" id="delete-button" value="Sil"
                                                        class="btn btn-danger custom-width">Sil
                                                </button>
                                            </td>

                                        </tr>
                                        </tbody>
                                    </table>


                                    <%--Nomenclature  textarea--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <jsp:include page="/view/layout/footer.jsp"/>
        <!-- /footer content -->
    </div>
</div>


<script src="${pageContext.request.contextPath}/static/angular/nomenclature/nomenApp.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/static/angular/nomenclature/nomenService.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/static/angular/nomenclature/nomenCtrl.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/static/angular/nomenclature/nomenModalCtrl.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/static/angular/nomenclature/nomenAddChildModalCtrl.js"
        type="text/javascript">
</script>

<%--<script src="https://raw.githubusercontent.com/michaelbromley/angularUtils/master/src/directives/pagination/dirPagination.js" type="text/javascript"></script>--%>
<script src="${pageContext.request.contextPath}/static/angular/dirPagination.js" type="text/javascript"></script>
<jsp:include page="/static/resource/jsp/resourcesJs.jsp"/>
</body>
</html>
