<%--
  Created by IntelliJ IDEA.
  User: Moonlight
  Date: 23.01.2019
  Time: 11:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>403 </title>

    <!-- Bootstrap -->
    <link href="../static/resource/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../static/resource/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../static/resource/css/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../static/resource/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
            <div class="col-middle">
                <div class="text-center text-center">
                    <h1 class="error-number">403</h1>
                    <h2>Access denied</h2>
                    <p>Full authentication is required to access this resource. <a href="#">Report this?</a>
                    </p>
                    <div class="mid_center">
                        <h3>Search</h3>
                        <form>
                            <div class="col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                          </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>

<!-- jQuery -->
<script src="../static/resource/js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../static/resource/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../static/resource/js/fastclick.js"></script>
<!-- NProgress -->
<script src="../static/resource/js/nprogress.js"></script>

<!-- Custom Theme Scripts -->
<script src="../static/resource/js/custom.min.js"></script>
</body>
</html>