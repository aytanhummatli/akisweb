<%--
  Created by IntelliJ IDEA.
  User: Moonlight
  Date: 28.01.2019
  Time: 12:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title></title>

    <jsp:include page="../layout/rakCss.jsp"/>

    <link rel="stylesheet" type="text/css" href="/static/resource/css/angular-ui-tree.css"/>

    <style>
        .addButton {
            float: left;
            font-size: 8px;
        }

        /*html {*/
        /*zoom: 90%;*/
        /*}*/

    </style>

</head>


<body class="nav-md" ng-app="appEmployee">

<div class="container body" ng-controller="employeeController as empCtrl">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="../Views/index.jsp" class="site_title"> <span style="float: center">    AKİS</span></a>
                </div>

                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="../static/images/smilee.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>  wellcome</span>
                        <h2>user</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <!-- Dropdown for selecting language -->

                <br/>

                <!-- sidebar menu -->
                <jsp:include page="../layout/sidebarMenu.jsp"/>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <jsp:include page="../layout/menuFooterButtons.jsp"/>
                <!-- /menu footer buttons -->

            </div>

        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="images/img.jpg" alt="">User
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="/Views/employeeDetails.jsp"> Profile</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li><a href="javascript:;">Help</a></li>
                                <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">

                    </div>

                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Əməkdaşlar</h2>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                    <tr class="headings">
                                        <th>
                                            <input type="checkbox" id="check-all" class="flat">
                                        </th>
                                        <th class="column-title">ID</th>
                                        <th class="column-title">Kimlik nömrəsi</th>
                                        <th class="column-title">Soyad</th>
                                        <th class="column-title">Adı</th>
                                        <th class="column-title">Atasının adı</th>
                                        <th class="column-title">Rütbəsi</th>
                                        <th class="column-title">Şəxsi iş nömrəsi</th>
                                        <th class="column-title">Doğum tarixi</th>
                                        <th class="column-title">Iş yeri</th>
                                        <th class="column-title"></th>
                                        <th class="column-title"></th>
                                        <th class="column-title"></th>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">

                                                <form ng-submit="submit()" name="myForm" class="form-horizontal">
                                                    <input type="hidden" ng-model="r.id"/>
                                                    <div class="row">
                                                        <div class="form-group col-md-3">

                                                            <input type="text" ng-model="employee.person.surName"
                                                                   name="surName"
                                                                   id="surName" class="field form-control input-sm"
                                                                   placeholder="Soyadı"/>
                                                        </div>
                                                        <div class="form-group col-md-2">

                                                            <input type="text" ng-model="employee.person.name"
                                                                   name="name"
                                                                   id="name" class="field form-control input-sm"
                                                                   placeholder="Adı"/>
                                                        </div>
                                                        <div class="form-group col-md-2">

                                                            <input type="text" ng-model="employee.person.patronymic"
                                                                   name="name"
                                                                   id="patronymic" class="field form-control input-sm"
                                                                   placeholder="Ata adı"/>

                                                        </div>
                                                        <div class="form-group col-md-5">

                                                            <div class="col-md-9">

                                                                <input type="text" ng-model="employee.tree.title"
                                                                       name="name"
                                                                       id="nahme" class="field form-control input-sm"
                                                                       placeholder="İş yeri"/>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <a class="  btn btn-default btn-xs" data-nodrag
                                                                   ng-click="selectTree(this)"><span
                                                                        class="glyphicon glyphicon-option-horizontal"></span></a>
                                                            </div>
                                                            <div class="col-md-1">

                                                                <a class="  btn btn-success btn-xs" data-nodrag
                                                                   ng-click="collectData(1)"><span
                                                                        class="glyphicon glyphicon-search"></span></a>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <a class="  btn btn-danger btn-xs" data-nodrag
                                                                   ng-click="clear()"><span
                                                                        class="glyphicon glyphicon-remove"></span></a>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </form>


                                            </div>

                                        </div>
                                    </tr>

                                    </thead>

                                    <tbody>
                                    <%--<tr class="even pointer"  ng-repeat="r in rankCtrl.ranks">--%>
                                    <%--<tr ng-show="rankCtrl.ranks.length <= 0"><td colspan="5" style="text-align:center;">Loading new data!!</td></tr>--%>
                                    <%--<tr  ng-repeat="e in employees" >--%>
                                    <tr class="even pointer" dir-paginate="e in employees |itemsPerPage:itemsPerPage"

                                        total-items="total_count" pagination-id="paginid">

                                        <td class="a-center ">

                                            <input type="checkbox" class="flat" name="table_records">

                                        </td>

                                        <td class=" "><span ng-bind="e.id"></span></td>
                                        <td class=" "><span ng-bind="e.policeCardNo"></span></td>
                                        <td class=" "><span ng-bind="e.person.surName"></span></td>
                                        <td class=" "><span ng-bind="e.person.name"> </span></td>
                                        <td class=" "><span ng-bind="e.person.patronymic"></span></td>
                                        <td class=" "><span ng-bind="e.rank.name"></span></td>
                                        <td class=" "><span ng-bind="e.personalMatterNo"></span></td>
                                        <td class=" "><span ng-bind="e.person.birthDate | date:'dd.MM.yyyy'"></span>
                                        </td>

                                        <%--<td><span>{{e.person.birthDate | date:'dd.MM.yyyy'}}</span></td>--%>

                                        <td class=" "><span ng-bind="e.tree.text"> </span></td>

                                        <td><input type="button" value="Open" ng-click="Open(e.id)"/></td>
                                        <td><input type="image"  src="/static/resource/images/pdf-icon.png"
                                                   alt="Submit" width="32" height="32" ng-click="openPdfModal(e.id)"/></td>
                                        <td><input class="content-footer" type="image"  src="/static/resource/images/Word-icon.png"
                                                   alt="Submit" width="32" height="32"  ng-click="openWordModal(e.id)"/></td>
                                    </tr>


                                    </tbody>

                                </table>

                                <dir-pagination-controls

                                        boundary-links="true"

                                        max-size="8"

                                        direction-links="true"

                                        on-page-change="collectData(newPageNumber)"

                                        pagination-id="paginid">

                                </dir-pagination-controls>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--word document--%>
    <div id="source-html" style="">
        <div id="main">
            <img width="50px" height="50px" src="http://localhost:8060/static/resource/images/arayishShekil.jpg"/>
            <img width="751px" style="margin-left:auto;margin-right: auto"
                 src='data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCADxBJEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDzT/gnt/wT2+HX7WHwZ1rxd4u1nxRp2pWXiCbSo4tFuraKExJbW0oYiS3kbdumYEhgMAcDkn6e/wCHKnwQ/wChp+IH/gxsf/kOj/giqf8AjFnxT/2Od1/6Q2Nff26gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Do/4cqfBD/oafiB/4MbH/wCQ6+/t1G6gD4B/4cqfBD/oafiB/wCDGx/+Q6P+HKnwQ/6Gn4gf+DGx/wDkOvv7dRuoA+Af+HKnwQ/6Gn4gf+DGx/8AkOj/AIcqfBD/AKGn4gf+DGx/+Q6+/t1G6gD4B/4cqfBD/oafiB/4MbH/AOQ6P+HKnwQ/6Gn4gf8Agxsf/kOvv7dRuoA+Af8Ahyp8EP8AoafiB/4MbH/5Drxb9sv/AIJhfCz9nf8AZt8X/EHw5r/i+91rR/sfkW+qXlrJbt5t5BA29UtkY4WViMMOQOo4r9Z91fK3/BUY/wDGCnxM/wC4Z/6c7SgD8AqKKKAP2n/4Iq/8ms+Kf+xzuv8A0hsa+/q+AP8Agiuf+MWvFP8A2Od1/wCkNjX38DQMdtpkjeWoJ55A45pHkKjOcc9a+YfiR8Urv4q6ld6Jo1/cWPg+2me3ub2zlaOXVGU7ZFR1GVt1OV3qymUhgCEX97y4jERw8OeRpSpSrO0T17xV8fvAHg/UptNv/EcNxqsJKy6XpcUmoXqEDOGt7dZJAT2yuT2zU3gz42+FPH2tvo+lTapDqawtcLa6tol9pjSxqVDtH9qhjEm0ugYKSV3AHGa+d4PEnhf4bW8GkwW0UNw6n7PoulW4eWQAYBEKjITnlm2quclh1rrPhPqV/wCI/jhYDxRpH/COSWei3F3oVh9qE0twxmWK8kuDGAivGrQKsatIuJmbcxAK+dhcfLEzso2R11sLGlG7ep9M/wAWO/8An/Gl20xW5I/z25/UU/Jr2zzw20baMmjJpAG2jbRk0ZNABto20ZNGTQAbaNtGTRk0AG2mt8uPy607JqOZvl6/Tgn9BR6AeceKv2hvBXg/xBc6JfXOrz6laqjXMeleH9Q1FYN6hlWSS3gkRGKlW2MQwVlJADAnS8H/ABq8EePr5bDRPEVpc6oyGQaXMTbXu0YyTbyhZABkZJXHNfO3izV7rwf8cPHkPhzQ38QaIstpfaqltcRrd2uoTRZkWNHIEqmEWsmN4dWmIAYMFXtdDuvB3xh0q5tpkh1VLSQfaLW8iaK6spgCRvRsSQSDnBIVgeRjGa8d46UanJJaHaqCcFJM+iEk3uVAPQENkYIOcd/Y1KRivJ/Deuaj8OdW0/Qdbvp9U0TU5fs2maxeSb54bjBItLh8fPkcRTMSzkbZMtteT1JZc8E5OM9McHpXrRlzx5kcbTi7EtFJuo3VYWFopN1G6gLC0Um6jdQFhaKTdRuoCwtFJuo3UBYWik3UbqAsLRSbqN1AWFopN1G6gLC0Um6jdQFhaKTdRuoCwtFJuo3UBYWik3UbqAsLRSbqN1AWFopN1G6gLC0Um6jdQFhaKTdRuoCwtFJuo3UBYWik3UbqAsLRSbqN1AWFopN1G6gLC0Um6jdQFhaKTdRuoCwtFJuo3UBYWik3UbqAsLRSbqN1AWFopN1G6gLC0Um6jdQFhaKTdRuoCwtFJuo3UBYWik3UbqAsLRSbqN1AWFopN1G6gLC0Um6jdQFhaKTdRuoCwtFJuo3UBYWik3UbqAsLRSbqN1AWFopN1G6gLC0Um6jdQFhaKTdRuoCwtFJuo3UBYWik3UbqAsLRSbqN1AWFopN1G6gLC0Um6jdQFhaKTdRuoCwtFJuo3UBYWik3UbqAsLRSbqN1AWFopN1G6gLC0Um6jdQFhaKTdRuoCwtFJuo3UBYWik3UbqAsLRSbqN1AWFopN1G6gLC0Um6jdQFhaKTdRuoCwtFJuo3UBYWik3UbqAsLRSbqN1AWFr5V/wCCo3/JivxM/wC4Z/6c7SvqndXyt/wVEb/jBX4mf9wz/wBOdpQFj8BKKKKBH7S/8EWT/wAYt+Kf+xyuv/SGxr79U5r4B/4It/8AJrfin/scrr/0hsa+/VPNJ76j30INQtUvbOa3k3GKVCj7CQ2CCDgjkHnqDkde1fFPjb4I/Fz4ZeB7i38Of2Jqmj6OgijuLa7kj1RrKIlRIIzbtH54jAyQGyVIRCSoH2+SGBH4GoJNsEY2jaowPlwAAP0x/L26jkrYaGISVToa06kqWsT8v9J8bQtd3OjaB43tft0xDajdeG7eW5IZgP8AXXAMlzdOdxwITGo4BePo3a+Gtd8K/BvxNo+oeFdA8V6h41inWS+8QeILdlWa3lIhkOoyTuJoIFDblJQFSuQshDg938cPAXjD4LazrN/4C0fQYtF8WatbKb3z0sprCWVEgaOO1EJWUggyK6hiAzlkbYWZLHRvG/hOEaHo3iHSLPWdQDXL2+naa8s+MDNxe3tzJIuxdozui5wUUNwB4yi8PVSS0PSclVp3Z9jaDrmn+JNNt9S0m/ttU025Tfb3lnKssUq9NyupIYdsg9jWpXzV+yj42TTb/WPhjPr2meLtS0sXOtSeItFuBKl01zfTPKLiGNBHaSiSTiFXkBG5iVOVH0ezt2GDnpjP4A5AzX0MXzRTPJlvZE1FVra4E0aOriRHXcrryGGOCCABznj1HIqcMPWnu9CR1FN3ijd709Ooa9h1FN3e9AYetIB1FJuFV7y8isofMllSJcgbpGAGScAZJH86PmHoWevtXN/EDxlpngPwteatquo6XpkKKUil1m9W0tmmYEIjSsDtDNgcAnngE4B3Gm2Kx3ZC+vAzjPJ7dR6V8dftcfEabxd8RNE+HOk6rp/hfWNBuLfXoNY1C4VXuZ5ILu3FvDbyQssilZcNKGJUsQFJU4xq1PZ03I0hBykkeIQ6p4f+K2oXviLV9P8AGPhD4jzOmo3Wuafpk6TJ5kYEQMVq7TGzwgChgTtV/wB4reYw04PiRbTXvkXvjWzTxfpduZbEaqrW2pzIMEpZ3cMQk5IANrcWspYnBWUKSeistJ8Y+MLNmTWdN/t/R8kLc2Mlnqdm7DBKTwyMksLbcDEJR1UhlJG2vVf2c/Cfif4tX/hD4keOtD0GzbS7O4TT5LC5W9a7aVoyJArJi22GNgQCJN+4YjAKnw8NH21Rtno1rU4qx13h/wAJfFj4g+GdJ0b4gWfhvStOluIJ9RaxvJJtQdIZFmjiMYjEUchKRqzpIwHzMm0ldvvyt1PXnI/Idf8APQikUbmOTkbQv1wTk/TmnEKvTjJJNfQRiopJHmbu7F3UbqbkUZFWLQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQduo3U3IoyKA0HbqN1NyKMigNB26jdTcijIoDQdur5X/AOCoh/4wV+Jf/cM/9OdpX1NkV8r/APBUL/kxf4l/9wz/ANOdpQB+BFFFFUI/aL/gi5/ya34o/wCxyuv/AEhsa+/Fr4B/4Iun/jFzxT/2OV1/6RWNffSNz+FS9ykTU1lDYyOhyPrRk01mKjI65xj8f8//AFuom/UGc18RPAun+PvC8+k3pntgZEuIbuyKpPbTIQ0csZZSu5So4YFSMhgVJB+NLfw34Q+G+heNdZ+IJvvHPiOHxBNYiw1adpm1K5L/AOiRGzUtC0jxGNlAjYIjAkhVYD6t+JXxp0b4bSRWU3natr1xH5lpounqHuZVJIEjElVjjyCPMkZV+UgFiQD836DrGmfFb4oal45v7jw/qPie3to9OSy0a6F2NKhVpSVZ+CZiZHDOVQkKoVQASfJx1WFNKS3R2YeEpO19A8A6T8R/tWn674d0nTfAusSyrLqd9qczTSahCMbLV7CEBI4Y1LCPMplQbhkM8hb0LxRbeI/FGraToHiee38YatqiSy2nh20H9maAscRQySXZZ5JbgjzFPk7pFc4PlgKzp1Gh/wAPKggggoMd85B7HPXHXvnFaPjDwze+IPD0Fzo20eItFuE1TSN7bU8+IECFj/CkqGSFsdFmYjB5rlwmKnOSUtjWvTjFXW5p+EYdD+A/gC1sPEHiTTdOs4HmkFzeOlhaxiSRpPKhR2IjiQsypGWYogUZ9Mq6/aq+HSsq6Zqt34lZgdreH9NuL+Jx7TRIYxz6t/8AW8TvfE2mfGT4pap4uitPMstPsLHSrAX0AEttMUNzdDBBKSBriKOQDGGgweVNdbG5KEFmIwSQTxx6jvRiM1dKo6cFsaUcD7VJvc66T9qJZmI0/wCG3jC+TOPOcafbLn0Imu0b/wAd/Co/+GhPE8vMHwyvAh6Lca1aK34hSwH4E/SuA8VfELQfAMdq+v6g1is5KxbYZZBxjOQisccjtjp+K2/xS0LVvCOt61oWp21//Z1rNO6MXyhRCw3ptDgEA4AUmuH+1q0uh6Kyao4qai7PyPRY/wBoDxLGoFx8LdWdupNnq2nyKR6AvNGc/UAe9Wof2jlVv+Jj8PfGelp08xrezu1H0Frcyse/QE8dK4Dw38UdH1STw7p899FDrmradHfx2qpIAwZSTtYjAIIPDEH2rrtC8Wabr2papYWN41xdaY6xXcTRupjLDKglgAcgE/Lkcda6KeaVeqMauVzo3cotJG/B+018OWkVL/xA3ho5w3/CSWFzpPPoDdRxjn1BPt61X+Mvw/tv2gvh/a2eka/Y3NvDcpexqwW703USEYCK4RT88RDhhhsBwjENtCm9b/NwxyMEAMcjpnGPwryrXPFWn/s8+PfEGq2Gk26f8JJocf2KwtYliS41S3ujFGhwAA0gv4tzdkhZjwCa9OGMVSPvqx5bo8rvE5rwT4r+IfgmbV9E0XWIbabSLgWd3o3iQPqtraMY0kVrW5WSOQoyOpCszAAAeVEflPl/jrwb8Rmnn8U+JdEsviPquNtzfWD+Y09oCSbQ2Ui4EXzZURFiHw3lvl8+qeCdHk0TSYoLi5+3Xjs9xeXbDH2m5lJknlIPQu7sSO2ccYAHpWi5bAPPByzZxkgDLEAnAwOoxwAcAk18y8XOrJwb909SNGNOKn1Pnfwb8PtA8deMfh1ffD7W9W8ODUnufObRpzIba0jhZZgLeYNDGqz+UjgoCjmMFdwBH3P4P8K6Z4I8O2Oh6TD5VjZxeWgfLu/JLPI55d2YszMeWZiTkkmvnzbo3wv+Kk+s+G7rw3F4n8S26rfeF729jtLzVAkjET2oJJ87naQylZCqgsuzcfc/BXj7S/GzXcdotxY6lZ4F9pWoRmK8tGYts8xD/C21tjqWRwrbWIBr6vB04xhzLqePiJSlLU6r/Pt+ApGPSjJpjnpXeYC5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RRkUzdRuoAfkUZFM3UbqAH5FGRTN1G6gB+RXyx/wVCP/GC/xL/7hn/pztK+pN1fLH/BUBv+MGfiX/3DP/TnaUdQPwNoooqyD9nf+CMB/wCMXfFH/Y5XX/pFY198qxzwa+BP+CMZ/wCMXvFH/Y43X/pFZV97q2DWb3LJ9x9aNx//AFiovM96PM96FfoBx3j34O+EfiZfadeeJNJ/tGWyDRr++kQSxMQTFKFYCWIkKSkgZcg8cnPHfHnwNHp/hvSfFOiacsd54RYyG2s4wPO04gC5hQDGSqIkqqOS1uqjhiD7GJB6+9JI2FGRgdQFGeByTgck8cYBOce9c9SjCpFprc0jOUGrHjHhm6ivrO3ubaVZ7WeITRSxtuDqw3KwxwcjGD0IxjvW74z8c2/w18DalrlxH9omhXy7WxVgHu7lsCG3Ungl3IXgcAknABNeQTeMNE/Z/wDEGseDNUncrAwvfDmn2qGa4vLCUsVgjVTk+Q6SxknhUWJmI3EnMmvtX8c61BrfiOBbIWgI07RVYSJZbgQ8kpHDXLKcNjKoGwpIJZ/lW1hJNNnrKLrpO2gvg3RZtB8PrHdzx3Gq3VxNe3twAVSW7nmaWdgMEgGSRyF6gYBJyBXMePNe8T2Pia6g0x9ShWOwSbRrWz0w3UGq3TNLmK5kETCFAoQk74iAxYFwrAehRt8pbdg7SCckEgAnt1wCcZ+lcT4t+M2m+F9fudEg0XU9e1K2VWmSwhiWKIyDcNzyyqMsu0kAkkEcEDjyqXNVm2ldnfNRjBRZH8XNQtdN8cfDu5v7iG3sVvLpjc3DhY9vljDAseQeOR3zjAIrmvFF9p3ifxt8QdT0GSO80mLwfcQXlxbH9zLcHcVGckFgg6+x9afqnxQ8QeIiBN4C0FhkkNrGovPJyACTGtsVJIAyfMJO0A5wCLFn4w8cQ2D2VvaeD7SxkUhrJdNnZWBGCDiZQRjjlce3NdX1Os3ex9FRzuhRpQjyu6030OZt/B11411jwfDZXLWur2fgeDULCcEAJOjjbkdwwLKccDPSuu+E/izxFqnh34pa3o9h5XiM3FuGtWUM8UoQJMQpwHYfMVUnBOBkA1f0vxP49tZoJ4rLwbNLHF9nieOxuLYxx5z5Yk3vgZGQAMcD0FdHovxG8XaLNNOfh5o8guX8yaXQ9ZHnTsCSWKyQxqTyeC4OD6ZxvDCzjJNk4vP44nDzouO9reWtzuPgzqmt6nZakNTudX1GxjmQWOoa/pv9nX8wMamUSQeTDtVZA4VjEpZccMBuMnx58C3fjPwOt3pFst34h8P3S6vp1uwB+0Oissluc/8APWJ5IxngMyseFqfwH8VLPxfrEmjSaNq3h7WYoGuhp+qQRqxjDKrsjxPJGwBZASGB+YZHHHotuudx2+YOysOuQRtP1zgZyMkA8E12cvR6HxnNZ3R8/eENatPEek22p2M3n2l0glikYEEg8nIOCpHAIPJIJIGMV2dxrll4Z0G+1bUX8uxs4GmnbbuIQDJwOpOM4A5Y4A5Iql48+Gep+FdXv/EvhCyfWLW7ma41Xw/Ew81pDgvdW+cKJGOS8LMA7Asp3ErJj+A9R0341eONM0bTLj7TpGgyJqutxSAxypOjsLS0ljIDRuZY3lZWUEfZ0GCGOeKjhJRqpdGzpnXUqWu5618MvhtCfAN4njLSbbUNW8SynUtcsryJZ08x1UJbkEFWWGNI4gcYzET1JJ6vwR8PtF+H9teR6PFcZvJFkmnvLqW6mbaoREMkzs5VVACrnaozgDJzvLwSc5OSCcFTwBgYPQcn8yRjNO8z3r7OEVGKS6Hgt8zbZLuPrTWY+tM8z3prNnFWIfu96N3vUe6jdSAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9G73qPdRuoAk3e9fLX/BT4/8YNfErn/oGf8Apzta+od1fLX/AAU8bP7DfxK/7hn/AKc7SmtwPwTooorUg/Zf/gjMf+MX/E//AGON1/6RWNfeynmvgb/gjSf+MYfE/wD2ON1/6RWVfekbfN+FYyfvF9CxRUfme9Hme9JsPQ5b4teMpfh78L/FfiS3jSW70vTLi7t45FLLJMkbNGhAIOGYKvUckc18zX2ueO/FMTjXPiDq4idRutdGig0+IZzwJY084cYORKDk9OAR65+1nfD/AIVRDpmf3mqa3plqqkD5lW6jnkH0McMgPsTXjmpNqEek3zaTEtxqq27G0ilJ2PMFJRWORgFuOSOoAycA/L5vi6lOcaVN2ue1gKEKkXOSH6D4Z0rQZrmSxtEiubomS4ui5ea4Y4y0kjZeQ9OWYmuhh7/if5Dj06D8h6V558N7/Uby6uBJe6vqenfZIZJZ9bsRaSw3RaQPGiiJdybQhDbdpJIDtggdvqGsWGg6fNfale21hZxjLz3Myxoo9SSSP6np3r5hxqOdm+ZnrRlCMbrRGncXkOm6fc3lzKsFrbxNNNLISFRFGWZiOQoAOcc4zivm7wLot18WvHmszy6heaNbXgbV7s2SxLNiR1hs4mMiMBiC3fdgAhlBzyc6XxK+KkXja2On2KXEXhhXUzyGJlm1M9VijiwXMbEj5cBnO0AMhPmenfBzwXc+E9AluNSGzWtUlF3dqrBhF8oEcIYcEKgXkcMzMwJBGPUpxeFoufVnK/3lXlWqQ/T/ANn3wcig3MGpam2cM17ql0yt9UEoT/x2uitfgL8NyqiXwJ4fuV/u3WnRTLn1IZSCffGeTzzXRxusS75GCL3ZiAPz7VL/AMJPpVrgSXqeuVJZfxYD9MfjWUatWX2mXOMI2tazMqP9n74bSLhfBekWoIxus7cW5UcH5THtI5x0xVpf2efDMbM2l3/iLQpnUjfba7csgJUgEJPJJEowSMbcEkDHNb1j4w0a4wE1CHng79yqfoSuK62xmW4TdA6zBgSCr7l4GRkjgDj064rvpVZ3VndnDLls7bnzu02o/Db4tQSarqlxq9voU8MZ1G8iihnOj6gojLP5arGPKvLUMzBQBHGxIr6gt8Mx5zzkMAVIIJ7HkcAH8a8v+L3g8XUln4qtbD+0Y7OKex1bT/K3m902baXBQcyMjANtwSyGZQAXzVbwj48Hw/0e0g127kvfBzRqdN8WF2mjigIGyO9kAOzaCFW4O5HAAZlfBf1P40VKHQ5tU7HtsIDKARu7BW5HXOBnoPbpWL4k+F/hrxpcRXupaX/xNLdSkWr2cstrfRAnJCXUBWRRnkgMASBnpWppt7b6jZw3VpcRz2kyho54pA6yKcAMCuVKnIwckHPbpXj+sah8Q1+Mbw2o1lLaPUrKPTrW3t4jpDaYfJN6082wsJgPtRVdysTHDhSrFm6KN1o0YTtJaG34/wDDvin4T+Bdb8UaJ4917U7bQbKXUjouuQ2t2k0MKmWSMSiFZ8sqEBmkJyRnJII9hikWZA6NuVlBUgdQSeQPTsOegpmqaTB4i0O+0y6Cva6hbPBLgZUrIpU8H2IwD1zXDfAfWJ9Y+DXgq4u2Jvl0i2guwTnE8aCKQZ/30cfQA9Sa9OOyOQ9AprHGKaZO45Pp61G7HjIJ9zVdAJN1G6od1G6pHYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3V8uf8ABTpv+MHfiT/3Df8A052lfTu6vl7/AIKbt/xg/wDEn/uG/wDpztKa3Quh+DVFFFdBB+yH/BG1tv7MXif/ALHC6/8ASKyr7yV+a+Cf+COJ2/syeJ/+xwuv/SKyr7wVua5pP3ma9Cx5nvQGLEYPfOP5/pUO4UqkM2M44Iz+BqeZC1TPCf2qroXF18N9Pzj/AInc98x9RFp9wgH0DTqfwFcLGw+YkdgQpGeQMA/mK6b9pSQz/FL4bwE8f2Xq9yV7ArJp6fniRvzNc3bnoSeMkdPc4/ma+EzmT+sW7H02B0o3R578evEmo+H/AAvpltpl5NY3upaktmLm3bEqRiN5ZCpPIJEeCRg5IOcgY8k8K/DzxF8QIbfWYLaOeJ3byNU8Q6pLdT7QSAyIwYknaSAWQYY/MOAep/aW1P7PrXhZCcR21nqF++eQGQQKDz3O+T8z6CvU/hlpj6H4A8M6dJkPZ6ZbQsCT94RKGz65IB5ojVeHw8ZxSuxxh7Wo77FPwB8JbPwveQanqF22s6yoPlzNEIobfPUQxBjgnJyzFm5IDAMQeh8bfErTPA1g013LtlIwiKQ7t1IAHUnJPU+tQeMvF9v4N8Pz39wSgVc7VIDOewA7knj6ZPavNfgl4T0b9prVNW0jxHctp+rqrS6fcwA7SMgmMrkAgDgkg8++K9bLcB9cTxmMb9imlp19DyMdjpUZ/U8Gk6rV/Jepy+v/ABu1PxZp+pyW2pLojwsqw24QvJMCRnJIIQ4z0596i+BfhPW/i18T9I0u61W4lhDGe5WS4ZgqDORjOOmeODx1rrviR+xf498H75tLjXXdOUgx/ZsiRQcnBHoADz64r6A/YX+Eb+EvC2o+ItQtWttQ1CVoEjkUh0jU8nHuQPyr3KWOlGpPC4OioU29G1eVvUWKy3KqeGpyxFedXE2v7ukF6nyJ8YvDev8Awp+J2taLDqt1AEmMtvmdk8yI5KELkgjAORnqBxXQaH8c9d+HculkauNcguIRJMsURR4jkZXk7X6ZwRk4znjn6I/bs+EFx4j03R/Euk2b3V7bv9kljgUl3DZwcD0x+H41438N/wBiHxr4w8qfV1j0LTXI3falDSMoIPCY6kZ5PpTnmFWpWjSxtBSguqST+8VHLsoqYaSwmInRxKV9buD8l2Ppf4M/HjR/iRawhJ4477hR+8KhmHOCOqng8HIOPTiuh1L4Rz2eoXOqeCNYh8PXE0jS3Gm3dqLnTp5G5ZmjDI0TMSSTEyg7mJVmOa+OPjToul/s3+L9N0XwpczXt/Cqz6ldSOQXbkiLqcAgjGMcgetfYn7PPxZtvib4Vt5TMXvYowxYnLspGBn/AGlOQR9PWqnhvYJVaOtKXXqn2Z5dHEyqS9hXVqq1duq7nCr8Po9B8TaPb6/4M/4QuTXL02a674D8TXNtbtctFJIpmhRLfeGEbDMiMAxUbiSDXq3wR1i+1r4caYdWuGu9U0+a50q7mmA3vPaXL27ucAAFmhDHAGTg9zUf7QX+jfC2XVf4tD1HTNY3tydtvewyy/QGNJAfYmovhOp0/wASfE/R8YjsfFMkqLnoLm0tbskfV7iT8QfpWk4pxTR1buzPVLYAL6ZwDj8BXk3wNxZ+Ftb048HT/E2t24X+6jajPLGv0CSRgewFesQ/dGOuOD+teTfDP/R/FXxUs/8An38WOdv/AF10+xuM/j536VeyTM3ueiCT0PzdR0/EZ7cZpr4DA7iRjCg5GFySMg9+efXAqPcCMHkdwaa7c5zk9yTk/nS5kw6km8UbxUO73o3e9K5dibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeKN4qHd70bvei4WJt4o3iod3vRu96LhYm3ijeKh3e9G73ouFibeK+X/+Cmjbv2IfiR/3Df8A052tfTW73r5h/wCCmLbv2I/iP/3Df/Tla1Ud0T0PwjooorqMj9iP+COrY/Zl8Tf9jfc/+kVlX3ar18Hf8EeWx+zP4m/7G+5/9IrKvuoPXFU+Jmy2LPme9Ksg3DPT/J/pVbzPekZtykZ6ggfiMVmOzPn/APaG+b4xeB8/9AHVgPqLjTwx/IL+VYcOP1wfx4P6Zrb/AGjMx/Fb4fSk8tpes24GOMtLYOP0iasKMjIP8O4n8OCP618Rm1pYm/Sx9Jgf4J82ftPM934ulss5/wCJAIgo9Zpmj/XYPyr6Tt0AGB8vBA49Bgfyr5t/aF/d/FFZJOV/svTif+A3szH+dfSALLC57qD27gkn9SayxFvYUrmlH4m0eB/HjxJb6t4gXR5TceXBbmaNbcrjz2JEYYkEhQobJGD8wrkPhR44n+H/AMRNG1y2ZgtnKodQT86MAGHXJBA3H3GK9B0P4J33xi+K2pxxavp2np523bcSgyYVVUALnOQPQE8dOSa+tPhh+xb4H8ErHLqUUniK9VB+9uf9XkHOQo9Dkc8YJGK+6x9GfssPg8O7Rik5Lz7niZVioUMJjsTJJYirLlg2r2j1PetD1aDXdJtNRtpPNtbqFZY3GMEEZH48/pXB/Gr4sWPwj8IXFziNNRmhuBp9uw+R5o4JJhuA5C/u+SPUAdRXoNrZ22nQLb20CW0Ma/KkahVA9AB0Ffnn+1V8Uj4+8d6ZDlodO0+/1LSQVPT7kLufcZ3DOcEijF13haXPb3j1uHMoecY1UpfCtWz7n8C/EPRfiPp7T6XcLcLGkTvHIoyvmRJKhx3BVxyOMgjqDWv4o1628K+H9Q1O44gs4GlfscAE8E9ycV+d37OfxK1HwX4x8LN+8UwX6+GdRtnY/NDMzNbZHTdHKJgcc7SvYmv0dv8AT7TXLF7e9tY7q2lXDwzKGQjrgg9aMNX+tUb7Mx4gyeWT4p0oS917H5A+PPFlz468catrd5J5rahcNIcggBScKOTxgAe/GRXsf7MfjzT/AAb8UY9O0i6updJcRyF7sBTuJVZQAAMjByc91NfTfxR/Yp8D+Nllu9LRtB1BssHtzmLPU5U9uB3wBkDrXydqHwcvfhB8WNNEmr6fqUYkZQtvMC+GRgcjORnJJ9+etVltKolXoYmXutXj2ucGb4yOJwWCxCivb03yTaVrx6H3p8dtLk1j4CfEexjOJp/DuopF32ubWQKR9GGR9Kw/hxqAvvip48nT/V39po+pqPeS3eMn8RCPwArqPFUhvPgxrsshI87Qrhmyc9bY/wAhj8j615/8EWZvGmrbuT/wifhsMffy7rP6Y/OtIt+yTODTnse6w/dH4H8iCf5V5H4IbZ8UPjAB0PiG0J+v9jacP5Y/KvXIfuj6H+Rrx3wFJ5vjz4uXI+7P4pQKfaPSdPiP/jyNTfwE/E9D0DzPekaSoPM96QvWRdibeaN5qHdRuoGTbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzRvNQ7qN1AE280bzUO6jdQBNvNG81Duo3UATbzXzF/wUubP7EvxG/wC4b/6crWvpfdXzJ/wUrb/jCf4jf9w3/wBOVrVx+JCex+FlFFFdxgfsB/wR9bH7NPiX/sbrn/0jsq+6Fb1r4U/4JAtj9mrxJ/2Ntz/6R2Vfcm/FefUdpM6Y6pFjcKXcPryOPxFV/M96BJtOc1k5JDszxb9piEW+pfDfUM5K6zPYucdFks7iQEn0LQoPqRXKxqQpHUY2/iBj+oru/wBpy3DfC06mBltH1TT788c+WtzGkh+gikkJPoDXCx/u1G4hdpOWbO0EKMEkc4JA/HFfI5zHlrJrqe/l7Xs3E+cf2pofsfiOC8HzvJolxJj18iVWx/5E/Wvd/FHivTPB/h251nVbr7Pp8YUtIilmZnYKoUDqzMQAPUivMf2ndEF1aeGrx1aNPPudNkbrtW4gMmScDjdboAMd/c55P4zeMLm6/Zn0C7eBIrS+itDqGovI4Fm8QWUhQqORI0yGFSQFDMuWAqPZurTpR7aE1KnsY1Jdkc/8QwdN+Ik95BL5Ud4EuYbiPKEoyqfMB5IBO7Gf7pr0v4T/ALWXxE8GXVnYC5bXrV3WOOyvjlucAKH6jBOeMdOvFefR6xdeK/h39rsS0Wp2VobS9jmiDSPYyn931JKlSCM5ySWxjbz6D+x78Ok+InxYtb+aH/QdMJu51ZM5cEAAkcAZA+ma+4zKFbE4ehjqP2bQl69z53h/GYKtgMXltafLOb9pB9u6TP0n0W6ub3RbO4vYVhvJoFaWJTwrEZIyffivz5/aU8BWXw58dwadf29wLDUNeGtWuo7GaIRzOBdRMRnlTHA4z1BOO9fZvxA+PXgj4a27HWNagFwvP2S3YSSEg4IwPcjrUvw38ceHfjt4Tg1yHT47i2juWEcd2iu0bKcBuRwcHjHvXLW9lioqipXme1k2bf2Ribzje61V7PyZ8d/s9/DWX4p/FfU9ftInHhKPxMdXS85Ak8ppWiVeBnc0wPTgIQSTivu3xRqF5pXhvUL3T7aO9vYIGlit5G2q5AJIJHfAIrlfir8TtA+Bfhe1v7uz2WslwsK29oAjDcTuYAYzjOTjnmn+A/jp4L+JVtG2ja1bzSSDJtZHCSr1G0qeSeDwKVFUsKnTUrSZlm+cPOMSklblVkt3bufAHxR/av8AiH4+mnsvt50SzZin2KxyhABwVL9TjH16c1xHwtt5Nd+J1vPI/nCEyyyyTSANtUFQSzHABLA5JA/Sus/ao+H4+G/xe1Roo9tnqDC9tSy4XJb58E54GDn3wO9c9H4U1TWPh9qag/ZLvxYsWmy30doXFrYMvlO3loCxZ0DAIAWZiCAQCDthHLL8JXx+Mdoy9xX3v3RzZxKhKhgcnw8+acf3lRr8Ez7m/wCF5eGfiX+zX8Vdc8K3K32j+HbLVtLS9idHS6e3s9xkjKs3yMXG3JyRgjhhW58J7A2PxC8eQY406LR9JJHZorFJSv4C4U/ia+TP2SfCtp4e/Zl+Lfh3TGuP7M1r4lf8I/Y20jK4EMs9nbsVZWYsBE7kknBEZIyCTX2D8FydSbxzryjI1jxTfMNwJBW2YWCMCAeGFnkHpjHrW1+aknHbQ5VpI9Uh5Uc+358f/X/CvFPhHL9stfF2pH/l+8VawQ3qIbuS1BHbBFvke2D3r2G41CLTdNuL25kEdrbxNNJITwqqCWJPbABPSvGfgbDNbfCLwo90DHeXliuo3EbDBWa4zPID7hpCD7gnuactIhFas9B3CkZvSofM96TfmufmRdmiXfRvqHcP8mjcP8mi4ybfRvqHcP8AJo3D/JouBNvo31DuH+TRuH+TRcCbfRvqHcP8mjcP8mi4E2+jfUO4f5NG4f5NFwJt9G+odw/yaNw/yaLgTb6N9Q7h/k0bh/k0XAm30b6h3D/Jo3D/ACaLgTb6N9Q7h/k0bh/k0XAm30b6h3D/ACaNw/yaLgTb6N9Q7h/k0bh/k0XAm30b6h3D/Jo3D/JouBNvo31DuH+TRuH+TRcCbfRvqHcP8mjcP8mi4E2+jfUO4f5NG4f5NFwJt9G+odw/yaNw/wAmi4E2+jfUO4f5NG4f5NFwJt9G+odw/wAmjcP8mi4E2+jfUO4f5NG4f5NFwJt9G+odw/yaNw/yaLgTb6N9Q7h/k0bh/k0XAm30b6h3D/Jo3D/JouBNvo31DuH+TRuH+TRcCbfRvqHcP8mjcP8AJouBNvo31DuH+TRuH+TRcCbfRvqHcP8AJo3D/JouBNvo31DuH+TRuH+TRcCbfRvqHcP8mjcP8mi4E2+jfUO4f5NG4f5NFwJt9G+odw/yaNw/yaLgTb6N9Q7h/k0bh/k0XAm30b6h3D/Jo3D/ACaLgTb6N9Q7h/k0bh/k0XAm30b6h3D/ACaNw/yaLgTb6N9Q7h/k0bh/k0XAm30b6h3D/Jo3D/JouBNvo31DuH+TRuH+TRcCbfRvqHcP8mjcP8mi4E2+jfUO4f5NG4f5NFwJt9G+odw/yaNw/wAmi4E2+jfUO4f5NG4f5NFwJt9G+odw/wAmjcP8mi4E2+jfUO4f5NG4f5NFwJt9G+odw/yaNw/yaLgTb6N9Q7h/k0bh/k0XAm30b6h3D/Jo3D/JouBNvo31DuH+TRuH+TRcCbfRvqHcP8mjcP8AJouBNvo31DuH+TRuH+TRcCbfRvqHcP8AJo3D/JouBNvo31DuH+TRuH+TRcCbfRvqHcP8mjcP8mi4E2+jfUO4f5NG4f5NFwJt9G+odw/yaNw/yaLgTb6N9Q7h/k0bh/k0XAm30b6h3D/Jo3D/ACaLgTb6N9Q7h/k0bh/k0XAm30b6h3D/ACaNw/yaLgTb6N9Q7h/k0bh/k0XAm30b6h3D/Jo3D/JouBNvo31DuH+TRuH+TRcCbfXzN/wUobd+xV8Rf+4b/wCnG1r6T3D/ACa+Z/8AgpI2f2LPiL/3Dv8A042tVB+8iXsz8N6KKK9I5j9ef+CQrY/Zr8Sf9jbc/wDpHZV9w7/evhr/AIJFNj9m3xH/ANjZc/8ApHZ19v7/AFry6vxs6o7Im8z3o8z3qHcKNwrK6W5fKZXjfw3F428Ga74flbamqWFxZFj28yNkznqCM5BHIIr5i0nVNX8TfDBLvS2W28R3GmOsTSrxFeBCuGH+zMrg8E4HAPAr6zDbiBnHIGT0GTjP68e+K+aTpo8L/EXxhoBH7hrv+2rQMODDdgu+B7XC3WB0AK4wCBXi5pHmgp22O/BS1cX1PLvGXh3UdS+HHid7ex1q0sbC1t9RtbfXboXV095BJJNcFXDyYVkWNApYYJbaoXBK/BFbDxR4B8R+Fr5PtVgJrmBosnMtpchpTzkHGZJlBz/yzFe1oqyZDjzAy7SHG7IwRg57YJz68k8182/DlW+F3xbbQ7jP2XzTohViOUIElhISxGSQTGOeXfBzjjzKU3VpTinqtT0akFGXdM8ch/4s78XNeNrrUmp2lhdDTp7i6C+TdW5jjJicqAN6sSCwAwy8g5yPXI/E2saH4Xur74fX8lvoF47Pex2yst3ASAWjlQ4bbjBUgdCMnozR/tOfCNf7Vh8VaNpM9xa3ttdR61/Z0Uku92jHlStBGWLsSGUtgAEhiw+8PFfA/jLVPC62F3pssv8AaElnHO8PmAxOCMbpHZgpypPykHBIPJYsfqMuxzpRU2uaL+KL2PyfNo18txD9o2o391rTl9P8jqZHl1Mz3rPK7qU82aV8qCThcuQcZzg55OeucV9V/sF/ESXQ/FeoeFLt2FvqCebb7l6yqfmGc8AdAOpznsa+adN+IPhbxlp+NX0zUfCt3dhWlktD/o0xHQyJkrgEEgZJIAOQRW74L/srwh4ks/EOk+O9KX+z7pS0moM1oSQxBwTkE9cEnA6ngGu36vllWo6+Gquk1ryPVN+vQ+tXFc8ZQjQxuHhWqJcqqRdpL5WPY/26PiFJ4r+Idv4cs5C1ppK/vDkBfOY5PJ5BAGM5wPxr5qjuLjSJ4J45Zre6bEkTRsYjgnG4P3xg8g+o716J4r8DSax4w1G61vxLCb66uPNeO1gkvJcOw2AogPXgHPA5wMqRW7Z+F9D8MCxF0GudS3pp1ra6oRJcF2gM8cUdupOzeroFMpG0zJk5DBfOrYjJcDNYrE1/bS/kX+fY9WlnWNjRlh8Bg40JyVnUesmvuKWmXPiD4hafpUnjOV9RtoHVtPiuCVnnAyNuSDtiHDMzcAAEkAE1ifETXtdtbXXtcu4WfSvDdpbC1tbG1aS3vNTlM6wSrcM0eJY4blVCqJQzMQEYAO/T6p4kSxm+13mrW9hqWnvHNNNJqVsi6epCbWnMqCOWYFWYRwzLGcKQWIBOh+yz8I4P2lPippvj6SwgT4aeF7gvapd6a9rNrurCNd16SrsoQSANjzXXKgeWCxr5yWPr8Q4y6jy0I7L+tLtfmefhsJTwNJx5uact33PV/hx8OV/Zd+Ffw98KajcnULjw1Zap8QNdMS7V8+OBkjgC9cGS6IUnDN9mJABBA7PW/h7438P+C/AOjWkeualZWWkNFep4YvEtbj+1yVZZpXaWPdCZDJ8uWXJyyNgFWXE3/CzviJuQeZbeINXQxAjDf2Ho8hYsoyf9bfNgEHDw3EecgED6ThQFTk7t2ASfQkE4HqRnI7nrnJr7Sb0jHsOPVnmPxivdXtv2fbzS9SuIZPEGtWVroE1xa4CtdXhitpZEA6BTK7gY6LnNb0CJbwxxRqESNQqoo4VcAAD8jXOfFK8PiD4oeDfDy5eDR0m8SXqEnO4q1taI3++Zrlh6G146CuhZ+2TnJBYgfNgDn8CTUyldWHHuTeZ70b/eodwo3elYmlibd70bveod5o3mi4rE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273o3e9Q7zRvNFwsTbvejd71DvNG80XCxNu96N3vUO80bzRcLE273r5p/4KQtn9i/4h/8AcO/9ONtX0hvNfNX/AAUebd+xj8Q/+4d/6cbarp/GhPZn4gUUUV6xyH65f8EjWx+zd4k/7Gy5/wDSOzr7c3V8P/8ABJE/8Y3+I/8AsbLn/wBI7OvtvcK8ms/3jOyHwol3CjcKi3CjcKwumaErfOrKOpVhjt0PX/PXFeP/AB908abqfhrxnFtEVncHS9RZuFFrcsgjkJHZJ0hBP8KvIRjJr1vcMEf1981neJNDsvFugajouop5thqNvJazxrxlXUqcEAkHBJDAEqcHBxgxUj7SDgwhLkkmjyKHOeCRyyneBkFVBII6dSAe3NeL/tFeDRI1l4jgLxIwGnXssX3ogx3W0+ezRynAPGDICThRjvfhp4kTxBos1u97HqF9pNw+m3V1GMLO8RAWdeSCsqeXICCQd+MnAx1eqaTa69pN5pl/Es9hdQPBPG4+Vo2BBBxz06DucDvXxlNvD1XfufQySrU1qcLoupz/ABg+FN/bDUZNF1m6hk0+4uLUndaXRUAsBleCWRlAZTsZTlSxx8gfET4f6l8K/EE2gyWum6rHZ20d8P7P3Qr9kYStJN5LdPLaFtzFgCGjyQWCj3bwnqt78GfiBeWGrTObVSsGoXDNtaWAkm3v+BjgYilYD5SsgIPlDP0Xqmh2HibRb7TtTt1vLHULWSznjc7fNikVldQwOQCHJGDkZHJPNepGq8NU5lrGR4ePyyhmtPlrL3kfl/rF3o/hpJ7ySWFUuIliMUa7TGrl2OEEqso3AAg5GRwQMA6+m2E/2d9OudLu5GBfzWikS7ieRCjy/KCE2ruJYsV2EHJUA7f0Q+GfwS8I/DS3v49LsnnkvixurnUpDcTupUIFcsSQgGAAMA5JIJJJxPDv7G/wz0XXGvYrC9uNObymk0K81GS6sDJEPlaRJSzSAdlcsgySFyAR3fXoVI2PmqfDCp0lGtNuXc+UvBf7SekaXZ2PhO8t7iz8PR2M8Nzquj83tw0oJEkJkjYAIieSGIwInkChc5Gr4p/aNsrmx1N/Dlslrpl20cuqaguozw3l4qqRvnMdzIGJ3HcShLAAgBsA/U//AAxN8ILrxAuqnw5st/PF0+lR3D/YXfIOPLJ4XKr8i4XAIxjir3gX9hH4O+ENfg1aLQbrUbm3mWaCPUtRnnhhZQAgCFyGCgAASBsdq8j+y8snP2tnc9RYXG06SpQq2S8j5+/Zv/Z2v/2pob2+8bXmuaX8J7bym0XRbFI9Nt792yZQ22NTKgAQmQYyz4UsQ237k8cNF4D+HuieBvCAGm3+oJHoOjLGxZrOIRAPOGYksYoQWDNkbhGpO6TNdit3Dp9vLczypbW0C+ZLNIQqRooyzMeAABk/gMEEgjwG8u7z4v8AikeUbi3bxFaG1t8ErNpnh4OplmAPKTXrkKucEAx5wYGJ+mw0Y042grI6XGSSU5czO/8AgTodlc2114nsYBbaRcW8WleHYEyBDpNuCsbg9jM5km3DlkaIEkx5r2a3buT2A5OPXHtxkn8PasjS7WGxtYbW2ijgtYUEcUEQASNQNqqMcBQNuMdga8++P3jKHR/Dem+Ek1JbDVPGF0mkQTFtrx2zFVupQR91wjlFP/PWaEDk4PRGXNq3oO2ljJ+HN7/wlV54h8bOSya/d/8AEvZhz/Z0G6G3IPXDlZJ8Hobjjg4rtdw9fTv/AJ9ap2VtBYWcFtbIkVvDGqRxRgBETaAioPQAEccEAEVPuFTKWuoRtsS7hRuqLcKNwo5kUTbqN1Q7qN1IVibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdRuqHdRuoCxNuo3VDuo3UBYm3Ubqh3UbqAsTbqN1Q7qN1AWJt1G6od1G6gLE26jdUO6jdQFibdXzX/wUbbP7GfxD/wC4d/6cbavo7dXzb/wUZb/jDX4hf9w//wBONtWlP40TL4WfiRRRRXsnEfrX/wAEkz/xjh4j/wCxsuf/AEjs6+2t3vXxH/wSVP8Axjj4i/7Gu5/9JLOvtjcO9eNW/iM7IfCh+4UbhUeRRw2AOpIA5xznpnoM9M+/risE9SyQHdx+BIPT3P8Annp3rg7yY/FfULjSLc48IQyeVqFwpIXUpA2GtUPUwqVIkIOThkDEhgKviTxBN421ifw7ok8ltptvKYtW1O3Yhy4IJs7dweHIJDsD8gOA2/Ij7fw7p9rpljbWdnBHbWkMKwxQRLtRFXICqOygYA9dpPXJrjq1uV2jqzqjSbtI8l+OfhmL4e+KtN+IthEItDu0j0jxEEAVI0BItLtgOcRMTEx/uSrj7vF2NcYyOwwGOTyCCDjg9j+Ir3W70Ow8T6NeaVqtpHf6dfRG3uLaUZWVCCNp/PjGMEg18y+H9Kv/AIb+Jrr4da5PJcXGnx/aNHv5iN2pacSRG5PeWMny3HXIU9GFeZjMN7SKqR+Z14eslLlkUfiz8PX8ZaTDfafGr65pwZoFfAFzEQPMgYnjDYBBPRlXkAk1zXwO+IKQw2/hm/lYQSFl0y4nyGUKfmtZAcESIQwUHkhSuNyk17JEMxkZ5OMEnBJ6/wBK8n+LnwzPnXPiTSraWWKTbLqljag+aCgBF1CBz5qbQTt+YqCQGYKDhh5e0j7Gey2N6l4vmjse12+VOCuDkEq3J56Z9Rjp3GSDyK17Yng5OenU/SvHfhH8ShrkdtpGrXMT6mYgbS+RgIdRjAzuBHRwCNwHDZMijBIr2K2+6ueO+CpHpkAHkY4yDzyDk5wM3TdKVmNyU4o2LdiQpzyMY554/wD11sWa/dCjvg7Mbjnjg+uSOTnAycHGDj2mTgYyewxntk+nbPfHqQMmuB+I3xKRI7zR9L1H+zoLdcaxrm4BbCMgZjjJGGnZSAB/BkMQcqp78PFzduhxVHZNEPxY8dW/iKWfR4Ve98PWU6wX0dqAx1i9yVj0+LJw67xmXkKSAjEKs4HoXwy8H3Hhyxu7/V5I7rxLqsv2jUZ4yWVGGVSBCQDsiUlAcDcfMcgGQ5434SeADG1lr2p2B00W8Zi0fRXBB0+MgZkkBORcsCCxblAwUEnzGb2KD5iRkAnAHGPoPYDJIHTr616Up8tox2OfzZe8xLWF5nZYo0TLM3ChQCcnsAOcnoBnkdR8tw3kHxm8Sax4v1G3W90jUrf+zNEtbhAynTASTKVIBBuG3yZPIUwg8xkHq/2gvGD+JrpPhrpcrCO4iS48RXEbkPDaNnbaAj7slwAAQekRkz99aqaaiRKiIipHtAVFUBcAkDA9MBSPoMYwMebi8RypUoPU68NR5nzSWho+CfE114d1Ky8N67cSXsFxIV0nVZGLvcDgm3lY9ZgASrk5lUjJLqSfRVb5FJ9ODnhhgHIPfryMDHA5OTXCXGi2XiLSZ9Nv4vOtJlKsgJUqeSHQggqyk7gykMpGQcipvDXiC+0nUk8OeI5fOvW3HTtUYBF1KMHJVzgATxgncAACPnAwWROvDV/axUZvVGGIp8krrY7XcKN1R7hwP1xj9DyDzyD04PfAXcO1d5y2JN3vRu96j3UbqBEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73o3e9R7qN1AEm73r5t/4KLH/jDb4g/wDcP/8ATjbV9Hbq+b/+Cih/4w3+IP8A3D//AE421a0vjXqTL4WfidRRRXuHHqfrP/wSYP8Axjn4i/7Gu5/9JLOvtb618T/8EmuP2c/EX/Y1XP8A6SWdfau4LyemRngnuB27E8E+hP1rw6/8VnVH4EOYj1x1GQM84ODj2OCfQAk8V53468YXWq30/hfw9cfZ7kALqmpRnIskIBMUZ5HnsCTzkRIVcg5GY/iP8QpdPvv+EW0K4jHiGSNZLq4Lqf7Ot2JxIVOd0rgERLgqSAWOAQcrwzo8GgWMdrbhlQElmcl2mkLFnkctjcxYlmJJJZ2POa8fFYiVJctNXbPRo0lLVs6fw5plpotjBZWEK29pCoWONQQApGRjuQcljuJJJycE4rttNPC9uAByB3wBk8d8D3PHXB8om+KXhbRbw2L6ql7qWSf7O0uNr26JJySYogzjJ6naeSMkZzWra6x458WKBpWnReDNOcEfbtWUT3zKeD5dsheNCQesrkjoY+SV5cPCbfN1OipJRXunqWt+PdE8C2cU2r3ixyzMY7eyhjaa4unAOVhiUF5GAByAvHU4AJHlvxA8N6/8bZbHVr5F8IPo8klxoltGsc90J2UKGvJVJQoxCB4YmKgKD5pIBi3fDfgXTPDVxLeg3GqavMAlxq2pyGe5lAIIQsSQqqQMIoCAgFQMV0O5d4dgW29ee2MAHPbBxj0JHQkV7aso8vc8zXm5up5L4Z1xtYsZkuYfsWq2Mht76zYljBKACcHAJVgQyNgb1ZSAM4rk/DfxG1HU/HX9my3OmOX1S6sJPD8UTC/sLeISGO9kkMpBjlMceP3YH+kRbWYgsfTfiB4OvLy7j8RaFEp1+3jEU1uWCrqUA3ERFjwHBcmNjyGJGQHY1S8Pa3aeIdNjvbIsIJSwaN12vGVYgxyLxtcPv3KRkMD1IJrwqtJ0W2kenTqKorI8v+JHwtOjtPrWjWskunNIbu7sLUlZreTG43Vrgbsg5LIp3ZyVBJKt1Pwv+LP242ml+ILyKWe42rp+txgfZtQUAlQSCFWUjJwNquDlSGYpH6LbjLcHGW3EL1zkHIHqSB+Gc8ZrzPx98HyzXWp6Bp8WowXRaTUPD0ijyrtics8O4FUcNyVYqrsxwVLbhdKpGsuSoKcXT1j1NPx58WRcR3unaFfixsbX5NU19cMtuc48m3PRpgcAsNwQ4ADPkJofC/4btcfYdZ1eyawsrJjLpWjy5Lo5yTd3RJy0xLFwucoSWP7wgR1vhr8KTapp2q+IbWGKazA/szQ4wGttMUAbScZRpiCclSVQHCkjLN7JbdQRu3Y5IOCeMDJ6nAJA9ASBgE11SnGmuWmYW5tWeWt8XdZh+LUnh5G0tI4dTt7CPw4bdhqN1bShC+oRP5gAhQs5IMW3bEQXDkKPSvG/jY+D9LgFpbpf6/qEnkaZprybVuZgMlmODtijA3yMegUhQzMql+veJLDwno7ajfFvK3COKO3jzNPI5VFihXqzsQAFGOmSQASOX8M6XfTahceI9eCDXb1TGtvE26KwtwQRbxngNyAXkA+dv9lY1Xpp3lqc8tHY8yvfhn4j+Hk17qttPL41gvpmvdWUQrFftcsAJJYwAFmQ7VxCRvQBUUyDbjZ8K+ILDxFZfatNuUu4UYxOASGjZcAxyKQGRlzgqygjIBAOc+sbhnPf1PXvz9eTzXH+LfhjpXirUBqsEk+ieI1QJHrGnsEmYLnasoIKzIMnCyAgZOMZJrkr4OE5ucdzop4iVNWexf0rO32zwzDOfoQcY/DNbGqeGLLxbo506/DeWSHinjk2y28i8pLG3VXU9CMccH5SQfN49S8WeCZNmtaIviPThyuqeHY/9I2jq0lkxL/XyTJkkYUDOOz8FfFTwp4qufsmna/ZtqOcSabNIILuI5HBgkKyKemQy57Y5rmp0Z0pWNZzjUVxmg6xf2OpSeHfEWF1qGJpre8WEpFqUIGTMq5yrLlfMjySmdwBVwx6XIPIPoCAwYA4B6gDPXOQACCMDrWp4k8F2HjbSFsrh2trm3mWe1vrYgXFlMMkSxkjCkAtkEFSrOCrBip4/RdU1D+0L3Q9dhjg8Q6akbXBgUiG4jkL+XPGCSQjlJBtJJUqy5KhWb21dxTPOl8VjcopmRRkUCH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH0UzIoyKAH184f8FEv+TOfiB/3D/wD04W1fRmRXzh/wUR/5M6+IH/cP/wDThbVpSf7yPqTL4WfipRRRXvnHqfrH/wAEm/8Ak3PxF/2NVz/6SWdfaoO3nJB6ZU4Pp1+lfFP/AASd4/Z18Rf9jVcf+kdnX2nXhYj+KzthpFHOeJ/hn4S8aTrca34c03ULpVEaXUlsomRR0VZAAygZOACAMn1NYcf7P3w4Xh/CWn3KYAMd2pnj4/2HJX26dAB0Arv6K5fNo05pFPRfD+k+G7JbPSNMstKtF+7b2NukMY+iqAP0q/xknqSclicn069emR9CRTKKa9BXb3H/ACntzjGe+B0H0HYdqPl9PzplFP5Cu+g/hcbeueF7HPUH8M1wXizwndaTqj+JPD8PmzygDUtKUhVv1UbUkj6BZ1GACcBwArHIVl7r0P5YNH8JOOMYKr3B4Ix3GCcjvUS96LTRcZOLucb4f1i08QabDf2EwntZRgSYKsrAkMCDgowIIZSAQQQepA3Y/vevHOeexA/IEgemTjrXO+JfCF7pepzeIvDSo93Kc32lMwSLUQoA3qTwk4AABOFcAKxGVZdDw34gsPE2npd2UrCNWaKWOcGKaCReHjmQjKyKeCmAQcDnPHjVsPKm+ZLRnoxqqorM6KL7oIBJAAAzxgdvYck8dyT3pdY8RWHhfS5NT1CfyrSHAUqpLyO3yokaAEuxYgBVBJJwATgHJ17xRYeFNNW7viylpBBDBGvmSzTMDiKNBgs5HIXoMksygEjO0Pw3qGo6pF4h8TRKmpKGWx0xSWh02PGMAk4kmIJDSAADJVflLFt6FOUrNrQ56kuVWRLo2i32tasPEPiOLyr1U8ux0vcGTTEIIbJBIaZgSGYEhQzRqxG5n6k4Zsnk4AHPAAJOAOw57dgB0AxH6fTA+noKWvUV49Dku3uP+Wjj/JplFP5C33H7gFIBwpOSoOAT6kdzWV4h8J6F4utRb67o2n61BjHl6haxzrj6MDWlRR8h7bHDx/A/wLDsFvoENoifcitZpYY1HoEVwoHsABW/4X8D+HPBMM8fh7Q9P0Zbgq05sbZYmmKjClyACxA4BYkgcCtmii726C3H0U3JoyaCh1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1FNyaMmgB1fOP/BRD/kzv4gf9w//ANOFtX0Zk185f8FED/xh38QP+4f/AOnC2rWl/Ej6kS+Fn4rUUUV9AeefrF/wSd/5N18Rf9jVc/8ApJZ19pfjj61+av8AwT5/an+F3wP+DGs6F428T/2Lqtxr819Fb/2fdT7oWt7ZA26KJl+9G4wSDx05Gfpz/h4d+z7/AND/AP8AlG1D/wCR68WvTnKo2lodsZJRV2fR20+o/P8A+tRtPqPz/wDrV84/8PDv2ff+h/8A/KNqH/yPR/w8O/Z9/wCh/wD/ACjah/8AI9c/sZ/ysvmXc+jtp9R+f/1qNp9R+f8A9avnH/h4d+z7/wBD/wD+UbUP/kej/h4d+z7/AND/AP8AlG1D/wCR6PYz/lYcy7n0dtPqPz/+tRtPqPz/APrV84/8PDv2ff8Aof8A/wAo2of/ACPR/wAPDv2ff+h//wDKNqH/AMj0exn/ACsOZdz6O2n1H5//AFqNp9R9cnP4HFfOP/Dw79n3/of/APyjah/8j0f8PDv2ff8Aof8A/wAo2of/ACPS9jP+VhzR7n0fzknPLck5Ofzx9PyrkfEvgu4m1A694euE0zxCqqsm/PkXqDjy5lA64yFkA3IcEbl3I/j3/Dw79n3/AKH/AP8AKNqH/wAj0f8ADw79n3/of/8Ayjah/wDI9P2M2rOLBSUdmey+F/Bs1jqH9u63cpqfiOSMxiVQfJsojj9zbqRlVPG5j8zlcsfuqvUhdvQqOAM9+OmTivnH/h4d+z7/AND/AP8AlG1D/wCR6P8Ah4d+z7/0P/8A5RtQ/wDkel7GaWkWPnT3Z9HbT6j8/wD61G0+o/P/AOtXzj/w8O/Z9/6H/wD8o2of/I9H/Dw79n3/AKH/AP8AKNqH/wAj0/Yz/lYuZdz6O2n1H5//AFqNp9R+f/1q+cf+Hh37Pv8A0P8A/wCUbUP/AJHo/wCHh37Pv/Q//wDlG1D/AOR6PYz/AJWHMu59HbT6j8//AK1G0+o/P/61fOP/AA8O/Z9/6H//AMo2of8AyPR/w8O/Z9/6H/8A8o2of/I9HsZ/ysOZdz6O2n1H5/8A1qXBHfP0/wD1V84f8PDv2ff+h/8A/KNqH/yPR/w8O/Z9/wCh/wD/ACjah/8AI9HsZ/ysOaPc+j6K+cP+Hh37Pv8A0P8A/wCUbUP/AJHo/wCHh37Pv/Q//wDlG1D/AOR6PY1P5WHPHufR9FfOH/Dw79n3/of/APyjah/8j0f8PDv2ff8Aof8A/wAo2of/ACPR7Gp/Kw549z6Por5w/wCHh37Pv/Q//wDlG1D/AOR6P+Hh37Pv/Q//APlG1D/5Ho9jU/lYc8e59H0V84f8PDv2ff8Aof8A/wAo2of/ACPR/wAPDv2ff+h//wDKNqH/AMj0exqfysOePc+j6K+cP+Hh37Pv/Q//APlG1D/5Ho/4eHfs+/8AQ/8A/lG1D/5Ho9jU/lYc8e59H0V84f8ADw79n3/of/8Ayjah/wDI9H/Dw79n3/of/wDyjah/8j0exqfysOePc+j6K+cP+Hh37Pv/AEP/AP5RtQ/+R6P+Hh37Pv8A0P8A/wCUbUP/AJHo9jU/lYc8e59H0V84f8PDv2ff+h//APKNqH/yPR/w8O/Z9/6H/wD8o2of/I9Hsan8rDnj3Po+ivnD/h4d+z7/AND/AP8AlG1D/wCR6P8Ah4d+z7/0P/8A5RtQ/wDkej2NT+Vhzx7n0fRXzh/w8O/Z9/6H/wD8o2of/I9H/Dw79n3/AKH/AP8AKNqH/wAj0exqfysOePc+j6K+cP8Ah4d+z7/0P/8A5RtQ/wDkej/h4d+z7/0P/wD5RtQ/+R6PY1P5WHPHufR9FfOH/Dw79n3/AKH/AP8AKNqH/wAj0f8ADw79n3/of/8Ayjah/wDI9Hsan8rDnj3Po+ivnD/h4d+z7/0P/wD5RtQ/+R6P+Hh37Pv/AEP/AP5RtQ/+R6PY1P5WHPHufR9FfOH/AA8O/Z9/6H//AMo2of8AyPR/w8O/Z9/6H/8A8o2of/I9Hsan8rDnj3Po+ivnD/h4d+z7/wBD/wD+UbUP/kej/h4d+z7/AND/AP8AlG1D/wCR6PY1P5WHPHufR9FfOH/Dw79n3/of/wDyjah/8j0f8PDv2ff+h/8A/KNqH/yPR7Gp/Kw549z6Por5w/4eHfs+/wDQ/wD/AJRtQ/8Akej/AIeHfs+/9D//AOUbUP8A5Ho9jU/lYc8e59H0V84f8PDv2ff+h/8A/KNqH/yPR/w8O/Z9/wCh/wD/ACjah/8AI9Hsan8rDnj3Po+ivnD/AIeHfs+/9D//AOUbUP8A5Ho/4eHfs+/9D/8A+UbUP/kej2NT+Vhzx7n0fRXzh/w8O/Z9/wCh/wD/ACjah/8AI9H/AA8O/Z9/6H//AMo2of8AyPR7Gp/Kw549z6Por5w/4eHfs+/9D/8A+UbUP/kej/h4d+z7/wBD/wD+UbUP/kej2NT+Vhzx7n0fRXzh/wAPDv2ff+h//wDKNqH/AMj0f8PDv2ff+h//APKNqH/yPR7Gp/Kw549z6Por5w/4eHfs+/8AQ/8A/lG1D/5Ho/4eHfs+/wDQ/wD/AJRtQ/8Akej2NT+Vhzx7n0fRXzh/w8O/Z9/6H/8A8o2of/I9H/Dw79n3/of/APyjah/8j0exqfysOePc+j6K+cP+Hh37Pv8A0P8A/wCUbUP/AJHo/wCHh37Pv/Q//wDlG1D/AOR6PY1P5WHPHufR9FfOH/Dw79n3/of/APyjah/8j0f8PDv2ff8Aof8A/wAo2of/ACPR7Gp/Kw549z6Por5w/wCHh37Pv/Q//wDlG1D/AOR6P+Hh37Pv/Q//APlG1D/5Ho9jU/lYc8e59H0V84f8PDv2ff8Aof8A/wAo2of/ACPR/wAPDv2ff+h//wDKNqH/AMj0exqfysOePc+j6K+cP+Hh37Pv/Q//APlG1D/5Ho/4eHfs+/8AQ/8A/lG1D/5Ho9jU/lYc8e59H0V84f8ADw79n3/of/8Ayjah/wDI9H/Dw79n3/of/wDyjah/8j0exqfysOePc+j6K+cP+Hh37Pv/AEP/AP5RtQ/+R6P+Hh37Pv8A0P8A/wCUbUP/AJHo9jU/lYc8e59H0V84f8PDv2ff+h//APKNqH/yPR/w8O/Z9/6H/wD8o2of/I9Hsan8rDnj3Po+ivnD/h4d+z7/AND/AP8AlG1D/wCR6P8Ah4d+z7/0P/8A5RtQ/wDkej2NT+Vhzx7n0fRXzh/w8O/Z9/6H/wD8o2of/I9H/Dw79n3/AKH/AP8AKNqH/wAj0exqfysOePc+j6K+cP8Ah4d+z7/0P/8A5RtQ/wDkej/h4d+z7/0P/wD5RtQ/+R6PY1P5WHPHufR9FfOH/Dw79n3/AKH/AP8AKNqH/wAj0f8ADw79n3/of/8Ayjah/wDI9Hsan8rDnj3Po+ivnD/h4d+z7/0P/wD5RtQ/+R6P+Hh37Pv/AEP/AP5RtQ/+R6PY1P5WHPHufR9FfOH/AA8O/Z9/6H//AMo2of8AyPR/w8O/Z9/6H/8A8o2of/I9Hsan8rDnj3Po+ivnD/h4d+z7/wBD/wD+UbUP/kej/h4d+z7/AND/AP8AlG1D/wCR6PY1P5WHPHufR9FfOH/Dw79n3/of/wDyjah/8j0f8PDv2ff+h/8A/KNqH/yPR7Gp/Kw549z6Por5w/4eHfs+/wDQ/wD/AJRtQ/8Akej/AIeHfs+/9D//AOUbUP8A5Ho9jU/lYc8e59H0V84f8PDv2ff+h/8A/KNqH/yPR/w8O/Z9/wCh/wD/ACjah/8AI9Hsan8rDnj3Po+ivnD/AIeHfs+/9D//AOUbUP8A5Ho/4eHfs+/9D/8A+UbUP/kej2NT+Vhzx7n0fRXzh/w8O/Z9/wCh/wD/ACjah/8AI9H/AA8O/Z9/6H//AMo2of8AyPR7Gp/Kw549z6Por5w/4eHfs+/9D/8A+UbUP/kej/h4d+z7/wBD/wD+UbUP/kej2NT+Vhzx7n0fRXzh/wAPDv2ff+h//wDKNqH/AMj0f8PDv2ff+h//APKNqH/yPR7Gp/Kw549z6Por5w/4eHfs+/8AQ/8A/lG1D/5Ho/4eHfs+/wDQ/wD/AJRtQ/8Akej2NT+Vhzx7n0fRXzh/w8O/Z9/6H/8A8o2of/I9H/Dw79n3/of/APyjah/8j0exqfysOePc+j6K+cP+Hh37Pv8A0P8A/wCUbUP/AJHo/wCHh37Pv/Q//wDlG1D/AOR6PY1P5WHPHufR9FfOH/Dw79n3/of/APyjah/8j0f8PDv2ff8Aof8A/wAo2of/ACPR7Gp/Kw549z6Por5w/wCHh37Pv/Q//wDlG1D/AOR6P+Hh37Pv/Q//APlG1D/5Ho9jU/lYc8e59H0V84f8PDv2ff8Aof8A/wAo2of/ACPR/wAPDv2ff+h//wDKNqH/AMj0exqfysOePc+j6K+cP+Hh37Pv/Q//APlG1D/5Ho/4eHfs+/8AQ/8A/lG1D/5Ho9jU/lYc8e59H0V84f8ADw79n3/of/8Ayjah/wDI9H/Dw79n3/of/wDyjah/8j0exqfysOePc+j6K+cP+Hh37Pv/AEP/AP5RtQ/+R6P+Hh37Pv8A0P8A/wCUbUP/AJHo9jU/lYc8e59H0V84f8PDv2ff+h//APKNqH/yPR/w8O/Z9/6H/wD8o2of/I9Hsan8rDnj3Po+ivnD/h4d+z7/AND/AP8AlG1D/wCR6P8Ah4d+z7/0P/8A5RtQ/wDkej2NT+Vhzx7n0fRXzh/w8O/Z9/6H/wD8o2of/I9H/Dw79n3/AKH/AP8AKNqH/wAj0exqfysOePc+j6K+cP8Ah4d+z7/0P/8A5RtQ/wDkej/h4d+z7/0P/wD5RtQ/+R6PY1P5WHPHufR9FfOH/Dw79n3/AKH/AP8AKNqH/wAj0f8ADw79n3/of/8Ayjah/wDI9Hsan8rDnj3Po+ivnD/h4d+z7/0P/wD5RtQ/+R6P+Hh37Pv/AEP/AP5RtQ/+R6PY1P5WHPHufR9fOX/BQ7/kzvx//wBuH/pwtqb/AMPDv2ff+h//APKNqH/yPXi/7ZX7ZXwe+K37N3i/wt4W8X/2pr1/9j+zWn9mXkO/y7yCR/nkhVRhEY8kdMDnGdaVKaqJtEylHlaufmDRRRXvHEFFFFDAKKKKQBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAgooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKa3H0CiiiqEf/Z'/>
            <div style="font-weight: bold">
                <h2 style="text-align: right;margin-left:25px">{{'ARAYIŞ-'+emp.personalMatterNo}}</h2>
                <h4 style="text-align: center;margin:-10px">{{emp.rank.name + ' ' + emp.rank.suffix}}</h4>
                <h2 style="text-align: center">{{emp.person.name + ' ' + emp.person.surName + ' ' +
                    emp.person.patronymic}}</h2>
            </div>
        </div>

        <div id="personInfo">
            <table align="center" style="text-align:left;width:750px;border-left: none;border-right: none;
            border-collapse: collapse;">

                <tr>
                    <th style="text-align:left;border-bottom: 2px solid black;">1.Şəxsi məlumatları</th>
                </tr>
                <tr>
                    <th style="text-align:left" width="85%">{{'1.1 Rütbəsi: '+emp.rank.name}}</th>
                    <th style="border-bottom: 2px solid black;border-top: 2px solid black;" width="15%" rowspan="7"><img
                            width="283px" height="358px"
                            src="${pageContext.request.contextPath}/static/resource/images/yoxdur.png"/></th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'1.2 İş yeri: '+emp.employeePosition.structurePosition.fullSttrName}}
                    </th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'1.3 Vəzifəsi:'+emp.positionName.name}}</th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'1.4 Məharət Dərəcəsi:'+successArray}}</th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'1.5 Hərbi xidməti:'+
                        emp.employeeMilitaryService.militaryDuty.description}}
                    </th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'1.6 Xidməti Vəsiqə:'+emp.policeCardNo}}</th>
                </tr>
                <tr>
                    <th style="text-align:left;border-bottom:2px solid black">{{'1.7 Təhsili:'+eduArray}}</th>
                </tr>
            </table>
            <br>
            <br>
        </div>
        <div id="personCard">
            <table align="center" style="width: 750px;border-top: none;border-left: none;border-right: none;
            ">
                <tr>
                    <th style="text-align:left;border-bottom: 2px solid black">{{'.'+'Şəxsiyyət vəsiqəsi məlumatları'}}</th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'Doğulduğu tarix:'+emp.person.birthDate}}</th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'Doğulduğu yer:'+emp.person.bornAddress.adress_fullname}}</th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'Etnik mənsubiyyəti:'+emp.nationality.description}}</th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'Ailə vəziyyəti:'+emp.person.marriedStatus.description}}</th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'Yaşayış yeri:'+ emp.passport.adress.adress_fullname + ' ' +
                        emp.passport.addressNote}}
                    </th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'Telefon:'+phoneArray}}</th>
                </tr>
                <tr>
                    <th style="text-align:left;border-bottom: 2px solid black">{{'Şəxs. vəs-nin nöm:'+emp.passport.passportSerialNo}}
                    </th>
                </tr>
            </table>
            <br>
            <br>
        </div>
        <%--Dio-da fealiyyeti--%>
        <div id="dio">
            <table align="center" style="width: 750px;
            border-collapse: collapse;">
                <tr>
                    <th colspan="4"
style="text-align:left;border-bottom: 1px solid black;border-left: none;border-top:none;border-right: none;">{{'DİO-da fəaliyyəti'}}
                    </th>
                </tr>
                <tr>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Daxil olub</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Çıxıb</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">İş yeri</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Vəzifəsi</th>
                </tr>
                <tr ng-repeat="dio in emp.dioList">
                    <td style="border: 1px solid black"><span ng-bind="dio.workStartDate"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="dio.workEndDate"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="dio.fullname"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="dio.structurePosition.positionName.name"></span>
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
        </div>

        <%--Ezamiyyet--%>
        <div id="ezam">
            <table align="center" style="width: 750px;
            border-collapse: collapse;">
                <tr>
                    <th colspan="5"
                        style="text-align:left;border-bottom:none;border-left: none;border-top:none;border-right: none;">
                        Ezamiyyət
                    </th>
                </tr>
                <tr>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Getmə vaxtı</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Gəlmə vaxtı</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Qurum</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Əmrin nömrəsi</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Əmrin tarixi</th>
                </tr>
                <tr ng-repeat="mission in emp.workMissions">
                    <td style="border: 1px solid black"><span ng-bind="mission.createDate"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="mission.endDate"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="mission.structureName.fullName"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="mission.missionOrder"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="mission.missionOrderDate"></span></td>
                </tr>
            </table>
            <br/>
            <br/>
        </div>
        <%--Tehsili--%>
        <div id="edu">
            <table align="center" style="width: 750px;
            border-collapse: collapse;">
                <thead>
                <tr>
                    <th colspan="4"
                        style="text-align:left;border-bottom: 1px solid black;border-left: none;border-top:none;border-right: none;">
                        Təhsili
                    </th>
                </tr>
                <tr>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Daxil olub</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Bitirib</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Təhsil müəssisəsinin
                        adı
                    </th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Ixtisası</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="edu in emp.educationHistory">
                    <td style="border: 1px solid black"><span ng-bind="edu.startYear"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="edu.endYear"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="edu.universityName.name"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="edu.profession.profession"></span></td>
                </tr>


                </tbody>

            </table>
            <br/>
            <br/>
        </div>

        <%--hansi xarici dilleri bilir--%>
        <div id="lang">
            <table align="center" style="width: 750px;border-top: none;border-left: none;border-right: none;
            ">
                <tr>
                    <th style="border-bottom: 1px solid black;text-align: left">Hansı xarici dilləri bilir</th>
                </tr>
                <tr>
                    <th style="border-bottom: 1px solid black;text-align: left;background-color:#e7e7c8"><span>{{langArray}}</span>
                    </th>
                </tr>
            </table>
            <br>
            <br>
        </div>


        <%--5.mukafat ve intizam tenbehleri--%>
        <div id="prizeAndPunish">
            <table align="center" style="width: 750px;border-collapse: collapse;
            ">
                <tr>
                    <th colspan="3"
                        style="text-align:left;border-bottom: 1px solid black;border-left: none;border-top:none;border-right: none;">
                        Mükafat və intizam tənbehləri
                    </th>
                </tr>
                <tr style="border: 1px solid black;border-left:none;border-right:none;">
                    <th style="border-bottom:1px solid black;text-align: center;background-color:#e7e7c8">Dövlət
                        təltifləri:{{emp.dioPrizeList.length}}
                    </th>
                    <th style="border-bottom:1px solid black;text-align: center;background-color:#e7e7c8">
                        Mükafatları:{{emp.prize.length}}
                    </th>
                    <th style="border-bottom:1px solid black;text-align: center;background-color:#e7e7c8">İntizam
                        tənbehləri:{{emp.punishlist.length}}
                    </th>
                </tr>
            </table>
            <br/><br/>
        </div>

        <%--5.1dovlet mukafatlari--%>
        <div id="dioPrize">
            <table align="center" style="width: 750px;
            border-collapse: collapse;">
                <tr>
                    <th colspan="3"
                        style="text-align:left;border-bottom: 1px solid black;border-left: none;border-top:none;border-right: none;">
                        Dövlət mükafatları
                    </th>
                </tr>
                <tr>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Orqan və tarixi</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Növü</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Səbəb</th>

                </tr>
                <tr ng-repeat="dioPrize in emp.dioPrizeList">
                    <td style="border: 1px solid black"><span ng-bind="dioPrize.orderOrgan.fullName"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="dioPrize.prizeType.combo.description"></span>
                    </td>
                    <td style="border: 1px solid black"><span ng-bind="dioPrize.reason.name"></span></td>

                </tr>
            </table>
            <br/>
            <br/>
        </div>

        <%--5.2 mukafat ve heveslendirmeleri--%>
        <div id="prize">
            <table align="center" style="width: 750px;
            border-collapse: collapse;">
                <tr>
                    <th colspan="3"
                        style="text-align:left;border-bottom: 1px solid black;border-left: none;border-top:none;border-right: none;">
                        Mükafat və həvəsləndirmələri
                    </th>
                </tr>
                <tr>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Orqan və tarixi</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Növü</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Səbəb</th>

                </tr>
                <tr ng-repeat="prize in emp.prize">
                    <td style="border: 1px solid black"><span ng-bind="prize.orderOrgan.fullName"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="prize.combo.description"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="prize.reason.name"></span></td>

                </tr>
            </table>
            <br/>
            <br/>
        </div>

        <%--5.3 intzam tenbehleri--%>
        <div id="punish">
            <table align="center" style="width: 750px;
            border-collapse: collapse;">
                <tr>
                    <th colspan="4"
                        style="text-align:left;border-bottom: 1px solid black;border-left: none;border-top:none;border-right: none;">
                        İntizam tənbehləri
                    </th>
                </tr>
                <tr>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Orqan və tarixi</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Növü</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Səbəb</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Götürülüb</th>
                </tr>
                <tr ng-repeat="punish in emp.punishlist">
                    <td style="border: 1px solid black"><span ng-bind="punish.orderOrgan.fullName"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="punish.combo.description"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="punish.reason.name"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="punish.punishEndReason.name"></span></td>
                </tr>
            </table>
            <br/>
            <br/>
        </div>
        <%--DIO_dan evvelki fealiyyeti--%>
        <div id="beforeDio">
            <table align="center" style="width: 750px;
            border-collapse: collapse;">
                <tr>
                    <th colspan="4"
                        style="text-align:left;border-bottom: 1px solid black;border-left: none;border-top:none;border-right: none;">
                        DİO-dan əvvəlki fəaliyyəti
                    </th>
                </tr>
                <tr>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Daxil olub</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Çıxıb</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">İş yeri</th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Vəzifəsi</th>
                </tr>
                <tr ng-repeat="beforeDio in emp.workActivityBefore">
                    <td style="border: 1px solid black"><span ng-bind="beforeDio.enterDate"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="beforeDio.exitDate"></span></td>
                    <td style="border: 1px solid black"><span ng-bind=""></span></td>
                    <td style="border: 1px solid black"><span ng-bind="beforeDio.positionName.name"></span></td>
                </tr>
            </table>
            <br/>
            <br/>
        </div>
        <%--6.qohumlari barede melumat--%>
        <div id="relative">
            <table align="center" style="width: 750px;
            border-collapse: collapse;">
                <tr>
                    <th colspan="5"
                        style="text-align:left;border-bottom:none;border-left: none;border-top:none;border-right: none;">
                        Qohumları barədə məlumat
                    </th>
                </tr>
                <tr>
                    <th colspan="5"
                        style="text-align:left;border-bottom: 1px solid black;border-left: none;border-top:none;border-right: none;">
                        {{'(anketin son doldurulma tarixi:'+'dynamic'+'):'}}
                    </th>
                </tr>
                <tr>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Qohumluq dərəcəsi
                    </th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Soyadı, adı,
                        atasının adı
                    </th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Anadan olduğu il və
                        yer
                    </th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">İş yeri və
                        vəzifəsi
                    </th>
                    <th style="border: 1px solid black;text-align: center;background-color:#e7e7c8">Ünvanı</th>
                </tr>
                <tr ng-repeat="family in emp.relatives">
                    <td style="border: 1px solid black"><span ng-bind="family.relativeDegree.description"></span></td>
                    <td style="border: 1px solid black"><span>{{family.person.surName+' '+family.person.name+' '+family.person.patronymic}}</span>
                    </td>
                    <td style="border: 1px solid black"><span>{{family.person.birthDate + ' ' + family.person.bornAddress.adress_fullname}}</span>
                    </td>
                    <td style="border: 1px solid black"><span ng-bind="family.workPlace"></span></td>
                    <td style="border: 1px solid black"><span ng-bind="family.livingAddress.adress_fullname"></span>
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
        </div>


        <%--xidmeti vesiqenin hazirlanmasinda teleb olunan melumatlar--%>
        <div id="card">
            <table align="center" style="width: 750px;border-top: none;border-left: none;border-right: none;
            ">
                <tr>
                    <th style="text-align:left;border-bottom: 2px solid black">Xidməti vəsiqənin hazırlanmasında tələb
                        olunan məlumatlar
                    </th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'Boyu:'+emp.policeCardNew.height}}</th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'Qan qrupu:'+emp.policeCardNew.blood}}</th>
                </tr>
                <tr>
                    <th style="text-align:left">{{'Gözünün rəngi:'+emp.policeCardNew.eyeColor}}</th>
                </tr>
                <tr>
                    <th style="text-align:left;border-bottom: 2px solid black">{{'Çəkisi:'+emp.policeCardNew.weight}}
                    </th>
                </tr>
            </table>
        </div>
        <div id="printEmpName">
            <p style="text-align: left;margin-left:-60px">{{'Çap edən əməkdaş:'+'dynamic'}}</p>
        </div>

    </div>
    <%--/word document--%>
    <!-- /page content -->

    <!-- footer content -->
    <footer>
        <div class="pull-right">
            Admin Template <a href="https://colorlib.com">Colorlib</a>
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
    <script type="text/ng-template" id="SelectTreeModalContent.html">

        <div class="modal-header">

            <h4> İş yerini seçin</h4>

        </div>

        <div class="modal-body">

            <div class="formcontainer">

                <form ng-submit="selectworkPlace()" name="myForm" class="form-horizontal">

                    <div class="table-wrapper-scroll-y">
                        <div class="col-sm-12">
                            <%--<p>Search: <input ng-model="query" ng-change="findNodes()"></p>--%>

                            <div ui-tree id="tree-root">
                                <ol ui-tree-nodes="" ng-model="data">
                                    <li ng-repeat="node in data" ui-tree-node ng-include="'nodes_renderer.html'"
                                        ui-tree-node data-nodrag ng-show="visible(node)"></li>

                                    <%--with drag and drop    <li ng-repeat="node in data" ui-tree-node ng-include="'nodes_renderer.html'"   ng-show="visible(node)"></li>--%>
                                </ol>
                            </div>


                        </div>

                    </div>
                    <div class="row" style="position: absolute; bottom:5px;right: 20px">
                        <div>

                            <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                        </div>
                    </div>

                </form>
            </div>


        </div>

    </script>

    <script type="text/ng-template" id="nodes_renderer.html">

        <div style="word-break: break-all" ui-tree-handle class="tree-node tree-node-content">


            <a class="btn btn-success btn-xs" data-nodrag ng-click="togglee(this)">
                <%--<a   data-nodrag ng-click="toggle(this)">--%>
                <span class="glyphicon"
                      ng-class="{'glyphicon glyphicon-menu-right':  node.yesChild  && collapsed, 'glyphicon glyphicon-menu-down':  node.yesChild  && !collapsed, 'glyphicon glyphicon-minus': ! node.yesChild }">
      </span>
            </a>

            <span style="word-break: break-all" data-nodrag ng-dblclick="selectworkPlace(this)">

         <a href="">  {{node.title}} </a>

             </span>

        </div>

        <ol ui-tree-nodes="" ng-model="node.nodes" ng-class="{hidden: collapsed}">
            <li ng-repeat="node in node.nodes" ui-tree-node ng-include="'nodes_renderer.html'" ng-show="visible(node)">
            </li>
        </ol>

    </script>

    <script type="text/ng-template" id="employeePersonalMatter.html">
        <div class="modal-header">
            <h4>Şəxsi İş</h4>
        </div>

        <div uib-alert ng-show="alert.show" ng-model="alert" ng-class="'alert-' +alert.type" close="closeAlert()">
            {{alert.msg}}
        </div>

        <div class="modal-body">


            <div class="formcontainer">
                <form ng-submit="openPdf(e.id)" name="myForm" class="form-horizontal">

                    <div class="row">
                        <div class="form-group col-md-1">
                            <input id="personCardCheck" type="checkbox" value="" name="check" style="margin-left: 20px">
                        </div>
                        <label class="col-md-11 control-lable" for="name">Şəxsiyyət vəsiqə məlumatları</label>
                    </div>


                    <div class="row">
                        <div class="form-group col-md-1">
                            <input id="dioCheck" type="checkbox" value="" name="check" style="margin-left: 20px">
                        </div>
                        <label class="col-md-11 control-lable" for="name">DİO-da fəaliyyəti</label>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-1">
                            <input id="ezamCheck" type="checkbox" value="" name="check" style="margin-left: 20px">
                        </div>
                        <label class="col-md-11 control-lable" for="name">Ezamiyyət</label>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-1">
                            <input id="eduCheck" type="checkbox" value="" name="check" style="margin-left: 20px">
                        </div>
                        <label class="col-md-11 control-lable" for="name">Təhsili</label>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-1">
                            <input id="langCheck" type="checkbox" value="" name="check" style="margin-left: 20px;">
                        </div>
                        <label class="col-md-11 control-lable" for="name">Hansı xarici dilləri bilir</label>

                    </div>

                    <div class="row">
                        <div class="form-group col-md-1">
                            <input ng-model="prizePunish" id="prizeAndPunishCheck" type="checkbox" value="" name="check"
                                   style="margin-left: 20px;">
                        </div>
                        <label class="col-md-11 control-lable" for="name">Mükafat və intizam tənbehləri</label>

                    </div>
                    <div class="row" style="margin-left: 20px">
                        <div class="form-group col-md-1">
                            <input ng-checked="prizePunish" id="dioPrizeCheck" type="checkbox" value="" name="check"
                                   style="margin-left: 20px">
                        </div>
                        <label class="col-md-11 control-lable" for="name">Dövlət Mükafatları</label>

                    </div>
                    <div class="row" style="margin-left: 20px">
                        <div class="form-group col-md-1">
                            <input ng-checked="prizePunish" id="prizeCheck" type="checkbox" value="" name="check"
                                   style="margin-left: 20px">
                        </div>
                        <label class="col-md-11 control-lable" for="name">Mükafat və həvəsləndirmələri</label>

                    </div>
                    <div class="row" style="margin-left: 20px">
                        <div class="form-group col-md-1">
                            <input ng-checked="prizePunish" id="punishCheck" type="checkbox" value="" name="check"
                                   style="margin-left: 20px">
                        </div>
                        <label class="col-md-11 control-lable" for="name">İntizam tənbehləri</label>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-1">
                            <input id="beforeDioCheck" type="checkbox" value="" name="check" style="margin-left: 20px">
                        </div>
                        <label class="col-md-11 control-lable" for="name">DİO-dan əvvəlki fəaliyyəti</label>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-1">
                            <input id="relativeCheck" type="checkbox" value="" name="check" style="margin-left: 20px">
                        </div>
                        <label class="col-md-11 control-lable" for="name">Qohumları barədə məlumat</label>

                    </div>

                    <div class="row">
                        <div class="form-group col-md-1">
                            <input id="cardCheck" type="checkbox" value="" name="check" style="margin-left: 20px">
                        </div>
                        <label class="col-md-11 control-lable" for="name">Vəsiqə məlumatları</label>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-1">
                            <input id="checkAll" type="checkbox" style="margin-left: 20px"
                                   onclick="for(c in document.getElementsByName('check')) document.getElementsByName('check').item(c).checked = this.checked">
                        </div>
                        <label class="col-md-11 control-lable" for="name">Hamısını seç</label>

                    </div>


                    <br><br>
                    <div class="row" style="position: absolute; bottom:5px;right: 20px">
                        <div>
                            <input type="submit" value="{{'Təsdiqlə'}}"
                                   class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                            <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Imtina Et</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </script>
    <%--word --%>


</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.1/angular.js"></script>
<script src="../../static/resource/js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../../static/resource/js/bootstrap.min.js"></script>


<!-- FastClick -->
<script src="../../static/resource/js/fastclick.js"></script>
<!-- NProgress -->
<script src="../../static/resource/js/nprogress.js"></script>
<!-- iCheck -->
<script src="../../static/resource/js/icheck.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="../../static/resource/js/custom.min.js"></script>


<script src="<c:url value='/static/resource/js/ui-bootstrap-tpls-2.5.0.min.js' />"></script>

<%--<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.js"></script>--%>

<%--<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.0.js"></script>--%>

<script src="<c:url value='/static/angular/dirPagination.js' />"></script>

<%--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.5/angular.min.js"></script>--%>

<%--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.5/angular-route.min.js"></script>--%>

<script src="${pageContext.request.contextPath}/static/angular/Employees/appEmployee.js"></script>

<%--<script src="<c:url value='/static/angular/Employees/employee_controller.js' />"></script>--%>

<script src="${pageContext.request.contextPath}/static/angular/Employees/employee_controller.js"></script>

<script src="<c:url value='/static/angular/Employees/employee_service.js' />"></script>

<script src="<c:url value='/static/angular/Employees/employeeSelectTreeModal.js' />"></script>

<script src="<c:url value='/static/angular/Employees/empCheckModalCtrl.js' />"></script>
<script src="<c:url value='/static/angular/Employees/empCheckWordModalCtrl.js' />"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.2/angular.js"></script>

<script type="text/javascript" src="/static/resource/js/angular-ui-tree.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.54/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.54/vfs_fonts.js"></script>
<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.15/angular-ui-router.js"></script>--%>

<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.15/angular-ui-router.min.js"></script>--%>

</body>


</html>