<%--
  Created by IntelliJ IDEA.
  User: Moonlight
  Date: 28.01.2019
  Time: 12:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> </title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <%--<jsp:include page="../static/jsp/css/RakCss.jsp"/>--%>
    <jsp:include page="../layout/rakCss.jsp"/>

    <link rel="stylesheet" type ="text/css" href ="/static/resource/css/styles.css" />

    <link rel="stylesheet" type ="text/css" href="/static/resource/css/angular-ui-tree.css"/>

    <link rel="stylesheet" href="/static/resource/css/bootstrap-treeview.css">

    <style>
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
        .table-wrapper-scroll-y {
            display: block;
            max-height: 500px;
            overflow-y: auto;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }
    </style>

</head>

<body   class="nav-md" ng-app="appStructure" >

<div   class="container body"  ng-controller="StructureMapController as stCtrl">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="../view/index.jsp" class="site_title"> <span style="float: center">    AKİS</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="../static/images/smilee.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>  wellcome</span>
                        <h2>user</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->
                <!-- Dropdown for selecting language -->

                <br />

                <!-- sidebar menu -->
                <jsp:include page="../layout/sidebarMenu.jsp" />
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <jsp:include page="../layout/menuFooterButtons.jsp" />
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <jsp:include page="../layout/topNavigation.jsp" />
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                    </div>

                </div>

                <div class="clearfix"></div>

                <div class="col-md-8 col-sm-8 col-xs-8">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Struktur xəritəsi</h2>
                            <%--<button type="button"   ng-click="stCtrl.testFunk(1550000000)" class="btn btn-success custom-width"> examplebut</button>--%>

                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        <%--<div class="x_content"  >--%>

                        <div class="table-wrapper-scroll-y" >

                            <div class="clearfix"></div>

                            <table  class="table table-bordered table-striped">

                                <%--<button ng-click="expandAll()">Expand all</button>--%>
                                <%--<button ng-click="collapseAll()">Collapse all</button>--%>
                                <div ui-tree id="tree-root">
                                    <ol ui-tree-nodes="" ng-model="data"   >
                                        <li ng-repeat="node in data" ui-tree-node ng-include="'nodes_renderer.html'" ui-tree-node data-nodrag   ng-show="visible(node)"></li>

                  <%--with drag and drop    <li ng-repeat="node in data" ui-tree-node ng-include="'nodes_renderer.html'"   ng-show="visible(node)"></li>--%>
                                    </ol>
                                </div>
                                     <%--<div id="treeview-checkable" class=""></div>--%>
                            </table>
                            </div>


  </div>
                </div>
            </div>


        <div class="col-md-4 col-sm-4 col-xs-4">

                    <div class="x_panel">


                        <div class="col-sm-12">

                            <div class="x-title">

                                <h2> Axtarış bölməsi</h2>

                            </div>

                            <!-- <form> -->
                            <form   name="myForm"  >

                                <div class="form-group">

                                  <%--<input type="input"   name="input_search" class="form-control" id="input-search" ng-model="query" ng-change="findNodes()" placeholder="Type to search..." value="">--%>
                          <%--<br>--%>
                                    <input type="input"   ng-model="myQuery"   placeholder="Type to search..." value="">
                                </div>


                                <button type="button" class="btn btn-success" id="btn-search"  ng-click="search()">Search</button>

                                <button type="button" class="btn btn-default" id="btn-clear-search"  ng-click="clear()"> Clear</button>

                            </form>
                        </div>

                      </div>
                        <div class="clearfix"></div>

                        <div   class="table-wrapper-scroll-y">
                        <div class="col-sm-12">
                            <%--<p>Search: <input ng-model="query" ng-change="findNodes()"></p>--%>

                                <div ui-tree id="tree-root">

                                    <ol ui-tree-nodes="" ng-model="searchResult"   type="1" >

                                        <li  class="default" ng-repeat="node in searchResult" ui-tree-node ng-include="'nodes_searchResult.html'" ui-tree-node data-nodrag   ng-show="visible(node)"></li>

                                        <%--with drag and drop    <li ng-repeat="node in data" ui-tree-node ng-include="'nodes_renderer.html'"   ng-show="visible(node)"></li>--%>
                                    </ol>

                                </div>


                        </div>

                        </div>
                    </div>
            </div>
    </div>

    </div>
    <!-- /page content -->


    <!-- footer content -->

    <footer>
        <div class="pull-right">
            Admin Template <a href="https://colorlib.com">Colorlib</a>
        </div>
        <div class="clearfix"></div>

    </footer>

    <!-- /footer content -->

<script type="text/ng-template" id="nodes_renderer.html">

    <div   style="word-break: break-all" ui-tree-handle class="tree-node tree-node-content">


        <a class="btn btn-success btn-xs" data-nodrag ng-click="togglee(this)"  >
            <%--<a   data-nodrag ng-click="toggle(this)">--%>
      <span class="glyphicon"
            ng-class="{'glyphicon glyphicon-menu-right':  node.yesChild  && collapsed, 'glyphicon glyphicon-menu-down':  node.yesChild  && !collapsed, 'glyphicon glyphicon-minus': ! node.yesChild }"  >
      </span>
        </a>

        <span  style="word-break: break-all"  data-nodrag ng-dblclick="chooseAction(this)">

         <a href="">  {{node.title}} </a>

        <a class="pull-right btn btn-danger btn-xs" data-nodrag ng-click="removee(this)"><span
                class="glyphicon glyphicon-remove"></span></a>


        <a class="pull-right btn btn-success btn-xs" data-nodrag ng-click="addElement(this)" style="margin-right: 8px;"><span
                class="glyphicon glyphicon-plus"></span></a>


        <a class="pull-right btn btn-success btn-xs" data-nodrag ng-click="updateElement(this)" style="margin-right: 8px;"><span
                class="glyphicon glyphicon-pencil"></span></a>
</span>
    </div>



    <ol ui-tree-nodes="" ng-model="node.nodes" ng-class="{hidden: collapsed}">
        <li ng-repeat="node in node.nodes" ui-tree-node ng-include="'nodes_renderer.html'" ng-show="visible(node)">
        </li>
    </ol>

</script>

<script type="text/ng-template" id="nodes_searchResult.html">

    <div   style="word-break: break-all" ui-tree-handle class="tree-node tree-node-content">
<br>

        <a  data-nodrag ng-click="detailedSearch(this)" style="margin-right: 8px;">

         <span  >
            {{node.title}} </span>

            </a>

    </div>

    <ol ui-tree-nodes="" ng-model="node.nodes" ng-class="{hidden: collapsed}">
        <li ng-repeat="node in node.nodes" ui-tree-node ng-include="'nodes_searchResult.html'" ng-show="visible(node)">
        </li>
    </ol>

</script>

<script type="text/ng-template" id="updateModalContent.html"   >

    <div class="modal-header">

        <h4>Seçilmiş elementin yenilənməsi</h4>

    </div>

    <div class="modal-body">

        <div class="formcontainer">

            <form ng-submit="submit()" name="myForm" class="form-horizontal">

                <div class="row">

                    <div class="form-group col-md-12">

                        <label class="col-md-4 control-lable" for="uName"> Strukturun adı</label>

                        <div class="col-md-7">

                            <input type="text" ng-model="element.title"   name="name" id="uName" class="field form-control input-sm" placeholder="Struktur adını daxil edin"/>

                             <div class="has-error" ng-show="myForm.$dirty">

                                <span ng-show="myForm.uName.$error.required">Strukturun adını daxil edin</span>
                                <%--  <span ng-show="myForm.name.$error.minlength">Başlığı 7 sinvoldan ibarət olmaldi</span>  --%>
                                <span ng-show="myForm.uName.$invalid">Daxil edilən məlumatlar düzgün deyil </span>

                              </div>

                        </div>

                   </div>

                 </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable" for="uType">Strukturun növü</label>
                        <div class="col-md-7">
                            <select name="organType"  id="uType" ng-model="organType"
                                    ng-options="organType.name for organType in OrganTypes track by organType.id" class="field form-control input-sm"    >

                          <option   value=""> {{organType.name}} </option>
                            </select>
                            <div class="has-error" ng-show="myForm.$dirty">
                                <%--  <span ng-show="myForm.$error.required">kategoryani daxil edin</span>
                                  <span ng-show="myForm.technicalFailure.id.$invalid">kategorya düzgün daxil edilməyib </span> --%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="form-group col-md-12">

                        <label class="col-md-4 control-lable" for="UShortName"> Qısa adı</label>

                        <div class="col-md-7">

                            <input type="text" ng-model="element.text"   name="name" id="UShortName" class="field form-control input-sm" placeholder="Qısa adı daxil edin"/>

                            <div class="has-error" ng-show="myForm.$dirty">

                                <span ng-show="myForm.UShortName.$error.required">Qısa adı daxil edin</span>
                                <%--  <span ng-show="myForm.name.$error.minlength">Başlığı 7 sinvoldan ibarət olmaldi</span>  --%>
                                <span ng-show="myForm.UShortName.$invalid">Daxil edilən məlumatlar düzgün deyil </span>

                            </div>

                        </div>

                    </div>

                </div>
                <div class="row">

                    <div class="form-group col-md-12">

                        <label class="col-md-4 control-lable" for="UupperSt"> Üstqurum sayı</label>

                        <div class="col-md-7">

                            <input type="number" ng-model="element.upperStr"   name="name" id="UupperSt" class="field form-control input-sm" placeholder=" Üstqurum sayını daxil edin"/>

                            <div class="has-error" ng-show="myForm.$dirty">

                                <span ng-show="myForm.UupperSt.$error.required"> Üstqurum adını daxil edin</span>
                                <%--  <span ng-show="myForm.name.$error.minlength">Başlığı 7 sinvoldan ibarət olmaldi</span>  --%>
                                <span ng-show="myForm.UupperSt.$invalid">Daxil edilən məlumatlar düzgün deyil </span>

                            </div>

                        </div>

                    </div>

                </div>
                <div class="row">

                    <div class="form-group col-md-12">

                        <label class="col-md-4 control-lable" for="uSuffix"> Şəkilçi</label>

                        <div class="col-md-7">

                            <input type="text" ng-model="element.suffix"   name="name" id="uSuffix" class="field form-control input-sm" placeholder="Şəkilçini daxil edin"/>

                            <div class="has-error" ng-show="myForm.$dirty">

                                <span ng-show="myForm.uSuffix.$error.required">Şəkilçini daxil edin</span>
                                <%--  <span ng-show="myForm.name.$error.minlength">Başlığı 7 sinvoldan ibarət olmaldi</span>  --%>
                                <span ng-show="myForm.uSuffix.$invalid">Daxil edilən məlumatlar düzgün deyil </span>

                            </div>

                        </div>

                    </div>

                </div>
                <br>
                 <div class="row">
                       <div class="form-group col-md-12">
                          <label class="col-md-4 control-lable" for="uname"></label>
                         </div>
                  </div>
                <div class="row" style="position: absolute; bottom:5px;right: 20px">
                    <div>
                        <input type="submit"  value="Yadda saxla" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/ng-template" id="doubleClick.html"   >

    <div  >

        <h4>Əməliyyat seçin</h4>

    </div>

    <div  >

        <div class="formcontainer">

            <form ng-submit="submit()" name="myForm"  >
                <div>


                    <a>  <span> Əlavə et  </span></a>
                        <a><span>Dəyiş</span></a>
                        <a><span>Sil</span></a>


                </div>

                <div class="row" style="position: absolute; bottom:5px;right: 20px">
                    <div>
                        <input type="submit"  value="Təsdiqlə" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/ng-template" id="addModalContent.html"   >

    <div class="modal-header">

        <h4>Yeni qurumun yaradılması</h4>

    </div>

    <div class="modal-body">

        <div class="formcontainer">

            <form ng-submit="submit()" name="myForm" class="form-horizontal">

                <div class="row">

                    <div class="form-group col-md-12">

                        <label class="col-md-4 control-lable" for="uName"> Strukturun adı</label>

                        <div class="col-md-7">

                            <input type="text" ng-model="element.title"   name="name" id="aName" class="field form-control input-sm" placeholder="Struktur adını daxil edin"  required />

                            <div class="has-error" ng-show="myForm.$dirty">

                                <span ng-show="myForm.uName.$error.required">Strukturun adını daxil edin</span>
                                <%--  <span ng-show="myForm.name.$error.minlength">Başlığı 7 sinvoldan ibarət olmaldi</span>  --%>
                                <span ng-show="myForm.uName.$invalid">Daxil edilən məlumatlar düzgün deyil </span>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable" for="aType">Strukturun növü</label>
                        <div class="col-md-7">
                            <select name="organType"  id="aType" ng-model="organType"
                                    ng-options="organType.name for organType in OrganTypes track by organType.id" class="field form-control input-sm"  required  >

                                <option   value=""> Seçim et </option>
                            </select>
                            <div class="has-error" ng-show="myForm.$dirty">
                                <%--  <span ng-show="myForm.$error.required">kategoryani daxil edin</span>
                                  <span ng-show="myForm.technicalFailure.id.$invalid">kategorya düzgün daxil edilməyib </span> --%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="form-group col-md-12">

                        <label class="col-md-4 control-lable" for="aShortName"> Qısa adı</label>

                        <div class="col-md-7">

                            <input type="text" ng-model="element.text"   name="aShortName" id="aShortName" class="field form-control input-sm" placeholder="Qısa adı daxil edin"/>

                            <div class="has-error" ng-show="myForm.$dirty">

                                <span ng-show="myForm.aShortName.$error.required">Qısa adı daxil edin</span>
                                <%--  <span ng-show="myForm.name.$error.minlength">Başlığı 7 sinvoldan ibarət olmaldi</span>  --%>
                                <span ng-show="myForm.aShortName.$invalid">Daxil edilən məlumatlar düzgün deyil </span>

                            </div>

                        </div>

                    </div>

                </div>
                <div class="row">

                    <div class="form-group col-md-12">

                        <label class="col-md-4 control-lable" for="AupperSt"> Üstqurum sayı</label>

                        <div class="col-md-7">

                            <input type="number" ng-model="element.upperStr"   name="AupperSt" id="AupperSt" class="field form-control input-sm" placeholder=" Üstqurum adını daxil edin"/>

                            <div class="has-error" ng-show="myForm.$dirty">

                                <span ng-show="myForm.AupperSt.$error.required"> Üstqurum adını daxil edin</span>
                                <%--  <span ng-show="myForm.name.$error.minlength">Başlığı 7 sinvoldan ibarət olmaldi</span>  --%>
                                <span ng-show="myForm.AupperSt.$invalid">Daxil edilən məlumatlar düzgün deyil </span>

                            </div>

                        </div>

                    </div>

                </div>
                <div class="row">

                    <div class="form-group col-md-12">

                        <label class="col-md-4 control-lable" for="aSuffix"> Şəkilçi</label>

                        <div class="col-md-7">

                            <input type="text" ng-model="element.suffix"   name="aSuffix" id="aSuffix" class="field form-control input-sm" placeholder="Şəkilçini daxil edin"/>

                            <div class="has-error" ng-show="myForm.$dirty">

                                <span ng-show="myForm.aSuffix.$error.required">Şəkilçini daxil edin</span>
                                <%--  <span ng-show="myForm.name.$error.minlength">Başlığı 7 sinvoldan ibarət olmaldi</span>  --%>
                                <span ng-show="myForm.aSuffix.$invalid">Daxil edilən məlumatlar düzgün deyil </span>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-md-12">

                        <label class="col-md-4 control-lable" for="aname"></label>

                    </div>

                </div>

                <br>

                               <div class="row" style="position: absolute; bottom:5px;right: 20px">

                    <div>

                        <input type="submit"  value="Əlavə et" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">

                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>

                    </div>

                </div>

            </form>

        </div>

    </div>

</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.1/angular.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular-route.min.js"></script>

    <%--<script src="https://cdn3.devexpress.com/jslib/18.2.5/js/dx.all.js"></script>--%>

    <script src="/static/angular/StructureMap/appStructure.js"></script>

    <script src="/static/angular/StructureMap/StructureMapservice.js"></script>

    <script src="/static/angular/StructureMap/StructureMapController.js"></script>

    <script src="/static/angular/StructureMap/StModalUpdate.js"></script>

    <script src="/static/angular/StructureMap/StModalAdd.js"></script>

    <script src="/static/angular/StructureMap/chooseActionModal.js"></script>


    <script src="/static/angular/StructureMap/tree.js"></script>


    <script src="../static/resource/js/jquery.min.js"></script>


    <script type="text/javascript" src="/static/resource/js/angular-ui-tree.js"></script>

    <!-- Bootstrap -->
    <script src="../static/resource/js/bootstrap.min.js"></script>


    <script src="/static/angular/bootstrap-treeview.js"></script>

    <!-- FastClick -->
    <script src="../static/resource/js/fastclick.js"></script>

    <!-- NProgress -->
    <script src="../static/resource/js/nprogress.js"></script>

    <!-- iCheck -->
    <script src="../static/resource/js/icheck.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../static/resource/js/custom.min.js"></script>


    <script src="<c:url value='/static/resource/js/ui-bootstrap-tpls-2.5.0.min.js' />"></script>

</body>

</html>