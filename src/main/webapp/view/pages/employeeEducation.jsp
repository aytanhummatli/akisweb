<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Təhsil və attestasiya </title>

    <jsp:include page="../layout/rakCss.jsp"/>

    <!-- bootstrap-wysiwyg -->
    <link href="../static/resource/css/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../static/resource/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../static/resource/css/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="../static/resource/css/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../static/resource/css/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../static/resource/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">

    <%--datepicker css--%>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>

<body class="nav-md" ng-app="appEmployeeEducation">
<div class="container body"  ng-controller="employeeEducationController as empCtrl">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <br>



                <!-- sidebar menu -->

                <jsp:include page="../layout/sidebarMenuForEmployeeDetails.jsp"/>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>

                <!-- /menu footer buttons -->

            </div>

        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>

                    <div class="nav toggle">

                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>

                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="images/img.jpg" alt="">John Doe
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;"> Profile</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li><a href="javascript:;">Help</a></li>
                                <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">

                <div class="page-title">

                </div>

                <div class="clearfix"></div>


                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Bildiyi dillər və elmi dərəcəsi

                                </h2>

                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form id="demo-form6" data-parsley-validate class="form-horizontal form-label-left">
                                    <%--  Soyad, şəxsi iş, şəxsi işi tamdır   --%>
                                    <div class="row">


                                        <div class="col-md-6 col-sm-6 col-xs-6">

                                            <div class="panel panel-default">

                                                <div class="panel-body">
                                                    <div class="col-md-12">
                                                        <div class="x_content">
                                                            Bildiyi dillər
                                                            <table id="contdact" class="table table-hover"
                                                                   style="font-size: small">
                                                                <thead>
                                                                <tr class="headings">
                                                                    <th class="column-title"><label> Dil </label></th>

                                                                    <th class="column-title"><label>Bilmə
                                                                        səviyyəsi</label></th>

                                                                    <th class="column-title">

                                                                        <button style="float: right" id="addButton"
                                                                                type="button" ng-click="rankCtrl.add(0)"
                                                                                class="btn btn-success  "> Əlavə et
                                                                        </button>
                                                                    </th>
                                                                </tr>

                                                                </thead>

                                                                <tbody>

                                                                <tr  ng-repeat="inf in employee.employeeLanguageLevel" >

                                                                    <td class=" "><span ng-bind="inf.language.description"></span></td>
                                                                    <td class=" "><span ng-bind="inf.languageLevel.description"></span></td>

                                                                    <td class="pull-right">
                                                                        <button type="button"
                                                                                ng-click="rankCtrl.edit(r.id)"
                                                                                class="btn btn-success custom-width">
                                                                            Redaktə
                                                                        </button>
                                                                        <%--<button type="button" ng-click="rankCtrl.remove(r.id)" class="btn btn-danger custom-width">Sil</button>--%>
                                                                        <button type="button"
                                                                                ng-click="removeLanguageLevel(inf.id)"
                                                                                class="btn btn-danger custom-width">Sil
                                                                        </button>

                                                                    </td>

                                                                </tr>


                                                                </tbody>

                                                            </table>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="panel panel-default">

                                                <div class="panel-body">

                                                    <div class="form-group ">

                                                        <label class="control-label col-md-3 col-sm-3 col-xs-3"

                                                               for="academicDegree"> Elmi dərəcə </label>

                                                        <div class="col-md-9 col-sm-9 col-xs-9">

                                                            <select name="academicDegree" id="academicDegree" ng-model="academicDegree"

                                                               ng-options="academicDegree.description for academicDegree in academicDegreeList track by academicDegree.id"

                                                                    class="field form-control input-sm" required>

                                                            </select>

                                                        </div>

                                                    </div>

                                                    <div class="form-group">

                                                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Alınma tarixi</label>

                                                        <div class="col-md-9 col-sm-9 col-xs-12">

                                                            <%--<input  ng-model="employee.academicDegreePosition.getDate   " class="form-control"--%>

                                                                                           <%--placeholder="Başlama tarixi">--%>
                                                                <div class='input-group date' id='datetimepicker1'>
                                                                    <input type='text' class="form-control" ng-model="employee.academicDegreePosition.getDate" />
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                                                </div>

                                                        </div>

                                                    </div>

                                                    <button style="float: right" id="addButton" type="button"  ng-click="rankCtrl.add(0)" class="btn btn-success  "> Yeni</button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            </div>

                            </form>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Təhsil tarixçəsi

                                </h2>

                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form id="demo-form6" data-parsley-validate class="form-horizontal form-label-left">
                                    <%--  Soyad, şəxsi iş, şəxsi işi tamdır   --%>
                                    <div class="row">

                                            <div class="panel panel-default">

                                                <div class="panel-body">
                                                    <div class="col-md-12">
                                                        <div class="x_content">

                                                            <table id="contdact" class="table table-hover"
                                                                   style="font-size: small">
                                                                <thead>
                                                                <tr class="headings">
                                                                    <th class="column-title"><label> Təhsil növü </label></th>
                                                                    <th class="column-title"><label> Dərəcəsi </label></th>
                                                                    <th class="column-title"><label> Təhsil aldığı qurum </label></th>
                                                                    <th class="column-title"><label> İxtisası </label></th>
                                                                    <th class="column-title"><label> Daxil olduğu il </label></th>
                                                                    <th class="column-title"><label> Bitirdiyi il </label></th>
                                                                    <th class="column-title"><label> Təhsil sənədi </label></th>
                                                                    <th class="column-title"><label> Verilmə tarixi </label></th>
                                                                    <th class="column-title"><label> Seriya nömrəsi </label></th>
                                                                    <th class="column-title"><label> Təhsili dava </label></th>
                                                                    <th class="column-title"><label> Təhsil şöbəsi </label></th>

                                                                    <th  class="column-title">

                                                                        <a class="  btn btn-default btn-xs"
                                                                           ng-click="collectData(1)"><span
                                                                                class="glyphicon glyphicon-plus">  </span></a>
                                                                    </th>
                                                                </tr>

                                                                </thead>

                                                                <tbody>

                                                                <tr  ng-repeat="inf in employee.educationHistory" >

                                                                    <td class=" "><span ng-bind="inf.educationType.description"></span></td>
                                                                    <td class=" "><span ng-bind="inf.educationLevel.description"></span></td>
                                                                    <td class=" "><span ng-bind="inf.universityName.name"></span></td>
                                                                    <td class=" "><span ng-bind="inf.profession.profession"></span></td>

                                                                    <td class=" "><span ng-bind="inf.startYear | date:'dd.mm.yyyy'"></span></td>
                                                                    <td class=" "><span ng-bind="inf.endYear | date:'dd.mm.yyyy'"></span></td>
                                                                    <td class=" "><span ng-bind="inf.certificateType.description"></span></td>
                                                                    <td class=" "><span ng-bind="inf.certificateGetDate | date:'dd.mm.yyyy' "></span></td>

                                                                    <td class=" "><span ng-bind="inf.sertificateNo"></span></td>
                                                                    <td class=" "><span ng-bind="inf.status"></span></td>
                                                                    <td class=" "><span ng-bind="inf.eyaniQiyabi.description"></span></td>

                                                                    <td  class="pull-right">
                                                                        <button type="button"
                                                                                ng-click="rankCtrl.edit(r.id)"
                                                                                class="btn btn-success btn-xs">
                                                                         <span
                                                                                 class="glyphicon glyphicon-edit">  </span>
                                                                        </button>
                                                                        <%--<button type="button" ng-click="rankCtrl.remove(r.id)" class="btn btn-danger custom-width">Sil</button>--%>
                                                                        <button type="button"
                                                                                ng-click="removeEducationHistory(inf.id)"
                                                                                class="btn btn-danger btn-xs"> <span
                                                                                class="glyphicon glyphicon-remove">  </span>
                                                                        </button>
                                                                    </td>


                                                                </tbody>

                                                                <a class="  btn btn-default btn-xs"

                                                                   ng-click="collectData(1)"> Əsas təhsil</a>

                                                            </table>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                    </div>

                            </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Attestasiya nəticələri

                                </h2>

                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form id="demo-form6" data-parsley-validate class="form-horizontal form-label-left">
                                    <%--  Soyad, şəxsi iş, şəxsi işi tamdır   --%>
                                    <div class="row">

                                        <div class="panel panel-default">

                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    <div class="x_content">

                                                        <table id="conhtdact" class="table table-hover"
                                                               style="font-size: small">
                                                            <thead>
                                                            <tr class="headings">
                                                                <th class="column-title"><label> Keçirilmə tarixi </label></th>
                                                                <th class="column-title"><label> Attestasiyanın nəticəsi </label></th>

                                                                <th class="column-title">

                                                                    <a class="  btn btn-default btn-xs"
                                                                       ng-click="collectData(1)"><span
                                                                            class="glyphicon glyphicon-plus">  </span></a>
                                                                </th>
                                                            </tr>

                                                            </thead>

                                                            <tbody>

                                                            <tr  ng-repeat="inf in employee.attestationHistory" >

                                                                <td class=" "><span ng-bind="inf.atteatationDate"></span></td>

                                                                <td class=" "><span ng-bind="inf.result.description"></span></td>

                                                                <td class="pull-right">

                                                                    <a class="  btn btn-default btn-xs"

                                                                       ng-click="collectData(1)"><span

                                                                            class="glyphicon glyphicon-edit">  </span></a>

                                                                    <a class="  btn btn-default btn-xs"

                                                                       ng-click="removeAttesstationHistory(inf.id)"><span

                                                                            class="glyphicon glyphicon-remove">  </span></a>

                                                                </td>

                                                            </tr>

                                                            </tbody>

                                                        </table>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                    </div>

                            </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- /page content -->

    <!-- footer content -->

    <footer>
        <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
        </div>
        <div class="clearfix"></div>
    </footer>

    <!-- /footer content -->

</div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.1/angular.js"></script>

<script src="<c:url value='/static/resource/js/ui-bootstrap-tpls-2.5.0.min.js' />"></script>

<script src="<c:url value='/static/angular/Employees/EmployeeEducation/appEmployeeEducation.js' />"></script>

<script src="/static/angular/Employees/EmployeeEducation/employeeEducationController.js"></script>

<script src="<c:url value='/static/angular/Employees/EmployeeEducation/employeeEducationService.js ' />"></script>


<script src="../static/resource/js/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="../static/resource/js/bootstrap.min.js"></script>

<!-- FastClick -->
<script src="../static/resource/js/fastclick.js"></script>

<!-- NProgress -->
<script src="../static/resource/js/nprogress.js"></script>

<!-- iCheck -->
<script src="../static/resource/js/icheck.min.js"></script>

<script src="../static/resource/js/bootstrap-progressbar.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../static/resource/js/moment.min.js"></script>
<script src="../static/resource/js/daterangepicker.js"></script>

<!-- bootstrap-wysiwyg -->
<script src="../static/resource/js/bootstrap-wysiwyg.min.js"></script>
<script src="../static/resource/js/jquery.hotkeys.js"></script>
<script src="../static/resource/js/prettify.js"></script>

<!-- jQuery Tags Input -->
<script src="../static/resource/js/jquery.tagsinput.js"></script>

<!-- Switchery -->
<script src="../static/resource/js/switchery.min.js"></script>

<!-- Select2 -->
<script src="../static/resource/js/select2.full.min.js"></script>

<!-- Parsley -->
<script src="../static/resource/js/parsley.min.js"></script>

<!-- Autosize -->
<script src="../static/resource/js/autosize.min.js"></script>

<!-- jQuery autocomplete -->
<script src="../static/resource/js/jquery.autocomplete.min.js"></script>

<!-- starrr -->
<script src="../static/resource/js/starrr.js"></script>

<!-- Custom Theme Scripts -->
<script src="../static/resource/js/custom.min.js"></script>

<%--datepicker js--%>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>
</body>

</html>
