<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ümumi məlumatlar </title>

    <jsp:include page="../layout/rakCss.jsp"/>

    <!-- bootstrap-wysiwyg -->
    <link href="../../static/resource/css/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../../static/resource/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../../static/resource/css/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="../../static/resource/css/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../../static/resource/css/daterangepicker.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <!-- Custom Theme Style -->
    <link href="../../static/resource/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">

    <link href="../../static/resource/css/daterangepicker.css" rel="stylesheet">
<%--datepicker css--%>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">


    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.1/angular.js"></script>

    <script src="<c:url value='/static/resource/js/ui-bootstrap-tpls-2.5.0.min.js' />"></script>

    <script src="<c:url value='/static/angular/Employees/EmployeeGeneral/appEmployeeGeneral.js' />"></script>

    <script src="/static/angular/Employees/EmployeeGeneral/employeeGeneralController.js"></script>

    <script src="<c:url value='/static/angular/Employees/EmployeeGeneral/employeeGeneralService.js ' />"></script>

    <script src="<c:url value='/static/angular/Employees/EmployeeGeneral/modals/modalEmployeeRank.js'  />"></script>

    <script src="<c:url value='/static/angular/Employees/EmployeeGeneral/modals/modalServiceDegree.js'  />"></script>

</head>

<body class="nav-md" ng-app="appEmployeeGeneral">

<div class="container body" ng-controller="employeeGeneralController as empCtrl">

    <div class="main_container">

        <div class="col-md-3 left_col">

            <div class="left_col scroll-view">

                <br>
                <!-- sidebar menu -->
                <jsp:include page="../layout/sidebarMenuForEmployeeDetails.jsp"/>

                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <div class="sidebar-footer hidden-small">

                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>

                </div>

                <!-- /menu footer buttons -->

            </div>

        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">

                <nav>

                    <div class="nav toggle">

                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>

                    </div>

                    <ul class="nav navbar-nav navbar-right">

                        <li class="">

                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="images/img.jpg" alt="">John Doe
                                <span class=" fa fa-angle-down"></span>
                            </a>

                            <ul class="dropdown-menu dropdown-usermenu pull-right">

                                <li><a href="javascript:;"> Profile</a></li>

                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>

                                <li><a href="javascript:;">Help</a></li>

                                <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>

                            </ul>

                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">

                <div class="page-title">

                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">

                                <h2> Rütbə məlumatları </h2>

                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form id="employeeGeneral">
                                    <%--  Soyad, şəxsi iş, şəxsi işi tamdır   --%>
                                    <div class="row">

                                        <div class="panel panel-default">

                                            <div class="panel-body">


                                                <div class="col-md-12">

                                                    <div class="x_content">

                                                        <table id="conhtdact" class="table table-hover"

                                                               style="font-size: small">

                                                            <thead>

                                                            <tr class="headings">

                                                                <th class="column-title"><label> Aldığı tarix </label>
                                                                </th>

                                                                <th class="column-title"><label>Rütbə növü</label></th>

                                                                <th class="column-title"><label> Rütbəsi </label></th>

                                                                <th class="column-title"><label>Əmr nömrəsi</label></th>

                                                                <th class="column-title">

                                                                    <button style="float: right" id="addButton"
                                                                            type="button" ng-click="addEmployeeRank()"
                                                                            class="btn btn-success  "> Əlavə et
                                                                    </button>

                                                                </th>

                                                            </tr>

                                                            </thead>

                                                            <tbody>

                                                            <tr ng-repeat="inf in employee.employeeRank">

                                                                <td class=" "><span ng-bind="inf.createDate"></span>
                                                                </td>
                                                                <td class=" "><span
                                                                        ng-bind="inf.cmbsService.description"></span>
                                                                </td>
                                                                <td class=" "><span ng-bind="inf.rank.name"></span></td>
                                                                <td class=" "><span ng-bind="inf.orderNo"></span></td>

                                                                <td class="pull-right">

                                                                    <button type="button"
                                                                            ng-click="updateEmployeeRank(this)"
                                                                            class="btn btn-success custom-width">Redaktə
                                                                    </button>

                                                                    <%--<button type="button" ng-click="rankCtrl.remove(r.id)" class="btn btn-danger custom-width">Sil</button>--%>

                                                                    <button type="button" ng-click="removeRank(inf.id)"
                                                                            class="btn btn-danger custom-width">Sil
                                                                    </button>

                                                                </td>

                                                            </tr>


                                                            </tbody>

                                                        </table>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>


                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2> Xidməti dərəcələr

                                </h2>

                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form id="form1">
                                    <%--  Soyad, şəxsi iş, şəxsi işi tamdır   --%>
                                    <div class="row">


                                        <div class="panel panel-default">

                                            <div class="panel-body">


                                                <div class="col-md-12">
                                                    <div class="x_content">

                                                        <table id="contdact" class="table table-hover"
                                                               style="font-size: small">
                                                            <thead>
                                                            <tr class="headings">
                                                                <th class="column-title"><label> Aldığı tarix </label>
                                                                </th>

                                                                <th class="column-title"><label>Məharət dərəcəsi</label>
                                                                </th>

                                                                <th class="column-title"><label>Əmr nömrəsi</label></th>

                                                                <th>
                                                                    <button style="float: right" id="adddButton"
                                                                            type="button" ng-click="addServiceDegree()"
                                                                            class="btn btn-success  "> Əlavə et
                                                                    </button>
                                                                </th>
                                                            </tr>

                                                            </thead>

                                                            <tbody>

                                                            <tr ng-repeat="inf in employee.successDegreeHistory">

                                                                <td class=" "><span ng-bind="inf.createDate"></span>
                                                                </td>
                                                                <td class=" "><span
                                                                        ng-bind="inf.successDegree.name"></span></td>
                                                                <td class=" "><span ng-bind="inf.orderNo"></span></td>
                                                                <td class="pull-right">
                                                                    <button type="button"
                                                                            ng-click="updateServiceDegree(this)"
                                                                            class="btn btn-success custom-width">Redaktə
                                                                    </button>
                                                                    <%--<button type="button" ng-click="rankCtrl.remove(r.id)" class="btn btn-danger custom-width">Sil</button>--%>
                                                                    <button type="button"
                                                                            ng-click="removeServiceDegree(inf.id)"
                                                                            class="btn btn-danger custom-width">Sil
                                                                    </button>
                                                                </td>

                                                            </tr>

                                                            </tbody>

                                                        </table>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Hərbi xidmət
                                </h2>
                                <ul class="nav navbar-right panel_toolbox">

                                    <li>
                                        <a ng-click="militaryServieUpdateOrInsert()" >Təsdiqlə</a>
                                    </li>

                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>

                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br/>
                                <form id="demo-form3">

                                    <div class="col-md-6 col-sm-6 col-xs-6">

                                        <div class="row">
                                            <div class="form-group ">

                                                <label class="control-label col-md-3 col-sm-3 col-xs-3"
                                                       for="militaryDuty"> Hərbi xidmət</label>

                                                <div class="col-md-9 col-sm-9 col-xs-9">

                                                    <select ng-change="setSwitch()" name="militaryDuty"

                                                            id="militaryDuty" ng-model="militaryDuty"

                                                            ng-options="militaryDuty.description for militaryDuty in militaryDutyList track by militaryDuty.id"

                                                            class="field form-control input-sm" required>

                                                        <%--<option  value="">Secim et</option>--%>

                                                        <%--<option ng-bind="militaryDuty.description" value=""></option>--%>

                                                    </select>

                                                </div>

                                            </div>

                                        </div>

                                        <br>

                                        <div class="row">

                                            <div class="form-group">

                                                <label class="control-label col-md-3 col-sm-3 col-xs-3">Başlama
                                                    tarixi</label>

                                                <div class="col-md-9 col-sm-9 col-xs-12">


                                                    <%--<input ng-disabled="date" id="militaryBeginDate"--%>
                                                           <%--ng-model="employeeMilitaryService.beginDate"--%>
                                                           <%--class="form-control" placeholder="Başlama tarixi">--%>
                                                        <div class='input-group date' id='datetimepicker1'>
                                                            <input type='text' class="form-control" ng-model="employeeMilitaryService.beginDate" />
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        </div>


                                                </div>

                                            </div>

                                        </div>

                                        <br>

                                        <div class="row">

                                            <div class="form-group">

                                                <label class="control-label col-md-3 col-sm-3 col-xs-3">Bitirmə
                                                    tarixi</label>

                                                <div class="col-md-9 col-sm-9 col-xs-12">

                                                    <%--<input class="form-control"--%>
                                                           <%--ng-disabled="date"--%>
                                                           <%--ng-model=" employeeMilitaryService.endDate"--%>
                                                           <%--placeholder="Bitirmə tarixi">--%>
                                                        <div class='input-group date' id='datetimepicker1'>
                                                            <input type='text' class="form-control" ng-model="employeeMilitaryService.endDate" />
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        </div>

                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="row">
                                            <div class="form-group">

                                                <label class="control-label col-md-3 col-sm-3 col-xs-3" for="seriya">
                                                    H/biletin seriyası
                                                    seriyası </label>

                                                <div class="col-md-8">

                                                    <input ng-disabled="serialNo" id="seriya" type="text"
                                                           class="form-control"

                                                           ng-model=" employeeMilitaryService.serialNo"

                                                           placeholder="H/biletin seriyası">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group">

                                                <label class="control-label col-md-3 col-sm-3 col-xs-3"
                                                       for="herbiHisse"> Hərbi hissə

                                                </label>

                                                <div class="col-md-8">

                                                    <input ng-disabled="armyPort" id="herbiHisse" type="text"
                                                           class="form-control"
                                                           ng-model=" employeeMilitaryService.armyPort"
                                                           placeholder="Hərbi hissə ">

                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="form-group">

                                                <div class="checkbox">

                                                    <label style="margin-left:30%">

                                                        <input ng-disabled="reg" ng-model="registration"
                                                               type="checkbox" name="DİN" value="">DİN-in xüsusi hərbi
                                                        qeydiyyatında

                                                    </label>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!-- /page content -->

        <!-- footer content -->

        <footer>

            <div class="pull-right">

                Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>

            </div>

            <div class="clearfix"></div>

        </footer>

        <!-- /footer content -->

    </div>


    <%--<script type="text/javascript">--%>

    <%--// Get your checkbox who determine the condition--%>

    <%--var determine = document.getElementById("militaryDuty");--%>

    <%--//  Make a function who disabled or enabled your conditioned checkbox--%>

    <%--//  militaryBeginDate--%>

    <%--//  var a = angular.element(document.getElementById('militaryBeginDate'));--%>

    <%--// var   a = angular.element(document.getElementById('militaryBeginDate')).val();--%>

    <%--var a= $("#militaryBeginDate").value;--%>

    <%--var disableCheckboxConditioned = function () {--%>

    <%--console.log('x= ',a);--%>

    <%--// if( a === 3027) {--%>
    <%--//--%>
    <%--//     document.getElementById("employeeMilitaryService.beginDate").disabled = true;--%>
    <%--// }--%>

    <%--}--%>

    <%--determine.onchange = disableCheckboxConditioned;--%>

    <%--disableCheckboxConditioned();--%>

    <%--</script>--%>


</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>


<jsp:include page="modals/add/addEmployeeRank.jsp"></jsp:include>


<jsp:include page="modals/update/updateEmployeeRank.jsp"></jsp:include>


<jsp:include page="modals/add/addServiceDegree.jsp"></jsp:include>


<jsp:include page="modals/update/updateServiceDegree.jsp"></jsp:include>

<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.1/angular.js"></script>--%>

<%--<script src="<c:url value='/static/resource/js/ui-bootstrap-tpls-2.5.0.min.js' />"></script>--%>

<%--<script src="<c:url value='/static/angular/Employees/EmployeeGeneral/appEmployeeGeneral.js' />"></script>--%>

<%--<script src="/static/angular/Employees/EmployeeGeneral/employeeGeneralController.js"></script>--%>

<%--<script src="<c:url value='/static/angular/Employees/EmployeeGeneral/employeeGeneralService.js ' />"></script>--%>

<%--<script src="<c:url value='/static/angular/Employees/EmployeeGeneral/modals/modalEmployeeRank.js'  />"></script>--%>

<%--<script src="<c:url value='/static/angular/Employees/EmployeeGeneral/modals/modalServiceDegree.js'  />"></script>--%>

<script src="../../static/resource/js/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="../../static/resource/js/bootstrap.min.js"></script>

<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>


<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


<!-- FastClick -->
<script src="../../static/resource/js/fastclick.js"></script>

<!-- NProgress -->
<script src="../../static/resource/js/nprogress.js"></script>

<!-- iCheck -->
<script src="../../static/resource/js/icheck.min.js"></script>

<script src="../../static/resource/js/bootstrap-progressbar.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../../static/resource/js/moment.min.js"></script>
<script src="../../static/resource/js/daterangepicker.js"></script>

<!-- bootstrap-wysiwyg -->
<script src="../../static/resource/js/bootstrap-wysiwyg.min.js"></script>
<script src="../../static/resource/js/jquery.hotkeys.js"></script>
<script src="../../static/resource/js/prettify.js"></script>

<!-- jQuery Tags Input -->
<script src="../../static/resource/js/jquery.tagsinput.js"></script>

<!-- Switchery -->
<script src="../../static/resource/js/switchery.min.js"></script>

<!-- Select2 -->
<script src="../../static/resource/js/select2.full.min.js"></script>

<!-- Parsley -->
<script src="../../static/resource/js/parsley.min.js"></script>

<!-- Autosize -->
<script src="../../static/resource/js/autosize.min.js"></script>

<!-- jQuery autocomplete -->
<script src="../../static/resource/js/jquery.autocomplete.min.js"></script>

<!-- starrr -->
<script src="../../static/resource/js/starrr.js"></script>

<!-- Custom Theme Scripts -->
<script src="../../static/resource/js/custom.min.js"></script>

<%--datepicker--%>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>

</body>

</html>
