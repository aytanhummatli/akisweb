<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 2/20/2019
  Time: 6:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body ng-app="nomApp" ng-controller="NomController">

<table class="table table-striped jambo_table bulk_action">
    <thead>
    <tr class="headings">
        <th class="column-title">Id</th>
        <th class="column-title">Qurum</th>
        <th class="column-title">Əlavə et</th>
        <th class="column-title">Redaktə</th>
        <th class="column-title">Sil</th>
    </tr>
    </thead>

    <tbody>
    <tr ng-show="c.nomList.length <= 0">
        <td colspan="5" style="text-align:center;">Loading new data!!</td>
    </tr>
    <td>
        <button ng-click="addNom(null)" id="add-button" value="Əlavə Et"
                class="btn btn-success custom-width">Əlavə et
        </button>
    </td>
    <tr class="odd pointer" pagination-id="nomPaginid"
        dir-paginate="nom in nomList|itemsPerPage:itemsPerPage"
        total-items="total_count">
        <td class=" "><span ng-bind="nom.id"></span></td>
        <td class=" " ng-click="selected(nom.id)"><span ng-bind="nom.name"></span></td>
        <td>
            <button ng-click="addChild(nom.id)" id="add-child-button" value="Əlavə Et"
                    class="btn btn-success custom-width">Əlavə et
            </button>
        </td>
        <td>
            <button ng-click="editNom(nom.id)" id="edit-button" value="Dəyiş"
                    class="btn btn-success custom-width">Dəyiş
            </button>
        </td>
        <td>
            <button ng-click="removeNom(nom.id)" id="delete-button" value="Sil"
                    class="btn btn-danger custom-width">Sil
            </button>
        </td>

    </tr>
    </tbody>
</table>
<dir-pagination-controls
        pagination-id="nomPaginid"
        max-size="8"
        direction-links="true"
        boundary-links="true"
        on-page-change="getList(newPageNumber)">
</dir-pagination-controls>

<script type="text/ng-template" id="nomAddUpdateModal.html">
    <div class="modal-header">
        <h4>Nomenklatura</h4>
    </div>

    <div uib-alert ng-show="alert.show" ng-model="alert" ng-class="'alert-' +alert.type" close="closeAlert()">
        {{alert.msg}}
    </div>

    <div class="modal-body">


        <div class="formcontainer">
            <form ng-submit="submit()" name="myForm" class="form-horizontal">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable">Qurum Adı</label>
                        <div class="col-md-7">
                                        <textarea type="text" ng-model="nom.name" rows="1" id="nomName"
                                                  class="field form-control input-sm" placeholder=""
                                                  required ng-minlength="7"></textarea><br>
                            <div class="has-error" ng-show="myForm.$dirty">
                                <span ng-show="myForm.name.$error.required">Adı daxil edin</span>
                                <span ng-show="myForm.name.$error.minlength">Ad 7 sinvoldan ibarət olmaldi</span>
                                <span ng-show="myForm.name.$invalid">Düzgün daxil edilməyib </span>
                            </div>
                        </div>

                        <label class="col-md-4 control-lable">Qurum tam adı</label>
                        <div class="col-md-7">
                                        <textarea type="text" ng-model="nom.str" rows="1" id="nomStr"
                                                  class="field form-control input-sm" placeholder=""
                                                  required ng-minlength="7"></textarea><br>
                        </div>

                        <%--<label class="col-md-4 control-lable" for="nomId">Qurumun sıra nömrəsi</label>--%>
                        <%--<div class="col-md-7">--%>
                        <%--<textarea type="text" ng-models="nom.ID" rows="1" id="nomId"--%>
                        <%--class="field form-control input-sm" placeholder=""--%>
                        <%--required ng-minlength="4"></textarea><br>--%>
                        <%--</div>--%>

                        <%--<label class="col-md-4 control-lable" for="nom">Nomenklatura Combo</label>--%>
                        <%--<div class="col-md-7">--%>
                        <%--<select name="nom" id="nom" ng-models="nom.ID"--%>
                        <%--ng-options="nom.ID as nom.NAME for nom in nomCombo "--%>
                        <%--class="field form-control input-sm" required>--%>
                        <%--<option value="">Seçim et</option>--%>
                        <%--</select>--%>
                        <%--<div class="has-error" ng-show="myForm.$dirty">--%>
                        <%--<span ng-show="myForm.technicalFailure.id.$error.required">kategoryani daxil edin</span>--%>
                        <%--<span ng-show="myForm.technicalFailure.id.$invalid">kategorya düzgün daxil edilməyib </span>--%>
                        <%--</div>--%>
                        <%--</div>--%>
                    </div>
                </div>

                <br><br>
                <div class="row" style="position: absolute; bottom:5px;right: 20px">
                    <div>
                        <input type="submit" value="{{!nom.id ? 'Əlavə et' : 'Yadda saxla'}}"
                               class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/ng-template" id="nomAddChildModal.html">
    <div class="modal-header">
        <h4>Nomenklatura</h4>
    </div>

    <div uib-alert ng-show="alert.show" ng-model="alert" ng-class="'alert-' +alert.type" close="closeAlert()">
        {{alert.msg}}
    </div>

    <div class="modal-body">


        <div class="formcontainer">
            <form ng-submit="submit()" name="myForm" class="form-horizontal">
                <div class="row">
                    <div class="form-group col-md-12">
                        <%--<label class="col-md-4 control-lable" for="nomChild">Tərkibi qurumun Adı</label>--%>
                        <%--<div class="col-md-7">--%>
                        <%--<textarea type="text" ng-models="nom.name" rows="1" id="nomChildName"--%>
                        <%--class="field form-control input-sm" placeholder=""--%>
                        <%--required ng-minlength="4"></textarea><br>--%>
                        <%--</div>--%>
                        <%--<label class="col-md-4 control-lable" for="nomChild">Tərkibi qurumun tam Adı</label>--%>
                        <%--<div class="col-md-7">--%>
                        <%--<textarea type="text" ng-models="nom.str" rows="1" id="nomChildStr"--%>
                        <%--class="field form-control input-sm" placeholder=""--%>
                        <%--required ng-minlength="4"></textarea><br>--%>
                        <%--</div>--%>
                        <%--<input type="hidden" ng-models="nom.id"/>--%>
                        <label class="col-md-4 control-lable" for="nomChild">Tərkibi qurumun İd-si</label>
                        <div class="col-md-7">
                                        <textarea type="text" ng-model="nom.id" rows="1" id="nomChildId"
                                                  class="field form-control input-sm" placeholder=""
                                                  required ng-minlength="4"></textarea><br>
                        </div>

                        <label class="col-md-4 control-lable" for="nomChild">Tərkibi qurum</label>
                        <div class="col-md-7">
                            <select name="nom" id="nomChild" ng-model="nom"
                                    ng-options="nom as nom.name for nom in nomCombo "
                                    class="field form-control input-sm" required>
                                <option value="">Seçim et</option>
                            </select>
                            <div class="has-error" ng-show="myForm.$dirty">
                                <span ng-show="myForm.technicalFailure.id.$error.required">kategoryani daxil edin</span>
                                <span ng-show="myForm.technicalFailure.id.$invalid">kategorya düzgün daxil edilməyib </span>
                            </div>
                        </div>
                    </div>
                </div>

                <br><br>
                <div class="row" style="position: absolute; bottom:5px;right: 20px">
                    <div>
                        <input type="submit" value="{{!nom.id ? 'Əlavə et' : 'Yadda saxla'}}"
                               class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/ng-template" id="nomRemoveModal.html">
    <div class="modal-header">
        <h4>Nomenklatura</h4>
    </div>

    <div uib-alert ng-show="alert.show" ng-model="alert" ng-class="'alert-' +alert.type" close="closeAlert()">
        {{alert.msg}}
    </div>

    <div class="modal-body">


        <div class="formcontainer">

            <form ng-submit="ok()" name="deleteForm" class="form-horizontal">
                <h3>Silməyə Əminsinizmi?</h3>
                <%--<input type="hidden" ng-models="nom"/>--%>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable"></label>
                    </div>
                </div>

                <div class="row" style="position: absolute; bottom:5px;right: 20px">
                    <div>
                        <input type="submit" value="Bəli" class="btn btn-primary btn-sm"
                               ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</script>
</body>
</html>
