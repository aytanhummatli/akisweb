<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 2/5/2019
  Time: 4:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body ng-app="positionApp" ng-controller="PositionController as ctrl">
<table class="table table-striped jambo_table bulk_action">
    <thead>
    <tr class="headings">
        <th class="column-title">Id</th>
        <th class="column-title">Vəzifə Qrupları</th>
        <th class="column-title">Vəzifə Adları</th>
        <th class="column-title">Redaktə</th>
        <th class="column-title">Sil</th>
    </tr>
    </thead>

    <tbody>
    <tr ng-show="ctrl.positionList.length <= 0">
        <td colspan="5" style="text-align:center;">Loading new data!!</td>
    </tr>
    <tr class="odd pointer" pagination-id="hpPaginid"
        dir-paginate="positionName in ctrl.positionList | itemsPerPage:ctrl.itemsPerPage"
        total-items="ctrl.total_count">
        <input type="button" ng-click="ctrl.add(null)" class="btn btn-success custom-width"
               id="add-button" value="Əlavə et">
        <td class=" "><span ng-bind="positionName.position.id"></span></td>
        <td class=" "><span ng-bind="positionName.position.positionGroup.name"></span></td>
        <td class=" "><span ng-bind="positionName.name"></span></td>

        <td>
            <button ng-click="ctrl.edit(positionName.position.id)" id="edit-button" value="Dəyiş"
                    class="btn btn-success custom-width">Dəyiş
            </button>
        </td>
        <td>
            <button ng-click="ctrl.remove(positionName.position.id)" id="delete-button" value="Sil"
                    class="btn btn-danger custom-width">Sil
            </button>
        </td>

    </tr>
    </tbody>
</table>
<dir-pagination-controls
        pagination-id="hpPaginid"
        max-size="8"
        direction-links="true"
        boundary-links="true"
        on-page-change="ctrl.getPositionList(newPageNumber)">
</dir-pagination-controls>


<script type="text/ng-template" id="positionAddModal.html">

    <div class="modal-header">
        <h4>HR Vəzifələr</h4>
    </div>

    <div uib-alert ng-show="alert.show" ng-model="alert" ng-class="'alert-' +alert.type" close="closeAlert()">
        {{alert.msg}}
    </div>

    <div class="modal-body">


        <div class="formcontainer">

            <form ng-submit="submit()" name="myForm" class="form-horizontal">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable">Vəzifə adları</label>
                        <div class="col-md-7">
                                        <textarea type="text" ng-model="positionName.name" rows="1" id="name"
                                                  class="field form-control input-sm" placeholder=""
                                                  required ng-minlength="7"></textarea></br>
                            <div class="has-error" ng-show="myForm.$dirty">
                                <span ng-show="myForm.name.$error.required">Adı daxil edin</span>
                                <span ng-show="myForm.name.$error.minlength">Ad 7 simvoldan ibarət olmaldi</span>
                                <span ng-show="myForm.name.$invalid">Düzgün daxil edilməyib </span>
                            </div>
                        </div>
                        <label class="col-md-4 control-lable">Qrup id</label>
                        <div class="col-md-7">
                            <textarea type="text" name="posGroupId" ng-model="positionName.position.positionGroup.id" rows="1" id="id"
                                      class="field form-control input-sm" placeholder="" ng-value="positionName.position.positionGroup.id"
                                      required ng-minlength="1"></textarea></br>
                        </div>
                        <label class="col-md-4 control-lable" for="positionGroup">Vəzifə qrupları</label>
                        <div class="col-md-7">
                            <select name="positionGroup" id="positionGroup" ng-model="positionName.position.positionGroup.id"
                                    ng-options="positionGroup.id as positionGroup.name for positionGroup in positionGroups "
                                    class="field form-control input-sm" required>
                                <option value="">Seçim et</option>
                            </select>
                            <div class="has-error" ng-show="myForm.$dirty">
                                <span ng-show="myForm.technicalFailure.id.$error.required">kategoryani daxil edin</span>
                                <span ng-show="myForm.technicalFailure.id.$invalid">kategorya düzgün daxil edilməyib </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable" for="positionGroup"></label>
                    </div>
                </div>


                <br><br>
                <div class="row" style="position: absolute; bottom:5px;right: 20px">
                    <div>
                        <input type="submit" value="{{'Əlavə et'}}"
                               class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/ng-template" id="positionUpdateModal.html">

    <div class="modal-header">
        <h4>HR Vəzifələr</h4>
    </div>

    <div uib-alert ng-show="alert.show" ng-model="alert" ng-class="'alert-' +alert.type" close="closeAlert()">
        {{alert.msg}}
    </div>

    <div class="modal-body">


        <div class="formcontainer">

            <form ng-submit="submit()" name="myForm" class="form-horizontal">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable">Vəzifə adlari</label>
                        <div class="col-md-7">
                            <input type="hidden"  ng-model="positionName.position.id" >
                        </div>
                        <div class="col-md-7">

                            <textarea type="text" ng-model="positionName.name" rows="1"
                                                  class="field form-control input-sm" placeholder=""
                                                  required ng-minlength="1"></textarea></br>
                            <div class="has-error" ng-show="myForm.$dirty">
                                <span ng-show="myForm.name.$error.required"></span>
                                <span ng-show="myForm.name.$error.minlength">Qrup adı 7 simvoldan ibarət olmaldi</span>
                                <span ng-show="myForm.name.$invalid">Düzgün daxil edilməyib </span>
                            </div>
                        </div>
                        <label class="col-md-4 control-lable">Qrup id</label>
                        <div class="col-md-7">
                            <textarea type="text" name="posGroupId" ng-model="positionName.position.positionGroup.id" rows="1" id="groupId"
                                      class="field form-control input-sm" placeholder=""
                                      required ng-minlength="1"></textarea></br>
                        </div>
                        <label class="col-md-4 control-lable" for="positionGroup">Vəzifə qrupları</label>
                        <div class="col-md-7">
                            <select name="positionGroup" id="" ng-model="positionName.position.positionGroup"
                                   ng-options="positionGroup as positionGroup.name for positionGroup in positionGroups "
                                    class="field form-control input-sm" required>
                                <option value="">Seçim et</option>
                            </select>
                            <div class="has-error" ng-show="myForm.$dirty">
                                <span ng-show="myForm.technicalFailure.id.$error.required">kategoryani daxil edin</span>
                                <span ng-show="myForm.technicalFailure.id.$invalid">kategorya düzgün daxil edilməyib </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable" for="positionGroup"></label>
                    </div>
                </div>


                <br><br>
                <div class="row" style="position: absolute; bottom:5px;right: 20px">
                    <div>
                        <input type="submit" value="{{'Yadda saxla'}}"
                               class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>


<script type="text/ng-template" id="positionRemoveModal.html">
    <div class="modal-header">
        <h4>HR Vəzifə qrupları</h4>
    </div>

    <div uib-alert ng-show="alert.show" ng-model="alert" ng-class="'alert-' +alert.type" close="closeAlert()">
        {{alert.msg}}
    </div>

    <div class="modal-body">


        <div class="formcontainer">

            <form ng-submit="ok()" name="deleteForm" class="form-horizontal">
                <h3>Silməyə Əminsinizmi?</h3>
                <input type="hidden" ng-model="positionName"/>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-4 control-lable">{{positionName.position.name}}</label>
                    </div>
                </div>

                <div class="row" style="position: absolute; bottom:5px;right: 20px">
                    <div>
                        <input type="submit" value="Bəli" class="btn btn-primary btn-sm"
                               ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="cancel()" class="btn btn-warning btn-sm">Bağla</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</script>
</body>
</html>
