<%--
  Created by IntelliJ IDEA.
  User: Moonlight
  Date: 26.02.2019
  Time: 11:32
  To change this template use File | Settings | File Templates.
--%>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- Bootstrap -->
<link href="../static/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../static/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="../static/css/nprogress.css" rel="stylesheet">
<!-- iCheck -->
<link href="../static/css/green.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="../static/css/custom.min.css" rel="stylesheet">

<link rel="stylesheet" type ="text/css" href ="/static/resource/css/styles.css" />

<link rel="stylesheet" type ="text/css" href="/static/resource/css/angular-ui-tree.css"/>

<link rel="stylesheet" href="/static/resource/css/bootstrap-treeview.css">

<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
    .table-wrapper-scroll-y {
        display: block;
        max-height: 500px;
        overflow-y: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
    }
</style>
