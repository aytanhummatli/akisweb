<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3></h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-university"></i> Struktur <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="/structureMap/">Struktur xəritəsi</a></li>
                    <li><a href="/position/">Vəzifələr</a></li>
                    <li><a href="/staffSchedule/">Ştat cədvəli</a></li>
                    <li><a href="index2.jsp">Təhsil qurumları</a></li>
                    <li><a href="/mainRanks/">Rütbələr</a></li>
                    <li><a href="/positionGroup/">Vəzifə qrupları</a></li>
                    <li><a href="/nomenclature/">Nomenklatura</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-users"></i>Kadrlar  <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="/employees/">Əməkdaşlar</a></li>
                    <li><a href="/employeeDetails/">Məzuniyyətlər</a></li>
                    <li><a href="form_validation.html">Rütbə vaxtı çatmış ...</a></li>
                    <li><a href="form_wizards.html">Anketi doldurma vaxtı..</a></li>
                    <li><a href="form_upload.html">Ad günləri.</a></li>
                    <li><a href="form_buttons.html">Veteranlar</a></li>
                    <li><a href="form_buttons.html">DİO-da xidmətə uyğun..</a></li>
                    <li><a href="form_buttons.html">Arxivdə olan şəxsi</a></li>
                    <li><a href="form_buttons.html">Köhnə baza məlumatları</a></li>
                    <li><a href="form_buttons.html">Qüsursuz xidmət</a></li>
                    <li><a href="form_buttons.html">DİO-dan mənfi motivlə ..</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-pencil-square-o"></i> Əmrlərlə iş <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="general_elements.html">General Elements</a></li>
                    <li><a href="media_gallery.html">Media Gallery</a></li>
                    <li><a href="typography.html">Typography</a></li>
                    <li><a href="icons.html">Icons</a></li>
                    <li><a href="glyphicons.html">Glyphicons</a></li>
                    <li><a href="widgets.html">Widgets</a></li>
                    <li><a href="invoice.html">Invoice</a></li>
                    <li><a href="inbox.html">Inbox</a></li>
                    <li><a href="calendar.html">Calendar</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-print"></i> Vəsiqə çapı <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="/mainRanks/">Ranks</a></li>
                    <li><a href="tables_dynamic.html">Table Dynamic</a></li>
                </ul>
            </li>

            </li>
            <li><a><i class="fa fa-trophy"></i> Medallar<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="general_elements.html">General Elements</a></li>
                    <li><a href="media_gallery.html">Media Gallery</a></li>
                    <li><a href="typography.html">Typography</a></li>
                    <li><a href="icons.html">Icons</a></li>
                    <li><a href="glyphicons.html">Glyphicons</a></li>
                    <li><a href="widgets.html">Widgets</a></li>
                    <li><a href="invoice.html">Invoice</a></li>
                    <li><a href="inbox.html">Inbox</a></li>
                    <li><a href="calendar.html">Calendar</a></li>
                </ul>
            </li>
            </li>
            <li><a><i class="fa fa-flag"></i> Qarabağ veteranları<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="general_elements.html">General Elements</a></li>
                    <li><a href="media_gallery.html">Media Gallery</a></li>
                    <li><a href="typography.html">Typography</a></li>
                    <li><a href="icons.html">Icons</a></li>
                    <li><a href="glyphicons.html">Glyphicons</a></li>
                    <li><a href="widgets.html">Widgets</a></li>
                    <li><a href="invoice.html">Invoice</a></li>
                    <li><a href="inbox.html">Inbox</a></li>
                    <li><a href="calendar.html">Calendar</a></li>
                </ul>
            </li>
            </li>
            <li><a><i class="fa fa-pencil-square-o"></i> Sərhədkeçmə<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="general_elements.html">General Elements</a></li>
                    <li><a href="media_gallery.html">Media Gallery</a></li>
                    <li><a href="typography.html">Typography</a></li>
                    <li><a href="icons.html">Icons</a></li>
                    <li><a href="glyphicons.html">Glyphicons</a></li>
                    <li><a href="widgets.html">Widgets</a></li>
                    <li><a href="invoice.html">Invoice</a></li>
                    <li><a href="inbox.html">Inbox</a></li>
                    <li><a href="calendar.html">Calendar</a></li>
                </ul>
            </li>
            </li>
            <li><a><i class="fa fa-pencil-square-o"></i> Sistemin idarə olunması<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="/userInfo/">İstifadəçi Məlumatları</a></li>
                    <li><a href="media_gallery.html">Struktur Qaydaları</a></li>
                    <li><a href="typography.html">Typography</a></li>
                    <li><a href="icons.html">Icons</a></li>
                    <li><a href="glyphicons.html">Glyphicons</a></li>
                    <li><a href="widgets.html">Widgets</a></li>
                    <li><a href="invoice.html">Invoice</a></li>
                    <li><a href="inbox.html">Inbox</a></li>
                    <li><a href="calendar.html">Calendar</a></li>
                </ul>
            </li>
        </ul>
    </div>


</div>
<!-- /sidebar menu -->
