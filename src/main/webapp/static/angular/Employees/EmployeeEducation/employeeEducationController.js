'use strict';

angular.module('appEmployeeEducation').controller('employeeEducationController', ['$window', '$scope', '$http', '$sce', '$uibModal', 'employeEducationService', function ($window, $scope, $http, $sce, $uibModal, employeEducationService) {

    //  $scope.empId=500000002465;

    //   $scope.empId= 10000030863

    //   $scope.empId= 250000004807

    // for attestation  1320000000242

    $scope.empId= 640000002958;

    //  $scope.empId= 1320000000242;

    // for educ history 2750000002302

    // $scope.empId= 2750000002302;


    $scope.employee = {

        id: null,

        person: {id: '', name: '', surName:'', patronymic: '' },

        image: {id: '', imgURL: '', ftpFoder: ''},

        employeeLanguageLevel:[{language:{description:''},languageLevel:{description:''}}],

        academicDegreePosition:{academicDegree:{description:'',id:''},getDate:''},

        educationHistory:[{id:'',certificateGetDate: null,endDate: null, endYear:null,sertificateNo: null, startYear: '',status: '',
            certificateType: {name: null,   id:null, description: ''},
            educationType: {name: null,   id: null, description: ''} ,
            eyaniQiyabi: {name: null,   id: null, description: null},
            educationLevel: {name: null,  id: 0, description: null},
            profession: {profession: '',professionType: {name: null, no: null, id: null, description: null}, id: null},
            universityName: {university: {id: null, country: {id: null, name: null}, eduStructureType: {name: null, no: null, id: 0, description: null}}, id: null, name:''}
        }],


        attestationHistory:[{id:'',atteatationDate: null, result: {name: null, id: null, description: ''}}]
    };

    //Employee list type of Employee object
    $scope.employees = [$scope.employee];

    $scope.academicDegree={description:'',id:''};

    getEmployeeEducation($scope.empId);

    getDegree();

    function getEmployeeEducation(empId) {

        employeEducationService.getEmployeeEducation(empId)
            .then(
                function (d) {

                    $scope.employee = d;

                    $scope.academicDegree=$scope.employee.academicDegreePosition.academicDegree;

                    console.log('getEmployeeDetails results $scope.academicDegree',$scope.academicDegree);
                },
                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }


    function getDegree() {

        var degree=15;

        employeEducationService.getCombo(degree)
            .then(
                function (d) {
                    $scope.academicDegreeList = d;

                    console.log('$scope.academicDegreeList =  ', $scope.academicDegreeList);
                },

                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }

    function deleteLanguageLevel(elementID) {

        employeEducationService.deleteLanguageLevel(elementID)
            .then(
                function (d) {

                },
                function (errResponse) {

                }
            );
    }
    /////// begin deleteLanguageLevel

    $scope.removeLanguageLevel = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';

        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: ModalInstanceCtrlLanguageLevel
            }
        );
        modalInstance.result.then(function () {
            deleteLanguageLevel(id);
        });
    };



    var empId = $scope.empId;

    var ModalInstanceCtrlLanguageLevel = function ($scope, $uibModalInstance) {

        $scope.ok = function () {

            getEmployeeEducation(empId);

            $uibModalInstance.close();
        };deleteLanguageLevel

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');
        };
    };

    /////// end deleteLanguageLevel


    /////// begin deleteEducationHistory

    function deleteEducationHistory(elementID) {

        employeEducationService.deleteEducationHistory(elementID)
            .then(
                function (d) {

                },
                function (errResponse) {

                }
            );
    }


    $scope.removeEducationHistory = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';

        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: ModalInstanceCtrlEducationHistory
            }
        );
        modalInstance.result.then(function () {
            deleteEducationHistory(id);
            getEmployeeEducation($scope.empId);
        });
    };



    var empId = $scope.empId;

    var ModalInstanceCtrlEducationHistory = function ($scope, $uibModalInstance) {

        $scope.ok = function () {

       //     getEmployeeEducation(empId);

            $uibModalInstance.close();
        };

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');
        };
    };
    /////// end deleteEducationHistory



    /////// begin deleteAttesstationHistory

    function deleteAttesstationHistory(elementID) {

        employeEducationService.deleteAttesstationHistory(elementID)
            .then(
                function (d) {

                },
                function (errResponse) {

                }
            );
    }


    $scope.removeAttesstationHistory = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';

        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: ModalInstanceCtrlAttesstationHistory
            }
        );
        modalInstance.result.then(function () {
            deleteAttesstationHistory(id);
            getEmployeeEducation($scope.empId);
        });
    };



    var empId = $scope.empId;

    var ModalInstanceCtrlAttesstationHistory = function ($scope, $uibModalInstance) {

        $scope.ok = function () {

            //     getEmployeeEducation(empId);

            $uibModalInstance.close();
        };

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');
        };
    };
    /////// end deleteAttesstationHistory
    $(function () {
        var bindDatePicker = function() {
            $(".date").datetimepicker({
                format:'DD.MM.YYYY',
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
            }).find('input:first').on("blur",function () {
                // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
                // update the format if it's yyyy-mm-dd
                var date = parseDate($(this).val());

                if (! isValidDate(date)) {
                    //create date based on momentjs (we have that)
                    date = moment().format('DD.MM.YYYY');
                }

                $(this).val(date);
            });
        }

        var isValidDate = function(value, format) {
            format = format || false;
            // lets parse the date to the best of our knowledge
            if (format) {
                value = parseDate(value);
            }

            var timestamp = Date.parse(value);

            return isNaN(timestamp) == false;
        }

        var parseDate = function(value) {
            var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
            if (m)
                value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

            return value;
        }

        bindDatePicker();
    });



}]);

