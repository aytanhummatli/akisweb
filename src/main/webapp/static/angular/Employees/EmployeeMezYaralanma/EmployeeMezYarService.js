'use strict';

angular.module('appEmployeeMezYar').factory('EmployeeMezYarService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:8060/';


     var factory = {

        getEmployeeMezYar:getEmployeeMezYar,
         deleteEmployeeVacation:deleteEmployeeVacation,
         deleteEmployeeInjured:deleteEmployeeInjured,

        getObject:getObject,

        setObject:setObject
                    };
    return factory;

    var myObject={};


    function  getObject() {

        return myObject;

    }

    function setObject(value) {

        myObject =value ;

    }

    function  getEmployeeMezYar(empId,year) {

    var deferred = $q.defer();

    $http.get(REST_SERVICE_URI+'Employee/getEmployeeMezYar/'+ empId+'/'+year)
        .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){

                console.error('Error while fetching data');

                deferred.reject(errResponse);
            }
        );
    return deferred.promise;
}

    function deleteEmployeeVacation(elementId) {

        var deferred = $q.defer();

        $http.put(REST_SERVICE_URI + 'employeeVacation/deleteEmployeeVacation/'+elementId)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.error(reason);
                }
            );

        return deferred.promise;
    }

    function deleteEmployeeInjured(elementId) {

        var deferred = $q.defer();

        $http.put(REST_SERVICE_URI + 'employeeInjured/deleteEmployeeInjured/'+elementId)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.error(reason);
                }
            );

        return deferred.promise;
    }

}]);