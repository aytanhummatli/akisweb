
'use strict';

angular.module('appEmployeeMezYar').controller('EmployeeMezYarController', ['$window', '$scope', '$http', '$sce', '$uibModal', 'EmployeeMezYarService', function ($window, $scope, $http, $sce, $uibModal,EmployeeMezYarService) {

    // for ezam
      $scope.empId =70000018491;

    // mezuniyyyet
    // $scope.empId =300000000055;

    // mezuniyyyet analiq
  //  $scope.empId =450000004564;

   // $scope.year=2011;

   //for yaralanma 860000010513

    $scope.employee ={

        injuredList: {prizePhunishReason: {id: null, name: ''}, id: null, createDate: ''},


        employeeVacations:[{address: null,
            addressNote: null,
            createDate: '',
            endDate: '',
            id: null,
            vacationCardNo: '',
            vacationNote: null,
            withFamily: null,
            dayCount:'',
            vacationType: {name: null, description: ''}

        }],

        employeeVacationMotherhood:[{address: null,
            addressNote: null,
            createDate: '',
            endDate: '',
            id: null,
            vacationCardNo: '',
            vacationNote: null,
            withFamily: null,
            dayCount:'',
            vacationType: {name: null, description: ''}

        }]
    };

    $scope.years = [{  id: 2019 },{  id: 2018 },{  id: 2017 },{  id: 2016 },{  id: 2015 },{  id: 2014 },
        {  id: 2013 },{  id: 2012 },{  id: 2011 },{  id: 2010 },{  id: 2009 }, {  id: 2008 }];

    $scope.selectedYear = $scope.years[0];

    //Employee list type of Employee object
    $scope.employees = [$scope.employee];

    $scope.getEmployeeMezYar = getEmployeeMezYar;

    getEmployeeMezYar($scope.years[0].id);

    function getEmployeeMezYar(year) {

     var   empId=70000018491;

     console.log('year',year);

        EmployeeMezYarService.getEmployeeMezYar(empId,year)
            .then(
                function (d) {
                    $scope.employee  = d;

                    console.log('getEmployeeDetails results d=',d);

                    console.log('getEmployeeDetails results $scope.employee',$scope.employee );
                },

                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }



    function deleteEmployeeVacation(elementID) {

        EmployeeMezYarService.deleteEmployeeVacation(elementID)
            .then(
                function (d) {

                },
                function (errResponse) {

                }
            );
    }

    $scope.removeEmployeeVacation = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';

        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: ModalInstanceCtrlEmployeeVacation
            }
        );
        modalInstance.result.then(function () {
            deleteEmployeeVacation(id);
            getEmployeeMezYar($scope.empId);
        });
    };

    var empId = $scope.empId;

    var ModalInstanceCtrlEmployeeVacation = function ($scope, $uibModalInstance) {

        $scope.ok = function () {

            //  getEmployeeEducation(empId);
           // getEmployeePrizeAndPunish(empId);
            $uibModalInstance.close();
        };

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');
        };
    };


    function deleteEmployeeInjured(elementID) {

        EmployeeMezYarService.deleteEmployeeInjured(elementID)
            .then(
                function (d) {

                },
                function (errResponse) {

                }
            );
    }

    $scope.removeEmployeeInjured = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';

        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: ModalInstanceCtrlEmployeeInjured
            }
        );
        modalInstance.result.then(function () {

            deleteEmployeeInjured(id);

            getEmployeeMezYar($scope.empId);
        });
    };

    var empId = $scope.empId;

    var ModalInstanceCtrlEmployeeInjured = function ($scope, $uibModalInstance) {

        $scope.ok = function () {

            //  getEmployeeEducation(empId);

            // getEmployeePrizeAndPunish(empId);

            $uibModalInstance.close();
        };

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');
        };
    };



}]);

