angular.module('appEmployee')
    .controller('EmployeeCheckWordModalController', ['$http', '$scope', 'editId', '$uibModalInstance', 'employeService', function ($http, $scope, editId, $uibModalInstance, employeService) {


        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };


        $scope.openPdf = function (empId) {
            $scope.emp = {

                id: null,
                person: {
                    bornAddress: {adress_fullname: ''},
                    contactInformation: [{typeId: null, id: null, name: '', value: ''}],
                    marriedStatus: {description: ''}
                },
                createDate: null,
                endDate: null,
                personalMatterNo: null,
                policeCardNo: null,
                nationality: {},
                civil: null,
                ok: null,
                cardMustBeChanged: null,
                cardReasonId: null,
                rank: {},
                image: {},
                positionName: {},
                fullStrname: null,
                tree: {},
                nomenclature: {},
                passport: {adress: {adress_fullname: ''}},
                employeePosition: {structurePosition: {fullSttrName: ''}},
                employeeRank: [{cmbsService: {description: ''}}],
                successDegreeHistory: [],
                employeeMilitaryService: {militaryDuty: {description: ''}},
                employeeLanguageLevel: [],
                academicDegreePosition: {},
                educationHistory: [],
                attestationHistory: [],
                workMissions: [],
                workActivityBefore: [],
                employeeVacations: [],
                employeeVacationMotherhood: [],
                injuredList: [],
                employeeWorkBook: {},
                lastFormFillDate: {},
                qualificationCourses: [],
                warActivities: [],
                policeCardNew: {},
                prize: [{
                    getDate: '', id: null, orderNo: '', orderOrgan: {fullName: null, name: '',},
                    organFullNameId: null, prizeType: {
                        combo: {name: null, no: null, id: null, description: ''},
                        id: null,
                        name: '',
                    },
                    punishEndDate: null,
                    punishEndOrder: null,
                    punishEndReason: {
                        id: null, name: null, type: {
                            id: null,
                            name: null
                        }
                    },
                    reason: {id: null, name: '', type: {description: null, id: null, name: null}}
                }],
                dioPrizeList: [{
                    getDate: '', id: null, orderNo: '', orderOrgan: {fullName: null, name: '',},
                    organFullNameId: null, prizeType: {
                        combo: {name: null, no: null, id: 0, description: ''},
                        id: null,
                        name: '',
                    },
                    punishEndDate: null,
                    punishEndOrder: null,
                    punishEndReason: {
                        id: null, name: null, type: {
                            id: 0,
                            name: null
                        }
                    },
                    reason: {id: null, name: '', type: {description: null, id: 0, name: null}}
                }],
                punishlist: [{
                    getDate: '', id: null, orderNo: '', orderOrgan: {fullName: null, name: '',},
                    organFullNameId: null, prizeType: {
                        combo: {name: null, no: null, id: null, description: ''},
                        id: null,
                        name: '',
                    },
                    punishEndDate: null,
                    punishEndOrder: null,
                    punishEndReason: {
                        id: null, name: null, type: {
                            id: null,
                            name: null
                        }
                    },
                    reason: {id: null, name: '', type: {description: null, id: null, name: null}}
                }],

                medalList: [{
                    getDate: '', id: null, orderNo: '', orderOrgan: {fullName: null, name: '',},
                    organFullNameId: null, prizeType: {
                        combo: {name: null, no: null, id: null, description: ''},
                        id: null,
                        name: '',
                    },
                    punishEndDate: null,
                    punishEndOrder: null,
                    punishEndReason: {
                        id: null, name: null, type: {
                            id: null,
                            name: null
                        }
                    },
                    reason: {id: null, name: '', type: {description: null, id: null, name: null}}
                }],

                relatives: [{
                    id: null, workPlace: '', bornYear: '', livingAddressNote: null,
                    livingAddress: {id: null, adress_fullname: ''},
                    person: {
                        id: null, name: '', surName: '', patronymic: '', birthDate: null,
                        bornAddress: {id: null, adress_fullname: ''}
                    },
                    relativeDegree: {description: ''},

                }],
                dioList: [{
                    id: null,
                    structurePosition: {
                        fullSttrName: null,
                        id: null,
                        structureName: {
                            id: 0,
                            achronym: null,
                            suffix: null,
                            name: null,
                            structureTree: {},
                            fullName: null
                        },
                        positionName: {
                            posId: null,
                            id: null,
                            name: '',
                            active: null,
                            position: {
                                id: null,
                                active: null,
                                createDate: null,
                                posGroupId: null,
                                endDate: null,
                                positionGroup: {
                                    id: null,
                                    createDate: null,
                                    endDate: null,
                                    name: null
                                }
                            }
                        },
                        partTime: null,
                        isEmpty: null,
                        endDate: null,
                        createDate: null,
                        ppxDyp: {
                            name: null,
                            no: null,
                            id: 0,
                            description: null
                        },
                        budget: null,
                        rankLimit: null
                    },
                    workStartDate: null,
                    workEndDate: null,
                    orderDate: null,
                    enterOrder: '',
                    endDate: null,
                    exitOrder: '',
                    createDate: null,
                    enterReason: {
                        name: null,
                        no: null,
                        id: 0,
                        description: null
                    },
                    labourCode: {
                        name: null,
                        no: null,
                        id: null,
                        description: null
                    },
                    fullname: ''
                }]
            };
            $scope.empList = [$scope.emp];

            console.log('x===', $scope.x);
            console.log('emp object : ', $scope.emp);
            var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' " +
                "xmlns:w='urn:schemas-microsoft-com:office:word' " +
                "xmlns='http://www.w3.org/TR/REC-html40'>" +
                "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
            var footer = "</body></html>";
            var personInfo=document.getElementById("personInfo").innerHTML;
            var dio='';
            var edu='';
            var personCard='';
            var prizeAndPunish='';
            var dioPrize='';
            var prize='';
            var punish='';
            var relative='';
            var ezam='';
            var beforeDio='';
            var lang='';
            var card='';
            var printEmpName=document.getElementById("printEmpName").innerHTML;

            var eduCheck = document.getElementById("eduCheck");
            var dioCheck = document.getElementById("dioCheck");
            var personCardCheck = document.getElementById("personCardCheck");
            var prizeAndPunishCheck = document.getElementById("prizeAndPunishCheck");
            var dioPrizeCheck = document.getElementById("dioPrizeCheck");
            var prizeCheck = document.getElementById("prizeCheck");
            var punishCheck = document.getElementById("punishCheck");
            var relativeCheck = document.getElementById("relativeCheck");
            var ezamCheck = document.getElementById("ezamCheck");
            var beforeDioCheck = document.getElementById("beforeDioCheck");
            var langCheck = document.getElementById("langCheck");
            var cardCheck = document.getElementById("cardCheck");
             $scope.numb=1;
             $scope.innerNumb=0;
            if(personCardCheck.checked){
                $scope.numb=$scope.numb+1;
                console.log('$scope.numb+1in personcard ',$scope.numb)
                personCard = document.getElementById("personCard").innerHTML;
            }else{
                personCard='';
            };
            if(dioCheck.checked){
                $scope.numb=$scope.numb+1;
                console.log('$scope.numb+1in dioCheck ',$scope.numb)
                 dio = document.getElementById("dio").innerHTML;
            }else{
                dio='';
            };
            if(ezamCheck.checked){
                $scope.numb=$scope.numb+1;
                console.log('$scope.numb+1 in ezam check',$scope.numb);
                ezam = document.getElementById("ezam").innerHTML;
            }else{
                ezam='';
            };
            if(eduCheck.checked){
                $scope.numb=$scope.numb+1;
                console.log('$scope.numb+1in educheck ',$scope.numb)
                edu = document.getElementById("edu").innerHTML;
            }else{
                edu='';
            };
            if(langCheck.checked){
                $scope.numb=$scope.numb+1;
                console.log('$scope.numb+1in langcheck ',$scope.numb)
                lang = document.getElementById("lang").innerHTML;
            }else{
                beforeDio='';
            };
            if(prizeAndPunishCheck.checked){
                $scope.numb=$scope.numb+1;
                console.log('$scope.numb+1in ppcheck ',$scope.numb)
                prizeAndPunish = document.getElementById("prizeAndPunish").innerHTML;
            }else{
                prizeAndPunish='';
            };
            if(dioPrizeCheck.checked){
                $scope.innerNumb=$scope.innerNumb+1;
                console.log('$scope.numb+1in dioprizeCheck ',$scope.innerNumb)
                dioPrize = document.getElementById("dioPrize").innerHTML;
            }else{
                dioPrize='';
            };

            if(prizeCheck.checked){
                $scope.innerNumb=$scope.innerNumb+1;
                console.log('$scope.numb+1in punishcheck ',$scope.innerNumb)
                prize = document.getElementById("prize").innerHTML;
            }else{
                prize='';
            };
            if(punishCheck.checked){
                $scope.innerNumb=$scope.innerNumb+1;
                console.log('$scope.numb+1in punishcheck ',$scope.innerNumb)
                punish = document.getElementById("punish").innerHTML;
            }else{
                punish='';
            };

            if(beforeDioCheck.checked){
                $scope.numb=$scope.numb+1;
                console.log('$scope.numb+1in beforedioCheck ',$scope.numb)
                beforeDio = document.getElementById("beforeDio").innerHTML;
            }else{
                beforeDio='';
            };
            if(relativeCheck.checked){
                $scope.numb=$scope.numb+1;
                console.log('$scope.numb+1in relativecheck ',$scope.numb)
                relative = document.getElementById("relative").innerHTML;
            }else{
                relative='';
            };



            if(cardCheck.checked){
                $scope.numb=$scope.numb+1;
                console.log('$scope.numb+1in card ',$scope.numb)
                card = document.getElementById("card").innerHTML;
            }else{
                card='';
            };


            var sourceHTML = header + document.getElementById("main").innerHTML+personInfo+personCard+dio+ezam+edu+lang+prizeAndPunish+dioPrize+prize+punish+beforeDio+relative+card+printEmpName+footer;

            var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
            var fileDownload = document.createElement("a");
            document.body.appendChild(fileDownload);
            fileDownload.href = source;
            fileDownload.download = 'document.doc';
            fileDownload.click();
            document.body.removeChild(fileDownload);
            $uibModalInstance.close();
        }
    }]);