'use strict';

angular.module('appEmployeeDIO').factory('employeeDIOService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:8060/';


     var factory = {

        getEmployeeDIO:getEmployeeDIO,
         deleteEmployeePosition:deleteEmployeePosition,

        getObject:getObject,
        setObject:setObject
                    };
    return factory;

    var myObject={};


    function  getObject() {

        return myObject;

    }

    function setObject(value) {

        myObject =value ;

    }

    function  getEmployeeDIO(empId) {

    var deferred = $q.defer();

    $http.get(REST_SERVICE_URI+'Employee/getEmployeeDIObyId/'+ empId)
        .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){

                console.error('Error while fetching data');

                deferred.reject(errResponse);
            }
        );
    return deferred.promise;
}
    function deleteEmployeePosition(elementId) {

        var deferred = $q.defer();

        $http.put(REST_SERVICE_URI + 'employeePosition/deleteEmployeePosition/'+elementId)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.error(reason);
                }
            );

        return deferred.promise;
    }

}]);