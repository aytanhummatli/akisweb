'use strict';

angular.module('appEmployeeDIO').controller('employeeDIOController', ['$window', '$scope', '$http', '$sce', '$uibModal', 'employeeDIOService', function ($window, $scope, $http, $sce, $uibModal, employeeDIOService) {

    //for ezam
    $scope.empId = 1320000000242;

    // beforeDio
    // $scope.empId =810000024694;


    $scope.employeeDIOList = [{
        createDate: null,
        endDate: null,
        enterOrder: '',
        enterReason: {name: null, no: null, id: 0, description: null},
        exitOrder: null,
        fullname: '',
        id: null,
        labourCode: {name: null, no: null, id: 0, description: ''},
        orderDate: null,
        structurePosition: {fullSttrName: null, id: null, positionName: {name: ''}, partTime: null},
        workEndDate: '',
        workStartDate: ''

    }];

    //Employee list type of Employee object
    $scope.employees = [$scope.employee];

    $scope.getEmployeeDIO = getEmployeeDIO;

    getEmployeeDIO($scope.empId);

    function getEmployeeDIO(empId) {

        employeeDIOService.getEmployeeDIO(empId)
            .then(
                function (d) {
                    $scope.employeeDIOList = d;
                    console.log('getEmployeeDetails results d=', d);
                    console.log('getEmployeeDetails results $scope.employee', $scope.employeeDIOList);
                },
                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }

    function deleteEmployeePosition(elementID) {

        employeeDIOService.deleteEmployeePosition(elementID)
            .then(
                function (d) {

                },
                function (errResponse) {

                }
            );
    }



    $scope.removeEmployeePosition = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';

        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: ModalInstanceCtrlEmployeePosition
            }
        );
        modalInstance.result.then(function () {
            deleteEmployeePosition(id);
            getEmployeeDIO($scope.empId);
        });
    };

    var empId = $scope.empId;

    var ModalInstanceCtrlEmployeePosition = function ($scope, $uibModalInstance) {

        $scope.ok = function () {

            //     getEmployeeEducation(empId);

            $uibModalInstance.close();
        };

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');
        };
    };



}]);

