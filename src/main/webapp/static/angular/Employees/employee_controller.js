'use strict';

angular.module('appEmployee').controller('employeeController', ['$window', '$scope', '$http', '$sce', '$uibModal', 'employeService', function ($window, $scope, $http, $sce, $uibModal, employeService) {
//test id =960000002607;



    $scope.employee = {
        id: null,
        person: {id: '', name: '', surName: '', patronymic: '', birthDate: ''},
        createDate: '',
        endDate: '',
        personalMatterNo: '',
        policeCardNo: '',

        nationalityId: '',
        civil: '',
        ok: '',
        cardMustBeChanged: '',
        cardReasonId: '',

        rank: {id: null, name: ''},
        image: {id: '', imgURL: '', ftpFoder: ''},
        positionName: '',
        fullStrname: '',
        tree: {id: '', title: ''}
    };


    //Employee list type of Employee object
    $scope.employees = [$scope.employee];

    var REST_SERVICE_URI = 'http://localhost:8060/Employee/';
    $scope.getData = getData;
    $scope.selectTree = selectTree;
    $scope.collectData = collectData;
    // scope.getEmployeeInfoPrintById=getEmployeeInfoPrintById;


    $scope.pageno = 1;
    $scope.itemsPerPage = 10;

    $scope.total_count = 100;

    $scope.myObj = {};

    $scope.myObj.st_id = 1550000000;

    $scope.myObj.ed_surname = "";

    $scope.myObj.ed_patron = "";

    $scope.myObj.ed_name = "";

    $scope.myObj.pageno = 1;

    $scope.myObj.itemsPerPage = 10;

    getData($scope.myObj);

    function collectData(pageno) {

        $scope.myObj.pageno = pageno;

        console.log('pageno', pageno);

        getData($scope.myObj);
    }

    function getData(myObj) {

        if (!$scope.employee.tree.id) {
            myObj.st_id = 1500000001
        } else {
            myObj.st_id = $scope.employee.tree.id;
        }

        myObj.ed_surname = $scope.employee.person.surName;

        myObj.ed_patron = $scope.employee.person.patronymic;

        myObj.ed_name = $scope.employee.person.name;

        if (!myObj.ed_surname) {

            myObj.ed_surname = "666";
        }

        if (!myObj.ed_patron) {

            myObj.ed_patron = "666";
        }

        if (!myObj.ed_name) {

            myObj.ed_name = "666";
        }
        console.log('my results', myObj);

        employeService.getData(myObj)
            .then(
                function (d) {
                    $scope.employees = d;
                    console.log('my daoo results', d);
                },
                function (errResponse) {

                    console.error('Error while fetching Ranks');
                }
            );
    }

    function selectTree() {

        var modalInstance = $uibModal.open({
            templateUrl: 'SelectTreeModalContent.html',
            controller: 'employeeSelectTreeModal',
            backdrop: 'static',
        });

        modalInstance.result.then(function () {
            $scope.employee.tree = employeService.getObject();
        });
    }



    function setDataToClass(empId) {

        employeService.setDataToClass(empId)
            .then(
                function (d) {

                },
                function (errResponse) {

                    console.error('Error while fetching Ranks');
                }
            );
    }

    $scope.Open = function (empId) {

        var $popup = $window.open("/employeeDetails/", "popup");

         setDataToClass(empId);

        console.log('empId', empId);

        $popup.empId = empId;

    }


    $scope.exportHTML = function (empId) {

    };

    function edit() {

    }

    $scope.clear = function () {

        $scope.employee.person.surName = "";

        $scope.employee.person.patronymic = "";

        $scope.employee.person.name = "";

        $scope.employee.tree = "";
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ***************************************************************************************************************************
    $scope.openPdfModal = function openEmpModal(empId) {
        var modalInstance = $uibModal.open({

            templateUrl: 'employeePersonalMatter.html',
            controller: 'EmployeeCheckPdfModalController',
            resolve: {
                editId: function () {
                    return empId;
                },
            },
            backdrop: 'static',

        });
        modalInstance.closed.then(function () {
                getData($scope.myObj);
            }
        );
        modalInstance.result.catch(function (res) {
            if (!(res === 'cancel' || res === 'escape key press')) {
                getData($scope.myObj);
                throw res;
            }
        });
    };
    $scope.openWordModal = function openEmpModal(empId) {
        employeService.getEmployeeInfoPrintById(empId)
            .then(
                function (value) {
                    $scope.emp = value;
                    $scope.successArray = '';
                    for (var i = 0; i < $scope.emp.successDegreeHistory.length; i++) {
                        $scope.successArray = $scope.successArray + $scope.emp.successDegreeHistory[i].successDegree.name + ',';
                    };
                    if ($scope.emp.educationHistory.length === 0) {
                        $scope.eduArray = '';
                    } else {
                        $scope.eduArray = '';
                        $scope.eduArray = $scope.eduArray + $scope.emp.educationHistory[0].educationType.description + ',';
                    }
                    $scope.phoneArray = '';
                    for (var i = 0; i < $scope.emp.person.contactInformation.length; i++) {
                        if($scope.emp.person.contactInformation.typeId===3 || $scope.emp.person.contactInformation.typeId===4 ){
                            $scope.phoneArray='yoxdur';
                        }else{
                            $scope.phoneArray = $scope.phoneArray + $scope.emp.person.contactInformation[i].name + '-' + $scope.emp.person.contactInformation[i].value + ',';
                        }

                    }
                    $scope.langArray='';
                    for(var i=0;i<$scope.emp.employeeLanguageLevel.length;i++){
                        $scope.langArray=$scope.langArray+$scope.emp.employeeLanguageLevel[i].language.description+'('+$scope.emp.employeeLanguageLevel[i].languageLevel.description+')'+',';
                    }
                }, function (reason) {
                    console.log('error while get emp info print by id ', reason)
                }
            );
        var modalInstance = $uibModal.open({

            templateUrl: 'employeePersonalMatter.html',
            controller: 'EmployeeCheckWordModalController',
            resolve: {
                editId: function () {
                    return empId;
                }
            },
            backdrop: 'static',

        });
        modalInstance.closed.then(function () {
                getData($scope.myObj);
            }
        );
        modalInstance.result.catch(function (res) {
            if (!(res === 'cancel' || res === 'escape key press')) {
                getData($scope.myObj);
                throw res;
            }
        });
    };


}]);