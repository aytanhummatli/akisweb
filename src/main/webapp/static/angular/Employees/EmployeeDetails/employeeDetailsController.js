'use strict';

angular.module('appEmployeeDetails').controller('employeeDetailsController', ['$window', '$scope', '$http', '$sce', '$uibModal', 'employeeDetailsService', function ($window, $scope, $http, $sce, $uibModal, employeeDetailsService) {

  //  $scope.empId = $window.empId;

   // console.log('$scope.empId',$scope.empId);

    getDataFromClass();

    $scope.employee = {

        id: null,

        person: {id: '', name: '', surName:'', patronymic: '', birthDate: '',bornAddress: {id: null, country: null, region: null, street: null, subRegion: null,adress_fullname:''},

          contactInformation:[{name:'',value:''}], maidenName:'', maleFemale: {name: null,  id: '', description: ''},marriedStatus: {name: null, id: '', description: ''}},

        rank: {id: null, name: ''},

        image: {id: '', imgURL: '', ftpFoder: ''},

        tree: {id:'',title:''},

        employeePosition: {id: null, structurePosition: {fullSttrName: ''  , id: null, structureName: {id: null, achronym: '', suffix: '', name: ''}, positionName: {id: null, posId: null, name: '', active: ''}, rankLimit: null}, workStartDate: '', workEndDate: '',endDate:'',fullStrname:'' },
        nationality: {name: '',   id: null, description: ''},
        nomenclature: {id: null, name:'', createDate: null, endDate: null, active: null,nomDependency: {id: null, nomId: null, depNomId: null},str:'',strTreeId:''},
        positionName: {id: null, posId: null, name: '', active: null},

        passport: {addressNote: null,adress: {id: null, adress_fullname:'',country: null, region: null, street: null, subRegion: null,village:null}, passportGetDate: null, passportSerialNo: null, policeDepartment: {id: null, achronym: null, suffix: null, name:''}},

        createDate: '',
        endDate: '',
        personalMatterNo: '',
        policeCardNo: '',
        nationalityId: '',
        civil: '',
        ok: '',
        cardMustBeChanged: '',
        cardReasonId: '',

        fullStrname: ''
    };

    //Employee list type of Employee object
    $scope.employees = [$scope.employee];


    $scope.getEmployeeDetails = getEmployeeDetails;

    function getDataFromClass() {

        employeeDetailsService.getDataFromClass()
            .then(
                function (d) {

                    $scope.empId=d;

                    getEmployeeDetails($scope.empId);

                    console.log('$scope.empId=',$scope.empId);
                },

                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }


    getNationality();
    getGender();
    getFamilyStatus();



    function getEmployeeDetails(empId) {

        employeeDetailsService.getEmployeeDetails(empId)
            .then(
                function (d) {
                    $scope.employee = d;

                    $scope.nationality=$scope.employee.nationality;

                    $scope.marriedStatus=$scope.employee.person.marriedStatus;

                    $scope.maleFemale=$scope.employee.person.maleFemale;

                    console.log('getEmployeeDetails results d=',d);

                    console.log('getEmployeeDetails results $scope.employee',$scope.employee);
                },
                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }

    function getNationality() {

        var comboNo=12;

        employeeDetailsService.getCombo(comboNo)
            .then(
                function (d) {
                    $scope.nationalities = d;

                    console.log('$scope.nationalities =  ', $scope.nationalities);
                },

                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }

    function getGender() {

        var comboNo=2;

        employeeDetailsService.getCombo(comboNo)
            .then(
                function (d) {
                    $scope.maleFemaleList = d;

                    console.log('$scope.maleFemaleList =  ', $scope.maleFemaleList);
                },

                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }

    function getFamilyStatus() {

        var comboNo=3;

        employeeDetailsService.getCombo(comboNo)
            .then(
                function (d) {
                    $scope.marriedStatusList = d;

                    console.log('$scope.marriedStatusList =  ', $scope.marriedStatusList);
                },

                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }


    $scope.selectNomenclature=function () {
        var modalInstance=$uibModal.open({
            templateUrl:'modalNomenclature.html',
            controller:'modalNomenclatureCtrl',
            backdrop:'static'
        });

        modalInstance.result.then(function () {

            $scope.employee.nomenclature=employeeDetailsService.getObject().nomenk;
            console.log('employeeDetailsService.getObject()',employeeDetailsService.getObject().nomenk);
      //      getEmployeeDetails($scope.empId)

        });
    }

    $scope.addContactInfo=function () {

        var myObj={};

       // console.log('dfdggfgfg',$scope.employee.person.id);

        myObj.personId=$scope.employee.person.id;

        myObj.empId=$scope.empId;

        employeeDetailsService.setObject(myObj);

        var modalInstance = $uibModal.open({
            templateUrl: 'addContactInformation.html',
            controller: 'modalContactInformation',
            backdrop: 'static'
        });

        modalInstance.result.then(function () {

            getEmployeeDetails($scope.empId);

        });
    }

    $scope.editContactInfo=function(){
        var modalInstance=$uibModal.open({
            templateUrl:'updateContactInformation.html',
            controller:'modalContactInformation',
            backdrop:'static'
        });
        modalInstance.result.then(function () {
            getEmployeeDetails($scope.empId)
        })
    };
    $scope.deleteContactInfo=function(){
        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';
        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: 'modalContactInformation',
            backdrop:'static'
            }
        );

        modalInstance.result.then(function () {
            getEmployeeDetails($scope.empId);
        });
    };

    $scope.workPlaceAndPosition=function () {
        var modalInstance=$uibModal.open({
            templateUrl:'modalWorkPlaceAndPosition.html',
            controller:'modalWorkPlaceAndPositionCtrl',
            backdrop:'static'
        });

        modalInstance.result.then(function () {
            getEmployeeDetails($scope.empId)
        });
    }

    $(function () {
        var bindDatePicker = function() {
            $(".date").datetimepicker({
                format:'DD.MM.YYYY',
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
            }).find('input:first').on("blur",function () {
                // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
                // update the format if it's yyyy-mm-dd
                var date = parseDate($(this).val());

                if (! isValidDate(date)) {
                    //create date based on momentjs (we have that)
                    date = moment().format('DD.MM.YYYY');
                }

                $(this).val(date);
            });
        }

        var isValidDate = function(value, format) {
            format = format || false;
            // lets parse the date to the best of our knowledge
            if (format) {
                value = parseDate(value);
            }

            var timestamp = Date.parse(value);

            return isNaN(timestamp) == false;
        }

        var parseDate = function(value) {
            var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
            if (m)
                value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

            return value;
        }

        bindDatePicker();
    });

}]);