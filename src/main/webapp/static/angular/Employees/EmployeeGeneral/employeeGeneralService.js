'use strict';

angular.module('appEmployeeGeneral').factory('employeeGeneralService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8060/';


    var factory = {

        getEmployeeGeneral: getEmployeeGeneral,
        getRankService: getRankService,
        getRankList: getRankList,
        createEmployeeRank: createEmployeeRank,
        deleteEmployeeRank:deleteEmployeeRank,
        updateRank:updateRank,

       // **** succes degree****

        getSuccessdegreeList:getSuccessdegreeList,
        deleteSuccessDegreeHistory:deleteSuccessDegreeHistory,
        updateServiceDegree:updateServiceDegree,
        addServiceDegree:addServiceDegree,


        getHerbiMukellefiyyetList:getHerbiMukellefiyyetList,
        insertMilitaryService:insertMilitaryService,
        updateMilitaryService:updateMilitaryService,




        getObject: getObject,
        setObject: setObject
    };
    return factory;

    var myObject = {};


    function getObject() {

        return myObject;

    }

    function setObject(value) {

        myObject = value;

    }


    function getEmployeeGeneral(empId) {
        console.log('i am in General service', empId);
        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'Employee/getGeneralIngormationById/' + empId)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {

                    console.error('Error while fetching general data');

                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getRankService() {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'employeeRank/getRankService')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {

                    console.error('Error while fetching RankServiceAndRanks');

                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getRankList() {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + 'rank/')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {

                    console.error('Error while fetching RankServiceAndRanks');

                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function createEmployeeRank(employeeRank) {

        var deferred = $q.defer();
     //   $http.post(REST_SERVICE_URI + 'EmployeeRank/createEmployeeRank' )

        console.log('create employee rank service',employeeRank);

       $http.post(REST_SERVICE_URI + 'EmployeeRank/createEmployeeRank', employeeRank)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while creating element');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function deleteEmployeeRank(Id) {
        var deferred = $q.defer();
        $http.put(REST_SERVICE_URI + 'EmployeeRank/delete/'+Id )
            .then(
                function (response) {
                    deferred.resolve(response.data);

                },
                function (errResponse) {
                    console.error('Error while create Rank' + errResponse);
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

       function updateRank(employeeRank){

        var deferred=$q.defer();

        $http.put(REST_SERVICE_URI+'EmployeeRank/update',employeeRank)

            .then (
           function(response){
               deferred.resolve(response.data);
           },

            function(errResponse){

                console.error('Error while update Rank' + errResponse);

                deferred.reject(errResponse);

            }

        );

        return deferred.promise;

    }

    //  *************************************** begin success degree History *******************************//


    function getSuccessdegreeList() {
        var deferred=$q.defer();
        $http.get(REST_SERVICE_URI+'SuccessDegreeHistory/getSuccessDegreeList')
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (errResponse) {
                 console.error(errResponse);
                }
            )
        return deferred.promise;

    }

    function deleteSuccessDegreeHistory(elementid)
    {
       var deferred=$q.defer();
        $http.put(REST_SERVICE_URI+'SuccessDegreeHistory/deleteSuccessHistory/'+elementid)
            .then(
                
                function (value) {
                    deferred.resolve(value.data);
                    
                },
                function (errResponse) {
                    
                    console.error(errResponse);
                    deferred.reject;
                }
            );
        return deferred.promise;
    }    
    
    function updateServiceDegree(succesDegreeHistory) {
    var   deferred=$q.defer();
        $http.put(REST_SERVICE_URI+'SuccessDegreeHistory/updateSuccessDegreeHistory',succesDegreeHistory)
            .then(
                function (value) {
                  deferred.resolve(value.data)

                },
                function (errResponse) {
                    console.error(errResponse);

                }

            );
        return deferred.promise;

    }
    

    function addServiceDegree(successDegreeHistory) {

        var deferred=$q.defer();

        $http.post(REST_SERVICE_URI+'SuccessDegreeHistory/insertSuccessDegreeHistory',successDegreeHistory)
            .then(
                function (value) {

                    deferred.resolve(value.data);
                },
                function (errResponse) {
                    console.error(errResponse);
                }   );
      return  deferred.promise;

    }


    //  *************************************** end success degree History *******************************//



    //  *************************************** begin military service*******************************//


      function getHerbiMukellefiyyetList(){

      var deferred=$q.defer();

        $http.get(REST_SERVICE_URI+'MilitaryService/getHerbiMukellefiyyetList')
            .then(
                function (value) {
                  deferred.resolve(value.data);

                },
                function (errResp) {
                    console .error(errResp);
                }

            );

        return deferred.promise;

      }

function insertMilitaryService(element){

        var deferred=$q.defer();

        $http.post(REST_SERVICE_URI+'MilitaryService/insertHerbiMukellefiyyet',element)
            .then(
                function (value) {
                deferred.resolve(value.data);
                },
                function (errResp) {
                    console.error(errResp);
                }
             );

        return deferred.promise;

}

function updateMilitaryService(element){

        var deferred=$q.defer();

        $http.put(REST_SERVICE_URI+'MilitaryService/updateHerbiMukellefiyyet',element)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (errResp) {
                    console.error(errResp);
                }
            );

        return deferred.promise;

}

    //  *************************************** end military service*******************************//





}]);