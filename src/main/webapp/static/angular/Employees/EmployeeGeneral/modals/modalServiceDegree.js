'use strict';

angular.module('appEmployeeGeneral').controller('modalServiceDegree', ['$scope', '$uibModalInstance', 'employeeGeneralService', function ($scope, $uibModalInstance, employeeGeneralService) {

    $scope.successDegreeHistory = {createDate: '', orderNo: '', empId: '', successDegree: {id: null, name: ''}};

    $scope.successDegree = {id: null, name: ''};

    var update = employeeGeneralService.getObject().update;
    $scope.empId = employeeGeneralService.getObject().empId;

    employeeGeneralService.getSuccessdegreeList()
        .then(
            function (type) {

                $scope.successDegreeList = type;

            },
            function (errResponse) {
                console.error('Error while fetching getSuccessdegreeList');
            }
        );

    if (update == true) {
        $scope.successDegreeHistory = employeeGeneralService.getObject().object;

        $scope.successDegree = $scope.successDegreeHistory.successDegree;


        console.log('successDegreeHistory', $scope.successDegreeHistory);
    }

    else {
        $scope.successDegreeHistory = {createDate: '', orderNo: '', empId: '', successDegree: {id: null, name: ''}};
    }

    function addServiceDegree(object) {

        employeeGeneralService.addServiceDegree(object)
            .then(
                function () {
                },
                function (errResponse) {
                    console.error('Error while fetching elements',errResponse);
                }
            );
    }

    function updateServiceDegree(object) {

        employeeGeneralService.updateServiceDegree(object)
            .then(
                //  getEmployeeGeneral($scope.empId),

                function (errResponse) {
                    console.error('update is failed', errResponse);
                }
            );
    }

    $scope.submit = function () {

        $scope.successDegreeHistory.successDegree = $scope.successDegree;

        if (update === true) {

            console.log(' $scope.successDegreeHistory', $scope.successDegreeHistory);

            updateServiceDegree($scope.successDegreeHistory);
        }

        else {
            $scope.successDegreeHistory.empId= $scope.empId;
            addServiceDegree($scope.successDegreeHistory);

        }
        $uibModalInstance.close();

    };

    $scope.cancel = function () {

        $uibModalInstance.dismiss('cancel');
    };


}]);