
'use strict';

angular.module('appEmployeeGeneral').controller('modalEmployeeRank', ['$scope', '$uibModalInstance', 'employeeGeneralService',  function ($scope, $uibModalInstance, employeeGeneralService) {


    $scope.rank=  {name: '',id:''};

    $scope.cmbsService={id:'', name:'', description:''};

    $scope.employeeRank={id:'',empId:'',rank:{name: '',id:''},createDate:'',rankOutOfTurn:'', orderNo:'',

    cmbsService:{id:'', name:'', description:''}};

    var update=employeeGeneralService.getObject().update;



    $scope.getRankService  = employeeGeneralService.getRankService()
        .then(
            function (type) {
                $scope.cmbsServiceList=type;

             //   console.log('$scope.rankServiceList',$scope.cmbsServiceList);
            },
            function (errResponse) {
                console.error('Error while fetching ContactInfo');
            }
        );
    $scope.getRank = employeeGeneralService.getRankList()
        .then(
            function (type) {
                $scope.rankList=type;

        console.log('type',type);

                            },
            function (errResponse) {
                console.error('Error while fetching ContactInfo');
            }
        );


    if (update==true)
    {

        $scope.employeeRank=employeeGeneralService.getObject().object;

        $scope.cmbsService=$scope.employeeRank.cmbsService;

        $scope.rank=$scope.employeeRank.rank;

        if ( $scope.employeeRank.rankOutOfTurn === "1" ) {

           $scope.rankOutOfTurn=true;

        }

    }

    else
    {

        $scope.employeeRank={id:'',empId:'',rank:{name: '',id:''},createDate:{},rankOutOfTurn:'', c:'', cmbsService:{id:'', name:'', description:''}};

    }


    function add(element){
        console.log(element);

        employeeGeneralService.createEmployeeRank(element)
            .then(
                function () {
console.log('add method invoke ',element);
                },
                function (errResponse) {
                    console.error('Error while fetching elements',errResponse);
                }
            );
    }

  function updateRank(object) {

      employeeGeneralService.updateRank(object)
          .then (
              function () {

              },
              function(errResponse) {
                  console.error('update is failed')
              }
          );
  }

    $scope.submit = function () {

        console.log('update ',update );

      if (update===false) {

        $scope.employeeRank.empId=employeeGeneralService.getObject().empId;

          $scope.employeeRank.rank=$scope.rank;

          $scope.employeeRank.cmbsService=$scope.cmbsService;

          if ($scope.rankOutOfTurn==true) {

              $scope.employeeRank.rankOutOfTurn = 1;

          }
          else
              $scope.employeeRank.rankOutOfTurn = 0;

          add($scope.employeeRank);

    //  $uibModalInstance.close();

      }
      else {

          if ($scope.rankOutOfTurn==true) {

              $scope.employeeRank.rankOutOfTurn = 1;
          }
          else
              $scope.employeeRank.rankOutOfTurn = 0;

          $scope.employeeRank.rank=$scope.rank;

          $scope.employeeRank.cmbsService=$scope.cmbsService;

              updateRank($scope.employeeRank);

              console.log('emprank ',$scope.employeeRank );

       employeeGeneralService.getEmployeeGeneral(employeeGeneralService.getObject().object.empId );
      }


   $uibModalInstance.close();
    };

    $scope.cancel = function () {

        $uibModalInstance.dismiss('cancel');
    };


}]);



