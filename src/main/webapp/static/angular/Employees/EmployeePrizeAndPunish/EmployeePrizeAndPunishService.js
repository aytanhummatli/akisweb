'use strict';

angular.module('appEmployeePrizeAndPunish').factory('EmployeePrizeAndPunishService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:8060/';


     var factory = {

        getEmployeePrizeAndPunish:getEmployeePrizeAndPunish,
        deletePrizeOrPunish:deletePrizeOrPunish,


        getObject:getObject,
        setObject:setObject

                    };
    return factory;

    var myObject={};

    function  getObject() {

        return myObject;

    }

    function setObject(value) {

        myObject =value ;

    }

    function  getEmployeePrizeAndPunish(empId) {

    var deferred = $q.defer();

    $http.get(REST_SERVICE_URI+'Employee/getEmployeePrizeAndPunish/'+ empId)
        .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){

                console.error('Error while fetching data');

                deferred.reject(errResponse);
            }
        );
    return deferred.promise;
}
    function deletePrizeOrPunish(elementId) {

        var deferred = $q.defer();

        $http.put(REST_SERVICE_URI + 'prizeOrPhunish/deletePrizeOrPhunish/'+elementId)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.error(reason);
                }
            );

        return deferred.promise;
    }

}]);