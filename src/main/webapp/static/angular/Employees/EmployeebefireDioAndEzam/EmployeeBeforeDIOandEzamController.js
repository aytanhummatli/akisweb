'use strict';

angular.module('appEmployeeBeforeDIOandEzam').controller('EmployeeBeforeDIOandEzamController', ['$window', '$scope', '$http', '$sce', '$uibModal', 'EmployeeBeforeDIOandEzamService', function ($window, $scope, $http, $sce, $uibModal, EmployeeBeforeDIOandEzamService) {

    //for ezam
   // $scope.empId =1340000464324;

   // beforeDio
     $scope.empId =810000024694;

    $scope.employee ={

        workMissions:[{ id: null, missionOrderDate: '', missionOrder: '', endDate: null,missionTermLess: null,

            createDate:'',

            structureName: {id: 0,  fullName:''}

        }],
        workActivityBefore:[{
            enterDate: '',
            enterDateYear: '',
            enterOrder: '',
            exitDate: null,
            exitDateYear: null,
            exitOrder: '',
            fullStrName: null,
            id: null,
            labourCode: {name: null, description: ''},
            positionName: {id: null, name: ''},
            workEndDate: null,
            workStartDate: null,

        }]



    };

    //Employee list type of Employee object
    $scope.employees = [$scope.employee];

    $scope.getEmployeeBeforeDIOandEzam = getEmployeeBeforeDIOandEzam;

    getEmployeeBeforeDIOandEzam($scope.empId);

    function getEmployeeBeforeDIOandEzam(empId) {

        EmployeeBeforeDIOandEzamService.getEmployeeBeforeDIOandEzam(empId)
            .then(
                function (d) {
                    $scope.employee  = d;
                    console.log('getEmployeeDetails results d=',d);
                    console.log('getEmployeeDetails results $scope.employee',$scope.employee );
                },
                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }

  ///delete

    function deleteWorkMission(elementID) {

        EmployeeBeforeDIOandEzamService.deleteWorkMission(elementID)
            .then(
                function (d) {

                },
                function (errResponse) {

                }
            );
    }


    $scope.removeWorkMission = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';

        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: ModalInstanceCtrlWorkMission
            }
        );
        modalInstance.result.then(function () {
            deleteWorkMission(id);
            getEmployeeBeforeDIOandEzam($scope.empId);
        });
    };

    var empId = $scope.empId;

    var ModalInstanceCtrlWorkMission = function ($scope, $uibModalInstance) {

        $scope.ok = function () {

            //     getEmployeeEducation(empId);

            $uibModalInstance.close();
        };

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');
        };
    };

///////// delete

    function deleteWorkActivityBefore(elementID) {

        EmployeeBeforeDIOandEzamService.deleteWorkActivityBefore(elementID)
            .then(
                function (d) {

                },
                function (errResponse) {

                }
            );
    }



    $scope.removeWorkActivityBefore = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';

        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: ModalInstanceCtrlWorkActivityBefore
            }
        );
        modalInstance.result.then(function () {
            deleteWorkActivityBefore(id);
            getEmployeeBeforeDIOandEzam($scope.empId);
        });
    };

    var empId = $scope.empId;

    var ModalInstanceCtrlWorkActivityBefore = function ($scope, $uibModalInstance) {

        $scope.ok = function () {

            //  getEmployeeEducation(empId);

            $uibModalInstance.close();
        };

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');
        };
    };



}]);

