'use strict';

angular.module('appEmployeeBeforeDIOandEzam').factory('EmployeeBeforeDIOandEzamService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:8060/';


     var factory = {

        getEmployeeBeforeDIOandEzam:getEmployeeBeforeDIOandEzam,
         deleteWorkMission:deleteWorkMission,
         deleteWorkActivityBefore:deleteWorkActivityBefore,

        getObject:getObject,
        setObject:setObject
                    };
    return factory;

    var myObject={};


    function  getObject() {

        return myObject;

    }

    function setObject(value) {

        myObject =value ;

    }

    function  getEmployeeBeforeDIOandEzam(empId) {

    var deferred = $q.defer();

    $http.get(REST_SERVICE_URI+'Employee/getEmployeeBeforeDIOandEzam/'+ empId)
        .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){

                console.error('Error while fetching data');

                deferred.reject(errResponse);
            }
        );
    return deferred.promise;
}
    function deleteWorkMission(elementId) {

        var deferred = $q.defer();

        $http.put(REST_SERVICE_URI + 'workMission/deleteWorkMission/'+elementId)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.error(reason);
                }
            );

        return deferred.promise;
    }

    function deleteWorkActivityBefore(elementId) {

        var deferred = $q.defer();

        $http.put(REST_SERVICE_URI + 'workActivityBefore/deleteWorkActivityBefore/'+elementId)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.error(reason);
                }
            );

        return deferred.promise;
    }
}]);