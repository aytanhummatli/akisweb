'use strict';

angular.module('appEmployee').factory('employeService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:8060/Employee/';


    var factory = {

        getData:getData,
        Data:Data,
        getObject:getObject,
        setObject:setObject,
        setDataToClass:setDataToClass,
         getEmployeeInfoPrintById:getEmployeeInfoPrintById

                    };
    return factory;

    var myObject={};


    function  getObject() {

        return myObject;

    }

    function setObject(value) {

        myObject =value ;

    }


    function  setDataToClass(empId) {

        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI+'setEmployeeId/'+empId)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){

                    console.error('Error while fetching data');

                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function  Data(parentId) {
        console.log('parentId'+parentId);

        var deferred = $q.defer();

        $http.get('http://localhost:8060/StructureMap/'+'getParents/'+parentId)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                    console.log('response.data'+response.data);
                },
                function(errResponse){
                    console.log('http://localhost:8060/StructureMap/'+'getParents/'+parentId);
                    console.error('Error while fetching trees');
                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;

    }
    function  getData(myObj) {

    console.log('i am in getData from services',myObj.ed_surname);

    var deferred = $q.defer();

    $http.get(REST_SERVICE_URI+'get_employees/'+ myObj.st_id+'/'+myObj.ed_surname+'/'+myObj.ed_patron+'/'+myObj.ed_name+'/'+myObj.pageno+'/'+myObj.itemsPerPage)
        .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){

                console.error('Error while fetching data');

                deferred.reject(errResponse);
            }
        );
    return deferred.promise;
}




// ******************************************************************************************************************

    function getEmployeeInfoPrintById(empId) {

        var def=$q.defer();
        $http.get(REST_SERVICE_URI + 'getEmployeeInfoPrintById/'+empId)
            .then(
                function (value) {
                    console.log('function value ')
                    def.resolve(value.data);
                    console.log('value.data :   ',value.data)
                },function (reason) {
                    def.reject(reason);
                    console.log('erro while get employee ref by id: ',reason )
                }
            );
        return def.promise;
    }

}]);