'use strict';

angular.module('appEmployeeOther').controller('employeeOtherController', ['$window', '$scope', '$http', '$sce', '$uibModal', 'EmployeeOtherService', function ($window, $scope, $http, $sce, $uibModal, EmployeeOtherService) {

    // for
    $scope.empId=80000002160;

    // for cource
    //$scope.empId=350000000565;

    // for war
//    $scope.empId = 500000005200;


    $scope.employee = {

        employeeWorkBook: {checked:'',sendDate: null, note2: {id:'',description:''}, note1: {id:'',description:''}, isReceipt: '', envelopNumber: null, id: ''},

        image: {id: null, employee: null, addDate: null, imgURL: null, ftpFolder: null},

        lastFormFillDate: {id: null, fillDate: ''},

        policeCardNew: {
            weight: '',
            validity: '',
            showGender: null,
            height: '',
            blood: '',
            eyeColor: '',
            armSerialNo: '',
            givenDate: ''
        },

        qualificationCourses: [{courseName: '', finishDate: '', beginDate: '', id: ''}],

        warActivities: [{warName: '', beginDate: '', endDate: '', id: ''}]
    };

    $scope.employees = [$scope.employee];

    $scope.note1={id:'',description:''};

    $scope.note2={id:'',description:''};

    $scope.getEmployeeOther = getEmployeeOther;

    getEmployeeOther($scope.empId);

    getnote1();

    getnote2();

    getEyeColor();

    getBloodGroup();

    function getEmployeeOther(empId) {

        EmployeeOtherService.getEmployeeOther(empId)
            .then(
                function (d) {
                    $scope.employee = d;

                    $scope.note1 = $scope.employee.employeeWorkBook.note1;

                    $scope.note2 = $scope.employee.employeeWorkBook.note2;


                   if ($scope.employee.employeeWorkBook.checked==1){

                       $scope.checked=true;
                   }

                   else {  $scope.checked=false; }

                    if ($scope.employee.employeeWorkBook.isReceipt==1){

                        $scope.isReceipt=true;
                    }

                    else {  $scope.isReceipt=false; }



                    console.log('getEmployeeDetails $scope.note1=', $scope.note1);

                    console.log('getEmployeeDetails results d=', d);

                    console.log('getEmployeeDetails results $scope.employee', $scope.employee);

                },

                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }

    function getnote1() {

        var note1Num=38;

        EmployeeOtherService.getCombo(note1Num)
            .then(
                function (d) {
                    $scope.note1List = d;

                    console.log('$scope.note1List =  ', $scope.note1List);
                },
                function (errResponse) {
                    console.error('Error while fetching details');
                }
            );

            }

    function getnote2() {

        var note2Num=37;

        EmployeeOtherService.getCombo(note2Num)
            .then(
                function (d) {
                    $scope.note2List = d;

                    console.log('$scope.note2List =  ', $scope.note2List);
                },
                function (errResponse) {
                    console.error('Error while fetching details');
                }
            );
    }


    function getEyeColor() {

        EmployeeOtherService.getCombo(27)
            .then(
                function (d) {
                    $scope.eyeColorList = d;
                },
                function (errResponse) {
                    console.error('Error while fetching details');
                }
            );
    }

    function getBloodGroup() {

        EmployeeOtherService.getCombo(25)
            .then(
                function (d) {
                    $scope.bloodGroupList = d;
                },
                function (errResponse) {
                    console.error('Error while fetching details');
                }
            );
    }

    function deleteQualificationCourse(elementID) {

        EmployeeOtherService.deleteQualificationCourse(elementID)
            .then(
                function (d) {
                },
                function (errResponse) {
                }
            );
    }

    $scope.removeQualificationCourse = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';

        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: ModalInstanceCtrlQualificationCourse
            }
        );
        modalInstance.result.then(function () {
            deleteQualificationCourse(id);
            getEmployeeOther($scope.empId);
        });
    };

    var empId = $scope.empId;

    var ModalInstanceCtrlQualificationCourse = function ($scope, $uibModalInstance) {

        $scope.ok = function () {

            //  getEmployeeEducation(empId);
            // getEmployeePrizeAndPunish(empId);
            $uibModalInstance.close();
        };

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');
        };
    };


    function deleteWarActivity(elementID) {

        EmployeeOtherService.deleteWarActivity(elementID)
            .then(
                function (d) {

                },
                function (errResponse) {

                }
            );
    }

    $scope.removeWarActivity = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';

        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: ModalInstanceCtrlWarActivity
            }
        );
        modalInstance.result.then(function () {
            deleteWarActivity(id);
            getEmployeeOther($scope.empId);
        });
    };

    var empId = $scope.empId;

    var ModalInstanceCtrlWarActivity = function ($scope, $uibModalInstance) {

        $scope.ok = function () {

            //  getEmployeeEducation(empId);

            // getEmployeePrizeAndPunish(empId);

            $uibModalInstance.close();
        };

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');
        };
    };

}]);

