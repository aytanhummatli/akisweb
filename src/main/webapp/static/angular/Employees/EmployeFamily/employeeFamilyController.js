'use strict';

angular.module('appEmployeeFamily').controller('employeeFamilyController', ['$window', '$scope', '$q','$http', '$sce', '$uibModal', 'employeeFamilyService', function ($window, $scope,$q, $http, $sce, $uibModal, employeeFamilyService) {


    $scope.empId = 1320000000242;

    $scope.employee = {

        relatives: [{

            id: null, workPlace: '', bornYear: '', livingAddressNote: null,

            livingAddress: {id: null, adress_fullname: ''},

            person: {

                id: null, name: '', surName: '', patronymic: '', birthDate: null, maidenName: '',

                bornAddress: {id: null, adress_fullname: ''}

            },

            relativeDegree: {id: '', description: ''}
        }]
    };

    //Employee list type of Employee object
    $scope.employees = [$scope.employee];

    $scope.getEmployeeFamily = getEmployeeFamily;

    getEmployeeFamily($scope.empId);

    function getEmployeeFamily(empId) {

        employeeFamilyService.getEmployeeFamily(empId)
            .then(
                function (d) {
                    $scope.employee = d;
                    console.log('getEmployeeDetails results d=', d);
                    console.log('getEmployeeDetails results $scope.employee', $scope.employee);
                },
                function (errResponse) {

                    console.error('Error while fetching details');
                }
            );
    }


    $scope.addRelative = function (scope) {

        var myObj = {};

        // myObj.object=scope.info;

        myObj.update = false;

        employeeFamilyService.setObject(myObj);

        var modalInstance = $uibModal.open({

            templateUrl: 'addRelative.html',
            controller: 'modalEmployeeFamily',
            backdrop: 'static'
        });

        modalInstance.result.then(
            function (value) {

            });
    }

    $scope.updateRelative = function (scope) {

        console.log('scope', scope);

        var myObj = {};

        myObj.object = scope.inf;

        myObj.update = true;

        employeeFamilyService.setObject(myObj);

        var modalInstance = $uibModal.open({

            templateUrl: 'updateRelative.html',
            controller: 'modalEmployeeFamily',
            backdrop: 'static'

        });

        modalInstance.result.then(
            function (value) {
            });
    }

    function clear(element) {
        element = '';
    }

    function deleteRelative(elementID) {

        employeeFamilyService.deleteRelative(elementID)
            .then(
                function (d) {

                },
                function (errResponse) {

                }
            );
    }

    $scope.removeRelative = function (id) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';

        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: ModalInstanceCtrlRelative
            }
        );
        modalInstance.result.then(function () {
            deleteRelative(id);
        });
    };



    var empId = $scope.empId;

    var ModalInstanceCtrlRelative = function ($scope, $uibModalInstance) {

        $scope.ok = function () {

            getEmployeeFamily(empId);

           $uibModalInstance.close();
        };

        $scope.cancel = function () {

            $uibModalInstance.dismiss('cancel');
        };
    };

}]);