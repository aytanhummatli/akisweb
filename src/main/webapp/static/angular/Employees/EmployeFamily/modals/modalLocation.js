'use strict';

  angular.module('appEmployeeFamily').controller('modalLocation', ['$scope', '$uibModalInstance','$uibModal','employeeFamilyService', function ($scope, $uibModalInstance,$uibModal, employeeFamilyService) {

      $scope.getCountryList=getCountryList;
      $scope.getRegionList=getRegionList;
      $scope.getSubRegionList=getSubRegionList;
      $scope.getVillageList=getVillageList;
      $scope.getStreetList=getStreetList;

      $scope.mylisyList={id:'',name:''};

      function getCountryList( ){

          employeeFamilyService.getCountryList()
              .then(
                  function (value) { $scope.mylisyList=value;},

                  function (reason) { console.error(reason) }
              ); }

      function getRegionList( ){

          console.log('i am in modalLocation contr from getRegionList' );

          employeeFamilyService.getRegionList()
              .then(
                  function (value) { $scope.mylisyList=value;
                      console.log('$scope.mylisyList',$scope.mylisyList);},

                  function (reason) { console.error(reason) }
              ); }
      function getSubRegionList(){

          employeeFamilyService.getSubRegionList()
              .then(
                  function (value) { $scope.mylisyList=value;},

                  function (reason) { console.error(reason) }
              ); }

      function getVillageList(){

          employeeFamilyService.getVillageList()
              .then(
                  function (value) {
                      console.log('village ', value);
                      $scope.mylisyList=value;
                    //  console.log('village ', $scope.mylisyList);
                      },

                  function (reason) { console.error(reason) }
              ); }

      function getStreetList (){

          employeeFamilyService.getStreetList()
              .then(
                  function (value) { $scope.mylisyList=value;


                  },

                  function (reason) { console.error(reason) }
              ); }

      $scope.submit = function () {

        $uibModalInstance.close();

         };

       $scope.cancel = function () {

       $uibModalInstance.dismiss('cancel');

      };


}]);



