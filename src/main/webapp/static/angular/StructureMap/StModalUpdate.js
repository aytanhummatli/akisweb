'use strict';


angular.module('appStructure').controller('StructureUpdateModalController', ['$scope', '$uibModalInstance', 'StructureMapservice',  function ($scope, $uibModalInstance, StructureMapservice) {

    $scope.elements = [];

    $scope.element={id:null,title:'',organType:{id:null,name:''}};

    $scope.organType={id:null,name:''};

    $scope.OrganTypes=[$scope.organType];

    $scope.parents=[];

    $scope.parent={};


    init();

    function init() {

        var myObj={};

        myObj= StructureMapservice.getObject();

        console.log('i am in modal ',myObj);

        $scope.OrganTypes = StructureMapservice.getOrgantypes().then(

            function (data) {

                $scope.OrganTypes=data;

                $scope.element.title = myObj.$modelValue.title;

                $scope.organType.name=myObj.$modelValue.organType.name;

                $scope.element.text=myObj.$modelValue.text;

                $scope.element.upperStr=myObj.$modelValue.upperStr;

                $scope.element.suffix=myObj.$modelValue.suffix;

            },

            function (errResponse) {

                console.error('Error while fetching Organtypes');

            }

        );


  }


  function collectFields() {

      var myObj= StructureMapservice.getObject();

      myObj.$modelValue.title = $scope.element.title  ;

      myObj.$modelValue.organType= $scope.organType;

      myObj.$modelValue.text = $scope.element.text;

      myObj.$modelValue.upperStr= parseInt($scope.element.upperStr);

      myObj.$modelValue.suffix= $scope.element.suffix;

      StructureMapservice.setObject(myObj);

  }

    $scope.submit = function () {

       collectFields();

        $uibModalInstance.close();
    };

    $scope.cancel = function () {

        $uibModalInstance.dismiss('cancel');
    };


}]);



