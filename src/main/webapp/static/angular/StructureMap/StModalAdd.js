'use strict';

angular.module('appStructure').controller('StructureAddModalController', ['$scope', '$uibModalInstance', 'StructureMapservice',  function ($scope, $uibModalInstance, StructureMapservice) {

    $scope.elements = [];
    $scope.element={id:null,title:''};

    $scope.parents=[];
    $scope.parent={};

    $scope.organType={id:null,name:''};

    $scope.OrganTypes=[$scope.organType];

    init();

    function init() {

        $scope.OrganTypes = StructureMapservice.getOrgantypes().then(

            function (data) {
                $scope.OrganTypes=data;

            },

            function (errResponse) {

                console.error('Error while fetching Organtypes');
            }
        );
    }

function add(){

    var myObj= StructureMapservice.getObject();

   //  preparing new node for insert

    var newNode={};

    newNode.title= $scope.element.title;

    newNode.text= $scope.element.text;

    newNode.upperstr= parseInt( $scope.element.upperStr);

    newNode.suffix=$scope.element.suffix;

    newNode.organType=$scope.organType;

    newNode.parentId=myObj.$modelValue.id;

      // add new node to our nodelist

    if ( myObj.$modelValue.nodes== null) {
        myObj.$modelValue.nodes=[];
    }

      myObj.$modelValue.nodes.push(newNode);

    StructureMapservice.setObject(myObj);

        console.log('myObj from add function',myObj);

}
    $scope.submit = function () {


        add();

        $uibModalInstance.close();

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


}]);



