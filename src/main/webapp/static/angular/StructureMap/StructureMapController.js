'use strict';

angular.module('appStructure').controller('StructureMapController', ['$sce','$timeout', '$window', '$scope', '$uibModal', '$http', '$q', '$log', 'StructureMapservice', function ($sce,$timeout, $window, $scope, $uibModal, $http, $q, $log, StructureMapservice) {

    $scope.search = search;
    $scope.clear = clear;
    $scope.detailedSearch = detailedSearch;
    $scope.elementForAdd = {};
    $scope.togglee = togglee;
    $scope.parentList = [];
    $scope.data = [$scope.myObject];
    $scope.myObject = {id: '', title: '', parentId: '', nodes: {}};

    Data(1500000001);

    $scope.visible = function (item) {

        return !($scope.query && $scope.query.length > 0

            && item.title.indexOf($scope.query) == -1);
    };

    $scope.findNodes = function () {

    };

    $scope.removee = function (scope) {

        var message = "Seçilmiş elementi silmək istəyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';
        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
            template: modalHtml,
            controller: ModalInstanceCtrl
        });

        modalInstance.result.then(function () {

            scope.remove();

            var nodeData = scope.$modelValue;

            StructureMapservice.removeElement(nodeData.id)
                .then(
                    function () {

                    },
                    function (errResponse) {

                        console.error('Error while delete element' + errResponse);

                    }
                );
        });

    };

    var ModalInstanceCtrl = function ($scope, $uibModalInstance) {
        $scope.ok = function () {

            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    };

    $scope.addElement = function (scope) {

        var nodes = scope.$modelValue.nodes;

        if (nodes != null) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].id == 1) {
                    nodes.splice(i, 1);
                }
            }
        }
        scope.$modelValue.nodes = nodes;

        StructureMapservice.setObject(scope);

        var modalInstance = $uibModal.open({
            templateUrl: 'addModalContent.html',
            controller: 'StructureAddModalController',
            backdrop: 'static'
        });

        modalInstance.result.then(function () {

             scope = StructureMapservice.getObject();

            console.log('scope from add',scope.$modelValue.nodes);

            console.log('scope.$modelValue.nodes[0]',scope.$modelValue.nodes[0]);

             scope.$modelValue.nodes=  scope.$modelValue.nodes.reverse();
            console.log('scope from add after reverse',scope.$modelValue.nodes);
             StructureMapservice.addElement( scope.$modelValue)
                .then(
                    function (id) {

                        console.log('new id',id);

                        scope.$modelValue.nodes[0].id=id;

                        togglee(scope);

                        if (scope.collapsed) {

                            scope.toggle();
                        }
                    },

                    function (errResponse) {

                        console.error('Error while creating elements');
                    }
                );


        });

    };

    $scope.updateElement = function (scope) {

        StructureMapservice.setObject(scope);

        console.log('i am in updateelement from stcont', scope);

        var modalInstance = $uibModal.open({
            templateUrl: 'updateModalContent.html',
            controller: 'StructureUpdateModalController',
            backdrop: 'static'
        });

        modalInstance.result.then(function () {


            var newElem=StructureMapservice.getObject();

            StructureMapservice.updateElement(newElem.$modelValue)
                .then(
                    function () {
                        scope = StructureMapservice.getObject();
                    },
                    function (errResponse) {

                        console.error('Error while update element' + errResponse);

                    }
                );

        });

    };

    $scope.CustomCollapse = function (scope) {

    }

    function togglee(scope) {

        scope.toggle();

        if (!scope.collapsed) {

            var nodeData = scope.$modelValue;

            var childList = nodeData.nodes;

            if (childList != null && childList.length < 2) {

                StructureMapservice.Data(nodeData.id)
                    .then(
                        function (d) {
                            nodeData.nodes = d;

                            console.log('d=', d);
                        },

                        function (errResponse) {
                            console.error('Error while fetching Tree');

                        }
                    );

            }
        }
        $log.log("Our Promise has finished");

    };

    // $scope.togglee = function (scope) {
    //
    //
    // scope.toggle();
    //
    // if (!scope.collapsed){
    //
    //     var nodeData = scope.$modelValue;
    //
    //
    //
    //     var childList=nodeData.nodes;
    //
    //     if (childList != null && childList.length<2  ) {
    //
    //         nodeData.nodes = getChilds(nodeData.id);
    //
    //         console.log('i am newSubItem functionnn childList=', $scope.myChildList);
    //     }
    //
    // }
    //
    // };

    $scope.moveLastToTheBeginning = function () {

        var a = $scope.data.pop();

        $scope.data.splice(0, 0, a);
        console.log('i am moveLastToTheBeginning functionnn coll value', a);

    };

    $scope.newSubItem = function (scope) {

        var nodeData = scope.$modelValue;
        $scope.elementForAdd = nodeData;
        $scope.myValue = nodeData.title;


        // nodeData.nodes.push({
        //     id: nodeData.id * 10 + nodeData.nodes.length,
        //     title: nodeData.title + '.' + (nodeData.nodes.length + 1),
        //     nodes: []
        // });
        //  console.log('nodeData',nodeData);
    };

    $scope.addStructure = function () {

        var myElement = $scope.elementForAdd;

        myElement.nodes.push({
            id: nodeData.id * 10 + nodeData.nodes.length,
            title: $scope.myValue,
            nodes: []
        });
        console.log('nodeData', nodeData);
    }

    $scope.collapseAll = function () {
        $scope.$broadcast('angular-ui-tree:collapse-all');
        console.log('i am collapseAll functionnn')
    };

    $scope.expandAll = function () {
        $scope.$broadcast('angular-ui-tree:expand-all');
        console.log('i am expandAll functionnn')
    };

    function collapseall() {
        $scope.$broadcast('angular-ui-tree:collapse-all');
    }

    function Data(x) {

        StructureMapservice.Data(x)
            .then(
                function (d) {

                    $scope.data = d;

                },

                function (errResponse) {
                    console.error('Error while fetching Tree');
                }
            );

    }


    function getChilds(x) {
        console.log('i am data functionnn');
        var deferred = $q.defer();
        console.log('jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj ' + x);
        StructureMapservice.Data(x)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                    //$scope.myChildList=d;

                },

                function (errResponse) {
                    console.error('Error while fetching Tree');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function mySpilitFunction(str) {

        res = null;
        if (str != null && str != undefined) {
            var res = str.split("*");

            res.pop();
         //   console.log('myarray', res);
            res.reverse();
        }

        return res;
    }

     function detailedSearch(givenScope) {

        // console.log('givenScope ',givenScope );
        //
        // console.log('$scope.data',$scope.data);

        var  myList= $scope.data;

        var givenId=givenScope.$modelValue.id;

        var givenFullName=givenScope.$modelValue.fullName;

        var found=false,newNodes=[];

        $scope.parentList=mySpilitFunction(givenFullName);

        console.log('givenId=', givenId);

        var  myList= $scope.data;

        angular.forEach($scope.parentList,function (value,key) {

         found=false;

         for(var i=0; i< myList.length;i++){

              if (myList[i].id == parseInt(value)){

                  found=true;

                  if (myList[i].id==givenId){

                      console.log('searched element=', myList[i].title);

                        break;
                  }
                      if (myList[i].nodes==null){
                          console.log('1111111111111111111' );
                          break;}

                    else   if (myList[i].childCount>0){

                          console.log('2222222222222222222' );

                          var childId=myList[i].id;

                          console.log('myList[i] ',myList[i]);

                          Data(childId).then(

                              function ( ) {

                          })

                    StructureMapservice.Data(childId)
                          .then(
                              function (d) {
                                  console.log(' before d',d);
                                  newNodes= d;
                                  console.log('new nodes ',d);
                                  myList[i].nodes=[];

                                  myList[i].nodes=newNodes;
                                  myList= myList[i].nodes;
                              },

                              function (errResponse) {
                                  console.error('Error while fetching elements');
                              }
                          );

                       //     myList[i].nodes=[];
                       //
                       // myList[i].nodes=newNodes;
                }

              // else {
              //         myList= myList[i].nodes;
              //       }
              }
      }
         if ( found==false) {
             console.log('3333333333333333' );
           var newObject={};

              StructureMapservice.getElementById(value)
                  .then(
                      function (d) {
                          console.log('444444444444' );
                          newObject = d;
                          console.log('55555555555' ,d);
                          myList=[];

                          myList.push(newObject);

                      },

                      function (errResponse) {
                          console.error('Error while fetching elements');
                      }
                  );
          }
            console.log('666666666666666666',myList );
      })

    }

    function search() {

          searchresult($scope.myQuery);

        // $scope.query=$scope.myQuery;
        // console.log('$scope.input_search', $scope.query);

    }

    function searchresult(searchValue) {

        console.log('searchValue', searchValue);

        StructureMapservice.searchResults(searchValue)
            .then(
                function (d) {

                    $scope.searchResult = d;

                    console.log('  $scope.searchResult',   $scope.searchResult);
                },

                function (errResponse) {

                    console.error('Error while fetching elements');
                }
            );
    }

    function clear() {

        console.log('i am from clear function');

        $scope.query = "";


        // var getData=  $scope.data;
        // var myObj={
        //       'id': 1,
        //         'title': 'yeniiiii',
        //         'nodes': [
        //
        //                 ]
        //             };
        //
        // getData.push(myObj);

    }

    function showSearchResults(node, object) {

        var myList=[];
        //
        // myList=scope.data;
        //
        // foreach ()
        //
        //
        // fullname = node.fullName;

        // console.log('node=', node);
        //
        // $scope.parentList = mySpilitFunction(fullname);
        //
        // for (var i = 0; i < $scope.parentList.length; i++) {
        //
        //     if (object[i].id === parseInt($scope.parentList[i])) {
        //
        //     }
        //
        //     else {
        //         object[i].push(node);
        //
        //     }
        //
        // }

    }

     $scope.chooseAction =function(scope){

         var modalInstance = $uibModal.open({
             templateUrl: 'doubleClick.html',
             controller: 'chooseActionModal',
             backdrop: 'static'
         });

         modalInstance.result.then(function () {

        console.log('sdfff');

         });

     }


    // $scope.data = [{
    //     'id': 1,
    //     'title': 'node1',
    //     'nodes': [
    //         {
    //             'id': 11,
    //             'title': 'node1.1',
    //             'nodes': [
    //                 {
    //                     'id': 111,
    //                     'title': 'node1.1.1',
    //                     'nodes': []
    //                 }
    //             ]
    //         },
    //         {
    //             'id': 12,
    //             'title': 'node1.2',
    //             'nodes': []
    //         }
    //     ]
    // }, {
    //     'id': 2,
    //     'title': 'node2',
    //     'nodes': [
    //         {
    //             'id': 21,
    //             'title': 'node2.1',
    //             'nodes': []
    //         },
    //         {
    //             'id': 22,
    //             'title': 'node2.2',
    //             'nodes': []
    //         }
    //     ]
    // }, {
    //     'id': 3,
    //     'title': 'node3',
    //     'nodes': [
    //         {
    //             'id': 31,
    //             'title': 'node3.1',
    //             'nodes': []
    //         }
    //     ]
    // }, {
    //     'id': 4,
    //     'title': 'node4',
    //     'nodes': [
    //         {
    //             'id': 41,
    //             'title': 'node4.1',
    //             'nodes': []
    //         }
    //     ]
    // }];
    // ///

    //////////////////////////// StaffSection //////////////////////////////////////////////////////

    $scope.staff={full_name:''};

    $scope.staffList=[ $scope.staff];

    $scope.getEmployees= function(scope){
        console.log( 'scope', scope);
      var  nodeId,structureNameId;
        $scope.staffList=[];
          nodeId=scope.$modelValue.id;

          structureNameId=scope.$modelValue.structureNameId;

        var deferred = $q.defer();

        console.log(scope.$modelValue);

        StructureMapservice.getEmployees(scope.$modelValue)
            .then(
                function (d) {

                    $scope.staffList=d;

                    console.log(' $scope.staffList', $scope.staffList);

                },

                function (errResponse) {

                    console.error('Error while fetching Tree');

                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;

    }

   $scope.selected=function(scope){

    }

}]);


angular.module('appStructure').config(function (treeConfig) {
    treeConfig.defaultCollapsed = true; // collapse nodes by default
    treeConfig.appendChildOnHover = false; // append dragged nodes as children by default
});

