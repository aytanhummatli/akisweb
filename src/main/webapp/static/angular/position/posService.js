'use strict';

angular.module('positionApp').factory('PositionService',['$http','$q',function ($http,$q) {

    var REST_SERVICE_URL='http://localhost:8060/';

    var factory={
        getPositionList:getPositionList,
        createPosition:createPosition,
        updatePosition:updatePosition,
        deletePosition:deletePosition,
        getCategory:getCategory,
        getPositionNamesById:getPositionNamesById,
        setEditId:setEditId,
        getEditId:getEditId
    };

    return factory;


    function getPositionList(pageNumber) {
        var deferred=$q.defer();
        $http.get(REST_SERVICE_URL + 'getPositionList/'+25+'/'+pageNumber)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errorResponse) {
                    console.log('Error'+errorResponse);
                    deferred.reject(errorResponse);
                }
            );
        return deferred.promise;
    }
    function createPosition(hp) {
        var deferred=$q.defer();
        $http.post(REST_SERVICE_URL+'addPosition',hp)
            .then(
                function (response) {
                    deferred.resolve(response.data)
                },
                function (errResponse) {
                    deferred.reject(errResponse)
                    console.log('error servie while create position'+errResponse)
                }
            );
        return deferred.promise;
    }
    function  updatePosition(hp) {
        var deferred=$q.defer();
        $http.put(REST_SERVICE_URL+'updatePosition',hp)
            .then(
                function (response) {
                    deferred.resolve(response.data)
                    console.log('ugurlu in services')
                },
                function (errResponse) {
                    deferred.reject(errResponse)
                    console.log('error services while update position'+errResponse)
                }
            );
        return deferred.promise;
    }

    function deletePosition(id) {
        var deferred=$q.defer();
        $http.post(REST_SERVICE_URL + 'deletePosition/'+id)
            .then(
                function (response) {
                    deferred.resolve(response.data)
                },
                function (errResponse) {
                    deferred.reject(errResponse)
                    console.log('error while delete position'+errResponse)
                }
            );
        return deferred.promise;
    }
    function getCategory (){
        var deferred=$q.defer();
        $http.get(REST_SERVICE_URL +'getPositionGroupNameList')
            .then(
                function (response) {
                    deferred.resolve(response.data)
                },
                function (errResponse) {
                    deferred.reject(errResponse)
                    console.log('error while get category'+errResponse)
                }
            );
        return deferred.promise;
    }

    function getPositionNamesById(id) {
        var deferred=$q.defer();
        console.log('services gethpbyid')
        $http.get(REST_SERVICE_URL +'getPositionNamesById/'+id)
            .then(
                function (response) {
                    console.log('services gethpbyid function ')
                    deferred.resolve(response.data)
                },
                function (errResponse) {
                    console.log('error'+errResponse)
                    deferred.reject(errResponse)
                }
            );
return deferred.promise;
    }
    function setEditId(id) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URL + 'setHpgId/' + id)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while set id');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getEditId() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URL + 'getHpgId/')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while get id hpg');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

}]);