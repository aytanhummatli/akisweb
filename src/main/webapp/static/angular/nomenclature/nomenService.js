'use strict';
angular.module('nomApp').factory('NomService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URL = 'http://localhost:8060/';

    var factory = {
        getList: getList,
        getNomListById: getNomListById,
        getComboList: getComboList,
        getNomNameById: getNomNameById,
        deleteNom: deleteNom,
        createNom: createNom,
        updateNom:updateNom,
        createChildNom:createChildNom,
        deleteChildNom:deleteChildNom
    };

    return factory;

    function getList(pageNumber) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URL + 'getNomenclatureList/' + 25 + '/' + pageNumber)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (err) {
                    deferred.reject(err);
                }
            );
        return deferred.promise;

    }

    function getNomListById(id) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URL + 'getNomListById/' + id)
            .then(
                function (response) {
                    console.log('services response')
                    deferred.resolve(response.data);
                },
                function (reason) {
                    deferred.reject(reason);
                }
            );
        return deferred.promise;
    }

    function getComboList() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URL + 'getComboList')
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    console.log('error while get combo', reason);
                }
            );
        return deferred.promise;
    }

    function getNomNameById(id) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URL + 'getNomNameById/' + id)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    deferred.reject(reason);
                    console.log('error while get nom name', reason);
                }
            );
        return deferred.promise;
    }

    function deleteNom(id) {
        var def = $q.defer();
        $http.put(REST_SERVICE_URL + 'deleteNom/' + id)
            .then(
                function (value) {
                    def.resolve(value.data);
                },
                function (err) {
                    def.reject(err);
                    console.log('error while deleteNom', err);
                }
            );
        return def.promise;
    }

    function createNom(nom) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URL + 'createNom', nom)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    deferred.reject(reason);
                    console.log('error while add nom', reason);
                }
            );
        return deferred.promise;
    }

    function createChildNom(dep){
        var deferred=$q.defer();
        $http.post(REST_SERVICE_URL+'createChildNom',dep)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    deferred.reject(reason);
                    console.log('error while create child nom',reason);
                }
            );
        return deferred.promise;
    }

    function updateNom(nom) {
        var deferred = $q.defer();
        $http.put(REST_SERVICE_URL + 'updateNom', nom)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    deferred.reject(reason);
                    console.log('deyishilmede xeta');
                }
            );
        return deferred.promise;
    }

    function deleteChildNom(id) {
        var deferred=$q.defer();
        $http.post(REST_SERVICE_URL+'deleteChildNom/'+id)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },
                function (reason) {
                    deferred.reject(reason);
                    console.log('error while delete child nom',reason)
                }
            );
        return deferred.promise;
    }


}]);