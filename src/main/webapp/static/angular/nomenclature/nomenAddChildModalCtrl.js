angular.module('nomApp').controller('NomAddModalController', ['$scope', '$uibModal', '$uibModalInstance', 'NomService', 'addId', function ($scope, $uibModal, $uibModalInstance, NomService, addId) {

    $scope.nom = {id: null, name: '', str: '', depNomId: null};
    $scope.nomCombo = [$scope.nom];
    // NomService.getNomNameById(addId).then(
    //     function (value) {
    //         $scope.nameById = value;
    //         console.log('nameby id ', $scope.nameById.name+'     '+$scope.nameById.id+'    '+$scope.nameById.str);
    //     },
    //     function (reason) {
    //         console.log('error while get name by id', reason);
    //     }
    // );


    $scope.createChildNom = createChildNom;

    $scope.nomCombo = NomService.getComboList().then(
        function (value) {
            $scope.nomCombo = value;
            console.log('combolist', $scope.nomCombo);
            console.log('$scope.nom.depNomId', $scope.nom.depNomId);
            console.log('$scope.nom.id', $scope.nom.id);
        }, function (reason) {
            console.log('error while getcombo', reason)
        }
    );

    function createChildNom(dep) {
        NomService.createChildNom(dep)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'elave edildi', show: true}
                },
                function (reason) {
                    $scope.alert = {typw: 'error', msg: 'xeta bash verdi', show: true}
                    console.log('error while create child nom ', reason);
                }
            );
    }



    $scope.submit = function () {
        $scope.nom.depNomId = addId;
        createChildNom($scope.nom);
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}]);