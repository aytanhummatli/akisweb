angular.module('UserInfoApp').factory('UserInfoService', ['$http', '$q', function ($http, $q) {
    var REST_SERVICE_URL = 'http://localhost:8060/';

    var factory = {
        getUserInfoList: getUserInfoList,
        searchUserFullName: searchUserFullName,
        searchNomName: searchNomName,
        createUserInfo: createUserInfo,
        getUserNameById: getUserNameById,
        updateUserInfo: updateUserInfo,
        deleteUser: deleteUser,
        getButtonRuleCombo: getButtonRuleCombo,
        getRuleCombo: getRuleCombo,
        // checkUsername:checkUsername,
        createUserRule: createUserRule,
        createButtonUserRule: createButtonUserRule,
        getUserRuleById:getUserRuleById,
        getUserButtonRuleById:getUserButtonRuleById,
        updateUserButtonRule:updateUserButtonRule,
        updateUserRule:updateUserRule
    };
    return factory;


    function getUserInfoList(pageNumber) {
        var def = $q.defer();
        $http.get(REST_SERVICE_URL + 'getUserInfoList/' + 15 + '/' + pageNumber)
            .then(
                function (response) {
                    def.resolve(response.data);
                }, function (error) {
                    def.reject(error);
                    console.log('error while get user info list', error)
                }
            );
        return def.promise;
    }

    // function checkUsername(username) {
    //     var def=$q.defer();
    //     $http.post(REST_SERVICE_URL + 'checkUsername/' + username)
    //         .then(
    //             function (value) {
    //                 def.resolve(value.data);
    //                 console.log('data:  ',value)
    //             },function (reason) {
    //                 def.reject(reason)
    //                 console.log('error :  ',reason)
    //             }
    //         );
    //     return def.promise;
    // }
    function searchUserFullName(fullName) {
        var def = $q.defer();
        $http.get(REST_SERVICE_URL + 'searchUserFullName/' + fullName)
            .then(
                function (value) {
                    def.resolve(value.data);
                }, function (reason) {
                    def.reject(reason);
                }
            );
        return def.promise;
    }

    function searchNomName(nomName) {
        var def = $q.defer();
        $http.get(REST_SERVICE_URL + 'searchNomName/' + nomName)
            .then(
                function (value) {
                    def.resolve(value.data);
                },
                function (reason) {
                    def.reject(reason);
                    console.log('error while search nom name', reason)
                }
            );
        return def.promise;
    }

    function createUserInfo(userInfo) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URL + 'createUserInfo/', userInfo)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                }, function (reason) {
                    deferred.reject(reason);
                    console.log('error while create user ', reason);
                }
            );
        return deferred.promise;
    }

    function getUserNameById(id) {
        var def = $q.defer();
        console.log('id  ', id);
        $http.get(REST_SERVICE_URL + 'getUserNameById/' + id)
            .then(
                function (value) {
                    def.resolve(value.data);
                    console.log('value', value.data);
                }, function (reason) {
                    def.reject(reason);
                    console.log('error while get username by id', reason);
                }
            );
        return def.promise;
    }
    function getUserRuleById(id) {
        var d=$q.defer();
        $http.get(REST_SERVICE_URL+'getUserRuleListById/'+id)
            .then(
                function (value) {
                    d.resolve(value.data);
                    console.log('data : ',value.data);
                },function (reason) {
                    d.reject(reason);
                    console.log('error while get user rule by id  ',reason);
                }
            );
        return d.promise;
    }
    function getUserButtonRuleById(id) {
        var d=$q.defer();
        $http.get(REST_SERVICE_URL+'getUserButtonRuleListById/'+id)
            .then(
                function (value) {
                    d.resolve(value.data);
                    console.log('data : ',value.data);
                },function (reason) {
                    d.reject(reason);
                    console.log('error while get user rule by id  ',reason);
                }
            );
        return d.promise;
    }

    function updateUserInfo(user) {
        var def = $q.defer();
        $http.put(REST_SERVICE_URL + 'updateUser/', user)
            .then(
                function (value) {
                    def.resolve(value.data);
                }, function (reason) {
                    def.reject(reason);
                    console.log('error while update user ', reason);
                }
            );
        return def.promise;
    }

    function updateUserRule(userRule) {
        var deferred=$q.defer();
        $http.post(REST_SERVICE_URL+'updateUserRule/',userRule)
            .then(
                function (value) {
                    deferred.resolve(value.data);
                },function (reason) {
                    deferred.reject(reason)
                    console.log('error while update user button rule')
                }
            );
        return deferred.promise;
    }

function updateUserButtonRule(userButtonRule) {
    var deferred=$q.defer();
    $http.post(REST_SERVICE_URL+'updateUserButtonRule/',userButtonRule)
        .then(
            function (value) {
                deferred.resolve(value.data);
            },function (reason) {
                deferred.reject(reason)
                console.log('error while update user button rule')
            }
        );
    return deferred.promise;
}
    function deleteUser(id) {
        var def = $q.defer();
        $http.put(REST_SERVICE_URL + 'deleteUser/' + id)
            .then(
                function (value) {
                    def.resolve(value.data);
                }, function (reason) {
                    def.reject(reason);
                    console.log('error while delete user', reason);
                }
            );
        return def.promise;
    }

    function getButtonRuleCombo() {
        var def = $q.defer();
        $http.get(REST_SERVICE_URL + 'getButtonRuleCombo')
            .then(
                function (value) {
                    def.resolve(value.data);
                }, function (reason) {
                    def.reject(reason);
                    console.log('error while get button rule combo', reason)
                }
            );
        return def.promise;
    }

    function getRuleCombo() {
        var defer = $q.defer();
        $http.get(REST_SERVICE_URL + 'getRuleCombo')
            .then(
                function (value) {
                    defer.resolve(value.data);
                }, function (reason) {
                    defer.reject(reason);
                    console.log('error while get rule combo', reason);
                }
            );
        return defer.promise;
    }

    // function getUserRuleListById(userId) {
    //     var def = $q.defer();
    //     $http.get(REST_SERVICE_URL + 'getUserRuleListById/' + userId)
    //         .then(
    //             function (value) {
    //                 def.resolve(value.data);
    //             }, function (reason) {
    //                 def.reject(reason);
    //                 console.log('error while get user rule list', reason);
    //             }
    //         );
    //     return def.promise;
    // }

    // function getUserButtonRuleListById(userId) {
    //     var def = $q.defer();
    //     $http.get(REST_SERVICE_URL + 'getUserButtonRuleListById/' + userId)
    //         .then(
    //             function (value) {
    //                 def.resolve(value.data);
    //                 console.log('=1=1=1=1=1=1=1=1=')
    //             }, function (reason) {
    //                 def.reject(reason);
    //                 console.log('error while get user button rule list', reason);
    //             }
    //         );
    //     return def.promise;
    // }

    // function getNomNameById(userId) {
    //     var deferred = $q.defer();
    //     $http.get(REST_SERVICE_URL + 'getNomNameByUserId/' + userId)
    //         .then(
    //             function (value) {
    //                 deferred.resolve(value.data);
    //                 console.log('value nom name  ',value.data);
    //             },
    //             function (reason) {
    //                 deferred.reject(reason);
    //                 console.log('error while get nom name', reason);
    //             }
    //         );
    //     return deferred.promise;
    // }

    function createUserRule(userRule) {
        var def = $q.defer();
        $http.post(REST_SERVICE_URL + 'createUserRule/', userRule)
            .then(
                function (value) {
                    def.resolve(value.data);
                }, function (reason) {
                    def.reject(reason);
                    console.log('erro whlie create user rule ');
                }
            );
        return def.promise;
    }

    function createButtonUserRule(userButtonRule) {
        var def = $q.defer();
        $http.post(REST_SERVICE_URL + 'createButtonUserRule/', userButtonRule)
            .then(
                function (value) {
                    def.resolve(value.data);
                }, function (reason) {
                    def.reject(reason);
                    console.log('error while create button user rule ', reason);
                }
            );
        return def.promise;
    }
}]);