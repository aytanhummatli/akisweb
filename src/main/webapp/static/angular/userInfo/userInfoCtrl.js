angular.module("UserInfoApp").controller('UserInfoController', ['$scope', '$uibModal', 'UserInfoService', function ($scope, $uibModal, UserInfoService) {

    $scope.pageNumber = 1;
    $scope.total_count = 0;
    $scope.itemsPerPage = 15;
    $scope.userInfo = {user: {}, employee: {}, person: {}, card: {}, nomenclature: {}, imgHistory: {}, password: {}};
    $scope.rule = {id: null,createByUserId: null, ruleName: '', createDate: '', endDate: '',active: '' };
    $scope.ruleList = [];
    $scope.user = {id: null,userName: '',employeeId: null, createDate: '',endDate: '', active: '',ident: null,nomenklaturaId: null, doupdate: ''};

    $scope.buttonRule = {
        id: null,
        createByUserId: null,
        ruleNames: '',
        createDate: '',
        endDate: '',
        active: ''
    };
    $scope.buttonRuleList = [];

    $scope.employee = {
        id: null,
        person: {},
        createDate: '',
        endDate: '',
        personalMatterNo: '',
        policeCardNo: '',
        nationalityId: null,
        civil: '',
        ok: '',
        cardMustBeChanged: '',
        cardReasonId: null,
        rank: {},
        image: {},
        positionName: {},
        fullStrName: '',
        tree: {}
    };
    $scope.person = {
        id: null, name: '', surname: '', patronymic: '', birthDate: '', marriedStatus: null, bornAddressId: null,
        bornAddressNote: '', relationId: null, maidenName: '', maleFemaleId: null, fullName: ''
    };
    $scope.card = {id: null, cardData: ''};
    $scope.nomenclature = {id:null,name: '',createDate:'',endDate:'',active:null,str:null,strTreeId:null,depNomId:null};
    $scope.imgHistory = {imgUrl: '', ftpFolder: ''};
    $scope.password = {id: null, userId: null, password: '', createDate: '', endDate: '', active: '', newPassword: ''};
    $scope.userList = [$scope.userInfo];

    $scope.getUserInfoList = getUserInfoList;
    $scope.add = add;
    $scope.edit = edit;
    $scope.remove = remove;
    $scope.getList = getList;


    getUserInfoList($scope.pageNumber);

    function getList() {
        $scope.selected();
    }

    function getUserInfoList(pageNumber) {
        UserInfoService.getUserInfoList(pageNumber)
            .then(
                function (data) {
                    $scope.userList = data;
                    $scope.total_count = 500;
                }, function (error) {
                    console.log('error while get user info list', error)
                }
            );
    }


    function add() {
        var modalInstance = $uibModal.open({
            templateUrl: 'userInfoAddModal.html',
            controller: 'UserInfoAddModalController',
            backdrop: 'static',
            resolve: {
                editId: function () {
                    return null;
                }
            }
        });
        modalInstance.closed.then(function (value) {
                getUserInfoList($scope.pageNumber);
            }
        );
        modalInstance.result.catch(function (reason) {
            if (!(reason === 'cancel' || reason === 'escape key'))
                getUserInfoList($scope.pageNumber);
            throw reason;
        });
    }

    function edit(id) {
        var modalInstance = $uibModal.open({
            templateUrl: 'userInfoEditModal.html',
            controller: 'UserInfoEditModalController',
            backdrop: 'static',
            resolve: {
                editId: function () {
                    return id;
                    console.log('id   ', id);
                }
            }
        });
        modalInstance.closed.then(function (value) {
            getUserInfoList($scope.pageNumber);
        });
        modalInstance.result.catch(function (reason) {
            if (!(reason === 'cancel' || reason === 'escape press key'))
                getUserInfoList($scope.pageNumber);
            throw  reason;
        });
    }

    function remove(id) {
        var modalInstance = $uibModal.open({
            templateUrl: 'userRemoveModal.html',
            controller: 'UserInfoEditModalController',
            backdrop: 'static',
            resolve: {
                editId: function () {
                    return id;
                }
            }
        })
    }



}]);