angular.module('UserInfoApp').controller('UserInfoEditModalController', ['$scope', '$uibModalInstance', 'editId', 'UserInfoService', function ($scope, $uibModalInstance, editId, UserInfoService) {
    $scope.userInfo = {user: {}, employee: {}, person: {}, card: {}, nomenclature: {}, imgHistory: {}, password: {}};
    $scope.user = {
        id: null,
        userName: '',
        employeeId: null,
        createDate: '',
        endDate: '',
        active: '',
        ident: null,
        nomenklaturaId: null,
        doupdate: ''
    };
    $scope.nomenclature = {
        id: null,
        name: '',
        createDate: '',
        endDate: '',
        active: '',
        str: '',
        strTreeId: null,
        depNomId: null,
        nomDependency: {}
    };

    console.log('editId  ', editId);
    if (!(editId === null || editId === '')) {
        UserInfoService.getUserNameById(editId)
            .then(
                function (value) {
                    $scope.userInfo = value;
                    console.log('username by id', value);
                    console.log('user name ', $scope.userInfo.user.userName);
                    console.log('user id ', $scope.userInfo.user.id);
                    console.log('nom name  ', $scope.userInfo.nomenclature.name);
                }, function (reason) {
                    console.log('error while get username by id ', reason)
                }
            );
        UserInfoService.getUserRuleById(editId)
            .then(
                function (value) {
                    $scope.userRule.userRules=value;
                    console.log('rule list : ',$scope.userButtonRule.userButtonRules);
                },function (reason) {
                    console.log("error while get user rule by id",reason)
                }
            );
        UserInfoService.getUserButtonRuleById(editId)
            .then(
                function (value) {

                    $scope.userButtonRule.userButtonRules=value;
                    console.log('button rule list:',$scope.userRule.userRules);
                },function (reason) {
                    console.log('error while get user button rule by id ',reason)
                }
            );

    } else {
        $scope.userInfo.user = {
            id: null,
            userName: '',
            employeeId: null,
            createDate: '',
            endDate: '',
            active: '',
            ident: null,
            nomenklaturaId: null,
            doupdate: ''
        };
    }
    $scope.nomNameList = [$scope.nomenclature];

    $scope.searchNomName = function () {


        if (!$scope.userInfo.nomenclature.name) {
            $scope.valNomName = 0;
        } else {
            $scope.valNomName = $scope.userInfo.nomenclature.name;
            console.log(' $scope.nomname', $scope.userInfo.nomenclature.name);
        }

        UserInfoService.searchNomName($scope.valNomName)
            .then(
                function (value) {
                    $scope.nomNameList = value;
                    console.log('nom list', value);
                }, function (reason) {
                    console.log('error while search nom name list', reason);
                }
            )
    }

    function updateUserInfo(userInfo) {
        UserInfoService.updateUserInfo(userInfo)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'uğurla dəyişdirildi', show: true};
                }, function (reason) {
                    $scope.alert = {type: 'error', msg: 'xəta baş verdi', show: true};
                    console.log('error while update user ', reason);
                }
            );
    }

    function updateUserRule(userRule){
        UserInfoService.updateUserRule(userRule)
            .then(
                function () {
                    $scope.alert={type:'success',msg:'uğurla dəyişdirildi',show:true};
                },function (reason) {
                    $scope.alert = {type: 'error', msg: 'xəta baş verdi', show: true};
                    console.log('error while update user rule',reason)
                }
            );
    }
    function updateUserButtonRule(userButtonRule){
        UserInfoService.updateUserButtonRule(userButtonRule)
            .then(
                function () {
                    $scope.alert={type:'success',msg:'uğurla dəyişdirildi',show:true};
                },function (reason) {
                    $scope.alert = {type: 'error', msg: 'xəta baş verdi', show: true};
                    console.log('error while update user rule',reason)
                }
            );
    }

    function deleteUser(id) {
        UserInfoService.deleteUser(id)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'uğurla silindi', show: true}
                }, function (reason) {
                    $scope.alert = {type: 'error', msg: 'xəta baş verdi', show: true};
                    console.log('error while delete user ', reason);
                }
            );
    }

    $scope.buttonRules = UserInfoService.getButtonRuleCombo()
        .then(
            function (value) {
                $scope.buttonRules = value;
                console.log('button rule combo ', $scope.buttonRules);

            }, function (reason) {
                console.log('error while get button rule combo', reason);
            }
        );
    $scope.rules = UserInfoService.getRuleCombo()
        .then(
            function (value) {
                $scope.rules = value;
                console.log('rule combo', $scope.rules);
            }, function (reason) {
                console.log('error while get rule combo', reason)
            }
        );
    $scope.buttonRule = {id: null, createByUserId: null, ruleNames: '', createDate: '', endDate: '', active: '',userButtonRule:{}};
    $scope.buttonRules = [$scope.buttonRule];
    $scope.rule = {id: null, createByUserId: null, ruleName: '', createDate: '', endDate: '', active: '',userRule:{}}
    $scope.rules = [$scope.rule];

    $scope.userButtonRule={};
    $scope.userButtonRule.userButtonRules = [];
    $scope.userRule={};
    $scope.userRule.userRules = [];


    app.filter('propsFilter', function () {
        return function (items, props) {
            var out = [];

            if (angular.isArray(items)) {
                var keys = Object.keys(props);

                items.forEach(function (item) {
                    var itemMatches = false;

                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    });


    $scope.disabled = undefined;
    $scope.searchEnabled = undefined;

    $scope.setInputFocus = function () {
        $scope.$broadcast('UiSelectDemo1');
    };

    $scope.enable = function () {
        $scope.disabled = false;
    };

    $scope.disable = function () {
        $scope.disabled = true;
    };

    $scope.enableSearch = function () {
        $scope.searchEnabled = true;
    };

    $scope.disableSearch = function () {
        $scope.searchEnabled = false;
    };

    $scope.clear = function() {
        $scope.person.selected = undefined;
        $scope.address.selected = undefined;
        $scope.country.selected = undefined;
    };

    $scope.someGroupFn = function (item) {

        if (item.name[0] >= 'A' && item.name[0] <= 'M')
            return 'From A - M';

        if (item.name[0] >= 'N' && item.name[0] <= 'Z')
            return 'From N - Z';

    };

    $scope.firstLetterGroupFn = function (item) {
        return item.name[0];
    };

    $scope.reverseOrderFilterFn = function (groups) {
        return groups.reverse();
    };


    $scope.counter = 0;
    $scope.onSelectCallback = function (item, model) {
        $scope.counter++;
        $scope.eventResult = {item: item, model: model};
    };

    $scope.removed = function (item, model) {
        $scope.lastRemoved = {
            item: item,
            model: model
        };
    };

    $scope.tagTransform = function (newTag) {
        var item = {
            name: newTag,
            email: newTag.toLowerCase() + '@email.com',
            age: 'unknown',
            country: 'unknown'
        };

        return item;
    };



    $scope.appendToBodyDemo = {
        remainingToggleTime: 0,
        present: true,
        startToggleTimer: function () {
            var scope = $scope.appendToBodyDemo;
            var promise = $interval(function () {
                if (scope.remainingTime < 1000) {
                    $interval.cancel(promise);
                    scope.present = !scope.present;
                    scope.remainingTime = 0;
                } else {
                    scope.remainingTime -= 1000;
                }
            }, 1000);
            scope.remainingTime = 3000;
        }
    };

    $scope.address = {};
    $scope.refreshAddresses = function(address) {
        var params = {address: address, sensor: false};
        return $http.get(
            'http://maps.googleapis.com/maps/api/geocode/json',
            {params: params}
        ).then(function(response) {
            $scope.addresses = response.data.results;
        });
    };


    $scope.submit = function () {
        updateUserInfo($scope.userInfo);
        updateUserRule($scope.userRule.userRules);
        console.log('updateUserRule($scope.userRules);    ',$scope.userRule.userRules)
        updateUserButtonRule($scope.userButtonRule.userButtonRules);
        console.log('updateUserRule($scope.userButtonRules);    ',$scope.userButtonRule.userButtonRules)
        $uibModalInstance.close();
    }

    $scope.ok=function(){
        deleteUser($scope.userInfo.user.id);
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}]);