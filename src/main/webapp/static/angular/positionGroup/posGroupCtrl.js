angular.module('hpgApp').controller('HpgController', ['$window', '$scope', '$sce', '$uibModal', 'HpgService', function ($window, $scope, $sce, $uibModal, HpgService) {
    var self = this;


    self.pageNumber = 1;
    self.total_count = 0;
    self.itemsPerPage = 15;


    var REST_SERVICE_URL = 'http://localhost:8088/';
    self.hpg = {
        id: null,
        name: '',
        createDate: '',
        endDate: '',
        active: '',
        groupCode: '',
        groupParent: null,
        total: null
    };
    self.hpgList = [];
    self.deleteHpg = deleteHpg;
    self.edit = edit;
    self.remove = remove;
    self.add = add;
    self.getHpgList = getHpgList;

    getHpgList(self.pageNumber);

    function getHpgList(pageNumber) {
        HpgService.getHpgList(pageNumber)
            .then(
                function (data) {
                    self.hpgList = data;
                    self.total_count = self.hpg.total;
                    console.log('totalCount', self.hpg.total);
                    console.log('total_count', self.total_count);
                },
                function (errResponse) {
                    console.error('Error while fetching hpg' + errResponse);
                }
            );
    }

    function deleteHpg(id) {
        HpgService.deleteHpg(id)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'Məqalə silindi', show: true};
                },
                function (errResponse) {
                    $scope.alert = {type: 'danger', msg: 'Xəta baş verdi', show: true},
                        console.error('Error while delete hpg' + errResponse);
                }
            );
    }


    function edit(id) {
        var modalInstance = $uibModal.open({

            templateUrl: 'positionGorupAddUpdateModal.html',
            controller: 'HpgModalController',
            resolve: {
                editId: function () {
                    return id;
                }
            },
            backdrop: 'static'
        });
        modalInstance.closed.then(function () {
                getHpgList(self.pageNumber);
            }
        );
        modalInstance.result.catch(function (res) {
            if (!(res === 'cancel' || res === 'escape key press')) {
                getHpgList(self.pageNumber);
                throw res;
            }
        });
    }

    function add() {
        var modalInstance = $uibModal.open({
            templateUrl: 'positionGorupAddUpdateModal.html',
            controller: 'HpgModalController',
            resolve: {
                editId: function () {
                    return null;
                }
            },
            backdrop: 'static'
        });
        modalInstance.closed.then(function () {
                getHpgList(self.pageNumber);
            }
        );
        modalInstance.result.catch(function (res) {
            if (!(res === 'cancel' || res === 'escape key press')) {
                getHpgList(self.pageNumber);
                throw res;
            }
        });
    }

    function remove(id) {
        var modalInstance = $uibModal.open({
            templateUrl: 'positionGroupRemoveModal.html',
            controller: 'HpgModalController',
            resolve: {
                editId: function () {
                    return id;
                }
            },
            backdrop: 'static'
        });
        modalInstance.closed.then(function () {
                getHpgList(self.pageNumber);
            }
        );
        modalInstance.result.catch(function (res) {
            if (!(res === 'cancel' || res === 'escape key press')) {
                getHpgList(self.pageNumber);
                throw res;
            }
        });
    }
}]);



