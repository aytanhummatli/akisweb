'use strict';

angular.module('myApp').controller('RankController', ['$window', '$scope','$http','$sce', '$uibModal', 'RankService','$uibModal', function($window,$scope, $http, $sce, $uibModal, RankService) {


    var self = this;

    self.rank={id:null,name:'',rankType:{},nextRankPeriod:''};

    self.ranks=[self.rank];

    var REST_SERVICE_URI = 'http://localhost:8060/Rank/';



    // vm.users = []; //declare an empty array

    self.pageno = 1; // initialize page no to 1

    self.total_count = 0;

    self.itemsPerPage = 5; //this could be a dynamic value from a drop down

    self.getData=getData;


    self.edit = edit;
   // self.remove = remove;
    self.reset = reset;
    self.view = view;
    self.add=add;
    self.askDelete=askDelete;


    getData(self.pageno);

    function getData(pageno){

        console.log("pageeeee    "+pageno);

        RankService.getData(pageno)
            .then(
                function(d) {
                    self.ranks = d;
                    self.total_count = 20;
                },
                function(errResponse){
                    console.error('Error while fetching Ranks');
                }
            );
    }

    function fetchAllRanks(){
     // console.log("RankService"+RankService.createRank.toString());
        RankService.fetchAllRanks()
            .then(
                function(d) {
                    self.ranks = d;
                },
                function(errResponse){
                    console.error('Error while fetching Ranks');
                }
            );
    }


    function createRank(rank){
        RankService.createRank(rank)
            .then(
                fetchAllRanks,
                function(errResponse){
                    console.error('Error while creating Rank');
                }
            );
    }

    function updateRank(rank, id){
        RankService.updateRank(rank, id)
            .then(
                fetchAllRanks,
                function(errResponse){
                    console.error('Error while updating Rank');
                }
            );
    }

    function deleteRank(id) {
        RankService.deleteRank(id)
            .then(
                function () {
                    $scope.alert = {type: 'success', msg: 'Seçilmiş rütbə məlumatları silindi', show: true};
                },
                function (errResponse) {
                    $scope.alert = {type: 'danger', msg: 'Xəta baş verdi', show: true},
                        console.error('Error while update Rank' + errResponse);
                }
            );
    }

    function view(id) {
        RankService.setEditId(id)
            .then(
                function () {
                    $window.location.href = REST_SERVICE_URI + 'viewRank';
                },
                function (errResponse) {
                    console.error('Error set edit id');
                }
            );
    }

    function edit(id) {

        console.log('id= '+id);
        var modalInstance = $uibModal.open({

            templateUrl: 'myModalContent.html',
            controller: 'RanksModalController',
            resolve: {
                editId: function () {
                    return id;
                }
            },
            backdrop: 'static'
        });
        modalInstance.closed.then(function () {
                fetchAllRanks();
            }
        );
        modalInstance.result.catch(function (res) {
            if (!(res === 'cancel' || res === 'escape key press')) {
               fetchAllRanks();
                throw res;
            }
        });
    }

    function add(id) {
            var modalInstance = $uibModal.open({
                templateUrl: 'myModalContent.html',
                controller: 'RanksModalController',
                resolve: {
                    editId: function () {
                        return id;
                    }
                },
                backdrop: 'static'
            });
            modalInstance.closed.then(function () {
                    fetchAllRanks();
                }
            );

            modalInstance.result.catch(function (res) {
                if (!(res === 'cancel' || res === 'escape key press')) {

                    fetchAllRanks()

                    throw res;
                }
            });

        }

    // function remove(id) {
    //     if(confirm("Are you sure to delete ")) {
    //         deleteRank(id);
    //         fetchAllRanks();
    //     }
    // }

    function reset(){

        self.rank={id:null,name:'',rankType:'',nextRankPeriod:''};

        $scope.myForm.$setPristine(); //reset Form
    }


   function  askDelete(id) {

        var message = "Seçilmiş məlumatları silmək istədiyirsniz?";

        var modalHtml = '<div class="modal-body">' + message + '</div>';
        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

        var modalInstance = $uibModal.open({
            template: modalHtml,
            controller: ModalInstanceCtrl
        });

    modalInstance.result.then(function() {
        deleteRank(id);
        fetchAllRanks();
    });
    }

    var ModalInstanceCtrl = function($scope, $uibModalInstance) {
    $scope.ok = function() {
        fetchAllRanks();
        $uibModalInstance.close();
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
};

}]);

