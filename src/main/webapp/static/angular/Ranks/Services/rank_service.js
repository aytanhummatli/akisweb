'use strict';

angular.module('myApp').factory('RankService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:8060/rank/';


    var factory = {
        fetchAllRanks: fetchAllRanks,
        createRank: createRank,
        updateRank:updateRank,
        deleteRank:deleteRank,
        getRankTypes: getRankTypes,
        getRankById:getRankById,
        setEditId: setEditId,
        getEditId: getEditId,
        getData:getData
                    };
    return factory;


    function fetchAllRanks() {
            console.log("jcndxjkcdjncjdncjdncjdncjdncjcjnc");
            var deferred = $q.defer();
            console.log("deferred  "+deferred);
            $http.get(REST_SERVICE_URI)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                        console.log("response.data  from fetch  "+response.data);

                    },
                    function(errResponse){
                        console.log("REST_SERVICE_URI   "+REST_SERVICE_URI);
                        console.error('Error while fetching Ranks');
                        deferred.reject(errResponse);
                    }
            );
        return deferred.promise;
    }

    function  getData(pageno) {

    var deferred = $q.defer();

    $http.get(REST_SERVICE_URI+'pagination/'+5+'/'+pageno)
        .then(
            function (response) {
                deferred.resolve(response.data);
                console.log("response.data  from getdata  "+response.data);

            },
            function(errResponse){
                console.log("REST_SERVICE_URI   "+REST_SERVICE_URI);
                console.error('Error while fetching Ranks');
                deferred.reject(errResponse);
            }
        );
    return deferred.promise;
}

    function createRank(rank) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI, rank)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                    console.log("response.data  from fetch  "+response.data);
                },
                function(errResponse){
                    console.error('Error while creating Rank');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function updateRank(rank) {
        var deferred = $q.defer();
        $http.put(REST_SERVICE_URI + 'update', rank)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while create Rank' + errResponse);
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getRankById(id) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI + 'getRankById/' + id)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while get Rank by id Rank' + errResponse);
                    deferred.reject(errResponse.data);
                }
            );
        return deferred.promise;
    }

    function  getRankTypes() {
        console.log("i am in getrankTypes from rankservice.js  ")
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI + 'getRankTypes/')
            .then(

                function (response) {
                    console.log("url=   "+REST_SERVICE_URI + 'getRankTypes/')
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while fetching Rank types' + errResponse);
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }
    function setEditId(id) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI + 'setRankId/' + id)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while set id');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getEditId() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI + 'getRankId')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while get id Rank');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }


    function deleteRank(id) {
        var deferred = $q.defer();
        $http.delete(REST_SERVICE_URI  + id)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while delete Rank' + errResponse);
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

}]);