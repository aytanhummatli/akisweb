<!-- jQuery -->
<script src="${pageContext.request.contextPath}/static/resource/js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/static/resource/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/static/resource/js/fastclick.js"></script>
<!-- NProgress -->
<script src="${pageContext.request.contextPath}/static/resource/js/nprogress.js"></script>
<!-- Chart.js -->
<script src="${pageContext.request.contextPath}/static/resource/js/Chart.min.js"></script>
<!-- gauge.js -->
<script src="${pageContext.request.contextPath}/static/resource/js/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="${pageContext.request.contextPath}/static/resource/js/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="${pageContext.request.contextPath}/static/resource/js/icheck.min.js"></script>
<!-- Skycons -->
<script src="${pageContext.request.contextPath}/static/resource/js/skycons.js"></script>
<!-- Flot -->
<script src="${pageContext.request.contextPath}/static/resource/js/jquery.flot.js"></script>
<script src="${pageContext.request.contextPath}/static/resource/js/jquery.flot.pie.js"></script>
<script src="${pageContext.request.contextPath}/static/resource/js/jquery.flot.time.js"></script>
<script src="${pageContext.request.contextPath}/static/resource/js/jquery.flot.stack.js"></script>
<script src="${pageContext.request.contextPath}/static/resource/js/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="${pageContext.request.contextPath}/static/resource/js/jquery.flot.orderBars.js"></script>
<script src="${pageContext.request.contextPath}/static/resource/js/jquery.flot.spline.min.js"></script>
<script src="${pageContext.request.contextPath}/static/resource/js/curvedLines.js"></script>
<!-- DateJS -->
<script src="${pageContext.request.contextPath}/static/resource/js/date.js"></script>
<!-- JQVMap -->
<script src="${pageContext.request.contextPath}/static/resource/js/jquery.vmap.js"></script>
<script src="${pageContext.request.contextPath}/static/resource/js/jquery.vmap.world.js"></script>
<script src="${pageContext.request.contextPath}/static/resource/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="${pageContext.request.contextPath}/static/resource/js/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/static/resource/js/daterangepicker.js"></script>
<!-- Custom Theme Scripts -->
<script src="${pageContext.request.contextPath}/static/resource/js/custom.min.js"></script>

